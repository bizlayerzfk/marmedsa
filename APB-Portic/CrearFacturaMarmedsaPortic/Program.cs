﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data.OleDb;
using AgiKeyMailLib;

namespace CrearFacturaMarmedsaPortic
{
    class Program
    {
        #region Varibles
        public static String sFilein="";
        public static String sFileout = "";
        public static String sFileExtension = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Extensions.xml";
        public static ArrayList oCiudad = null;
        public static ArrayList oDepartamento = null;
        public static String sDirectorioFacturas = "";
        public static String sDirectorioFacturasenviar = "";
        public static String sDirectorioMonitorizacion = "";
        public static ArrayList aListaEmpresas = null;
        public static String sFileListaEmpresasMarmedsa = "";
        public static ArrayList aListaSuplidos = null;
        public static String sFileListaSuplidos = "";
        public static String[] sCIFExcluidos;

		//Variables para el informe
		private static ArrayList ResultadoProceso = null;
		private static ArrayList errorInforme = null;
		private static ArrayList intInforme = null;
		#endregion

		static void Main(string[] args)
        {
            sFileListaEmpresasMarmedsa = ConfigurationManager.AppSettings["sListaMarmedsa"];
            sFileListaSuplidos = ConfigurationManager.AppSettings["sListaSuplidos"];
            Init();
            sDirectorioFacturas = ConfigurationManager.AppSettings["sDirectorioFacturas"];
            sDirectorioFacturasenviar = ConfigurationManager.AppSettings["sDirectorioFacturasenviar"];
            sDirectorioMonitorizacion = procesarArchivos(sDirectorioFacturas);
            String scif = ConfigurationManager.AppSettings["sCifs"];
            sCIFExcluidos = scif.Split(';');
            if (sDirectorioMonitorizacion != "")
            {
                RenombrarFicheroAXml(sDirectorioMonitorizacion);
                string[] fileNames = Directory.GetFiles(sDirectorioMonitorizacion, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    int j = i + 1;
                    Console.WriteLine("Procesando " + j + " de " + fileNames.Length);
                    if (EsFacturaE(fileNames[i])) TratarFacturaE(fileNames[i]);
                    else
                    {
                        if (fileNames[i].Contains("PDF"))
                            CrearPDF(fileNames[i], sDirectorioMonitorizacion, sDirectorioFacturasenviar);
                    }
                }
                //Movemos todo a la subcarpetas procesadas.
                BorrarFacturasIntermedias(sDirectorioMonitorizacion);
                moverAProcesadas(sDirectorioMonitorizacion);
                RenombrarFicheroAReferenciaFactura(sDirectorioFacturasenviar);
				GenerarMailProceso();
			}

            Console.WriteLine("Fin proceso");

        }

        #region init vars
        private static void Init()
        {
            oCiudad = new ArrayList();
            oDepartamento = new ArrayList();
            oDepartamento.Add("AAE"); oCiudad.Add("ANNABA");
            oDepartamento.Add("ARG"); oCiudad.Add("ALGER");
            oDepartamento.Add("AZW"); oCiudad.Add("ARZEW");
            oDepartamento.Add("BJA"); oCiudad.Add("BEJAIA");
            oDepartamento.Add("DJE"); oCiudad.Add("Djen-Djen");
            oDepartamento.Add("MOS"); oCiudad.Add("MOSTAGANEM");
            oDepartamento.Add("ORN"); oCiudad.Add("ORAN");
            oDepartamento.Add("SKI"); oCiudad.Add("SKIKDA");
            oDepartamento.Add("BCN"); oCiudad.Add("BARCELONA");
            oDepartamento.Add("AGP"); oCiudad.Add("MALAGA");
            oDepartamento.Add("ALC"); oCiudad.Add("ALICANTE");
            oDepartamento.Add("ALD"); oCiudad.Add("ALCUDIA");
            oDepartamento.Add("ALG"); oCiudad.Add("ALGECIRAS");
            oDepartamento.Add("BIO"); oCiudad.Add("BILBAO");
            oDepartamento.Add("CAD"); oCiudad.Add("CADIZ");
            oDepartamento.Add("CAS"); oCiudad.Add("CASTELLON");
            oDepartamento.Add("GIJ"); oCiudad.Add("GIJON");
            oDepartamento.Add("HUV"); oCiudad.Add("HUELVA");
            oDepartamento.Add("LCG"); oCiudad.Add("LA CORUÑA");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS DE GRAN CANARIA");
            oDepartamento.Add("MAD"); oCiudad.Add("MADRID");
            oDepartamento.Add("PMI"); oCiudad.Add("PALMA DE MALLORCA");
            oDepartamento.Add("SCT"); oCiudad.Add("SANTA CRUZ DE TENERIFE");
            oDepartamento.Add("SVQ"); oCiudad.Add("SEVILLA");
            oDepartamento.Add("TAR"); oCiudad.Add("TARRAGONA");
            oDepartamento.Add("VGO"); oCiudad.Add("VIGO");
            oDepartamento.Add("VLC"); oCiudad.Add("VALENCIA");
            oDepartamento.Add("CAR"); oCiudad.Add("CARTAGENA");
            oDepartamento.Add("LEI"); oCiudad.Add("ALMERIA");
            oDepartamento.Add("MOT"); oCiudad.Add("MOTRIL");
            oDepartamento.Add("SDR"); oCiudad.Add("SANTANDER");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS");
            oDepartamento.Add("LEH"); oCiudad.Add("LE HAVRE");
            oDepartamento.Add("MRS"); oCiudad.Add("MARSEILLE");
            oDepartamento.Add("MTU"); oCiudad.Add("MARTIGUES");
            oDepartamento.Add("PAR"); oCiudad.Add("PARIS");
            oDepartamento.Add("CSB"); oCiudad.Add("CASABLANCA");
            oDepartamento.Add("TNG"); oCiudad.Add("TANGIER");
            oDepartamento.Add("LIS"); oCiudad.Add("LISBOA");
            oDepartamento.Add("OPO"); oCiudad.Add("PORTO");
            oDepartamento.Add("SIE"); oCiudad.Add("SINES");
            oDepartamento.Add("SET"); oCiudad.Add("SETUBAL");
            oDepartamento.Add("OPO"); oCiudad.Add("OPORTO");
            //Cargamos la lista de empresas desde el excel.
            CargarListaEmpresas();
            CargarListaSuplidos();
        }

        private static void CargarListaEmpresas()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaEmpresasMarmedsa);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaEmpresas == null) aListaEmpresas = new ArrayList();
                            CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[1].ToString();
                            oEmpresa.CIF = rdrExcelData[2].ToString();
                            oEmpresa.DEL = rdrExcelData[3].ToString();
                            oEmpresa.PROV = rdrExcelData[4].ToString();
                            oEmpresa.PAIS = rdrExcelData[5].ToString();
                            oEmpresa.DIRECCION = rdrExcelData[6].ToString();
                            oEmpresa.CODIGOPOSTAL = rdrExcelData[7].ToString();
                            oEmpresa.MAIL = rdrExcelData[8].ToString();
                            aListaEmpresas.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas empresas.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }

        private static void CargarListaSuplidos()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaSuplidos);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaSuplidos == null) aListaSuplidos = new ArrayList();
                            CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[3].ToString();
                            oEmpresa.CIF = rdrExcelData[1].ToString();
                            aListaSuplidos.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas Suplidos.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }
        #endregion

        #region Procesar Ficheros FacturaE

        #region Tratar Fichero FacturaE
        private  static void TratarFacturaE(String sFile)
        {
            try
            {
                String sFichero = sFile;
                String sFicheroPdf = sFile.Replace("xml", "pdf");
                try
                {
                    
                    sFilein = sFichero;
                    sFileout = sFichero.Replace(".xml", "_sinfirma.xml");
                    DeleteSignature(sFilein, sFileout);

                    //Añadimos centro administrativo.
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_sinfirma.xml", "_conceros.xml");
                    AddAdministrativeCentreBuyer(sFilein, sFileout);

                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_conceros.xml", "_conadmcentre.xml");
                    AddAdministrativeCentreSeller(sFilein, sFileout);


                    //Borramos las lineas con valor Cero
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_conadmcentre.xml", "_sincero.xml");
                    DeleteInvoiceLineZero(sFilein, sFileout);

                    //Ponemos el <SequenceNumber>1</SequenceNumber> antes de ItemDescription
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_sincero.xml", "_conSequenceNumber.xml");
                    AddSequenceNumberInvoiceLine(sFilein, sFileout);

                    //Informamos el TotalCost y Taxable/TotalAmount de las lineas que no vienen informada en Portic
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_conSequenceNumber.xml", "_totalamountlinea.xml");
                    CalcularTotalCost_TaxableBaseTotalAmount_LineaFactura(sFilein, sFileout);

                    //Informamos el Taxable/TotalAmount de la factura
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_totalamountlinea.xml", "_totalamountfactura.xml");
                    CalcularTaxableBaseTotalAmount_Factura(sFilein, sFileout);


                    //Informamos el TaxRate de la factura, puede también venir vacio.
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_totalamountfactura.xml", "_taxratefactura.xml");
                    CalcularTaxRate_Factura(sFilein, sFileout);

                    //Añadimos xml original 
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_taxratefactura.xml", "_conxml.xml");
                    AddAttachmentDocument_xml(sFilein, sFichero, sFileout);

                    

                    //Añadimos extension
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_conxml.xml", "_conespacios.xml");
                    AddExtension(sFilein, sFileout);

                    //Quitamos los espacios en la referencia factura
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_conespacios.xml", "_Mailextension.xml");
                    QuitarEspaciosReferenciaFactura(sFilein, sFileout);

                    //Añadimos el mail de notificacion si viene en la factura o dependiendo empresas marmedsa
                    sFilein = sFileout;
                    sFileout = sFilein.Replace("_Mailextension.xml", ".xml");
                    AddMailExtension(sFilein, sFileout);
					String sTipoError = "OK";
					if (IsCifEmisorExcluido(sFichero))
                    {
                        moverAErrorExcluida(sFichero, sDirectorioMonitorizacion);
                        moverAErrorExcluida(sFicheroPdf, sDirectorioMonitorizacion);
						sTipoError = "moverAErrorExcluida";
					}
                    //Si la receptora no es del grupo marmedsa, tratamos como error suplido
                    if (!bErrorSuplidos(sFichero))
                    {
                        moverAErrorSuplidos(sFichero, sDirectorioMonitorizacion);
                        moverAErrorSuplidos(sFicheroPdf, sDirectorioMonitorizacion);
						sTipoError = "moverAErrorSuplidos";
					}
                    //Copiamos los ficheros a la carpeta del toolkit
                    CopiarArchivo(sFichero, sDirectorioFacturasenviar);
                    CopiarArchivo(sFicheroPdf, sDirectorioFacturasenviar);
					if (ResultadoProceso == null) ResultadoProceso = new ArrayList();
					ResultadoProceso.Add(sTipoError);
				}
                catch (Exception)
                {
                }


            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Es Facturae
        private static Boolean EsFacturaE(String sFilein)
        {
            String sfileor = sFilein;
            Boolean bFacturae = false;
            XmlDocument documento = new XmlDocument();
            
            try
            {
                //documento.Load(sFilein);
                documento = leerXML(sFilein);
                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }
                if (nodo != null)
                {
                    bFacturae = true;
                }
                documento = null;
                return bFacturae;
            }
            catch (Exception e)
            {

                return bFacturae;

            }
            finally
            {
                documento = null;
            }


        }
        #endregion

        #region Eliminar Firma
        private static void DeleteSignature(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                //documento.Load(sFilein);
                documento = leerXML(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }



                if (nodo != null)
                {
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    XmlNode node1 = documento.SelectSingleNode("//Signature");
                    if (node1 == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                        xmlNsM.AddNamespace("ds", @"http://www.w3.org/2000/09/xmldsig#");
                        node1 = documento.SelectSingleNode("//ds:Signature", xmlNsM);
                    }
                    if (node1 != null)
                        nodo.RemoveChild(node1);
                    //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
                    documento.Save(sFileOut);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
        #endregion

        #region Quitar Lineas a 0
        private static void DeleteInvoiceLineZero(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    XmlDocument documento = new XmlDocument();
                    //documento.Load(sFilein);
                    documento = leerXML(sFilein);

                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "ItemDescription")
                                    {
                                        if(oinvoiceline.InnerText.ToUpper()=="TOTAL")
                                        {

                                        }
                                    }
                                    if (oinvoiceline.Name == "UnitPriceWithoutTax")
                                    {
                                        if (isCero(oinvoiceline.InnerText))
                                        {
                                            nItems.RemoveChild(oNodo);
                                            documento.Save(sFilein);
                                            documento = null;
                                            bsalir = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }

        private static Boolean isCero(String value)
        {
            try
            {
                if (value.Equals("0.000000")) return true;
                else return false;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        #endregion

        #region Añadir AdministrativeCentres
        private static void AddAdministrativeCentreBuyer(String sFilein, String sFileOut)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                //documento.Load(sFilein);
                documento = leerXML(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                XmlNode nAddressInSpain = null;
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach(XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        //Cargamos al Adresspain para el nuevo nodo.

                    }
                }
                //Si no existe lo añadimos.
                if (baddAdministrativeCentres)
                {
                    nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                    Boolean binsertCentro = false;
                    if (nBuyerParty != null)
                    {
                        foreach (XmlNode oNodo in nBuyerParty)
                        {
                            //Para el sabadell si hay partyidentification
                            if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                            {
                                XmlNode nodo1 = oNodo;
                                nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                documento.Save(sFileout);
                                documento = null;
                                binsertCentro = true;
                                break;
                            }
                        }
                        //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                        if (!binsertCentro)
                        {
                            nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                        documento.Save(sFileout);
                                        documento = null;
                                        binsertCentro = true;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        documento.Save(sFileout);
                        documento = null;
                    }
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        private static void AddAdministrativeCentreSeller(String sFilein, String sFileOut)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                //documento.Load(sFilein);
                documento = leerXML(sFilein);

                XmlNode nBuyerParty = documento.SelectSingleNode("//SellerParty");
                XmlNode nAddressInSpain = null;
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        //Cargamos al Adresspain para el nuevo nodo.

                    }
                }
                //Si no existe lo añadimos.
                if (baddAdministrativeCentres)
                {
                    nBuyerParty = documento.SelectSingleNode("//SellerParty");
                    Boolean binsertCentro = false;
                    if (nBuyerParty != null)
                    {
                        foreach (XmlNode oNodo in nBuyerParty)
                        {
                            if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                            {
                                XmlNode nodo1 = oNodo;
                                nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                documento.Save(sFileout);
                                documento = null;
                                binsertCentro = true;
                                break;
                            }
                            //Cargamos al Adresspain para el nuevo nodo.

                        }
                        //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                        if (!binsertCentro)
                        {
                            nBuyerParty = documento.SelectSingleNode("//SellerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                        documento.Save(sFileout);
                                        documento = null;
                                        binsertCentro = true;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        //<AdministrativeCentres>
        //  <AdministrativeCentre>
        //    <CentreCode>BCN</CentreCode>
        //    <AddressInSpain>
        //      <Address>ZAL 2 - C/ NYEPA, 2-5</Address>
        //      <PostCode> 08820</PostCode>
        //      <Town> EL PRAT DE LLOBREGAT</Town>
        //      <Province></Province>
        //      <CountryCode>ESP</CountryCode>
        //    </AddressInSpain>
        //  </AdministrativeCentre>
        //</AdministrativeCentres>
        private static XmlElement CrearNodo_AdministrativeCentre(XmlDocument xmlDoc,XmlNode oAddressInSpain)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");
                
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");
                foreach (XmlNode oNodo in oAddressInSpain)
                {
                    if (oNodo.Name == "Address")
                    {
                        Address.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "PostCode")
                    {
                        PostCode.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Town")
                    {
                        Town.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Province")
                    {
                        Province.InnerText = oNodo.InnerText;
                        CentreCode.InnerText = getCentreCode(Province.InnerText);
                    }
                    if (oNodo.Name == "CountryCode")
                    {
                        CountryCode.InnerText = oNodo.InnerText;
                    }

                }



                
                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");
                
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve el codigo de centro según provincia receptora
        /// </summary>
        /// <param name="sProvincia"></param>
        /// <returns></returns>
        private static String getCentreCode(String sProvincia)
        {
            try
            {
                String scentrecode = "";
                for(int i=0;i<oCiudad.Count;i++)
                {
                    String sciudad = oCiudad[i].ToString();
                    if (sProvincia.ToUpper().Trim() == sciudad.ToUpper().Trim())
                    {
                        scentrecode = oDepartamento[i].ToString();
                        break;
                    }
                    
                }
                return scentrecode;
            }
            catch(Exception e)
            {
                return "";
            }
        }
        #endregion

        #region Añadir SequenceNumber a las lineas
        private static void AddSequenceNumberInvoiceLine(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    int sSecuencia = 1;
                    XmlDocument documento = new XmlDocument();
                    //documento.Load(sFilein);
                    documento = leerXML(sFilein);

                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "ItemDescription")
                                    {
                                        XmlNode nodo1 = oinvoiceline;
                                        oNodo.InsertBefore(CrearNodo_SequenceNumber(documento, sSecuencia), nodo1);
                                        documento.Save(sFilein);
                                        sSecuencia++;
                                    }
                                }
                            }
                        }
                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }



        private static XmlElement CrearNodo_SequenceNumber(XmlDocument xmlDoc,int iSecuencia)
        {
            try
            {
                XmlElement SequenceNumber = xmlDoc.CreateElement("SequenceNumber");
                SequenceNumber.InnerText = iSecuencia.ToString();

                return SequenceNumber;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir xml attachment
        private static Boolean AddAttachmentDocument_xml(String sFileFacturain, String sFilexml, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//AdditionalData");

                if (nodoAdd == null) //No existe el nodo AdditionalData
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoice");
                    String sxmlB64 = getFileB64(sFilexml);
                    nodo.AppendChild(CrearNodo_AdditionalData(xmlDoc, "NONE", "xml", "BASE64", sxmlB64));
                    xmlDoc.Save(sFileFacturaEout);
                }
                else
                {
                    XmlNode nodo1 = nodoAdd.FirstChild;
                    String sxmlB64 = getFileB64(sFilexml);
                    nodoAdd.InsertBefore(CrearNodo_RelatedDocuments(xmlDoc, "NONE", "xml", "BASE64", sxmlB64), nodo1);
                    xmlDoc.Save(sFileFacturaEout);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Crea el nodo AdditionalData con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_AdditionalData(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);


                XmlElement AdditionalData = xmlDoc.CreateElement("AdditionalData");
                AdditionalData.AppendChild(RelatedDocuments);

                return AdditionalData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Crea el nodo RelatedDocuments con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_RelatedDocuments(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);

                return RelatedDocuments;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir Extensiones
        private static Boolean AddExtension(String sFileFacturain, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Facturae");

                if (nodoAdd == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodoAdd != null)
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoices");
                    XmlNode onodo = xmlDoc.CreateElement("Extensions");
                    onodo.InnerXml = LeerFileExtension(sFileExtension);
                    //nodo.AppendChild(onodo);
                    //nodoAdd.AppendChild(onodo);
                    nodoAdd.InsertAfter(onodo, nodo);
                    xmlDoc.Save(sFileFacturaEout);
                }
                //{"El nodo que desea insertar pertenece a otro contexto de documento."}
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static String LeerFileExtension(String sFileExtension)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileExtension);
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Extensions");
                if (nodoAdd != null)
                {
                    return nodoAdd.InnerXml;
                }
                else return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }

        #endregion

        #region Calcular TotalCost y TaxableBase/TotalAmount por linea
        private static void CalcularTotalCost_TaxableBaseTotalAmount_LineaFactura(String sFilein, String sFileOut)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sFilein);

                XmlNodeList nodeItems = xDoc.GetElementsByTagName("InvoiceLine");
                if (nodeItems != null)
                {

                    foreach (XmlNode onodoInvoiceLine in nodeItems)
                    {
                        XmlNodeList Quantity = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("Quantity");
                        XmlNodeList UnitPriceWithoutTax = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("UnitPriceWithoutTax");
                        Decimal iTotalCost = Convert.ToDecimal(Quantity[0].InnerText.Replace(".",",")) * Convert.ToDecimal(UnitPriceWithoutTax[0].InnerText.Replace(".", ","));
                        iTotalCost = Math.Round(iTotalCost, 2);
                        XmlNodeList TotalCost = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TotalCost");
                        if (TotalCost[0].InnerText=="")
                            TotalCost[0].InnerText = iTotalCost.ToString().Replace(",",".") + "0000";

                        //Buscamos el valor en el nodo TAX
                        XmlNodeList GrossAmount = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("GrossAmount");
                        XmlNodeList TaxesOutputs = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TaxesOutputs");
                        XmlNodeList Tax = ((XmlElement)TaxesOutputs[0]).GetElementsByTagName("Tax");
                        XmlNodeList TaxableBase = ((XmlElement)Tax[0]).GetElementsByTagName("TaxableBase");
                        XmlNodeList TotalAmount = ((XmlElement)TaxableBase[0]).GetElementsByTagName("TotalAmount");
                        //if (TotalAmount[0].InnerText == "")
                        //Ponemos a 2 decimales   
                        String stotalamount = GrossAmount[0].InnerText;
                        int ipunto = stotalamount.IndexOf(".");
                        TotalAmount[0].InnerText = stotalamount.Substring(0,ipunto+3);

                        //19-05-2017 Ticket #1002804 El totalcost no se calcula se pone directamente el grossamount.
                        TotalCost[0].InnerText = GrossAmount[0].InnerText;
                       
                    }
                }
            }
            catch (Exception e)
            {
               
            }
            finally
            {
                xDoc.Save(sFileOut);
                xDoc = null;
            }

        }
        #endregion

        #region Calcular TaxableBase/TotalAmount factura
        private static void CalcularTaxableBaseTotalAmount_Factura(String sFilein, String sFileOut)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sFilein);

                //Primero vamos a los nodos de impuestos por la factura
                //cogemos el tipo de impuesto y buscamos en las lineas sumando el total.
                //Por defecto ponemos 0.00 por si no lo encontramos.
                XmlNodeList nodosTaxesOutputs = xDoc.GetElementsByTagName("TaxesOutputs");
                //XmlNode nodosTaxesOutputs = xDoc.SelectSingleNode("//TaxesOutputs");
                if (nodosTaxesOutputs != null)
                {
                    foreach (XmlNode onodoTaxes in nodosTaxesOutputs)
                    {
                        XmlNode parent = onodoTaxes.ParentNode;
                        if (parent.Name.Equals("Invoice"))
                        {
                            XmlNodeList Tax = ((XmlElement)onodoTaxes).GetElementsByTagName("Tax");
                            XmlNodeList TaxTypeCode = ((XmlElement)Tax[0]).GetElementsByTagName("TaxTypeCode");
                            String sImpuesto = TaxTypeCode[0].InnerText;
                            XmlNodeList TaxableBase = ((XmlElement)Tax[0]).GetElementsByTagName("TaxableBase");
                            XmlNodeList TotalAmount = ((XmlElement)TaxableBase[0]).GetElementsByTagName("TotalAmount");
                            Decimal iTotalAmount = Convert.ToDecimal("0.00");
                            //ahora buscamos en las lineas.
                            XmlNodeList nodeItems = xDoc.GetElementsByTagName("InvoiceLine");
                            if (nodeItems != null)
                            {
                                iTotalAmount = Convert.ToDecimal("0.00");
                                foreach (XmlNode onodoInvoiceLine in nodeItems)
                                {

                                    XmlNodeList TaxesOutputsline = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TaxesOutputs");
                                    XmlNodeList Taxline = ((XmlElement)TaxesOutputsline[0]).GetElementsByTagName("Tax");
                                    XmlNodeList TaxTypeCodeline = ((XmlElement)Taxline[0]).GetElementsByTagName("TaxTypeCode");
                                    XmlNodeList TaxableBaseline = ((XmlElement)Taxline[0]).GetElementsByTagName("TaxableBase");
                                    XmlNodeList TotalAmountline = ((XmlElement)TaxableBaseline[0]).GetElementsByTagName("TotalAmount");
                                    if (sImpuesto.Equals(TaxTypeCodeline[0].InnerText))
                                        iTotalAmount = iTotalAmount + Convert.ToDecimal(TotalAmountline[0].InnerText.Replace(".", ","));
                                }
                            }
                            //if (TotalAmount[0].InnerText == "")
                                TotalAmount[0].InnerText = Math.Round(iTotalAmount, 2).ToString().Replace(",", ".");
                            if (TotalAmount[0].InnerText.Equals("0")) TotalAmount[0].InnerText = "0.00";
                        }
                    }
                }

            }
            catch (Exception e)
            {
                
            }
            finally
            {
                xDoc.Save(sFileOut);
                xDoc = null;
            }

        }
        #endregion

        #region Informar TaxRate a nivel factura
        private static void CalcularTaxRate_Factura(String sFilein, String sFileOut)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sFilein);

                //Primero vamos a los nodos de impuestos por la factura
                //cogemos el tipo de impuesto y buscamos en las lineas sumando el total.
                //Por defecto ponemos 0.00 por si no lo encontramos.
                XmlNodeList nodosTaxesOutputs = xDoc.GetElementsByTagName("TaxesOutputs");
                //XmlNode nodosTaxesOutputs = xDoc.SelectSingleNode("//TaxesOutputs");
                if (nodosTaxesOutputs != null)
                {
                    foreach (XmlNode onodoTaxes in nodosTaxesOutputs)
                    {
                        XmlNode parent = onodoTaxes.ParentNode;
                        if (parent.Name.Equals("Invoice"))
                        {
                            XmlNodeList Tax = ((XmlElement)onodoTaxes).GetElementsByTagName("Tax");
                            XmlNodeList TaxTypeCode = ((XmlElement)Tax[0]).GetElementsByTagName("TaxTypeCode");
                            XmlNodeList TaxRate = ((XmlElement)Tax[0]).GetElementsByTagName("TaxRate");
                            String sImpuesto = TaxTypeCode[0].InnerText;

                            //ahora buscamos en las lineas.
                            XmlNodeList nodeItems = xDoc.GetElementsByTagName("InvoiceLine");
                            if (nodeItems != null)
                            {
                                foreach (XmlNode onodoInvoiceLine in nodeItems)
                                {

                                    XmlNodeList TaxesOutputsline = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TaxesOutputs");
                                    XmlNodeList Taxline = ((XmlElement)TaxesOutputsline[0]).GetElementsByTagName("Tax");
                                    XmlNodeList TaxTypeCodeline = ((XmlElement)Taxline[0]).GetElementsByTagName("TaxTypeCode");
                                    XmlNodeList TaxRateline = ((XmlElement)Taxline[0]).GetElementsByTagName("TaxRate");
                                    if (sImpuesto.Equals(TaxTypeCodeline[0].InnerText))
                                        //if (TaxRate[0].InnerText == "")
                                            TaxRate[0].InnerText = TaxRateline[0].InnerText;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                xDoc.Save(sFileOut);
                xDoc = null;
            }

        }

        #endregion

        #region Quitar espacios Referencia
        /// <summary>
        /// Quitamos espacios intermedios en blanco
        /// <BatchIdentifier>ESA8090318008 / 409 / 90</BatchIdentifier>
        /// <InvoiceNumber>08 / 409 / 90</InvoiceNumber>
        /// </summary>
        /// <param name="sFileFacturain"></param>
        /// <param name="sFileFacturaEout"></param>
        /// <returns></returns>
        private static Boolean QuitarEspaciosReferenciaFactura(String sFileFacturain, String sFileFacturaEout)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                //documento.Load(sFileFacturain);
                documento = leerXML(sFileFacturain);

                XmlNode nBatchIdentifier = documento.SelectSingleNode("//BatchIdentifier");
                if (nBatchIdentifier != null)
                {
                    nBatchIdentifier.InnerText = nBatchIdentifier.InnerText.Replace(" ", "");
                }
                XmlNode nInvoiceNumber = documento.SelectSingleNode("//InvoiceNumber");
                if (nInvoiceNumber != null)
                {
                    nInvoiceNumber.InnerText = nInvoiceNumber.InnerText.Replace(" ", "");
                }
                documento.Save(sFileFacturaEout);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }

        #endregion

        #region CIF receptor Excluidos
        private static Boolean IsCifEmisorExcluido(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bCifExluido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//SellerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < sCIFExcluidos.Length; i++)
                    {
                        if (sTaxIdentificationNumber == sCIFExcluidos[i])
                        { bCifExluido = true; break; }
                    }
                }

                return bCifExluido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
        #endregion

        #region mail notificación
        private static void AddMailExtension(String sFilein, String sFileOut)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sPais = "";
                String sTaxIdentificationNumber = "";
                String sReceiverEmailAddresses = "";
                //Buscamos el TaxIdentificationNumber
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")//
                        {
                            foreach (XmlNode oTaxIdentificationNumber in oNodo)
                            {
                                if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentificationNumber.InnerText.ToString();
                                    break;
                                }
                            }

                        }
                        //Miramos el mail si existe en el administrative centre
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            foreach (XmlNode oAdministrativeCentre in oNodo)
                            {
                                if (oAdministrativeCentre.Name == "AdministrativeCentre")
                                {
                                    foreach (XmlNode oContactDetails in oAdministrativeCentre)
                                    {
                                        if (oContactDetails.Name == "ContactDetails")
                                        {
                                            foreach (XmlNode oElectronicMail in oContactDetails)
                                            {
                                                if (oElectronicMail.Name == "ElectronicMail")
                                                {
                                                    sReceiverEmailAddresses = oElectronicMail.InnerText;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    //Ahora revisamos si lo tenemos (el mail)informado en nuestra tabla de empresa, si no, se quedará el de Mariona.
                    if (sReceiverEmailAddresses == "") sReceiverEmailAddresses = MailEmpresaMarmedsa(sTaxIdentificationNumber);

                    if (sReceiverEmailAddresses != "")
                    {
                        //si existe cambiamos el campo mail de la extension
                        try
                        {
                            XmlNode nodo = documento.SelectSingleNode("//InvoiceExtensions");

                            //Buscamos extension marmedsa
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("marmedsa", @"http://marmedsa.com/Factura_Extension.xsd");
                                nodo = documento.SelectSingleNode("//marmedsa:InvoiceExtensions", xmlNsM);
                            }
                            //si no lo encontramos buscamos ns4
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                                nodo = documento.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                            }

                            foreach (XmlNode onodoExtension in nodo)
                            {
                                foreach (XmlNode oReceiverEmailAddresses in onodoExtension)
                                {
                                    if (oReceiverEmailAddresses.Name == "EmailAdress")
                                    {
                                        oReceiverEmailAddresses.InnerText = sReceiverEmailAddresses;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }

                    }


                }
                //Grabamos el fichero.
                documento.Save(sFileout);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        /// <summary>
        /// Devuelve el mail de la empresa para la notificación.
        /// </summary>
        /// <param name="sCif"></param>
        /// <returns></returns>
        private static String MailEmpresaMarmedsa(String sCif)
        {
            String sMailEmpresaMarmedsa = "";
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                }
                return sMailEmpresaMarmedsa;
            }
            catch (Exception e)
            {
                return sMailEmpresaMarmedsa;
            }
        }
        #endregion
        #endregion

        #region Procesar Ficheros PDF
        private static void CrearPDF(String strFicheroTemp, String sDirectorioIn, String sDirectorioOut)
        {
            try
            {
                String sFileXML = CrearFicheroXMLContienePDF(strFicheroTemp);
                if (sFileXML != "")
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(sFileXML);
                    XmlNode nodo = xmlDoc.SelectSingleNode("//FACB64");
                    String spdfbase64 = nodo.InnerXml;
                    byte[] bBase64;
                    bBase64 = Convert.FromBase64String(spdfbase64);

                    String nombreFichero = strFicheroTemp.Replace(".xml", ".pdf");
                    nombreFichero = nombreFichero.Replace(".XML", ".pdf");
                    nombreFichero = nombreFichero.Replace(sDirectorioIn, sDirectorioOut);
                    byteArrayToFile(bBase64, nombreFichero);
                }
                else //viene directamente el pdf.
                {
                    String nombreFichero = strFicheroTemp.Replace(".xml", ".pdf");
                    nombreFichero = nombreFichero.Replace(".XML", ".pdf");
                    nombreFichero = nombreFichero.Replace(sDirectorioIn, sDirectorioOut);
                    File.Move(strFicheroTemp, nombreFichero);
                }
            }
            catch (Exception e)
            { }
        }

        


        private static String LeerFicheroContienePDF(String sFile)
        {
            try
            {
                String line;
                String scontenido = "";
                // Read the file and display it line by line.  
                System.IO.StreamReader file =
                    new System.IO.StreamReader(sFile);
                while ((line = file.ReadLine()) != null)
                {
                    scontenido = scontenido + line;
                }

                file.Close();
                return scontenido;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private static String CrearFicheroXMLContienePDF(String sFilewithpdf)
        {
            try
            {
                String sContenido = LeerFicheroContienePDF(sFilewithpdf);

                XmlDocument doc = new XmlDocument();
                XmlElement FileContentPDF = doc.CreateElement("FileContentPDF");
                FileContentPDF.InnerXml = sContenido;
                String sFile = sFilewithpdf + "_contentpdf" + ".xml";
                doc.AppendChild(FileContentPDF);
                if (File.Exists(sFile)) File.Delete(sFile);
                doc.Save(sFile);

                return sFile;
            }
            catch (Exception e)
            {
                return "";
            }
        }
        #endregion

        #region Tratamiento Ficheros
        private static string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {
                string directorioDestino = "";
                bool OK = false;


                //myLog.WriteEntry("Los ficheros se procesan en: "+directorioDestino,EventLogEntryType.Information);	

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                //Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
                if (arrayFicheros.Length > 0)
                {
                    DateTime dt = DateTime.Now;
                    string fecha, hora;

                    //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    hora = dt.ToLongTimeString().Replace(":", "_");
                    string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                }
                // Al procesar el nuevo archivo incluido en el directorio especificado
                // debemos copiarlo al directorio temporal destinado para ello
                for (int i = 0; i < arrayFicheros.Length; i++)
                {
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            File.Delete(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }
                return directorioDestino;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Movemos a Excluidas las facturas que son de cif exluidos.
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorExcluida(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "CifExcluidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Si no encontramos el suplido las movemos a error para no enviar a facturación
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorSuplidos(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "ErrorCifSuplidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Devuelve el fichero pdf en B64
        /// </summary>
        /// <param name="sfilePdf"></param>
        /// <returns></returns>
        private static String getFileB64(String sfilePdf)
        {
            try
            {
                String sBase64 = "";
                sBase64 = Convert.ToBase64String(readBinaryFile(sfilePdf));
                return sBase64;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve byte[] del fichero a leer
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }

        /// <summary>
        /// Movemos a procesadas para no crearlas en los siguientes procesos.
        /// </summary>
        private static void moverAProcesadas(String sDirectorioMonitorizacion)
        {
            try
            {
                String sDirectorioEnviada = sDirectorioMonitorizacion + "Procesadas";
                Directory.CreateDirectory(sDirectorioEnviada);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*.*"))
                {
                    //string[] valores = fi.Name.Split(".".ToArray());

                    //String sfileout = sDirectorioEnviada + "\\" + valores[3] + "." + valores[valores.Length - 1];
                    String sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }

            }
            catch (Exception)
            {
            }
        }

        private static void BorrarFacturasIntermedias(String sDirectorioMonitorizacion)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*_sinfirma.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conceros.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conadmcentre.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_sincero.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conSequenceNumber.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conxml.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_contentpdf.xml"))
                {
                    File.Delete(fi.FullName);
                }

                foreach (var fi in di.GetFiles("*_totalamountlinea.xml"))
                {
                    File.Delete(fi.FullName);
                }

                foreach (var fi in di.GetFiles("*_totalamountfactura.xml"))
                {
                    File.Delete(fi.FullName);
                }
                
                foreach (var fi in di.GetFiles("*_taxratefactura.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conespacios.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_Mailextension.xml"))
                {
                    File.Delete(fi.FullName);
                }
            }
            catch (Exception)
            {

            }
        }


        /// <summary>
        /// tratar todos los ficheros como xml
        /// </summary>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void RenombrarFicheroAXml(string sDirectorioMonitorizacion)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                string pathLog = sDirectorioMonitorizacion + "\\Originales\\";
                String directorioDestinoOriginales = "";
                if (Directory.Exists(pathLog))
                {
                    directorioDestinoOriginales = pathLog;
                }
                else
                {
                    directorioDestinoOriginales = Directory.CreateDirectory(pathLog).FullName;
                }
                Console.WriteLine("Comienzo a renombrar");
                foreach (var fi in di.GetFiles("*.*"))
                {
                    Console.WriteLine("Renombrando " + fi.FullName);
                    if (fi.Extension.ToUpper() != ".XML")
                    {
                        String sFiledestino = fi.FullName.Replace(sDirectorioMonitorizacion, directorioDestinoOriginales);
						if (!File.Exists(sFiledestino))
							File.Copy(fi.FullName, sFiledestino);
						if (File.Exists(fi.FullName + ".xml")) File.Delete(fi.FullName + ".xml");
							File.Move(fi.FullName, fi.FullName + ".xml");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error renombrar " + e.Message);
            }
        }


        /// <summary>
        /// Los deja solo con referenciafactura.xml o .pdf
        /// </summary>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void RenombrarFicheroAReferenciaFactura(string sDirectorioMonitorizacion)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);

                foreach (var fi in di.GetFiles("*.*"))
                {
                    try
                    {
                        string[] valores = fi.Name.Split(".".ToArray());
                        String sfileout = valores[3] + "." + valores[valores.Length - 1];
                        String nombreFichero = fi.FullName;
                        nombreFichero = nombreFichero.Replace(fi.Name, sfileout);
                        if (File.Exists(nombreFichero)) File.Delete(nombreFichero);
                        File.Move(fi.FullName, nombreFichero);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }

        private static void byteArrayToFile(byte[] data, string filePath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(data, 0, data.GetUpperBound(0) + 1);
            }
            catch (Exception e)
            {

            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }
        #endregion

        #region leer xml
        private static XmlDocument leerXML(String sFile)
        {
            using (var stream = new StreamReader(sFile, Encoding.Default))
            {
                using (var reader = XmlReader.Create(stream))
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(reader);
                    return xdoc;
                }
            }
        }
        #endregion

        #region Error Suplidos (el cif receptor no es de marmedsa y no hemos encontrado el suplido en la tabla de suplidos)
        private static Boolean bErrorSuplidos(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bokSuplido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < aListaEmpresas.Count; i++)
                    {
                        CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                        oEmpresa = (CrearFacturaMarmedsaPortic.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                        if (sTaxIdentificationNumber.IndexOf(oEmpresa.CIF) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.IndexOf(sTaxIdentificationNumber) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.Equals(sTaxIdentificationNumber))
                        {
                            bokSuplido = true;
                            break;
                        }
                    }
                }

                return bokSuplido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
		#endregion

		#region Generar mail de resultado

		/// <summary>
		/// Envía el mail del informe del proceso.
		/// </summary>
		private static void GenerarMailProceso()
		{
			try
			{
				//Creamos el cuerpo del mensaje
				String sCuerpo = "";
				if (ResultadoProceso != null)
				{
					sCuerpo = "Facturas Procesadas: " + ResultadoProceso.Count.ToString();
					sCuerpo = sCuerpo + "<p></p>" + "<p></p>";
					for (int i = 0; i < ResultadoProceso.Count; i++)
					{
						GenerarCuerpoMensaje(ResultadoProceso[i].ToString());
					}
					for (int i = 0; i < errorInforme.Count; i++)
					{
						sCuerpo = sCuerpo + errorInforme[i].ToString() + " - " + intInforme[i].ToString();
						sCuerpo = sCuerpo + "<p></p>";
					}
				}
				String sAsunto = "Informe Facturas Portic Marmedsa " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo,"");
			}
			catch (Exception e)
			{
			}
		}

		/// <summary>
		/// Genera el mensaje con la cantidad de errores.
		/// </summary>
		/// <param name="sMensaje"></param>
		private static void GenerarCuerpoMensaje(String sMensaje)
		{
			try
			{
				if (errorInforme == null)
				{
					errorInforme = new ArrayList();
					intInforme = new ArrayList();
					errorInforme.Add(sMensaje);
					intInforme.Add(1);
				}
				else
				{
					Boolean bNew = true;
					for (int i = 0; i < errorInforme.Count; i++)
					{
						if (errorInforme[i].Equals(sMensaje))
						{
							intInforme[i] = (int)intInforme[i] + 1;
							bNew = false;
							break;
						}
					}
					if (bNew)
					{
						errorInforme.Add(sMensaje);
						intInforme.Add(1);
					}
				}

			}
			catch (Exception e)
			{
			}

		}
		#endregion
	}
}
