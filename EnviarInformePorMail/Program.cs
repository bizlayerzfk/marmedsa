﻿using AgiKeyMailLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace EnviarInformePorMail
{
	class Program
	{
		static void Main(string[] args)
		{
			EnviarMail();
		}

		private static void EnviarMail()
		{
			try
			{
				String sDirAdjuntosIn = ConfigurationManager.AppSettings["sDirAdjuntosIn"];
				String sDirAdjuntosOut = ConfigurationManager.AppSettings["sDirAdjuntosOut"];
				String sAsunto = ConfigurationManager.AppSettings["sAsunto"] + " " + DateTime.Now.ToString();
				String sCuerpo = ConfigurationManager.AppSettings["sCuerpo"];
				String sEnviarSinAdjuntos = ConfigurationManager.AppSettings["sEnviarSinAdjuntos"];
				string[] fileNames = Directory.GetFiles(sDirAdjuntosIn, "*.*");
				EnvioMail oMail = new EnvioMail();
				if (fileNames.Length > 0)
				{
					if (oMail.EnviarMail(sAsunto, sCuerpo, fileNames))
					{
						Console.WriteLine("Mail enviado correctamente...");
						//Movemos los ficheros adjuntos a la carpeta out.
						for (int i = 0; i < fileNames.Length; i++)
						{
							String sFilein = fileNames[i];
							String sFileOut = sFilein.Replace(sDirAdjuntosIn, sDirAdjuntosOut);
							MoverFichero(sFilein, sFileOut);
						}
					}
					else Console.WriteLine("No se ha enviado el mail...");
				}
				else
				{
					if (sEnviarSinAdjuntos.Equals("SI"))
					{
						if (oMail.EnviarMail(sAsunto, sCuerpo))
							Console.WriteLine("Mail enviado correctamente...");
						else Console.WriteLine("No se ha enviado el mail...");
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Error enviar mail " + e.Message);
			}
		}

		private static void MoverFichero(string sFile, string sFileOut)
		{
			try
			{
				if (File.Exists(sFileOut)) File.Delete(sFileOut);
				File.Move(sFile, sFileOut);
			}
			catch (Exception e)
			{
			}
		}
	}
}
