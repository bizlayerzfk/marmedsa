//




//

namespace Com.Bizlayer.Pki.Xades

{
		
		#region Namespace Import Declarations
		
			using System;
			using System.Collections.Generic;
			using System.Linq;
			using System.Security.Cryptography.X509Certificates;
			using System.Xml;
			
		#endregion
		
	internal class CertificateValues
	
	{
		
		#region Constructors
		
			public CertificateValues (XmlDocument xmlDoc, List<X509Certificate2> listaCertificados)
			
			{
				this.addCertificateValues (xmlDoc, listaCertificados);
			}
			
		#endregion
		
		#region Methods
		
			private void addCertificateValues (XmlDocument xmlDoc, List<X509Certificate2> listaCertificados)
			
			{
				int i1;
				string string1 = Consts.URI_ETSI;
				XmlElement xmlElement1 = xmlDoc.CreateElement (Consts.CertificateValues, string1);
				XmlAttribute xmlAttribute1 = xmlDoc.CreateAttribute (Consts.Id);
				xmlAttribute1.InnerText = Consts.CertificateValues;
				XmlAttribute xmlAttribute2 = xmlElement1.Attributes.Append (xmlAttribute1);
				XmlElement xmlElement2 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.UnsignedSignatureProperties).Item (0));
				if (xmlElement2 == null)
				{
					xmlElement2 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("xades:" + Consts.UnsignedSignatureProperties)).Item (0));
					if (xmlElement2 == null)
					{
						throw new Exception (Consts.NodeUnsignedSignaturePropertiesNotExist);
					}
				}
				XmlNode xmlNode1 = xmlElement2.AppendChild (((XmlNode) xmlElement1));
				for (i1 = 0; (i1 < listaCertificados.Count); i1++)
				{
					this.addEncapsulateX509Certificate (xmlDoc, Convert.ToBase64String (Enumerable.ElementAt<X509Certificate2> (listaCertificados, i1).Export (X509ContentType.Cert)));
				}
			}
			
			private void addEncapsulateX509Certificate (XmlDocument xmlDoc, string value)
			
			{
				string string1 = Consts.URI_ETSI;
				XmlElement xmlElement1 = xmlDoc.CreateElement (Consts.EncapsulatedX509Certificate, string1);
				xmlElement1.InnerText = value;
				XmlElement xmlElement2 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.CertificateValues).Item (0));
				if (xmlElement2 == null)
				{
					xmlElement2 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("xades:" + Consts.CertificateValues)).Item (0));
					if (xmlElement2 == null)
					{
						throw new Exception (Consts.NodeCertificateValuesNotExist);
					}
				}
				XmlNode xmlNode1 = xmlElement2.AppendChild (((XmlNode) xmlElement1));
			}
			
		#endregion
	}
	
}

