namespace Com.Bizlayer.Pki.Xades
{

    #region Namespace Import Declarations

    using Org.BouncyCastle.Cms;
    using Org.BouncyCastle.Math;
    using Org.BouncyCastle.Ocsp;
    using Org.BouncyCastle.Tsp;
    using Org.BouncyCastle.X509;
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Cryptography.Xml;
    using System.Xml;
    using X509Certificate = Org.BouncyCastle.X509.X509Certificate;

    #endregion

    public class XadesSignature
    {
        #region Constants

        public const string ROL_FIRMANTE_EMISOR = "supplier";
        public const string ROL_FIRMANTE_RECEPTOR = "customer";
        public const string ROL_FIRMANTE_TERCERO = "third party";

        #endregion

        #region Constructors

        public XadesSignature()
        {
        }

        #endregion

        #region Methods


        private X509Certificate2 GetCertificadoPorDn(string dn)
        {
            return Utils.BuscarCertificadoPorDN(dn);
        }

        private string GetDNCertificate(X509Certificate2 certificate)
        {
            if (certificate != null)
            {
                return certificate.Subject;
            }
            else
            {
                throw new Exception(Consts.ErrorCertificateNull);
            }
        }

        public static X509Certificate2 getIssuerX509(X509Certificate2 cert)
        {
            X509Store x509Store = new X509Store(StoreName.CertificateAuthority, StoreLocation.CurrentUser);
            x509Store.Open(OpenFlags.OpenExistingOnly);
            X509Certificate2Collection x509Certificate2Collection1 = x509Store.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, cert.IssuerName.Name, false);
            x509Store.Close();
            if (x509Certificate2Collection1.Count > 0)
            {
                return x509Certificate2Collection1[0];
            }
            else
            {
                x509Store = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
                x509Store.Open(OpenFlags.OpenExistingOnly);
                x509Certificate2Collection1 = x509Store.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, cert.IssuerName.Name, false);
                x509Store.Close();
                if (x509Certificate2Collection1.Count != 0)
                {
                    return x509Certificate2Collection1[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Muestra el certificado del firmante
        /// </summary>
        /// <param name="xmlDocument">XML Firmado</param>
        public void MostrarCertifiacdo(XmlDocument xmlDocument)
        {
            X509Certificate2 certificado = this.ObtenerCertificadoDeFirma(xmlDocument);
            if (certificado != null)
            {
                X509Certificate2UI.DisplayCertificate(certificado);
            }
        }

        /// <summary>
        /// Obtiene el certificado de firma del XML
        /// </summary>
        /// <param name="xmlDocument">XML firmado</param>
        /// <returns></returns>
        public X509Certificate2 ObtenerCertificadoDeFirma(XmlDocument xmlDocument)
        {
            return Utils.getCertificado(xmlDocument);
        }

        public string ObtenerDatosEmisor(XmlDocument xmlFirmado)
        {
            X509Certificate2 x509Certificate2_1 = Utils.getCertificado(xmlFirmado);
            string string1 = x509Certificate2_1.Subject.Substring(x509Certificate2_1.Subject.IndexOf("CN"));
            int i1 = string1.IndexOf(",");
            return string1.Substring(0, i1);
        }

        public X509Certificate2 ObtenerCertificadoFirmante(XmlDocument xmlFirmado)
        {
            return Utils.getCertificado(xmlFirmado);
        }

        /// <summary>
        /// Obtiene la fecha (signingTime) de la firma digital realizada.
        /// </summary>
        /// <param name="xmlFirmado">XML firmado</param>
        /// <returns>Fecha de la firma</returns>
        public DateTime ObtenerFechaFirma(XmlDocument xmlFirmado)
        {
            return Utils.ObtenerFechaFirma(xmlFirmado);
        }


        public string ObtenerTipoFirma(XmlDocument xmlDocument)
        {
            return Utils.StructureValidation(xmlDocument);
        }

        /// <summary>
        /// Valida la firma digital XAdES inclu�da en el documento XML. Retorna una excepci�n si la firma es inv�lida
        /// </summary>
        /// <param name="xmlDocument">Documento XML que contiene la firma digital XAdES</param>
        public void ValidarFirmaDigital(XmlDocument xmlFirmado)
        {
            SignedXml signedXml1 = new SignedXml(xmlFirmado);
            XmlElement xmlElement1 = ((XmlElement)xmlFirmado.DocumentElement.GetElementsByTagName(Consts.Signature).Item(0));
            if (xmlElement1 == null)
            {
                xmlElement1 = ((XmlElement)xmlFirmado.DocumentElement.GetElementsByTagName(("ds:" + Consts.Signature)).Item(0));
            }
            if (xmlElement1 == null)
            {
                throw new Exception(Consts.ErrorSignNotExist);
            }
            else
            {
                signedXml1.LoadXml(xmlElement1);
                if (!signedXml1.CheckSignature())
                {
                    throw new Exception(Consts.ErrorCheckSignature);
                }
            }
        }

        private void saveDN(string path, string dn)
        {
            using (StreamWriter streamWriter1 = new StreamWriter(path))
            {
                streamWriter1.Write(dn);
            }
        }

        private void saveDN(string path, X509Certificate2 certificate)
        {
            using (StreamWriter streamWriter1 = new StreamWriter(path))
            {
                streamWriter1.Write(certificate.Subject);
            }
        }

        private X509Certificate2 SelectCertificate()
        {
            X509Certificate2 x509Certificate2_1 = Utils.GetCertificate();
            if (x509Certificate2_1 != null)
            {
                return x509Certificate2_1;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Genera una firma XAdES-EPES del documento origen. El documento firmado se almacenar� en el mismo xmlDoc
        /// </summary>
        /// <param name="xmlDoc">Documento XML a firmar</param>
        /// <param name="role">Rol del firmante</param>
        /// <param name="certificado">Certificado del firmante</param>
        /// <param name="entidad"></param>
        /// <param name="politica"></param>
        private void XadesEpes(XmlDocument xmlDoc, string role, X509Certificate2 certificado, string entidad, string politica)
        {
            if (certificado != null)
            {
                SignatureXadesEpes signatureXadesEpes1 = new SignatureXadesEpes(xmlDoc, "", role, false, certificado, entidad, politica);
            }
            else
            {
                throw new Exception(Consts.ErrorCertificateNull);
            }
        }
        /// <summary>
        /// Genera una firma XAdES-EPES del documento origen. El documento firmado se almacenar� en el mismo xmlDoc
        /// </summary>
        /// <param name="xmlDoc">Documento XML a firmar</param>
        /// <param name="role">Rol del firmante</param>
        /// <param name="certificado">Certificado digital a utilizar</param>
        public void XadesEpes(XmlDocument xmlDoc, string role, X509Certificate2 certificado)
        {
            if (certificado != null)
            {
                SignatureXadesEpes signatureXadesEpes1 = new SignatureXadesEpes(xmlDoc, "", role, certificado);
            }
            else
            {
                throw new Exception(Consts.ErrorCertificateNull);
            }
        }

        /// <summary>
        /// Genera una firma XAdES-EPES del documento origen. El documento firmado se almacenar� en el mismo xmlDoc
        /// </summary>
        /// <param name="xmlDoc">Documento XML a firmar</param>
        /// <param name="role">Rol del firmante</param>
        /// <param name="dnCertificate">DN del Certificado a utilizar</param>
        public void XadesEpes(XmlDocument xmlDoc, string role, string dnCertificate)
        {
            X509Certificate2 x509Certificate2_1 = this.GetCertificadoPorDn(dnCertificate);
            if (x509Certificate2_1 != null)
            {
                SignatureXadesEpes signatureXadesEpes1 = new SignatureXadesEpes(xmlDoc, "", role, x509Certificate2_1);
            }
            else
            {
                throw new Exception(Consts.ErrorCertificateNull);
            }
        }

        /// <summary>
        /// Genera una firma XAdES-EPES del documento origen. El documento firmado se almacenar� en el mismo xmlDoc
        /// </summary>
        /// <param name="xmlDoc">XML d firmar</param>
        /// <param name="role">Rol del firmante</param>
        public void XadesEpes(XmlDocument xmlDoc, string role)
        {
            SignatureXadesEpes signatureXadesEpes1 = new SignatureXadesEpes(xmlDoc, "", role);
        }

        /// <summary>
        /// Genera una firma XAdES-EPES del documento origen. El documento firmado se almacenar� en el mismo xmlDoc
        /// </summary>
        /// <param name="xmlDoc">XML a Firmar</param>
        /// <param name="role">Rol del firmante</param>
        /// <param name="uriCertificate">Localizaci�n del fichero P12 que contiene el certificado y su clave privada</param>
        /// <param name="passwordCertificate">Password que protege al fichero P12</param>
        public void XadesEpes(XmlDocument xmlDoc, string role, string uriCertificate, string passwordCertificate)
        {
            SignatureXadesEpes signatureXadesEpes1 = new SignatureXadesEpes(xmlDoc, "", role, false, uriCertificate, passwordCertificate);
        }




        #endregion
    }

}

