

namespace Com.Bizlayer.Pki.Xades
{

    #region Namespace Import Declarations

    using Org.BouncyCastle.Asn1.X509;
    using Org.BouncyCastle.Crypto;
    using Org.BouncyCastle.Crypto.Parameters;
    using Org.BouncyCastle.Math;
    using Org.BouncyCastle.Utilities.Encoders;
    using Org.BouncyCastle.X509;
    using System;
    using System.Collections;
    using System.IO;
    using System.Net;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Security.Cryptography.Xml;
    using System.Text;
    using System.Xml;
    using X509Certificate = System.Security.Cryptography.X509Certificates.X509Certificate;

    #endregion

    internal class SignatureXadesEpes
    {

        #region Fields
        [CompilerGeneratedAttribute()]
        private bool _correcto;
        private string fecha;
        #endregion

        #region Constructors

        public SignatureXadesEpes(XmlDocument xmlDoc, string uriDoc, string role)
        {
            bool detached = true;
            X509Certificate2 x509Certificate2_1;
            KeyInfo keyInfo1;
            XmlDocument xmlDocument1;
            XmlElement xmlElement1;
            DataObject dataObject1;
            Exception exception1;
            this.fecha = string.Empty;
            try
            {
                x509Certificate2_1 = Utils.GetCertificate();
                if (x509Certificate2_1 == null)
                {
                    throw new Exception(Consts.ErrorCertificateNull);
                }
                else
                {
                    keyInfo1 = this.getKeyInfo(x509Certificate2_1);
                    xmlDocument1 = new XmlDocument();
                    xmlElement1 = this.getXmlSignedProperties_Epes(xmlDocument1, x509Certificate2_1, role, (!detached), xmlDoc);
                    XmlNode xmlNode1 = xmlDocument1.AppendChild(((XmlNode)xmlElement1));
                    //xmlDocument2 = new XmlDocument ();
                    //xmlElement2 = this.getXmlSignedProperties_Epes (xmlDocument2, x509Certificate2_1, role, false, xmlDoc);
                    //XmlNode xmlNode2 = xmlDocument2.AppendChild (((XmlNode) xmlElement2));
                    dataObject1 = this.getObjectSignedProperties(xmlDocument1);
                    this.sign(xmlDoc, uriDoc, xmlDocument1, x509Certificate2_1, keyInfo1, dataObject1, detached);
                    this.Correcto = true;
                }
            }
            catch (Exception exception2)
            {
                exception1 = exception2;
                throw exception1;
            }
        }


        public SignatureXadesEpes(XmlDocument xmlDoc, string uriDoc, string role, X509Certificate2 certificado)
        {
            KeyInfo keyInfo1;
            XmlDocument xmlDocument1;
            XmlElement xmlElement1;
            XmlDocument xmlDocument2;
            XmlElement xmlElement2;
            DataObject dataObject1;
            Exception exception1;
            this.fecha = string.Empty;
            bool detached = true;

            try
            {
                keyInfo1 = this.getKeyInfo(certificado);
                xmlDocument1 = new XmlDocument();
                xmlElement1 = this.getXmlSignedProperties_Epes(xmlDocument1, certificado, role, (!detached), xmlDoc);
                XmlNode xmlNode1 = xmlDocument1.AppendChild(((XmlNode)xmlElement1));
                xmlDocument2 = new XmlDocument();
                xmlElement2 = this.getXmlSignedProperties_Epes(xmlDocument2, certificado, role, false, xmlDoc);
                XmlNode xmlNode2 = xmlDocument2.AppendChild(((XmlNode)xmlElement2));
                dataObject1 = this.getObjectSignedProperties(xmlDocument2);
                this.sign(xmlDoc, uriDoc, xmlDocument1, certificado, keyInfo1, dataObject1, detached);
                this.Correcto = true;
            }
            catch (Exception exception2)
            {
                exception1 = exception2;
                throw exception1;
            }
        }


        public SignatureXadesEpes(XmlDocument xmlDoc, string uriDoc, string role, bool detached, string uriCertificate, string password)
        {
            X509Certificate2 certificado;
            KeyInfo keyInfo1;
            XmlDocument xmlDocument1;
            XmlElement xmlElement1;
            XmlDocument xmlDocument2;
            XmlElement xmlSignedProperties;
            DataObject dataObject1;
            Exception exception1;
            this.fecha = string.Empty;
            try
            {
                certificado = new X509Certificate2(uriCertificate, password);
                keyInfo1 = this.getKeyInfo(certificado);
                xmlDocument1 = new XmlDocument();
                xmlElement1 = this.getXmlSignedProperties_Epes(xmlDocument1, certificado, role, (!detached), xmlDoc);
                XmlNode xmlNode1 = xmlDocument1.AppendChild(((XmlNode)xmlElement1));
                xmlDocument2 = new XmlDocument();
                xmlSignedProperties = this.getXmlSignedProperties_Epes(xmlDocument2, certificado, role, false, xmlDoc);
                XmlNode xmlNode2 = xmlDocument2.AppendChild(((XmlNode)xmlSignedProperties));
                dataObject1 = this.getObjectSignedProperties(xmlDocument2);
                this.sign(xmlDoc, uriDoc, xmlDocument1, certificado, keyInfo1, dataObject1, detached);
                this.Correcto = true;
            }
            catch (Exception exception2)
            {
                exception1 = exception2;
                throw exception1;
            }
        }


        public SignatureXadesEpes(XmlDocument xmlDoc, string uriDoc, string role, bool detached, X509Certificate2 certificado, string entidad, string politca)
        {
            KeyInfo keyInfo1;
            XmlDocument xmlDocument1;
            XmlElement xmlElement1;
            XmlDocument xmlDocument2;
            XmlElement xmlElement2;
            DataObject dataObject1;
            Exception exception1;
            this.fecha = string.Empty;
            try
            {
                keyInfo1 = this.getKeyInfo(certificado);
                xmlDocument1 = new XmlDocument();
                xmlElement1 = this.getXmlSignedProperties_Epes_Especial(xmlDocument1, certificado, role, (!detached), xmlDoc, politca);
                XmlNode xmlNode1 = xmlDocument1.AppendChild(((XmlNode)xmlElement1));
                xmlDocument2 = new XmlDocument();
                xmlElement2 = this.getXmlSignedProperties_Epes_Especial(xmlDocument2, certificado, role, false, xmlDoc, politca);
                XmlNode xmlNode2 = xmlDocument2.AppendChild(((XmlNode)xmlElement2));
                dataObject1 = this.getObjectSignedProperties(xmlDocument2);
                this.sign(xmlDoc, uriDoc, xmlDocument1, certificado, keyInfo1, dataObject1, detached, entidad);
            }
            catch (Exception exception2)
            {
                exception1 = exception2;
                throw exception1;
            }
        }

        #endregion

        #region Properties

        public bool Correcto
        {
            get
            {
                return this._correcto;
            }
            set
            {
                this._correcto = value;
            }
        }

        #endregion

        #region Methods

        private string getHashPolicyUrl(string uriPolicy)
        {
            //TODO Cambiada pol�tica para que no la calcule din�micamente de la URL. 
            //        Se asigna est�ticamente la pol�tica de facturae
            //        Habr�a que cambiarlo para obtener el hash diferente en funci�n de las diferentes pol�ticas de firma
            return "Ohixl6upD6av8N7pEvDABhEL6hM=";
            //if (uriPolicy == Consts.URI_POLICY)
            //{
            //        return "Ohixl6upD6av8N7pEvDABhEL6hM=";
            //}
            //else
            //{
            //        throw new Exception(Consts.ErrorCheckPolicy);
            //}

            //WebRequest webRequest1;
            //byte[] byteArray1;
            //WebResponse webResponse1;
            //byte[] byteArray2;
            //string string1;
            //try
            //{
            //    webRequest1 = WebRequest.Create(uriPolicy);
            //    webRequest1.Timeout = 25000;
            //    webResponse1 = webRequest1.GetResponse();
            //    byteArray1 = Utils.ReadFully(webResponse1.GetResponseStream(), 0);
            //    byteArray2 = new SHA1Managed().ComputeHash(byteArray1);
            //    string1 = Convert.ToBase64String(byteArray2);
            //}
            //catch (Exception)
            //{
            //    throw new Exception(Consts.ErrorCheckPolicy);
            //}
            //return string1;
        }

        /// <summary>
        /// Genera el nodo KeyInfo a partir del certificado del firmante
        /// </summary>
        /// <param name="certificado"></param>
        /// <returns></returns>
        private KeyInfo getKeyInfo(X509Certificate2 certificado)
        {
            KeyInfo keyInfo1;
            Org.BouncyCastle.X509.X509Certificate x509Certificate1;
            RsaKeyParameters rsaKeyParameters1;
            RSAParameters rSAParameters1;
            RSAKeyValue rSAKeyValue1;
            KeyInfo keyInfo2;
            try
            {
                keyInfo1 = new KeyInfo();
                keyInfo1.Id = Consts.OBJECTKEYINFO;
                x509Certificate1 = Utils.ReadCertificate(certificado);
                rsaKeyParameters1 = ((RsaKeyParameters)x509Certificate1.GetPublicKey());
                rSAParameters1 = new RSAParameters();
                ASCIIEncoding aSCIIEncoding1 = new ASCIIEncoding();
                rSAParameters1.Exponent = rsaKeyParameters1.Exponent.ToByteArrayUnsigned();
                rSAParameters1.Modulus = rsaKeyParameters1.Modulus.ToByteArrayUnsigned();
                rSAKeyValue1 = new RSAKeyValue();
                rSAKeyValue1.Key.ImportParameters(rSAParameters1);
                keyInfo1.AddClause(((KeyInfoClause)new KeyInfoX509Data(((X509Certificate)certificado))));
                keyInfo1.AddClause(((KeyInfoClause)rSAKeyValue1));
                keyInfo2 = keyInfo1;
            }
            catch (Exception)
            {
                throw new Exception(Consts.ErrorKeyInfo);
            }
            return keyInfo2;
        }

        private DataObject getObjectSignedProperties(XmlDocument xmlSignedProperties)
        {
            DataObject dataObject1;
            DataObject dataObject2;
            try
            {
                dataObject1 = new DataObject();
                dataObject1.Id = Consts.OBJECT_1;
                dataObject1.Data = xmlSignedProperties.ChildNodes;
                dataObject2 = dataObject1;
            }
            catch (Exception)
            {
                throw new Exception(Consts.ErrorDataObjectSignedProperies);
            }
            return dataObject2;
        }

        private XmlElement getQualifyingProperties(XmlDocument doc, bool namespaces, XmlDocument xmlDoc)
        {
            int i1;
            XmlAttribute xmlAttribute5;
            string string1 = Consts.URI_ETSI;
            XmlElement xmlElement1 = doc.CreateElement(Consts.QualifyingProperties, string1);
            XmlElement xmlElement2 = doc.CreateElement(Consts.SignedProperties, string1);
            XmlElement xmlElement3 = doc.CreateElement(Consts.SignedSignatureProperties, string1);
            XmlAttribute xmlAttribute1 = doc.CreateAttribute(Consts.Id);
            XmlAttribute xmlAttribute2 = doc.CreateAttribute(Consts.Target);
            XmlAttribute xmlAttribute3 = doc.CreateAttribute(Consts.Id);
            XmlAttribute xmlAttribute4 = doc.CreateAttribute(Consts.Id);
            xmlAttribute2.InnerText = Consts.URI_SIGNATURE_1;
            xmlAttribute1.InnerText = Consts.OBJECTQUALIFYINGPROPERTIES;
            xmlAttribute3.InnerText = Consts.OBJECTSIGNEDPROPERTIES;
            xmlAttribute4.InnerText = Consts.OBJECTSIGNEDSIGNATUREPROPERTIES;
            XmlAttribute xmlAttribute6 = xmlElement1.Attributes.Append(xmlAttribute1);
            XmlAttribute xmlAttribute7 = xmlElement1.Attributes.Append(xmlAttribute2);
            XmlAttribute xmlAttribute8 = xmlElement2.Attributes.Append(xmlAttribute3);
            XmlAttribute xmlAttribute9 = xmlElement3.Attributes.Append(xmlAttribute4);
            if (namespaces)
            {
                for (i1 = 0; (i1 < xmlDoc.DocumentElement.Attributes.Count); i1++)
                {
                    if (!xmlDoc.DocumentElement.Attributes[i1].Name.Equals("xmlns"))
                    {
                        xmlAttribute5 = doc.CreateAttribute(((xmlDoc.DocumentElement.Attributes[i1].Name != null) ? xmlDoc.DocumentElement.Attributes[i1].Name : ""));
                        xmlAttribute5.Value = xmlDoc.DocumentElement.Attributes[i1].Value;
                        XmlAttribute xmlAttribute10 = xmlElement2.Attributes.Append(xmlAttribute5);
                    }
                }
            }
            XmlNode xmlNode1 = xmlElement2.AppendChild(((XmlNode)xmlElement3));
            XmlNode xmlNode2 = xmlElement1.AppendChild(((XmlNode)xmlElement2));
            return xmlElement1;
        }

        private XmlElement getXmlSignedProperties_Best_1_Or_2(XmlDocument doc, X509Certificate2 certificado, bool namespaces, XmlDocument xmlDoc)
        {
            string string1 = Consts.URI_ETSI;
            string string2 = Consts.URI_XMLDSIGN;
            XmlElement xmlElement1 = this.getQualifyingProperties(doc, namespaces, xmlDoc);
            XmlElement xmlElement2 = doc.CreateElement(Consts.SigningCertificate, string1);
            XmlElement xmlElement3 = doc.CreateElement(Consts.Cert, string1);
            XmlElement xmlElement4 = doc.CreateElement(Consts.CertDigest, string1);
            XmlElement xmlElement5 = doc.CreateElement(Consts.DigestMethod, string2);
            XmlElement xmlElement6 = doc.CreateElement(Consts.DigestValue, string2);
            XmlElement xmlElement7 = doc.CreateElement(Consts.IssuerSerial, string1);
            XmlElement xmlElement8 = doc.CreateElement(Consts.X509IssuerName, string2);
            XmlElement xmlElement9 = doc.CreateElement(Consts.X509SerialNumber, string2);
            XmlElement xmlElement10 = doc.CreateElement(Consts.SigningTime, string1);
            XmlAttribute xmlAttribute1 = doc.CreateAttribute(Consts.Algorithm);
            if (namespaces)
            {
                xmlElement10.InnerText = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
                this.fecha = xmlElement10.InnerText;
            }
            else
            {
                xmlElement10.InnerText = this.fecha;
            }
            xmlAttribute1.InnerText = Consts.URI_XMLDSIGNSHA1;
            xmlElement6.InnerText = Convert.ToBase64String(certificado.GetCertHash());
            Org.BouncyCastle.X509.X509Certificate x509Certificate1 = Utils.ReadCertificate(certificado);
            try
            {
                //xmlElement8.InnerText = Utils.obtenerIssuerName (x509Certificate1.IssuerDN.ToString (false, X509Name.RFC2253Symbols));
                xmlElement8.InnerText = x509Certificate1.IssuerDN.ToString(false, X509Name.RFC2253Symbols);
            }
            catch (Exception)
            {
                throw new Exception(Consts.DNNotValid);
            }
            BigInteger bigInteger1 = new BigInteger(Hex.Decode(certificado.GetSerialNumberString()));
            xmlElement9.InnerText = bigInteger1.ToString();
            XmlAttribute xmlAttribute2 = xmlElement5.Attributes.Append(xmlAttribute1);
            XmlNode xmlNode1 = xmlElement4.AppendChild(((XmlNode)xmlElement5));
            XmlNode xmlNode2 = xmlElement4.AppendChild(((XmlNode)xmlElement6));
            XmlNode xmlNode3 = xmlElement7.AppendChild(((XmlNode)xmlElement8));
            XmlNode xmlNode4 = xmlElement7.AppendChild(((XmlNode)xmlElement9));
            XmlNode xmlNode5 = xmlElement3.AppendChild(((XmlNode)xmlElement4));
            XmlNode xmlNode6 = xmlElement3.AppendChild(((XmlNode)xmlElement7));
            XmlNode xmlNode7 = xmlElement2.AppendChild(((XmlNode)xmlElement3));
            XmlNode xmlNode8 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement10));
            XmlNode xmlNode9 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement2));
            return xmlElement1;
        }

        private XmlElement getXmlSignedProperties_Epes(XmlDocument doc, X509Certificate2 certificado, string role, bool namespaces, XmlDocument xmlDoc)
        {
            string string1 = Consts.URI_ETSI;
            string string2 = Consts.URI_XMLDSIGN;
            string string3 = Consts.URI_POLICY;
            string string4 = Consts.DESCRIPCION;
            XmlElement xmlElement1 = this.getXmlSignedProperties_Best_1_Or_2(doc, certificado, namespaces, xmlDoc);
            XmlElement xmlElement2 = doc.CreateElement(Consts.SignaturePolicyIdentifier, string1);
            XmlElement xmlElement3 = doc.CreateElement(Consts.SignaturePolicyId, string1);
            XmlElement xmlElement4 = doc.CreateElement(Consts.SigPolicyId, string1);
            XmlElement xmlElement5 = doc.CreateElement(Consts.Identifier, string1);
            XmlAttribute xmlAttribute1 = doc.CreateAttribute(Consts.Qualifier);
            XmlElement xmlElement6 = doc.CreateElement(Consts.Description, string1);
            XmlElement xmlElement7 = doc.CreateElement(Consts.SigPolicyHash, string1);
            XmlElement xmlElement8 = doc.CreateElement(Consts.DigestMethod, string2);
            XmlAttribute xmlAttribute2 = doc.CreateAttribute(Consts.Algorithm);
            XmlElement xmlElement9 = doc.CreateElement(Consts.DigestValue, string2);
            XmlElement xmlElement10 = doc.CreateElement(Consts.SignerRole, string1);
            XmlElement xmlElement11 = doc.CreateElement(Consts.ClaimedRoles, string1);
            XmlElement xmlElement12 = doc.CreateElement(Consts.ClaimedRole, string1);
            xmlAttribute1.InnerText = Consts.OIDAsURI;
            xmlElement5.InnerText = string3;
            xmlElement6.InnerText = string4;
            xmlAttribute2.InnerText = Consts.URI_XMLDSIGNSHA1;
            xmlElement9.InnerText = this.getHashPolicyUrl(string3);
            xmlElement12.InnerText = role;
            XmlAttribute xmlAttribute3 = xmlElement5.Attributes.Append(xmlAttribute1);
            XmlAttribute xmlAttribute4 = xmlElement8.Attributes.Append(xmlAttribute2);
            XmlNode xmlNode1 = xmlElement4.AppendChild(((XmlNode)xmlElement5));
            XmlNode xmlNode2 = xmlElement4.AppendChild(((XmlNode)xmlElement6));
            XmlNode xmlNode3 = xmlElement7.AppendChild(((XmlNode)xmlElement8));
            XmlNode xmlNode4 = xmlElement7.AppendChild(((XmlNode)xmlElement9));
            XmlNode xmlNode5 = xmlElement3.AppendChild(((XmlNode)xmlElement4));
            XmlNode xmlNode6 = xmlElement3.AppendChild(((XmlNode)xmlElement7));
            XmlNode xmlNode7 = xmlElement2.AppendChild(((XmlNode)xmlElement3));
            XmlNode xmlNode8 = xmlElement11.AppendChild(((XmlNode)xmlElement12));
            XmlNode xmlNode9 = xmlElement10.AppendChild(((XmlNode)xmlElement11));
            XmlNode xmlNode10 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement2));
            XmlNode xmlNode11 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement10));
            return xmlElement1;
        }


        private XmlElement getXmlSignedProperties_Epes_Especial(XmlDocument doc, X509Certificate2 certificado, string role, bool namespaces, XmlDocument xmlDoc, string politica)
        {
            string string1 = Consts.URI_ETSI;
            string string2 = Consts.URI_XMLDSIGN;
            string string3 = Consts.URI_POLICY;
            string string4 = Consts.DESCRIPCION;
            XmlElement xmlElement1 = this.getXmlSignedProperties_Best_1_Or_2(doc, certificado, namespaces, xmlDoc);
            XmlElement xmlElement2 = doc.CreateElement(Consts.SignaturePolicyIdentifier, string1);
            XmlElement xmlElement3 = doc.CreateElement(Consts.SignaturePolicyId, string1);
            XmlElement xmlElement4 = doc.CreateElement(Consts.SigPolicyId, string1);
            XmlElement xmlElement5 = doc.CreateElement(Consts.Identifier, string1);
            XmlAttribute xmlAttribute1 = doc.CreateAttribute(Consts.Qualifier);
            XmlElement xmlElement6 = doc.CreateElement(Consts.Description, string1);
            XmlElement xmlElement7 = doc.CreateElement(Consts.SigPolicyHash, string1);
            XmlElement xmlElement8 = doc.CreateElement(Consts.DigestMethod, string2);
            XmlAttribute xmlAttribute2 = doc.CreateAttribute(Consts.Algorithm);
            XmlElement xmlElement9 = doc.CreateElement(Consts.DigestValue, string2);
            XmlElement xmlElement10 = doc.CreateElement(Consts.SignerRole, string1);
            XmlElement xmlElement11 = doc.CreateElement(Consts.ClaimedRoles, string1);
            XmlElement xmlElement12 = doc.CreateElement(Consts.ClaimedRole, string1);
            xmlAttribute1.InnerText = Consts.OIDAsURI;
            xmlElement5.InnerText = string3;
            xmlElement6.InnerText = string4;
            xmlAttribute2.InnerText = "http://www.w3.org/2000/09/xmldsig#sha1";
            xmlElement9.InnerText = politica;
            xmlElement12.InnerText = role;
            XmlAttribute xmlAttribute3 = xmlElement5.Attributes.Append(xmlAttribute1);
            XmlAttribute xmlAttribute4 = xmlElement8.Attributes.Append(xmlAttribute2);
            XmlNode xmlNode1 = xmlElement4.AppendChild(((XmlNode)xmlElement5));
            XmlNode xmlNode2 = xmlElement4.AppendChild(((XmlNode)xmlElement6));
            XmlNode xmlNode3 = xmlElement7.AppendChild(((XmlNode)xmlElement8));
            XmlNode xmlNode4 = xmlElement7.AppendChild(((XmlNode)xmlElement9));
            XmlNode xmlNode5 = xmlElement3.AppendChild(((XmlNode)xmlElement4));
            XmlNode xmlNode6 = xmlElement3.AppendChild(((XmlNode)xmlElement7));
            XmlNode xmlNode7 = xmlElement2.AppendChild(((XmlNode)xmlElement3));
            XmlNode xmlNode8 = xmlElement11.AppendChild(((XmlNode)xmlElement12));
            XmlNode xmlNode9 = xmlElement10.AppendChild(((XmlNode)xmlElement11));
            XmlNode xmlNode10 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement2));
            XmlNode xmlNode11 = xmlElement1.ChildNodes[0].ChildNodes[0].AppendChild(((XmlNode)xmlElement10));
            return xmlElement1;
        }

        private void sign(XmlDocument xmlDoc, string uriDoc, XmlDocument xmlSignedProperties, X509Certificate2 certificado, KeyInfo keyInfo, DataObject obj, bool detached)
        {
            Reference referenciaEnveloped;
            Reference referenciaSignedProperties;
            Reference referenciaKeyInfo;
            MySignXml mySignXml1;
            XmlDsigEnvelopedSignatureTransform xmlDsigEnvelopedSignatureTransform1;
            Exception exception1;
            XmlElement xmlFirma;
            XmlNode xmlNode1;
            Exception exception2;
            try
            {
                referenciaEnveloped = new Reference();
                referenciaEnveloped.Id = Consts.REFERENCE_1;
                referenciaEnveloped.Uri = uriDoc;
                referenciaSignedProperties = new Reference();
                referenciaSignedProperties.Id = Consts.REFERENCE_2;
                referenciaSignedProperties.Type = Consts.URI_ETSI_SIGNEDPROPERTIES;
                referenciaSignedProperties.Uri = Consts.URI_SIGNEDPROPERTIES_1;
                referenciaKeyInfo = new Reference();
                referenciaKeyInfo.Id = Consts.REFERENCE_3;
                referenciaKeyInfo.Uri = Consts.URI_KEYINFO_1;

                obj.Id = Consts.OBJECT_1;
                mySignXml1 = new MySignXml(xmlDoc);
                mySignXml1.SignedInfo.Id = Consts.OBJECT_SIGNEDINFO_1;
                mySignXml1.Signature.Id = Consts.OBJECT_SIGNATURE_1;
                mySignXml1.SigningKey = certificado.PrivateKey;
                if (mySignXml1.SigningKey == null)
                {
                    throw new Exception(Consts.ErrorPrivateKey);
                }
                else
                {
                    mySignXml1.doc2 = xmlSignedProperties;
                    mySignXml1.KeyInfo = keyInfo;
                    mySignXml1.AddObject(obj);
                    // Referencia a todo el xml
                    mySignXml1.AddReference(referenciaEnveloped);
                    // Referencia a las signedProperties
                    mySignXml1.AddReference(referenciaSignedProperties);

                    mySignXml1.AddReference(referenciaKeyInfo);
                    //mySignXml1.AddReference (reference4);
                    if (!detached)
                    {
                        xmlDsigEnvelopedSignatureTransform1 = new XmlDsigEnvelopedSignatureTransform();
                        referenciaEnveloped.AddTransform(((Transform)xmlDsigEnvelopedSignatureTransform1));
                    }
                    try
                    {
                        mySignXml1.ComputeSignature();
                    }
                    catch (Exception ex)
                    {
                        exception1 = ex;
                        if ((exception1.Message.Equals(Consts.ErrorSignatureSignedProperties) || exception1.Message.Equals(Consts.ErrorSignatureKeyInfo)) || exception1.Message.Equals(Consts.ErrorUnknowElementToSign))
                        {
                            throw exception1;
                        }
                        else
                        {
                            throw new Exception(Consts.ErrorCertificateNull);
                        }
                    }
                    xmlFirma = mySignXml1.GetXml();
                    xmlNode1 = xmlDoc.ImportNode(((XmlNode)xmlFirma), true);
                    if (!detached)
                    {
                        XmlNode xmlNode2 = xmlDoc.DocumentElement.AppendChild(xmlNode1);
                    }
                    else
                    {
                        XmlNode xmlNode3 = xmlDoc.AppendChild(xmlNode1);
                    }
                }
            }
            catch (Exception exception4)
            {
                exception2 = exception4;
                if ((((exception2.Message.Equals(Consts.ErrorPrivateKey) || exception2.Message.Equals(Consts.ErrorSignatureSignedProperties)) || exception2.Message.Equals(Consts.ErrorSignatureKeyInfo)) || exception2.Message.Equals(Consts.ErrorUnknowElementToSign)) || exception2.Message.Equals(Consts.ErrorCertificateNull))
                {
                    throw exception2;
                }
                else
                {
                    throw new Exception(Consts.ErrorToSign);
                }
            }
        }

        private void sign(XmlDocument xmlDoc, string uriDoc, XmlDocument xmlSignedProperties, X509Certificate2 certificado, KeyInfo keyInfo, DataObject obj, bool detached, string entidad)
        {
            Reference reference1;
            Reference reference2;
            Reference reference3;
            Reference reference4;
            DataObject dataObject1;
            XmlElement xmlElement1;
            MySignXml mySignXml1;
            XmlDsigEnvelopedSignatureTransform xmlDsigEnvelopedSignatureTransform1;
            Exception exception1;
            XmlElement xmlElement2;
            XmlNode xmlNode1;
            Exception exception2;
            try
            {
                reference1 = new Reference();
                reference1.Id = "Signature_1_Reference_1";
                reference1.Uri = uriDoc;
                reference2 = new Reference();
                reference2.Id = "Signature_1_Reference_2";
                reference2.Type = "http://uri.etsi.org/01903#SignedProperties";
                reference2.Uri = "#Signature_1_SignedProperties_1";
                reference3 = new Reference();
                reference3.Id = "Signature_1_Reference_3";
                reference3.Uri = "#Signature_1_KeyInfo_1";
                reference4 = new Reference();
                reference4.Id = "Signature_1_Object_4";
                reference4.Uri = "#Signature_1_Object_1";
                dataObject1 = new DataObject();
                dataObject1.Id = "Signature_1_Object_1";
                xmlElement1 = xmlDoc.CreateElement("Entidad");
                xmlElement1.InnerText = entidad;
                dataObject1.Data = xmlElement1.ChildNodes;
                obj.Id = "Signature_1_Object_2";
                mySignXml1 = new MySignXml(xmlDoc);
                mySignXml1.SignedInfo.Id = "Signature_1_SignedInfo_1";
                mySignXml1.Signature.Id = "Signature_1";
                mySignXml1.SigningKey = certificado.PrivateKey;
                if (mySignXml1.SigningKey == null)
                {
                    throw new Exception(Consts.ErrorPrivateKey);
                }
                else
                {
                    mySignXml1.doc2 = xmlSignedProperties;
                    mySignXml1.KeyInfo = keyInfo;
                    mySignXml1.AddObject(dataObject1);
                    mySignXml1.AddObject(obj);
                    mySignXml1.AddReference(reference1);
                    mySignXml1.AddReference(reference2);
                    mySignXml1.AddReference(reference3);
                    mySignXml1.AddReference(reference4);
                    if (!detached)
                    {
                        xmlDsigEnvelopedSignatureTransform1 = new XmlDsigEnvelopedSignatureTransform();
                        reference1.AddTransform(((Transform)xmlDsigEnvelopedSignatureTransform1));
                    }
                    try
                    {
                        mySignXml1.ComputeSignature();
                    }
                    catch (Exception exception3)
                    {
                        exception1 = exception3;
                        if ((exception1.Message.Equals(Consts.ErrorSignatureSignedProperties) || exception1.Message.Equals(Consts.ErrorSignatureKeyInfo)) || exception1.Message.Equals(Consts.ErrorUnknowElementToSign))
                        {
                            throw exception1;
                        }
                        else
                        {
                            throw new Exception(Consts.ErrorCertificateNull);
                        }
                    }
                    xmlElement2 = mySignXml1.GetXml();
                    xmlNode1 = xmlDoc.ImportNode(((XmlNode)xmlElement2), true);
                    if (!detached)
                    {
                        XmlNode xmlNode2 = xmlDoc.DocumentElement.AppendChild(xmlNode1);
                    }
                    else
                    {
                        XmlNode xmlNode3 = xmlDoc.AppendChild(xmlNode1);
                    }
                }
            }
            catch (Exception exception4)
            {
                exception2 = exception4;
                if ((((exception2.Message.Equals(Consts.ErrorPrivateKey) || exception2.Message.Equals(Consts.ErrorSignatureSignedProperties)) || exception2.Message.Equals(Consts.ErrorSignatureKeyInfo)) || exception2.Message.Equals(Consts.ErrorUnknowElementToSign)) || exception2.Message.Equals(Consts.ErrorCertificateNull))
                {
                    throw exception2;
                }
                else
                {
                    throw new Exception(Consts.ErrorToSign);
                }
            }
        }

        #endregion
    }

}

