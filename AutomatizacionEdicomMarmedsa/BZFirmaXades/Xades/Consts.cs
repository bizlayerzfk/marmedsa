
namespace Com.Bizlayer.Pki.Xades

{
		
		#region Namespace Import Declarations
		
			using System;
			
		#endregion
		
	internal class Consts
	
	{
		
		#region Fields
			public static string Algorithm;
			public static string APLICACIONOCSPREQUEST;
			public static string APLICACIONTIMESTAMPQUERY;
			public static string Basic;
			public static string ByName;
			public static string CanonicalizationMethod;
			public static string Cert;
			public static string CertDigest;
			public static string CertificadoOK;
			public static string CertificateIsNotTrust;
			public static string CertificateNotValid;
			public static string CertificateValues;
			public static string CertRefs;
			public static string ClaimedRole;
			public static string ClaimedRoles;
			public static string CompleteCertificateRefs;
			public static string CompleteCertificationRefs;
			public static string CompleteRevocationRefs;
			public static string CRLIdentifier;
			public static string CRLRef;
			public static string CRLRefs;
			public static string CRLValues;
			public static string DESCRIPCION;
			public static string Description;
			public static string DigestAlgAndValue;
			public static string DigestMethod;
			public static string DigestValue;
			public static string DNNotValid;
			public static string EncapsulatedCRLValue;
			public static string EncapsulatedOCSPValue;
			public static string EncapsulatedTimeStamp;
			public static string EncapsulatedX509Certificate;
			public static string Encoding;
			public static string ErrorCertificateExpired;
			public static string ErrorCertificateId;
			public static string ErrorCertificateNotStored;
			public static string ErrorCertificateNull;
			public static string ErrorCertificateRevoked;
			public static string ErrorCertificateRevokedDate;
			public static string ErrorCertificateValues;
			public static string ErrorCertificateVerify;
			public static string ErrorCheckCertificate;
			public static string ErrorCheckOnLineCRL;
			public static string ErrorCheckOnLineOCSP;
			public static string ErrorCheckOnLineTimeStamp;
			public static string ErrorCheckPolicy;
			public static string ErrorCheckSignature;
			public static string ErrorCompleteCertificateRefs;
			public static string ErrorCRLOCSPNotFound;
			public static string ErrorDataObjectSignedProperies;
			public static string ErrorFileNotFound;
			public static string ErrorKeyInfo;
			public static string ErrorNextUpdateCRL;
			public static string ErrorOCSPDate;
			public static string ErrorOIDCRL;
			public static string ErrorOIDOCSP;
			public static string ErrorPrivateKey;
			public static string ErrorRevocaionsRefs;
			public static string ErrorRevocationRefsAndValues;
			public static string ErrorSignatureKeyInfo;
			public static string ErrorSignatureSignedProperties;
			public static string ErrorSignNotExist;
			public static string ErrorToSign;
			public static string ErrorUnknowElementToSign;
			public static string ErrorUnknownCertificate;
			public static string Exponent;
			public static string GET;
			public static string Id;
			public static string Identifier;
			public static string IPEnabled;
			public static string Issuer;
			public static string IssuerSerial;
			public static string IssueTime;
			public static string KeyInfo;
			public static string KeyValue;
			public static string MacAddress;
			public static string MalformedEncapsulateTimeStamp;
			public static string MalformedTimeStampSARTS;
			public static string MalformedTimeStampSTS;
			public static string Modulus;
			public static string NodeCertificateValuesNotExist;
			public static string NodeCertRefsNotExist;
			public static string NodeCompleteCertificateRefsNotExist;
			public static string NodeCompleteRevocationRefsNotExist;
			public static string NodeQualifyingPropertiesNotExist;
			public static string NodeRevocationValuesNotExist;
			public static string NodeSigAndRefsTimeStampNotExist;
			public static string NodeSignatureTimeStampNotExist;
			public static string NodeSignatureValueNotExist;
			public static string NodeUnsignedSignaturePropertiesNotExist;
			public static string NodeX509CertificateNotExist;
			public static string OBJECT_1;
			public static string OBJECT_2;
			public static string OBJECT_SIGNATURE_1;
			public static string OBJECT_SIGNEDINFO_1;
			public static string OBJECTKEYINFO;
			public static string OBJECTQUALIFYINGPROPERTIES;
			public static string OBJECTSIGNEDPROPERTIES;
			public static string OBJECTSIGNEDSIGNATUREPROPERTIES;
			public static string OCSPIdentifier;
			public static string OCSPRef;
			public static string OCSPRefs;
			public static string OCSPValues;
			public static string OIDAsURI;
			public static string POST;
			public static string ProducedAt;
			public static string Qualifier;
			public static string QualifyingProperties;
			public static string REFERENCE_1;
			public static string REFERENCE_2;
			public static string REFERENCE_3;
			public static string REFERENCE_4;
			public static string RefsOnlyTimeStamp;
			public static string ResponderID;
			public static string RevocationValues;
			public static string RSAKeyValue;
			public static string SigAndRefsTimeStamp;
			public static string Signature;
			public static string SignaturePolicyId;
			public static string SignaturePolicyIdentifier;
			public static string SignatureTimeStamp;
			public static string SignatureValue;
			public static string SignedProperties;
			public static string SignedSignatureProperties;
			public static string SignerRole;
			public static string SigningCertificate;
			public static string SigningTime;
			public static string SigPolicyHash;
			public static string SigPolicyId;
			public static string Target;
			public static string TimeStampSARTSNotValid;
			public static string TimeStampSTSNotValid;
			public static string UnsignedProperties;
			public static string UnsignedSignatureProperties;
			public static string URI_APLICATION;
			public static string URI_C14N;
			public static string URI_ETSI;
			public static string URI_ETSI_DER;
			public static string URI_ETSI_SIGNEDPROPERTIES;
			public static string URI_KEYINFO_1;
			public static string URI_OBJECT_1;
			public static string URI_POLICY;
			public static string URI_SIGNATURE_1;
			public static string URI_SIGNEDPROPERTIES_1;
			public static string URI_XMLDSIGN;
			public static string URI_XMLDSIGNSHA1;
			public static string WarningSignatureValidCertificateRevoked;
			public static string Win32_NetworkAdapterConfiguration;
			public static string X509Certificate;
			public static string X509Data;
			public static string X509IssuerName;
			public static string X509SerialNumber;
			public static string XADESREVOCATIONVALUES;
			public static string XMLNAMESPACE;
		#endregion
		
		#region Constructors
		
			public Consts ()
			
			{
			}
			
			
			static Consts ()
			
			{
				Consts.URI_ETSI = "http://uri.etsi.org/01903/v1.3.2#";
				Consts.URI_ETSI_DER = "http://uri.etsi.org/01903/v1.2.2#DER";
				Consts.URI_XMLDSIGN = "http://www.w3.org/2000/09/xmldsig#";
				Consts.URI_XMLDSIGNSHA1 = "http://www.w3.org/2000/09/xmldsig#sha1";
				Consts.URI_C14N = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
				Consts.URI_POLICY = "http://www.facturae.es/politica_de_firma_formato_facturae/politica_de_firma_formato_facturae_v3_1.pdf";


				Consts.URI_ETSI_SIGNEDPROPERTIES = "http://uri.etsi.org/01903#SignedProperties";
				Consts.Id = "Id";
				Consts.Algorithm = "Algorithm";
				Consts.Encoding = "Encoding";
				Consts.Target = "Target";
				Consts.CertificateValues = "CertificateValues";
				Consts.UnsignedSignatureProperties = "UnsignedSignatureProperties";
				Consts.EncapsulatedX509Certificate = "EncapsulatedX509Certificate";
				Consts.CompleteCertificateRefs = "CompleteCertificateRefs";
				Consts.CertRefs = "CertRefs";
				Consts.Cert = "Cert";
				Consts.CertDigest = "CertDigest";
				Consts.DigestMethod = "DigestMethod";
				Consts.DigestValue = "DigestValue";
				Consts.IssuerSerial = "IssuerSerial";
				Consts.X509IssuerName = "X509IssuerName";
				Consts.X509SerialNumber = "X509SerialNumber";
				Consts.X509Certificate = "X509Certificate";
				Consts.CompleteRevocationRefs = "CompleteRevocationRefs";
				Consts.OCSPRef = "OCSPRef";
				Consts.OCSPIdentifier = "OCSPIdentifier";
				Consts.ResponderID = "ResponderID";
				Consts.ByName = "ByName";
				Consts.ProducedAt = "ProducedAt";
				Consts.OCSPRefs = "OCSPRefs";
				Consts.CRLRef = "CRLRef";
				Consts.CRLIdentifier = "CRLIdentifier";
				Consts.Issuer = "Issuer";
				Consts.IssueTime = "IssueTime";
				Consts.CRLRefs = "CRLRefs";
				Consts.KeyInfo = "KeyInfo";
				Consts.X509Data = "X509Data";
				Consts.KeyValue = "KeyValue";
				Consts.RSAKeyValue = "RSAKeyValue";
				Consts.Modulus = "Modulus";
				Consts.Exponent = "Exponent";
				Consts.RevocationValues = "RevocationValues";
				Consts.XADESREVOCATIONVALUES = "xades:RevocationValues";
				Consts.EncapsulatedCRLValue = "EncapsulatedCRLValue";
				Consts.CRLValues = "CRLValues";
				Consts.EncapsulatedOCSPValue = "EncapsulatedOCSPValue";
				Consts.OCSPValues = "OCSPValues";
				Consts.SigAndRefsTimeStamp = "SigAndRefsTimeStamp";
				Consts.CanonicalizationMethod = "CanonicalizationMethod";
				Consts.EncapsulatedTimeStamp = "EncapsulatedTimeStamp";
				Consts.SignatureValue = "SignatureValue";
				Consts.SignatureTimeStamp = "SignatureTimeStamp";
				Consts.SignaturePolicyIdentifier = "SignaturePolicyIdentifier";
				Consts.SignaturePolicyId = "SignaturePolicyId";
				Consts.SigPolicyId = "SigPolicyId";
				Consts.Identifier = "Identifier";
				Consts.Qualifier = "Qualifier";
				Consts.Description = "Description";
				Consts.SigPolicyHash = "SigPolicyHash";
				Consts.SignerRole = "SignerRole";
				Consts.ClaimedRoles = "ClaimedRoles";
				Consts.ClaimedRole = "ClaimedRole";
				Consts.SigningCertificate = "SigningCertificate";
				Consts.SigningTime = "SigningTime";
				Consts.QualifyingProperties = "QualifyingProperties";
				Consts.SignedProperties = "SignedProperties";
				Consts.SignedSignatureProperties = "SignedSignatureProperties";
				Consts.UnsignedProperties = "UnsignedProperties";
				Consts.Signature = "Signature";
				Consts.RefsOnlyTimeStamp = "RefsOnlyTimeStamp";
				Consts.CompleteCertificationRefs = "CompleteCertificationRefs";
				Consts.DigestAlgAndValue = "DigestAlgAndValue";
				Consts.CertificateIsNotTrust = "CertificateIsNotTrust";
				Consts.DNNotValid = "DNNotValid";
				Consts.ErrorCRLOCSPNotFound = "ErrorCRLOCSPNotFound";
				Consts.ErrorOIDCRL = "ErrorOIDCRL";
				Consts.ErrorOIDOCSP = "ErrorOIDOCSP";
				Consts.ErrorSignatureSignedProperties = "ErrorSignatureSignedProperties";
				Consts.ErrorSignatureKeyInfo = "ErrorSignatureKeyInfo";
				Consts.ErrorUnknowElementToSign = "ErrorUnknowElementToSign";
				Consts.ErrorCertificateNull = "ErrorCertificateNull";
				Consts.ErrorKeyInfo = "ErrorKeyInfo";
				Consts.ErrorDataObjectSignedProperies = "ErrorDataObjectSignedProperies";
				Consts.ErrorPrivateKey = "ErrorPrivateKey";
				Consts.ErrorToSign = "ErrorToSign";
				Consts.ErrorCheckPolicy = "ErrorCheckPolicy";
				Consts.ErrorCheckCertificate = "ErrorCheckCertificate";
				Consts.ErrorCheckOnLineTimeStamp = "ErrorCheckOnLineTimeStamp";
				Consts.ErrorCertificateNotStored = "ErrorCertificateNotStored";
				Consts.ErrorCertificateVerify = "ErrorCertificateVerify";
				Consts.ErrorCertificateExpired = "ErrorCertificateExpired";
				Consts.ErrorUnknownCertificate = "ErrorUnknownCertificate";
				Consts.ErrorCertificateRevoked = "ErrorCertificateRevoked";
				Consts.ErrorCertificateRevokedDate = "ErrorCertificateRevokedDate";
				Consts.ErrorNextUpdateCRL = "ErrorNextUpdateCRL";
				Consts.ErrorCheckOnLineCRL = "ErrorCheckOnLineCRL";
				Consts.ErrorCheckOnLineOCSP = "ErrorCheckOnLineOCSP";
				Consts.ErrorCertificateId = "ErrorCertificateId";
				Consts.WarningSignatureValidCertificateRevoked = "WarningSignatureValidCertificateRevoked";
				Consts.ErrorSignNotExist = "ErrorSignNotExist";
				Consts.ErrorCheckSignature = "ErrorCheckSignature";
				Consts.MalformedTimeStampSTS = "MalformedTimeStampSTS";
				Consts.TimeStampSTSNotValid = "TimeStampSTSNotValid";
				Consts.ErrorCompleteCertificateRefs = "ErrorCompleteCertificateRefs";
				Consts.MalformedTimeStampSARTS = "MalformedTimeStampSARTS";
				Consts.TimeStampSARTSNotValid = "TimeStampSARTSNotValid";
				Consts.CertificateNotValid = "CertificateNotValid";
				Consts.ErrorCertificateValues = "ErrorCertificateValues";
				Consts.ErrorRevocaionsRefs = "ErrorRevocaionsRefs";
				Consts.ErrorOCSPDate = "ErrorOCSPDate";
				Consts.MalformedEncapsulateTimeStamp = "MalformedEncapsulateTimeStamp";
				Consts.ErrorRevocationRefsAndValues = "ErrorRevocationRefsAndValues";
				Consts.ErrorFileNotFound = "ErrorFileNotFound";
				Consts.NodeCertificateValuesNotExist = "NodeCertificateValuesNotExist";
				Consts.NodeX509CertificateNotExist = "NodeX509CertificateNotExist";
				Consts.NodeUnsignedSignaturePropertiesNotExist = "NodeUnsignedSignaturePropertiesNotExist";
				Consts.NodeCertRefsNotExist = "NodeCertRefsNotExist";
				Consts.NodeCompleteRevocationRefsNotExist = "NodeCompleteRevocationRefsNotExist";
				Consts.NodeRevocationValuesNotExist = "NodeRevocationValuesNotExist";
				Consts.NodeSignatureValueNotExist = "NodeSignatureValueNotExist";
				Consts.NodeQualifyingPropertiesNotExist = "NodeQualifyingPropertiesNotExist";
				Consts.NodeSignatureTimeStampNotExist = "NodeSignatureTimeStampNotExist";
				Consts.NodeCompleteCertificateRefsNotExist = "NodeCompleteCertificateRefsNotExist";
				Consts.NodeSigAndRefsTimeStampNotExist = "NodeSigAndRefsTimeStampNotExist";
				Consts.APLICACIONOCSPREQUEST = "application/ocsp-request";
				Consts.APLICACIONTIMESTAMPQUERY = "application/timestamp-query";
				Consts.POST = "POST";
				Consts.GET = "GET";
				Consts.XMLNAMESPACE = "xmlns";
				Consts.OBJECTQUALIFYINGPROPERTIES = "Signature_1_QualifyingProperties_1";
				Consts.OBJECTSIGNEDPROPERTIES = "Signature_1_SignedProperties_1";
				Consts.OBJECTSIGNEDSIGNATUREPROPERTIES = "Signature_1_SignedSignatureProperties_1";
				Consts.OBJECTKEYINFO = "Signature_1_KeyInfo_1";
				Consts.OBJECT_1 = "Signature_1_Object_1";
				Consts.OBJECT_2 = "Signature_1_Object_2";
				Consts.REFERENCE_1 = "Signature_1_Reference_1";
				Consts.REFERENCE_2 = "Signature_1_Reference_2";
				Consts.REFERENCE_3 = "Signature_1_Reference_3";
				Consts.REFERENCE_4 = "Signature_1_Reference_4";
				Consts.URI_SIGNEDPROPERTIES_1 = "#Signature_1_SignedProperties_1";
				Consts.URI_KEYINFO_1 = "#Signature_1_KeyInfo_1";
				Consts.URI_SIGNATURE_1 = "#Signature_1";
				Consts.URI_OBJECT_1 = "#Signature_1_Object_1";
				Consts.OBJECT_SIGNATURE_1 = "Signature_1";
				Consts.OBJECT_SIGNEDINFO_1 = "Signature_1_SignedInfo_1";
                Consts.DESCRIPCION = "Especificaciones de firma electronica e-Factura v1.3";
				Consts.OIDAsURI = "OIDAsURI";
				Consts.Basic = "Basic";
				Consts.IPEnabled = "IPEnabled";
				Consts.MacAddress = "MacAddress";
				Consts.Win32_NetworkAdapterConfiguration = "Win32_NetworkAdapterConfiguration";
				Consts.CertificadoOK = "CertificadoOK";
			}
			
		#endregion
	}
	
}

