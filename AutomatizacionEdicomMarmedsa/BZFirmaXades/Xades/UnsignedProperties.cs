namespace Com.Bizlayer.Pki.Xades

{
		
		#region Namespace Import Declarations
		
			using System;
			using System.Xml;
			
		#endregion
		
	internal class UnsignedProperties
	
	{
		
		#region Constructors
		
			public UnsignedProperties (XmlDocument xmlDoc, string urlTSA)
			
			{
				this.addUnsignedProperties (xmlDoc, urlTSA);
			}
			
		#endregion
		
		#region Methods
		
			private void addUnsignedProperties (XmlDocument xmlDoc, string urlTSA)
			
			{
				string string1 = Consts.URI_ETSI;
				XmlElement xmlElement1 = xmlDoc.CreateElement (Consts.UnsignedProperties, string1);
				XmlElement xmlElement2 = xmlDoc.CreateElement (Consts.UnsignedSignatureProperties, string1);
				XmlNode xmlNode1 = xmlElement1.AppendChild (((XmlNode) xmlElement2));
				XmlElement xmlElement3 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.QualifyingProperties).Item (0));
				if (xmlElement3 == null)
				{
					xmlElement3 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("xades:" + Consts.QualifyingProperties)).Item (0));
					if (xmlElement3 == null)
					{
						throw new Exception (Consts.NodeQualifyingPropertiesNotExist);
					}
				}
				XmlNode xmlNode2 = xmlElement3.AppendChild (((XmlNode) xmlElement1));
			}
			
		#endregion
	}
	
}

