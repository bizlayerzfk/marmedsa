

namespace Com.Bizlayer.Pki.Xades

{
		
		#region Namespace Import Declarations
		
			using System.Security.Cryptography.Xml;
			using System;
			using System.Xml;
			
		#endregion
		
	internal class MySignXml : SignedXml
	
	{
		
		#region Fields
			private XmlDocument pDoc2;
		#endregion
		
		#region Constructors
		
			public MySignXml (XmlDocument doc)
				 : base (doc)
			
			{
			}
			
		#endregion
		
		#region Properties
		
			public XmlDocument doc2
			
			{
				get
				{
					return this.pDoc2;
				}
				set
				{
					this.pDoc2 = value;
				}
			}
			
		#endregion
		
		#region Methods
		
			public override XmlElement GetIdElement (XmlDocument doc, string id)
			
			{
				string string1;
				XmlDocument xmlDocument1;
				XmlElement xmlElement1;
				int i1;
				XmlAttribute xmlAttribute1;
				XmlAttribute xmlAttribute2;
				XmlElement xmlElement2;
				XmlElement xmlElement3;
				XmlElement xmlElement4;
				XmlElement xmlElement5;
				XmlElement xmlElement6;
				XmlElement xmlElement7;
				Exception exception1;
				try
				{
					if (id.Equals (Consts.OBJECTSIGNEDPROPERTIES))
					{
						try
						{
							return base.GetIdElement (this.doc2, id);
						}
						catch (Exception)
						{
							throw new Exception (Consts.ErrorSignatureSignedProperties);
						}
					}
					else if (id.Equals (Consts.OBJECTKEYINFO))
					{
						try
						{
							string1 = Consts.URI_XMLDSIGN;
							xmlDocument1 = new XmlDocument ();
							xmlElement1 = xmlDocument1.CreateElement (Consts.KeyInfo, string1);
							if (doc != null)
							{
								for (i1 = 0; (i1 < doc.DocumentElement.Attributes.Count); i1++)
								{
									if ((! doc.DocumentElement.Attributes[i1].Name.Equals ("xmlns")) && doc.DocumentElement.Attributes[i1].Name.Contains ("xmlns"))
									{
										xmlAttribute1 = xmlDocument1.CreateAttribute (((doc.DocumentElement.Attributes[i1].Name != null) ? doc.DocumentElement.Attributes[i1].Name : ""));
										xmlAttribute1.Value = doc.DocumentElement.Attributes[i1].Value;
										XmlAttribute xmlAttribute3 = xmlElement1.Attributes.Append (xmlAttribute1);
									}
								}
							}
							xmlAttribute2 = xmlDocument1.CreateAttribute (Consts.Id);
							xmlAttribute2.Value = Consts.OBJECTKEYINFO;
							XmlAttribute xmlAttribute4 = xmlElement1.Attributes.Append (xmlAttribute2);
							XmlNode xmlNode1 = xmlDocument1.AppendChild (((XmlNode) xmlElement1));
							xmlElement2 = xmlDocument1.CreateElement (Consts.X509Data, string1);
							xmlElement3 = xmlDocument1.CreateElement (Consts.X509Certificate, string1);
							xmlElement3.InnerText = base.KeyInfo.GetXml ().GetElementsByTagName (Consts.X509Certificate).Item (0).InnerText;
							XmlNode xmlNode2 = xmlElement2.AppendChild (((XmlNode) xmlElement3));
							XmlNode xmlNode3 = xmlDocument1.DocumentElement.AppendChild (((XmlNode) xmlElement2));
							xmlElement4 = xmlDocument1.CreateElement (Consts.KeyValue, string1);
							xmlElement5 = xmlDocument1.CreateElement (Consts.RSAKeyValue, string1);
							xmlElement6 = xmlDocument1.CreateElement (Consts.Modulus, string1);
							xmlElement7 = xmlDocument1.CreateElement (Consts.Exponent, string1);
							xmlElement6.InnerText = base.KeyInfo.GetXml ().GetElementsByTagName (Consts.Modulus).Item (0).InnerText;
							xmlElement7.InnerText = base.KeyInfo.GetXml ().GetElementsByTagName (Consts.Exponent).Item (0).InnerText;
							XmlNode xmlNode4 = xmlElement5.AppendChild (((XmlNode) xmlElement6));
							XmlNode xmlNode5 = xmlElement5.AppendChild (((XmlNode) xmlElement7));
							XmlNode xmlNode6 = xmlElement4.AppendChild (((XmlNode) xmlElement5));
							XmlNode xmlNode7 = xmlDocument1.DocumentElement.AppendChild (((XmlNode) xmlElement4));
							return xmlDocument1.DocumentElement;
						}
						catch (Exception)
						{
							throw new Exception (Consts.ErrorSignatureKeyInfo);
						}
					}
					else
					{
						try
						{
							return base.GetIdElement (doc, id);
						}
						catch (Exception)
						{
							throw new Exception (Consts.ErrorUnknowElementToSign);
						}
					}
				}
				catch (Exception exception5)
				{
					exception1 = exception5;
					throw exception1;
				}
			}
			
		#endregion
	}
	
}

