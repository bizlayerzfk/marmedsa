namespace Com.Bizlayer.Pki.Xades

{
		
		#region Namespace Import Declarations
		
			using Org.BouncyCastle.Asn1;
			using Org.BouncyCastle.Asn1.X509;
			using Org.BouncyCastle.Crypto;
			using Org.BouncyCastle.Math;
			using Org.BouncyCastle.Ocsp;
			using Org.BouncyCastle.Tsp;
			using Org.BouncyCastle.Utilities.Date;
			using Org.BouncyCastle.X509;
			using System.Collections.Generic;
			using System.Collections;
			using System;
			using System.IO;
			using System.Linq;
			using System.Management;
			using System.Net;
			using System.Security.Cryptography;
			using System.Security.Cryptography.X509Certificates;
			using System.Text;
			using System.Xml;
			using X509Extension = System.Security.Cryptography.X509Certificates.X509Extension;
			using X509Certificate = System.Security.Cryptography.X509Certificates.X509Certificate;			
		#endregion
		
	internal class Utils
	
	{
		
		#region Constructors
		
			public Utils ()
			
			{
			}
			
		#endregion
		
		#region Methods
		

			
			public static X509Certificate2 BuscarCertificado (X509Certificate certificado)
			
			{
				X509Store x509Store1;
				X509Certificate2Collection x509Certificate2Collection1;
				X509Certificate2Enumerator x509Certificate2Enumerator1;
				X509Certificate2 x509Certificate2_1;
				X509Certificate2 x509Certificate2_2;
				bool b1 = false;
				try
				{
					x509Store1 = new X509Store (StoreName.CertificateAuthority);
					x509Store1.Open (OpenFlags.ReadOnly);
					x509Certificate2Collection1 = x509Store1.Certificates;
					x509Certificate2Enumerator1 = x509Certificate2Collection1.GetEnumerator ();
					x509Certificate2_1 = ((X509Certificate2) null);
					while (x509Certificate2Enumerator1.MoveNext () && (! b1))
					{
						if (x509Certificate2Enumerator1.Current.Subject.Equals (certificado.Issuer))
						{
							x509Certificate2_1 = x509Certificate2Enumerator1.Current;
							b1 = true;
						}
					}
					if (! b1)
					{
						x509Store1 = new X509Store (StoreName.Root);
						x509Store1.Open (OpenFlags.ReadOnly);
						x509Certificate2Collection1 = x509Store1.Certificates;
						x509Certificate2Enumerator1 = x509Certificate2Collection1.GetEnumerator ();
						while (x509Certificate2Enumerator1.MoveNext () && (! b1))
						{
							if (x509Certificate2Enumerator1.Current.Subject.Equals (certificado.Issuer))
							{
								x509Certificate2_1 = x509Certificate2Enumerator1.Current;
								b1 = true;
							}
						}
					}
					x509Store1.Close ();
					x509Certificate2_2 = x509Certificate2_1;
				}
				catch (Exception)
				{
					throw new Exception (Consts.ErrorCheckCertificate);
				}
				return x509Certificate2_2;
			}
			
			public static X509Certificate2 BuscarCertificadoPorDN (string dn)
			
			{
				X509Store x509Store1;
				X509Certificate2Collection x509Certificate2Collection1;
				X509Certificate2Enumerator x509Certificate2Enumerator1;
				X509Certificate2 x509Certificate2_1;
				X509Certificate2 x509Certificate2_2;
				bool b1 = false;
				try
				{
					x509Store1 = new X509Store (StoreName.My);
					x509Store1.Open (OpenFlags.ReadOnly);
					x509Certificate2Collection1 = x509Store1.Certificates;
					x509Certificate2Enumerator1 = x509Certificate2Collection1.GetEnumerator ();
					x509Certificate2_1 = ((X509Certificate2) null);
					while (x509Certificate2Enumerator1.MoveNext () && (! b1))
					{
						if (x509Certificate2Enumerator1.Current.Subject.Equals (dn))
						{
							x509Certificate2_1 = x509Certificate2Enumerator1.Current;
							b1 = true;
						}
					}
					x509Store1.Close ();
					x509Certificate2_2 = x509Certificate2_1;
				}
				catch (Exception)
				{
					throw new Exception (Consts.ErrorCheckCertificate);
				}
				return x509Certificate2_2;
			}
			


			public static bool comprobarFecha (X509Certificate certificado)
			
			{
				DateTime dateTime1 = Convert.ToDateTime (certificado.GetEffectiveDateString ());
				DateTime dateTime2 = Convert.ToDateTime (certificado.GetExpirationDateString ());
				DateTime dateTime3 = DateTime.Now;
				if (dateTime3 <= dateTime1)
				{
					return false;
				}
				else if (dateTime3 >= dateTime2)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			

			
			public static X509Certificate2Collection GetAll ()
			
			{
				X509Store x509Store1;
				X509Certificate2Collection x509Certificate2Collection1;
				X509Certificate2Collection x509Certificate2Collection2;
				int i1;
				X509Extension x509Extension1;
				X509Certificate2Collection x509Certificate2Collection3;
				X509ExtensionEnumerator x509ExtensionEnumerator1;
				try
				{
					x509Store1 = new X509Store (StoreName.My, StoreLocation.CurrentUser);
					x509Store1.Open (OpenFlags.OpenExistingOnly);
					x509Certificate2Collection1 = x509Store1.Certificates;
					x509Certificate2Collection2 = new X509Certificate2Collection ();
					for (i1 = 0; (i1 < x509Certificate2Collection1.Count); i1++)
					{
						x509ExtensionEnumerator1 = x509Certificate2Collection1[i1].Extensions.GetEnumerator ();
						while (x509ExtensionEnumerator1.MoveNext ())
						{
							x509Extension1 = x509ExtensionEnumerator1.Current;
                            // JJR Modificación para que muestre todos los certificados
                            
                            //if (x509Extension1.Oid.FriendlyName == "Uso de la clave")
                            //{
                            //    x509KeyUsageExtension1 = ((X509KeyUsageExtension) x509Extension1);
                            //    type1 = x509KeyUsageExtension1.KeyUsages.GetType ();
                            //    if (! Enum.IsDefined (type1, X509KeyUsageFlags.DigitalSignature))
                            //    {
                            //        continue;
                            //    }
                            //    int i2 = x509Certificate2Collection2.Add (x509Certificate2Collection1[i1]);
                            //}
						}
                        x509Certificate2Collection2.Add(x509Certificate2Collection1[i1]);
                    }

					x509Store1.Close ();
					x509Certificate2Collection3 = x509Certificate2Collection2;
				}
				catch (CryptographicException)
				{
					x509Certificate2Collection3 = ((X509Certificate2Collection) null);
				}
				return x509Certificate2Collection3;
			}
			
			public static X509Certificate2 getCertificado (XmlDocument xmlDoc)
			
			{
				byte[] byteArray1;
				Exception exception1;
				XmlElement xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.X509Certificate).Item (0));
				X509Certificate2 x509Certificate2_1 = ((X509Certificate2) null);
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("ds:" + Consts.X509Certificate)).Item (0));
				}
				if (xmlElement1 == null)
				{
					throw new Exception (Consts.NodeX509CertificateNotExist);
				}
				else
				{
					try
					{
						byteArray1 = Convert.FromBase64String (xmlElement1.FirstChild.Value);
						X509CertificateParser x509CertificateParser1 = new X509CertificateParser ();
						x509Certificate2_1 = new X509Certificate2 (byteArray1);
					}
					catch (Exception exception2)
					{
						exception1 = exception2;
						throw exception1;
					}
					return x509Certificate2_1;
				}
			}
			
			public static X509Certificate2 GetCertificate ()
			
			{
				X509Certificate2 x509Certificate2_1 = ((X509Certificate2) null);
				X509Certificate2Collection x509Certificate2Collection1 = X509Certificate2UI.SelectFromCollection (Utils.GetAll (), "Certificados", "Seleccione el certificado para firmar", X509SelectionFlag.SingleSelection);
				if (x509Certificate2Collection1.Count <= 0)
				{
					return x509Certificate2_1;
				}
				X509Certificate2Enumerator x509Certificate2Enumerator1 = x509Certificate2Collection1.GetEnumerator ();
				bool b1 = x509Certificate2Enumerator1.MoveNext ();
				return x509Certificate2Enumerator1.Current;
			}
			
			public static List<X509Certificate2> GetCertificatesList (XmlDocument xmlDoc)
			
			{
				List<X509Certificate2> list1 = new List<X509Certificate2> ();
				XmlElement xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.X509Certificate).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("ds:" + Consts.X509Certificate)).Item (0));
					if (xmlElement1 == null)
					{
						throw new Exception (Consts.NodeX509CertificateNotExist);
					}
				}
				byte[] byteArray1 = Convert.FromBase64String (xmlElement1.FirstChild.Value);
				X509Certificate2 x509Certificate2_1 = new X509Certificate2 (byteArray1);
				while (! x509Certificate2_1.Issuer.Equals (x509Certificate2_1.Subject))
				{
					x509Certificate2_1 = Utils.BuscarCertificado (((X509Certificate) x509Certificate2_1));
					if (x509Certificate2_1 == null)
					{
						throw new Exception (Consts.CertificateIsNotTrust);
					}
					else
					{
						list1.Add (x509Certificate2_1);
					}
				}
				return list1;
			}
			
			public static void getOCSPStatus (X509Certificate2 issuer, X509Certificate2 cert, string serviceAddr, XmlDocument xmlDoc)
			
			{
				OcspReqGenerator ocspReqGenerator1;
				OcspReq ocspReq1;
				byte[] byteArray1;
				Uri uri1;
				HttpWebRequest httpWebRequest1;
				Stream stream1;
				WebResponse webResponse1;
				Stream stream2;
				MemoryStream memoryStream1;
				byte[] byteArray2;
				int i1;
				byte[] byteArray3;
				BasicOcspResp basicOcspResp1;
				SingleResp singleResp1;
				CertificateID certificateID2;
				object object1;
				DateTime dateTime1;
				OcspResp ocspResp1 = ((OcspResp) null);
				Org.BouncyCastle.X509.X509Certificate x509Certificate1 = Utils.ReadCertificate (issuer);
				Org.BouncyCastle.X509.X509Certificate x509Certificate2 = Utils.ReadCertificate (cert);
				CertificateID certificateID1 = new CertificateID ("1.3.14.3.2.26", x509Certificate1, x509Certificate2.SerialNumber);
				try
				{
					ocspReqGenerator1 = new OcspReqGenerator ();
					ocspReqGenerator1.AddRequest (certificateID1);
					ocspReq1 = ocspReqGenerator1.Generate ();
					byteArray1 = ocspReq1.GetEncoded ();
					uri1 = new Uri (serviceAddr);
					httpWebRequest1 = ((HttpWebRequest) WebRequest.Create (uri1));
					httpWebRequest1.ContentType = Consts.APLICACIONOCSPREQUEST;
					httpWebRequest1.Method = Consts.POST;
					stream1 = httpWebRequest1.GetRequestStream ();
					stream1.Write (byteArray1, 0, byteArray1.Length);
					stream1.Close ();
					webResponse1 = httpWebRequest1.GetResponse ();
					stream2 = webResponse1.GetResponseStream ();
					memoryStream1 = new MemoryStream ();
					byteArray2 = new byte[1024];
					for (i1 = stream2.Read (byteArray2, 0, byteArray2.Length); (i1 > 0); i1 = stream2.Read (byteArray2, 0, byteArray2.Length))
					{
						memoryStream1.Write (byteArray2, 0, i1);
					}
					byteArray3 = memoryStream1.ToArray ();
					ocspResp1 = new OcspResp (byteArray3);
				}
				catch (Exception)
				{
					throw new Exception (Consts.ErrorCheckOnLineOCSP);
				}
				switch (ocspResp1.Status)
				{
					case 0:
					
					{
							basicOcspResp1 = ((BasicOcspResp) ocspResp1.GetResponseObject ());
							singleResp1 = basicOcspResp1.Responses[0];
							certificateID2 = singleResp1.GetCertID ();
							object1 = singleResp1.GetCertStatus ();
							if (object1 != null)
							{
								if (! (object1 is RevokedStatus))
								{
									if (! (object1 is UnknownStatus))
									{
										return;
									}
									else
									{
										throw new Exception (Consts.ErrorUnknownCertificate);
									}
								}
								dateTime1 = Utils.ObtenerFechaFirma (xmlDoc);
								if (dateTime1 == DateTime.MinValue)
								{
									throw new Exception (Consts.ErrorCertificateRevoked);
								}
								else if (((RevokedStatus) object1).RevocationTime > dateTime1)
								{
									throw new Exception (Consts.WarningSignatureValidCertificateRevoked);
								}
								else
								{
									throw new Exception (Consts.ErrorCertificateRevoked);
								}
							}
							if (certificateID1.Equals (certificateID2))
							{
								return;
							}
							else
							{
								throw new Exception (Consts.ErrorCertificateId);
							}
					}
					case 1:
					
					{
							throw new Exception (Consts.ErrorCheckOnLineOCSP);
					}
					case 2:
					
					{
							throw new Exception (Consts.ErrorCheckOnLineOCSP);
					}
					case 3:
					
					{
							throw new Exception (Consts.ErrorCheckOnLineOCSP);
					}
					case 5:
					
					{
							throw new Exception (Consts.ErrorCheckOnLineOCSP);
					}
					case 6:
					
					{
							throw new Exception (Consts.ErrorCheckOnLineOCSP);
					}
				}
				throw new Exception (Consts.ErrorCheckOnLineOCSP);
			}
			
			public static byte[] getSHA1 (byte[] signedValue)
			
			{
				SHA1 sHA1_1 = SHA1.Create ();
				byte[] byteArray1 = sHA1_1.ComputeHash (signedValue);
				return sHA1_1.Hash;
			}
			
			public static TimeStampResponse getTSResponse (byte[] hash, string user, string password, string serverTimestamp)
			
			{
				Uri uri1;
				HttpWebRequest httpWebRequest1;
				CredentialCache credentialCache1;
				Stream stream1;
				TimeStampRequestGenerator timeStampRequestGenerator1;
				BigInteger bigInteger1;
				TimeStampRequest timeStampRequest1;
				byte[] byteArray2;
				WebResponse webResponse1;
				Stream stream2;
				MemoryStream memoryStream1;
				byte[] byteArray3;
				int i1;
				byte[] byteArray1 = ((byte[]) null);
				TimeStampResponse timeStampResponse1 = ((TimeStampResponse) null);
				try
				{
					uri1 = new Uri (serverTimestamp);
					httpWebRequest1 = ((HttpWebRequest) WebRequest.Create (uri1));
					httpWebRequest1.ContentType = Consts.APLICACIONTIMESTAMPQUERY;
					httpWebRequest1.Method = Consts.POST;
					credentialCache1 = new CredentialCache ();
					credentialCache1.Add (uri1, "Basic", new NetworkCredential (user, password));
					httpWebRequest1.Credentials = ((ICredentials) credentialCache1);
					stream1 = httpWebRequest1.GetRequestStream ();
					timeStampRequestGenerator1 = new TimeStampRequestGenerator ();
					timeStampRequestGenerator1.SetCertReq (true);
					timeStampRequestGenerator1.SetReqPolicy ("0.4.0.2023.1.1");
					bigInteger1 = BigInteger.ValueOf (((long) DateTime.Now.Millisecond));
					timeStampRequest1 = timeStampRequestGenerator1.Generate (X509ObjectIdentifiers.IdSha1.Id, hash, bigInteger1);
					byteArray2 = timeStampRequest1.GetEncoded ();
					stream1.Write (byteArray2, 0, byteArray2.Length);
					stream1.Close ();
					webResponse1 = httpWebRequest1.GetResponse ();
					stream2 = webResponse1.GetResponseStream ();
					memoryStream1 = new MemoryStream ();
					byteArray3 = new byte[1024];
					for (i1 = stream2.Read (byteArray3, 0, byteArray3.Length); (i1 > 0); i1 = stream2.Read (byteArray3, 0, byteArray3.Length))
					{
						memoryStream1.Write (byteArray3, 0, i1);
					}
					byteArray1 = memoryStream1.ToArray ();
					timeStampResponse1 = new TimeStampResponse (byteArray1);
					timeStampResponse1.Validate (timeStampRequest1);
				}
				catch (Exception)
				{
					throw new Exception (Consts.ErrorCheckOnLineTimeStamp);
				}
				return timeStampResponse1;
			}
			
			public static string LeerMACAddress ()
			
			{
				ManagementObject managementObject1;
				ManagementClass managementClass1 = new ManagementClass (Consts.Win32_NetworkAdapterConfiguration);
				ManagementObjectCollection managementObjectCollection1 = managementClass1.GetInstances ();
				string string1 = string.Empty;
				using (ManagementObjectCollection.ManagementObjectEnumerator managementObjectEnumerator1 = managementObjectCollection1.GetEnumerator ())
				{
					while (managementObjectEnumerator1.MoveNext ())
					{
						managementObject1 = ((ManagementObject) managementObjectEnumerator1.Current);
						if (string.IsNullOrEmpty (string1))
						{
							if ((bool) managementObject1[Consts.IPEnabled])
							{
								string1 = managementObject1[Consts.MacAddress].ToString ();
							}
							managementObject1.Dispose ();
							continue;
						}
					}
				}
				return string1;
			}
			
			public static DateTime ObtenerFechaFirma (XmlDocument xmlDoc)
			
			{
				XmlElement xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (Consts.SigningTime).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDoc.DocumentElement.GetElementsByTagName (("xades:" + Consts.SigningTime)).Item (0));
				}
				if (xmlElement1 == null)
				{
					return DateTime.MinValue;
				}
				else
				{
					return DateTime.Parse (xmlElement1.InnerText);
				}
			}
			
			public static string obtenerIssuerName (string name)
			
			{
				int i2;
				string string2;
				char[] charArray1 = new char[] { '=' };
				string[] stringArray1 = name.Split (charArray1);
				List<String> list1 = new List<String> ();
				string string1 = "";
				for (int i1 = 0; true; i1++)
				{
					if (i1 >= (stringArray1.Length - 1))
					{
						i2 = (list1.Count - 1);
						string2 = "";
						while (i2 > 0)
						{
							string2 = (string2 + (Enumerable.ElementAt<String> (list1, i2) + ","));
							i2--;
						}
						return (string2 + Enumerable.ElementAt<String> (list1, 0));
					}
					else
					{
						if (i1 == 0)
						{
							string1 = (stringArray1[i1] + "0x03d" + stringArray1[(i1 + 1)].Substring (0, stringArray1[(i1 + 1)].IndexOf ('\u002C')));
						}
						else if (i1 != (stringArray1.Length - 2))
						{
							string1 = (stringArray1[i1].Substring (((int) (stringArray1[i1].LastIndexOf ('\u002C') + 1))) + "0x03d" + stringArray1[(i1 + 1)].Substring (0, stringArray1[(i1 + 1)].LastIndexOf ('\u002C')));
						}
						else
						{
							string1 = (stringArray1[i1].Substring (((int) (stringArray1[i1].LastIndexOf ('\u002C') + 1))) + "0x03d" + stringArray1[(i1 + 1)]);
						}
						list1.Add (string1);
					}
				}
			}
			
			public static Org.BouncyCastle.X509.X509Certificate ReadCertificate (X509Certificate2 certMS)
			
			{
				X509CertificateParser x509CertificateParser1 = new X509CertificateParser ();
				return x509CertificateParser1.ReadCertificate (certMS.RawData);
			}
			
			public static byte[] ReadFully (Stream stream, int initialLength)
			
			{
				byte[] byteArray3;
				byte[] byteArray4;
				if (initialLength < 1)
				{
					initialLength = 51200;
				}
				byte[] byteArray1 = new byte[initialLength];
				byte[] byteArray2 = new byte[51200];
				int i1 = 0;
				int i2 = stream.Read (byteArray2, 0, 51200);
				while (true)
				{
					if (i2 <= 0)
					{
						byteArray4 = new byte[i1];
						Array.Copy (((Array) byteArray1), ((Array) byteArray4), i1);
						return byteArray4;
					}
					if ((i1 + i2) < byteArray1.Length)
					{
						Array.Copy (((Array) byteArray2), 0, ((Array) byteArray1), i1, i2);
						i1 += i2;
					}
					else
					{
						byteArray3 = new byte[(byteArray1.Length * 2)];
						Array.Copy (((Array) byteArray1), ((Array) byteArray3), byteArray1.Length);
						byteArray1 = byteArray3;
						Array.Copy (((Array) byteArray2), 0, ((Array) byteArray1), i1, i2);
						i1 += i2;
					}
					i2 = stream.Read (byteArray2, 0, 51200);
				}
			}
			
			public static string StructureValidation (XmlDocument xmlDoc)
			
			{
				string string1;
				try
				{
					Utils.ValidationXLStructure (xmlDoc);
					string1 = "XADES-XL";
				}
				catch (Exception)
				{
					try
					{
						Utils.ValidationXStructure (xmlDoc);
						return "XADES-X";
					}
					catch (Exception)
					{
						try
						{
							Utils.ValidationCStructure (xmlDoc);
							return "XADES-C";
						}
						catch (Exception)
						{
							try
							{
								Utils.ValidationTStructure (xmlDoc);
								return "XADES-T";
							}
							catch (Exception)
							{
								return "XADES-EPES";
							}
						}
					}
				}
				return string1;
			}
			

			
			private static void ValidationCStructure (XmlDocument xmlDocument)
			
			{
				Utils.ValidationTStructure (xmlDocument);
				XmlElement xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.CompleteCertificateRefs).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.CompleteCertificateRefs)).Item (0));
				}
				if (xmlElement1 == null)
				{
					throw new Exception (Consts.NodeCompleteCertificateRefsNotExist);
				}
				else
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.CompleteRevocationRefs).Item (0));
					if (xmlElement1 == null)
					{
						xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.CompleteRevocationRefs)).Item (0));
					}
					if (xmlElement1 == null)
					{
						throw new Exception (Consts.NodeCompleteRevocationRefsNotExist);
					}
				}
			}
			
			private static void ValidationTStructure (XmlDocument xmlDocument)
			
			{
				XmlElement xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.SignatureTimeStamp).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.SignatureTimeStamp)).Item (0));
				}
				if (xmlElement1 == null)
				{
					throw new Exception (Consts.NodeSignatureTimeStampNotExist);
				}
			}
			
			private static void ValidationXLStructure (XmlDocument xmlDocument)
			
			{
				Utils.ValidationXStructure (xmlDocument);
				XmlElement xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.CertificateValues).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.CertificateValues)).Item (0));
				}
				if (xmlElement1 == null)
				{
					throw new Exception (Consts.NodeCertificateValuesNotExist);
				}
				else
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.RevocationValues).Item (0));
					if (xmlElement1 == null)
					{
						xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.RevocationValues)).Item (0));
					}
					if (xmlElement1 == null)
					{
						throw new Exception (Consts.NodeRevocationValuesNotExist);
					}
				}
			}
			
			private static void ValidationXStructure (XmlDocument xmlDocument)
			
			{
				Utils.ValidationCStructure (xmlDocument);
				XmlElement xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.SigAndRefsTimeStamp).Item (0));
				if (xmlElement1 == null)
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.SigAndRefsTimeStamp)).Item (0));
				}
				if (xmlElement1 != null)
				{
				}
				else
				{
					xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (Consts.RefsOnlyTimeStamp).Item (0));
					if (xmlElement1 == null)
					{
						xmlElement1 = ((XmlElement) xmlDocument.DocumentElement.GetElementsByTagName (("xades:" + Consts.RefsOnlyTimeStamp)).Item (0));
					}
					if (xmlElement1 == null)
					{
						throw new Exception (Consts.NodeSigAndRefsTimeStampNotExist);
					}
				}
			}
			
		#endregion
	}
	
}

