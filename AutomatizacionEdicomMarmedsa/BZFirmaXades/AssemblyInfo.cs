//




//
using System.Diagnostics;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly:  CompilationRelaxationsAttribute(0008)]
[assembly:  AssemblyConfigurationAttribute("")]
[assembly:  AssemblyCompanyAttribute("")]
[assembly:  DebuggableAttribute(System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly:  RuntimeCompatibilityAttribute(WrapNonExceptionThrows = true)]
[assembly:  AssemblyTitleAttribute("Xades")]
[assembly:  AssemblyDescriptionAttribute("")]
[assembly:  AssemblyTrademarkAttribute("")]
[assembly:  AssemblyFileVersionAttribute("1.0.0.0")]
[assembly:  AssemblyCopyrightAttribute("Accenture")]
[assembly:  AssemblyProductAttribute("XadesSigner")]
[assembly:  ComVisibleAttribute(false)]
[assembly: System.Reflection.AssemblyVersion("1.0.0.0")]
