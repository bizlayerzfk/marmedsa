﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BZServicesWeb
{
    public class InitServicesWeb
    {
        #region Variables y propiedades
        private string _URLServiceWeb = "https://pre.bizlayer.com/portal/ws/servicesweb.asmx";

        public string URLServiceWeb
        {
            set {_URLServiceWeb=value;}
            get { return _URLServiceWeb; }
            
        }

        private Int32 _TimeOut = 1800000;
        public Int32 TimeOut
        {
            set { _TimeOut = value; }
            get { return _TimeOut; }

        }

        #region Configuracion Proxy
        private Boolean _bUsarProxy = false;
        public Boolean bUsarProxy
        {
            get { return _bUsarProxy; }
            set { _bUsarProxy = value; }
        }

        private String _credencialesUsuario = "";
        public string CredencialesUsuario
        {
            set { _credencialesUsuario = value; }
            get { return _credencialesUsuario; }

        }
        
        private String _credencialesPassword = "";
        public string CredencialesPassword
        {
            set { _credencialesPassword = value; }
            get { return _credencialesPassword; }

        }
        
        private String _credencialesDominio = "";
        public string CredencialesDominio
        {
            set { _credencialesDominio = value; }
            get { return _credencialesDominio; }

        }
        
        private String _proxyIP = "";
        public string ProxyIP
        {
            set { _proxyIP = value; }
            get { return _proxyIP; }

        }

        private Int32 _proxyPuerto = 0;
        public Int32 ProxyPuerto
        {
            set { _proxyPuerto = value; }
            get { return _proxyPuerto; }

        }
        #endregion

        #endregion

        #region Constructor
        public InitServicesWeb()
        {
        }

        /// <summary>
        /// Constructor que se le pasa la url del servicio.
        /// </summary>
        /// <param name="url"></param>
        public InitServicesWeb(string url)
        {
            _URLServiceWeb = url;
        }
        #endregion

        #region Servicios Web

        public RespuestaServicesWebDescargaFacturas DescargarFacturas(String cifentidadpeticion,
                                                                            String cifEmisor,
                                                                            String cifReceptor,
                                                                            String FechaEntradaDesde,
                                                                            String FechaEntradaHasta,
                                                                            String Formato,
                                                                            String idUsuario,
                                                                            String password)

        {
            try
            {
                ServicesWeb servicio = new ServicesWeb();
                servicio.Url = _URLServiceWeb;
                servicio.Timeout = TimeOut;
                if (_bUsarProxy)
                {
                    System.Net.WebProxy proxy = new System.Net.WebProxy(_proxyIP, _proxyPuerto);
                    System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(_credencialesUsuario, _credencialesPassword, _credencialesDominio);
                    proxy.Credentials = credenciales;
                    servicio.Proxy = proxy;
                }

                RespuestaServicesWebDescargaFacturas respuesta = servicio.DescargarFacturas(cifentidadpeticion,
                                                                             cifEmisor,
                                                                             cifReceptor,
                                                                             FechaEntradaDesde,
                                                                             FechaEntradaHasta,
                                                                             Formato,
                                                                             idUsuario,
                                                                             password);
                return respuesta;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public RespuestaServicesWebDescargaFacturas DescargarFacturasMarmedsa(String cifentidadpeticion,
                                                                    String cifEmisor,
                                                                    String cifReceptor,
                                                                    String FechaEntradaDesde,
                                                                    String FechaEntradaHasta,
                                                                    String Formato,
                                                                    String idUsuario,
                                                                    String password)
        {
            try
            {
                ServicesWeb servicio = new ServicesWeb();
                servicio.Url = _URLServiceWeb;
                servicio.Timeout = TimeOut;
                if (_bUsarProxy)
                {
                    System.Net.WebProxy proxy = new System.Net.WebProxy(_proxyIP, _proxyPuerto);
                    System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(_credencialesUsuario, _credencialesPassword, _credencialesDominio);
                    proxy.Credentials = credenciales;
                    servicio.Proxy = proxy;
                }

                RespuestaServicesWebDescargaFacturas respuesta = servicio.DescargarFacturasMarmedsa(cifentidadpeticion,
                                                                             cifEmisor,
                                                                             cifReceptor,
                                                                             FechaEntradaDesde,
                                                                             FechaEntradaHasta,
                                                                             Formato,
                                                                             idUsuario,
                                                                             password);
                return respuesta;
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        public String NewUpdateEstadoFactura(String idFiscalEmisor, String idFiscalReceptor, String fechafactura, String referenciafactura, String sEstado, String sComentario)
        {
            try
            {
                String idUsuario = "T2xFbp2b8hy9dSdhuWGlO5b4qf0=";
                String password = "u8HXxgWk8sx5NKmrQwnW3A==";
                ServicesWeb servicio = new ServicesWeb();
                servicio.Url = _URLServiceWeb;
                servicio.Timeout = TimeOut;
                if (_bUsarProxy)
                {
                    System.Net.WebProxy proxy = new System.Net.WebProxy(_proxyIP, _proxyPuerto);
                    System.Net.NetworkCredential credenciales = new System.Net.NetworkCredential(_credencialesUsuario, _credencialesPassword, _credencialesDominio);
                    proxy.Credentials = credenciales;
                    servicio.Proxy = proxy;
                }
                String resultado = servicio.WSNewUpdateEstadoFactura(idFiscalEmisor, idFiscalEmisor, idFiscalReceptor, referenciafactura, fechafactura, sEstado, sComentario, idUsuario, password);
                resultado = idFiscalEmisor + ";" + idFiscalReceptor + ";" + referenciafactura + ";" + fechafactura + ";" + sEstado + ";" + sComentario + ";" + resultado;
                return resultado;
            }
            catch (Exception e)
            {
                return "ERROR:" + e.Message;
            }
        }
        #endregion
    }
}
