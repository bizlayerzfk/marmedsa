﻿using BZEdicom.WS;
using ClasesFacturaE.Facturae_v3_2;
using EnviarFacturasAEdicom;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace AutomatizacionEdicomMarmedsa.Edicom
{
    public class EnviarFacturasEdicom
    {
        public static String sDirectorioEdicom;
        public static String sURLEdicom;
        public static String sURLservicesweb = "";

        public EnviarFacturasEdicom()
        {
            sDirectorioEdicom = ConfigurationManager.AppSettings["sDirectorioEdicom"];
            sURLEdicom = ConfigurationManager.AppSettings["sURLEdicom"];
            sURLservicesweb = ConfigurationManager.AppSettings["sURLservicesweb"];
        }

        public void Procesar()
        {
            try
            {
                //Movemos los ficheros a tratar al directorio procesados_fecha
                
                ArrayList listaResultados = new ArrayList();
                Utiles oUtiles = new Utiles();
                sDirectorioEdicom = oUtiles.procesarArchivos(sDirectorioEdicom);
                //Creamos la carpeta Enviada
                string pathLog = sDirectorioEdicom + @"\Enviadas\";
                String directorioDestino = "";
                if (Directory.Exists(pathLog))
                {
                    directorioDestino = pathLog;
                }
                else
                {
                    directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                }
                string[] fileNames = Directory.GetFiles(sDirectorioEdicom, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    try
                    {
                        String sFile = fileNames[i];
                        String sFactura = Convert.ToBase64String(readBinaryFile(sFile));
                        String sBuzon = String.Empty;
                        String sRefFactura = String.Empty;
                        //Primero cargamos el objeto factura
                        BZFacturaEUtils.FacturaEUtils oF = new BZFacturaEUtils.FacturaEUtils(sFile);
                        if (oF.crearFactura32())
                        {
                            XMLFacturae_v3_2 oFactura = oF.ObjetoFacturaE32;
                            String sCIFEmisor = oFactura.Parties.SellerParty.TaxIdentification.TaxIdentificationNumber.ToString();
                            String sCIFReceptor = oFactura.Parties.BuyerParty.TaxIdentification.TaxIdentificationNumber.ToString();
                            sRefFactura = oFactura.Invoices[0].InvoiceHeader.InvoiceNumber.ToString();
                            String sFechaFactura = oFactura.Invoices[0].InvoiceIssueData.IssueDate.ToString();
                            //Luego con el receptor buscamos los datos del buzon.
                            //String sResultado = EnviarFacturaEdicom(sBuzon, sRefFactura, sFactura);
                            BuzonesEdicom oBuzon = new BuzonesEdicom();
                            sBuzon = oBuzon.GetBuzon(sCIFReceptor);
                            //Recogemos los datos del emisor.
                            //ClientID = "ESA08148710"; cif emisor
                            //User = "ESA08148710@sedeb2b.com"; buzon
                            //Password = "d2m6axawjj"; contraseña
                            //Dominio = "SEDEB2B.COM"; el dominio siempre es el mismo de momento
                            //Application = "ESA08148710@sedeb2b.com"; el buzon
                            String ClientID = "";
                            String User = "";
                            String Application = "";
                            String Password = "";
                            oBuzon.GetBuzonEmisor(sCIFEmisor, ref User,ref  Password,ref ClientID);
                            Application = User;
                            if (sBuzon != "")
                            {
                                String sResultado = EnviarFacturaEdicom(ClientID,User,Password,Application, sBuzon, sRefFactura, sFactura);
                                String sEstado = "2";
                                String sPrimeraLetra0 = "N";
                                if (sRefFactura.Substring(0, 1) == "0") sPrimeraLetra0 = "S";
                                //if (sResultado !="") //Actualizamos estado en Bizlayer
                                //{
                                //    try
                                //    {
                                //        BZServicesWeb.InitServicesWeb servicio = new BZServicesWeb.InitServicesWeb(sURLservicesweb);
                                //        String supdate = servicio.NewUpdateEstadoFactura(sCIFEmisor, sCIFReceptor, sFechaFactura, sRefFactura, sEstado, sResultado);
                                //    }
                                //    catch (Exception)
                                //    {

                                //    }

                                //}
								if (sResultado != "") //Actualizamos estado en Agikey
								{
									try
									{
										wsAgikey.FacturaEServiceSoapClient servicioagikey = new wsAgikey.FacturaEServiceSoapClient();

										String supdate = servicioagikey.ActualizarEstadoFacturaAgiKey(sCIFEmisor,sCIFEmisor, sCIFReceptor, sFechaFactura, sRefFactura, sEstado, sResultado,"maarcos@zertifika.com","AgiKey12");
									}
									catch (Exception)
									{

									}

								}
								String sLineaResultado = sCIFEmisor + ";" + sCIFReceptor + ";\"" + sRefFactura + "\";" + sFechaFactura + ";" + sEstado + ";\"" + sResultado + "\";" + sPrimeraLetra0;
                                listaResultados.Add(sLineaResultado);
                                int ifacturas = i + 1;
                                Console.WriteLine("Enviada " + ifacturas + " de " + fileNames.Length.ToString());

                                Utiles oUtil = new Utiles();
                                oUtiles.MoverFichero(fileNames[i], fileNames[i].Replace(sDirectorioEdicom, directorioDestino));
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                CrearFicheroCSV(directorioDestino + "\\Resultado_Envio_" + DateTime.Now.ToString("ddMMyyyy_HHmmssffff")+ ".csv", listaResultados);
            }
            catch (Exception)
            {
            }
        }


        #region Funciones Privadas
        private static String EnviarFacturaEdicom(String ClientID,String User,String Password,String Application, String oBuzon, String sReferenciaFactura, String sMessage)
        {
            String Dominio = "SEDEB2B.COM";
            String Schema = "SM";
            String Destination = oBuzon;
            String Reference = sReferenciaFactura;
            String Message = sMessage;
            EBIBrokerWebServiceImplService servicio = new EBIBrokerWebServiceImplService();
            String resultado = "";
            try
            {

                //MARMEDSA
                //ClientID = "ESA08148710"; cif emisor
                //User = "ESA08148710@sedeb2b.com"; buzon
                //Password = "d2m6axawjj"; contraseña
                //Dominio = "SEDEB2B.COM"; el dominio siempre es el mismo de momento
                //Application = "ESA08148710@sedeb2b.com"; el buzon

                servicio.Timeout = servicio.Timeout * 10;
                //servicio.Url = "https://adapters.sedeb2b.com:9020/ebiBroker/services/EBIBrokerWSV1";
                servicio.Url = sURLEdicom;
                resultado = servicio.TEBIBrokerInterface_PublishWS(ClientID, User, Password, Dominio, Application, Schema, Destination, Reference, Message, 1, 0);
                return resultado.ToString().Replace('"', ' ').Trim(); ;
            }
            catch (Exception e)
            {
                return "";
            }
        }


        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }


        private static void CrearFicheroCSV(String sFicheroOut, ArrayList listaFilas)
        {
            StringBuilder file = new StringBuilder();
            String sCabecera = "Emisor;Receptor;RefFactura;FechaFactura;Estado;Resultado;PrimeraLetra0";
            file.Append(sCabecera);
            file.Append(";\n");
            foreach (String sLinea in listaFilas)
            {
                file.Append(sLinea);
                file.Append(";\n");
            }

            //Convierte el StringBuilder a array de bytes
            byte[] buffer = new byte[file.Length];
            for (int i = 0; i < file.Length; i++)
            {
                buffer[i] = (byte)file[i];
            }

            FileStream fsRecibo = new FileStream(sFicheroOut, FileMode.OpenOrCreate, FileAccess.Write);
            fsRecibo.Write(buffer, 0, buffer.Length);
            fsRecibo.Close();
            fsRecibo = null;

        }
        #endregion
    }
}
