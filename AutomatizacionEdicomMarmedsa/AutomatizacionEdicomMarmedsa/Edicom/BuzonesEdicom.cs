﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnviarFacturasAEdicom
{
    public class BuzonesEdicom
    {
        public String GetBuzon(String sCIFReceptor)
        {
            try
            {
                String sBuzon= "";
                switch (sCIFReceptor)
                {
                    case "ESA63120174":
                    case "A63120174":
                        sBuzon="A63120174@sedeb2b.com";
                        break;
                    case "ESA78809662":
                    case "A78809662":
                        sBuzon="A78809662@sedeb2b.com";
                        break;
                    case "ESA80537327":
                    case "A80537327":
                        sBuzon="A80537327@sedeb2b.com";
                        break;
                    case "ESA81586893":
                    case "A81586893":
                        sBuzon="A81586893@sedeb2b.com";
                        break;
                    case "ESB57579914":
                    case "B57579914":
                        sBuzon="B57579914@sedeb2b.com";
                        break;
                    case "ESB63968267":
                    case "B63968267":
                        sBuzon="B63968267@sedeb2b.com";
                        break;
                    case "ESB84061654":
                    case "B84061654":
                        sBuzon="B84061654@sedeb2b.com";
                        break;
                    case "ESB85001782":
                    case "B85001782":
                        sBuzon="B85001782@sedeb2b.com";
                        break;
                    case "ESB97456396":
                    case "B97456396":
                        sBuzon="B97456396@sedeb2b.com";
                        break;
                    case "ESA81922791":
                    case "A81922791":
                    case "ESB81977365":
                    case "B81977365":
                    case "ESB86189560":
                    case "B86189560":
                        sBuzon="A81922791@sedeb2b.com";
                        break;
                    case "ESV28027118":
                    case "V28027118":
                    case "ESA86007572":
                    case "A86007572":
                    case "ESG83729434":
                    case "G83729434":
                        sBuzon = "sociedadesMM@sedeb2b.com";
                        break;
                    case "ESQ2866004A":
                    case "Q2866004A":
                        sBuzon = "Q2866004A@s2allegro.sedeb2b.com";
                        sBuzon = "ESQ2866004A@s2allegro.sedeb2b.com";
                        break;
                    case "ESB31907330":
                        sBuzon = "ESB31907330@s2allegro.sedeb2b.com";
                        break;
                    case "ESA28141935":
                    case "ESA28272474":
                    case "ESA79338638":
                    case "ESA80434699":
                    case "ESA82178468":
                    case "ESG28520443":
                    case "ESV81801904":
                    case "A28141935":
                    case "A28272474":
                    case "A79338638":
                    case "A80434699":
                    case "A82178468":
                    case "G28520443":
                    case "V81801904":
                    case "ESA08055741":
                    case "ESA28229599":
                    case "ESA85078301":
                    case "A08055741":
                    case "A28229599":
                    case "A85078301":
                    case "ESA28306033":
                    case "A28306033":
                        sBuzon = "G28010619@sedeb2b.com";
                        break;
					case "ESA39050349":
						sBuzon = "8481061777773@SEDEB2B.COM";
						break;
					case "ESA84818558":
						sBuzon = "EFACTURA-RTVE@s2allegro.sedeb2b.com";
						break;
					default:
                        sBuzon="";
                        break;
                }

                return sBuzon;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public void GetBuzonEmisor(String sCIFEmisor, ref String sBuzon, ref String sPassword, ref String ClientID)
        {
            try
            {
                switch (sCIFEmisor)
                {
                    case "ESA08148710":
                        sBuzon = "ESA08148710@sedeb2b.com";
                        sPassword = "d2m6axawjj";
                        ClientID = "ESA08148710";
                        break;
                    case "ESA17000704":
                        sBuzon = "ESA17000704@sedeb2b.com";
                        sPassword = "ha5hn5maek";
                        ClientID = "ESA08148710";
                        break;
					case "ESA87412557":
						sBuzon = "ESA87412557@sedeb2b.com";
						sPassword = "ndg5lacrw5";
						ClientID = "ESA87412557";
						break;
					//CARAT ESPAÑA
					case "A28343358":
                    case "ESA28343358":
                        sBuzon = "A28343358@sedeb2b.com";
                        sPassword = "nlb70xhdss";
                        ClientID = "A28343358";
                        break;
                    //CARAT INTERNACIONAL MEDIA SERVICE SA
                    case "A82792854":
                    case "ESA82792854":
                        sBuzon = "A82792854@sedeb2b.com";
                        sPassword = "4kvf4mnral";
                        ClientID = "A82792854";
                        break;
                    //INTELIGENCIA YMEDIA, S.A.
                    case "A84909498":
                    case "ESA84909498":
                        sBuzon = "A84909498@sedeb2b.com";
                        sPassword = "2tohsi0osi";
                        ClientID = "A84909498";
                        break;
                    //JC DECAUX ESPAÑA SL
                    case "B28104461":
                    case "ESB28104461":
                        sBuzon = "B28104461@sedeb2b.com";
                        sPassword = "p3r8itqvov";
                        ClientID = "B28104461";
                        break;
                    //JC DECAUX ATLANTIS SA
                    //EL MOBILIARIO URBANO SL
                    case "A35473750":
                    case "ESA35473750":
                    case "B28762003":
                    case "ESB28762003":
                        sBuzon = "B28762003@sedeb2b.com";
                        sPassword = "k7hgfkwe2u";
                        ClientID = "B28762003";
                        break;
                    //NETTHINK IBERIA S.L.U.
                    case "B82314089":
                    case "ESB82314089":
                        sBuzon = "B82314089@sedeb2b.com";
                        sPassword = "v1qbnhnpo5";
                        ClientID = "B82314089";
                        break;
                    //DENTSU AEGIS NETWORK IBERIA, S.L.U.
                    case "B83731240":
                    case "ESB83731240":
                        sBuzon = "B83731240@sedeb2b.com";
                        sPassword = "h0gn1eqdvo";
                        ClientID = "B83731240";
                        break;
                    //JCDECAUX TRANSPORT ESPAÑA SL
                    //CEMUSA CORPORACION EUROPEA DE MOBILIARIO URBANO SA
                    //CEMUSA - EL MOBILIARIO URBANO SLU UTE
                    case "B86076379":
                    case "ESB86076379":
                    case "A28928646":
                    case "ESA28928646":
                    case "U86994134":
                    case "ESU86994134":
                        sBuzon = "B86076379@sedeb2b.com";
                        sPassword = "5xwu1soayy";
                        ClientID = "B86076379";
                        break;
					
					// Buzon AtresMedia
					case "ESA78839271":
						sBuzon = "A78839271@SEDEB2B.COM";
						sPassword = "7hyvs0eyun";
						ClientID = "A78839271";
						break;
					//Default utilizamos Accenture
					default:
                        sBuzon = "B79217790@sedeb2b.com";
                        sPassword = "8i6amjyrpw";
                        ClientID = "B79217790";
                        break;
                }
            }
            catch (Exception)
            {
                sBuzon = "";
                sPassword = "";
            }

        }
    }
}
