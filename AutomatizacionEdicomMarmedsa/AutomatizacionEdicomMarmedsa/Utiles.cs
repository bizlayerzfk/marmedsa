﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace AutomatizacionEdicomMarmedsa
{
    public class Utiles
    {
        #region procesarArchivos
        /// <summary>
        /// Procesa los archivos que se encuentren en el directorio pasado por parámetro
        /// y crea un directorio de destino correspondiente al dia actual en el que se 
        /// copiarán los archivos originales
        /// </summary>
        public string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {

                string directorioDestino = "";
                bool OK = false;

                DateTime dt = DateTime.Now;
                string fecha, hora;

                //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-"); 
                fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                hora = dt.ToLongTimeString().Replace(":", "_");
                string pathLog = directorioMonitorizacion + "Procesados_" + fecha + @"\";
                //if (Directory.Exists(pathLog))
                //{
                //    directorioDestino = pathLog;
                //}
                //else
                //{
                //    directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                //}

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                // Al procesar el nuevo archivo incluido en el directorio especificado
                // debemos copiarlo al directorio temporal destinado para ello
                for (int i = 0; i < arrayFicheros.Length; i++)
                {
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            File.Delete(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }
                return directorioDestino;
            }
            catch (Exception )
            {
                return "";
            }
        }
        #endregion

        #region CopiarArchivo
        /// <summary>
        /// Copia el archivo ubicado en el directorio temporal en la carpeta de procesados indicados como parametro
        /// </summary>
        /// <param name="url">Ubicación del directorio temporal</param>
        /// <param name="directorioDestino">Ubicación del directorio de destino</param>
        /// <returns></returns>
        public bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }


        #endregion

        #region MoverArchivo
        public void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }


        #endregion
    }
}
