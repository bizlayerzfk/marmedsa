﻿using Com.Bizlayer.Pki.Xades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace AutomatizacionEdicomMarmedsa.PrepararFacturas
{
    public class PrepararFacturas
    {

        public static String sFilecertificado;
        public static String sPassword;
        public static String sDirectorioDescarga;
        public static String sDirectorioPreparada;
        public static String sDirectorioEdicom;

        public PrepararFacturas()
        {
            sDirectorioDescarga = ConfigurationManager.AppSettings["sDirectorioDescarga"];
            sDirectorioPreparada = ConfigurationManager.AppSettings["sDirectorioPreparada"];
            sDirectorioEdicom = ConfigurationManager.AppSettings["sDirectorioEdicom"];
            sFilecertificado = ConfigurationManager.AppSettings["sFileCertificado"]; ;
            sPassword = ConfigurationManager.AppSettings["sPassword"];
        }

        public void Procesar()
        {
            try
            {
                String sFilein = "";
                String sFileOut = "";
                //Movemos todos los ficheros a la carpeta de Preparada
                string[] fileNames = Directory.GetFiles(sDirectorioDescarga, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    sFilein = fileNames[i];
                    sFileOut = sFilein.Replace(sDirectorioDescarga, sDirectorioPreparada);
                    MoverFichero(sFilein, sFileOut);
                }


                //Ahora ya tratamos todos los ficheros.
                //Los movemos primero a procesados_fecha
                Utiles oUtiles = new Utiles();
                sDirectorioPreparada = oUtiles.procesarArchivos(sDirectorioPreparada);
                fileNames = Directory.GetFiles(sDirectorioPreparada, "*.xml");

                for (int i = 0; i < fileNames.Length; i++)
                {
                    //Primero debemos mirar si el campo SendInvoiceToReceiver está informado a S si no no se envia

                    Boolean bSendInvoiceToReceiver = false;
                    bSendInvoiceToReceiver = ifSendInvoiceToReceiver(fileNames[i]);
                    if (bSendInvoiceToReceiver)
                    {
                        sFilein = fileNames[i];
                        sFileOut = sFilein.Replace(".xml", "_quitarfirma.xml");
                        DeleteSignature(sFilein, sFileOut);
                        sFilein = sFileOut;
                        sFileOut = sFilein.Replace("_quitarfirma.xml", "_quitarextension.xml");
                        DeleteExtension(sFilein, sFileOut);
                        sFilein = sFileOut;
                        sFileOut = sFilein.Replace("_quitarextension.xml", "_quitarreferencia.xml");
                        DeteleReferencia(sFilein, sFileOut);
						sFilein = sFileOut;
						sFileOut = sFilein.Replace("_quitarreferencia.xml", "_quitarpdf.xml");
						DeletePDF(sFilein, sFileOut);
						
						//Si la firma ok lo dejamos en la carpeta de edicom para enviar.
						sFilein = sFileOut;
                        sFileOut = sFilein.Replace("_quitarpdf.xml", "_firmado.xml");
                        if (firmarXades(sFilein, sFileOut, sFilecertificado, sPassword))
                        {
                            CopiarArchivo(sFileOut, sDirectorioEdicom);
                        }
                    }
                    else
                    {
                        //Lo movemos a sDirectorioPreparada\noEnviar\
                        //Creamos la carpeta Enviada
                        string pathLog = sDirectorioPreparada + @"\NoEnviar\";
                        String directorioDestino = "";
                        if (Directory.Exists(pathLog))
                        {
                            directorioDestino = pathLog;
                        }
                        else
                        {
                            directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                        }
                        Utiles oUtil = new Utiles();
                        oUtiles.MoverFichero(fileNames[i], fileNames[i].Replace(sDirectorioPreparada, directorioDestino));
                    }
                }
                //Una vez preparados y copiados al directorioEdicom los movemos todos a Preparadas.
                fileNames = Directory.GetFiles(sDirectorioPreparada, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    //Creamos la carpeta Enviada
                    string pathLog = sDirectorioPreparada + @"\Preparadas\";
                    String directorioDestino = "";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                    Utiles oUtil = new Utiles();
                    oUtiles.MoverFichero(fileNames[i], fileNames[i].Replace(sDirectorioPreparada, directorioDestino));
                }
            }
            catch (Exception)
            {
            }
        }

        #region Funciones Privadas QuitarFirma
        private static void QuitarFirma(String sCarpeta, String sCarpetaOut)
        {
            try
            {
                string[] fileNames = Directory.GetFiles(sCarpeta, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFileSinSignature = fileNames[i].Replace(sCarpeta, sCarpetaOut);
                    DeleteSignature(fileNames[i], sFileSinSignature);
                }
            }
            catch (Exception)
            {
            }

        }

        private static void DeleteSignature(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }



                if (nodo != null)
                {
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    XmlNode node1 = documento.SelectSingleNode("//Signature");
                    if (node1 == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                        xmlNsM.AddNamespace("ds", @"http://www.w3.org/2000/09/xmldsig#");
                        node1 = documento.SelectSingleNode("//ds:Signature", xmlNsM);
                    }
                    if (node1 != null) nodo.RemoveChild(node1);
                    //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
                    documento.Save(sFileOut);
                    documento = null;
                }
                else documento.Save(sFileOut);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
        #endregion

        //#region Quitar pdf incrustado

        //private static void ProcesarCarpetaSinFirma(String sCarpeta, String sCarpetaOut)
        //{
        //    try
        //    {
        //        string[] fileNames = Directory.GetFiles(sCarpeta, "*.xml");
        //        for (int i = 0; i < fileNames.Length; i++)
        //        {
        //            String sFileSinPDF = fileNames[i].Replace(sCarpeta, sCarpetaOut);
        //            DeleteRelatedDocuments(fileNames[i], sFileSinPDF);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }

        //}

        //private static void DeleteRelatedDocuments(String sFilein, String sFileOut)
        //{
        //    String sfileor = sFilein;
        //    try
        //    {
        //        XmlDocument documento = new XmlDocument();
        //        documento.Load(sFilein);

        //        XmlNode nodo = documento.SelectSingleNode("//Facturae");
        //        //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
        //        if (nodo == null)
        //        {
        //            var xmlNsM = new XmlNamespaceManager(documento.NameTable);
        //            xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
        //            nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
        //        }



        //        if (nodo != null)
        //        {
        //            //Buscamos el AdditionalData
        //            XmlNode nodeAdditionalData = documento.SelectSingleNode("//AdditionalData");
        //            //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
        //            XmlNode node1 = documento.SelectSingleNode("//RelatedDocuments");
        //            if (node1 != null) nodeAdditionalData.RemoveChild(node1);
        //            //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
        //            documento.Save(sFileOut);
        //            documento = null;
        //        }
        //        else documento.Save(sFileOut);

        //        documento = null;
        //    }
        //    catch (Exception e)
        //    {
        //        sFilein = "";
        //    }


        //}
        //#endregion

        #region Quitar Extension Marmedsa
        private static void QuitarExtensionMarmedsa(String sCarpeta, String sCarpetaOut)
        {
            try
            {
                string[] fileNames = Directory.GetFiles(sCarpeta, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFileSinPDF = fileNames[i].Replace(sCarpeta, sCarpetaOut);
                    DeleteExtension(fileNames[i], sFileSinPDF);
                }
            }
            catch (Exception)
            {
            }

        }
        private static void DeleteExtension(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {

                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }



                if (nodo != null)
                {
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    XmlNode node1 = documento.SelectSingleNode("//Extensions");

                    if (node1 != null) nodo.RemoveChild(node1);
                    //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
                    documento.Save(sFileOut);
                    documento = null;
                }
                else documento.Save(sFileOut);
                documento = null;


            }
            catch (Exception e)
            {

            }
        }
        #endregion

        #region Funciones Privadas quitar "/" de la referencia
        private static void QuitarReferencia(String sCarpeta, String sCarpetaOut)
        {
            try
            {
                string[] fileNames = Directory.GetFiles(sCarpeta, "*.xml");
                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFileSinSignature = fileNames[i].Replace(sCarpeta, sCarpetaOut);
                    DeteleReferencia(fileNames[i], sFileSinSignature);
                }
            }
            catch (Exception)
            {
            }

        }

        private static void DeteleReferencia(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//BatchIdentifier");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo != null)
                {
                    nodo.InnerText = nodo.InnerText.Replace("/", "-");
                }

                nodo = documento.SelectSingleNode("//InvoiceNumber");

                if (nodo != null)
                {
                    nodo.InnerText = nodo.InnerText.Replace("/", "-");
                }
                documento.Save(sFileOut);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
        #endregion

        #region Funcion de firma
        private static Boolean firmarXades(String sFileSinFirma,String sFileFirmado, String ficheroPKCS12, String password)
        {
            try
            {
                XadesSignature xades;
                XmlDocument doc = new XmlDocument();
                string pathFichero;
                xades = new XadesSignature();
                pathFichero = sFileSinFirma;

                doc.PreserveWhitespace = true;
                doc.Load(pathFichero);
                //Generamos la firma digital xades_epes 
                xades.XadesEpes(doc, XadesSignature.ROL_FIRMANTE_EMISOR, ficheroPKCS12, password);
                doc.Save(sFileFirmado);
                //Quitar BOM
                using (var writer = new XmlTextWriter(sFileFirmado, new UTF8Encoding(false)))
                {
                    doc.Save(writer);
                }
                
                //Validamos la firma digital
                xades.ValidarFirmaDigital(doc);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                throw e;
            }
        }
		#endregion

		#region Quitar pdf adjunto
		private static void DeletePDF(String sFilein, String sFileOut)
		{
			String sfileor = sFilein;
			try
			{

				XmlDocument documento = new XmlDocument();
				documento.Load(sFilein);

				XmlNode nodo = documento.SelectSingleNode("//Invoice");
				////fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
				//if (nodo == null)
				//{
				//	var xmlNsM = new XmlNamespaceManager(documento.NameTable);
				//	xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
				//	nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
				//}



				if (nodo != null)
				{
					//ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
					XmlNode node1 = documento.SelectSingleNode("//AdditionalData");

					if (node1 != null) nodo.RemoveChild(node1);
					//sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
					documento.Save(sFileOut);
					documento = null;
				}
				else documento.Save(sFileOut);
				documento = null;


			}
			catch (Exception e)
			{

			}
		}
#endregion

		#region 
		private static Boolean ifSendInvoiceToReceiver(String sxmlFacturae)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sxmlFacturae);
                XmlNode nodo = xmlDoc.SelectSingleNode("//InvoiceExtensions");

                //Buscamos extension marmedsa
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    //ext="LeaseplanInvoiceExtension
                    xmlNsM.AddNamespace("marmedsa", @"http://marmedsa.com/Factura_Extension.xsd");
                    nodo = xmlDoc.SelectSingleNode("//marmedsa:InvoiceExtensions", xmlNsM);
                }
                //si no lo encontramos buscamos ns4
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    //ext="LeaseplanInvoiceExtension
                    xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                    nodo = xmlDoc.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                }

                //String sReceiverAcceptsEInvoice = "";
                String sSendInvoiceToReceiver = "";
                //String sReceiverEmailAddresses = "";
                //String sSegundoPagador = "";
                foreach (XmlNode onodoExtension in nodo)
                {
                    //if (onodoExtension.Name == "ReceiverAcceptsEInvoice")
                    //{
                    //    sReceiverAcceptsEInvoice = onodoExtension.InnerText.ToString();
                    //}
                    if (onodoExtension.Name == "SendInvoiceToReceiver")
                    {
                        sSendInvoiceToReceiver = onodoExtension.InnerText.ToString();
                    }
                    //foreach (XmlNode oReceiverEmailAddresses in onodoExtension)
                    //{
                    //    if (oReceiverEmailAddresses.Name == "EmailAdress")
                    //    {
                    //        sReceiverEmailAddresses = oReceiverEmailAddresses.InnerText.ToString() + ";" + sReceiverEmailAddresses;
                    //    }
                    //}
                    //if (onodoExtension.Name == "SegundoPagador")
                    //{
                    //    sSegundoPagador = onodoExtension.InnerText.ToString();
                    //}
                }
                if (sSendInvoiceToReceiver == "S" || sSendInvoiceToReceiver == "s")
                    return true;
                else return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                xmlDoc = null;
            }

        }

        #endregion

        #region Funciones privadas
        private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);

                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }
        #endregion
    }
}
