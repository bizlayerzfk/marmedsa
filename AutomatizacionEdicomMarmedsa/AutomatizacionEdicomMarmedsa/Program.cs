﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomatizacionEdicomMarmedsa
{
    class Program
    {
        static void Main(string[] args)
        {
            //DescargarFacturasBizlayer();
             PrepararFacturasEdicom();
             EnviarFacturasEdicom();
        }
		 
        private static void DescargarFacturasBizlayer()
        {
            try
            {
                DescargarFacturas.DescargarFacturas oDescargarFacturas = new DescargarFacturas.DescargarFacturas();
                oDescargarFacturas.Procesar();
                oDescargarFacturas = null;
                Console.WriteLine("Facturas descargadas..");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error descarga: " + e.Message);
            }
        }

        private static void PrepararFacturasEdicom()
        {
            try
            {
                PrepararFacturas.PrepararFacturas oPrepararFacturas = new PrepararFacturas.PrepararFacturas();
                oPrepararFacturas.Procesar();
                oPrepararFacturas = null;
                Console.WriteLine("Facturas preparadas..");

            }
            catch (Exception e)
            {
                Console.WriteLine("Error descarga: " + e.Message);
            }
        }


        private static void EnviarFacturasEdicom()
        {
            try
            {
                Edicom.EnviarFacturasEdicom oEdicom = new Edicom.EnviarFacturasEdicom();
                oEdicom.Procesar();
                oEdicom = null;
                Console.WriteLine("Facturas enviadas a Edicom..");

            }
            catch (Exception e)
            {
                Console.WriteLine("Error descarga: " + e.Message);
            }
        }
    }
}
