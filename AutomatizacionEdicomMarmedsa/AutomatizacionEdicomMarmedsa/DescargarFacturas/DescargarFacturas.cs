﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace AutomatizacionEdicomMarmedsa.DescargarFacturas
{
    public class DescargarFacturas
    {
        public static String sDirectorioDescarga = "";
        public static ArrayList oEmisoras = new ArrayList();
        public static ArrayList oReceptoras = new ArrayList();
        public static String sFechaini = "2017-07-03";
        public static String sFechafin = "2017-07-10";
        public static String sURLservicesweb = "";

        public DescargarFacturas()
        {
            //Cargamos la configuración
            sDirectorioDescarga = ConfigurationManager.AppSettings["sDirectorioDescarga"];
            sURLservicesweb = ConfigurationManager.AppSettings["sURLservicesweb"];
            sFechaini = ConfigurationManager.AppSettings["sFechaini"];
            sFechafin = ConfigurationManager.AppSettings["sFechafin"];

            String scifEmisores = ConfigurationManager.AppSettings["sCifEmisores"];
            String scifreceptores = ConfigurationManager.AppSettings["sCifReceptores"];

            String[] sEmisores = scifEmisores.Split(';');
            String[] sReceptores = scifreceptores.Split(';');
            for (int i = 0; i < sEmisores.Length; i++)
            {
                oEmisoras.Add(sEmisores[i]);
            }
            for (int i = 0; i < sReceptores.Length; i++)
            {
                oReceptoras.Add(sReceptores[i]);
            }
            if (sFechaini=="") sFechaini = sFechayyyy_mm_dd(DateTime.Now.AddDays(-1));
            //sFechafin = sFechayyyy_mm_dd(DateTime.Now);
            if (sFechafin == "") sFechafin = sFechayyyy_mm_dd(DateTime.Now.AddDays(-1));
        }

        public void Procesar()
        {
            try
            {
                ArrayList oResultado = new ArrayList();

                for (int i = 0; i < oEmisoras.Count; i++)
                {
                    for (int j = 0; j < oReceptoras.Count; j++)
                    {
                        String sResultado = DescargaFactura(oEmisoras[i].ToString(), oEmisoras[i].ToString(), oReceptoras[j].ToString(), sFechaini, sFechafin);
                    }
                }
                //CrearFicheroCSV(sFileOut, oResultado);
                Console.WriteLine("Proceso finalizado.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error proceso: " + e.Message);
            }

        }

        #region funciones privadas
        private static String DescargaFactura(String sEntidadPeticion, String sEmisor, String sReceptor, String sFechaini, String sFechafin)
        {
            String idUsuario = "fli6hpxtmhc2hCJY8K8vXZ8kpkY=";
            BZServicesWeb.InitServicesWeb servicio = new BZServicesWeb.InitServicesWeb(sURLservicesweb);
            BZServicesWeb.RespuestaServicesWebDescargaFacturas respuesta = servicio.DescargarFacturas(sEntidadPeticion, sEmisor, sReceptor, sFechaini, sFechafin, "XML", idUsuario, "u8HXxgWk8sx5NKmrQwnW3A==");

            String sResultado = "";
            if (respuesta.StatusOK)
            {
                if (respuesta.ListaFacturas != null)
                {
                    //foreach (ServiceWebReference.FacturaSWebDescargaBean oFactura in respuesta.ListaFacturas)
                    foreach (BZServicesWeb.FacturaSWebDescargaBean oFactura in respuesta.ListaFacturas)
                    {
                        String snombrefichero = oFactura.RefFactura + "+" + oFactura.IdFiscalEmisor + "+" + oFactura.IdFiscalReceptor + "+" + sFechaddmmyyyy(oFactura.FechaFactura);
                        if (oFactura.FicheroB64FacturaE != "" && oFactura.FicheroB64FacturaE != null)
                        { byteArrayToFile(Convert.FromBase64String(oFactura.FicheroB64FacturaE), sDirectorioDescarga + snombrefichero + ".xml"); }
                        sResultado = oFactura.IdFiscalEmisor + ";" + oFactura.IdFiscalReceptor + ";" + oFactura.RefFactura + ";" + oFactura.FechaFactura + ";" + "OK";
                    }
                }
                Console.WriteLine("Respuesta OK");
            }
            else
            {
                Console.WriteLine("Respuesta KO");
            }
            return sResultado;
        }

        private static void byteArrayToFile(byte[] data, string filePath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(data, 0, data.GetUpperBound(0) + 1);
            }
            catch (Exception e)
            {
                //no hacemos nada por si ya existe que no pete..
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private static String sFechaddmmyyyy(DateTime fecha)
        {
            try
            {
                String sdia = "";
                String smes = "";
                String saño = "";
                if (fecha.Day < 10)
                    sdia = "0" + fecha.Day.ToString();
                else sdia = fecha.Day.ToString();
                if (fecha.Month < 10)
                    smes = "0" + fecha.Month.ToString();
                else smes = fecha.Month.ToString();
                saño = fecha.Year.ToString();
                return sdia + smes + saño;
            }
            catch (Exception)
            {
                return "";
            }

        }

        private static String sFechayyyy_mm_dd(DateTime fecha)
        {
            try
            {
                String sdia = "";
                String smes = "";
                String saño = "";
                if (fecha.Day < 10)
                    sdia = "0" + fecha.Day.ToString();
                else sdia = fecha.Day.ToString();
                if (fecha.Month < 10)
                    smes = "0" + fecha.Month.ToString();
                else smes = fecha.Month.ToString();
                saño = fecha.Year.ToString();
                return saño + "-" + smes + "-" + sdia;
            }
            catch (Exception)
            {
                return "";
            }

        }
        #endregion
    }
}
