﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using ClasesFacturaE.Facturae_v3_2;
using System.Xml;
using Efec.Core.Io.Facturae_v3_2_1;

namespace BZFacturaEUtils
{
    public class FacturaEUtils
    {
        #region Variables y propiedades
        private String _sFileFacturaE = "";
        private String _sMensajeError = "";
        private XMLFacturae_v3_2 _objetoFacturaE32 = null;

        public String MensajeError
        {
            get { return _sMensajeError; }
        }

        public XMLFacturae_v3_2 ObjetoFacturaE32
        {
            get { return _objetoFacturaE32; }
        }
        #endregion

        #region Métodos Constructores
        public FacturaEUtils()
        {

        }

        public FacturaEUtils(String sFileFacturaE)
        {
            _sFileFacturaE = sFileFacturaE;
        }

        #endregion

        #region Métodos públicos
        public Boolean ValidateFacturaE(String sFileFacturaE,int idFormatoFacturaE)
        {
            try
            {

                _sFileFacturaE = sFileFacturaE;
                return ValidarFacturaE(idFormatoFacturaE);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Añade el documento pdf en el additionaldata.
        /// </summary>
        /// <param name="sFileFacturaEIn">Fichero facturaE de entrada, al que hay que añadir el pdf.</param>
        /// <param name="sFilepdf">Fichero pdf</param>
        /// <param name="sFileFacturaEout">Fichero facturaE de salida</param>
        /// <returns>True si todo ok, o false si hay algún error.</returns>
        public Boolean AddAttachmentDocumentPDF(String sFileFacturaEIn,String sFilepdf,String sFileFacturaEout, int iFormatoFacturaE)
        {
            try
            {
                _sFileFacturaE = sFileFacturaEIn;
                if (AddAttachmentDocument_PDF(sFilepdf, sFileFacturaEout))
                {
                    //Ahora validamos el facturaE
                    _sFileFacturaE = sFileFacturaEout;
                    return ValidarFacturaE(iFormatoFacturaE);
                }
                else return false;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        /// <summary>
        /// Devuelve el objeto facturaE 3.2
        /// </summary>
        /// <returns></returns>
        public Boolean crearFactura32()
        {
            try
            {
                FileStream fs = null;
                fs = new FileStream(_sFileFacturaE, FileMode.Open, FileAccess.Read);
                XmlSerializer serializer = new XmlSerializer(typeof(XMLFacturae_v3_2));
                XMLFacturae_v3_2 oFacturaE = (XMLFacturae_v3_2)serializer.Deserialize(fs);
                fs.Close();
                fs = null;
                _objetoFacturaE32 = oFacturaE;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        #endregion

        #region Métodos privados
        private Boolean ValidarFacturaE(int iFormatoFacturaE)
        {
            FileStream fs = null;
            try
            {
                //facturae_v32 = 15,
                //facturae_V321 = 19
                if (iFormatoFacturaE == 15)
                {

                    fs = new FileStream(_sFileFacturaE, FileMode.Open, FileAccess.Read);
                    XmlSerializer serializer = new XmlSerializer(typeof(XMLFacturae_v3_2));
                    XMLFacturae_v3_2 oFacturaE = (XMLFacturae_v3_2)serializer.Deserialize(fs);
                    fs.Close();
                    fs = null;
                }
                else
                    if (iFormatoFacturaE == 19)
                    {
                        fs = new FileStream(_sFileFacturaE, FileMode.Open, FileAccess.Read);
                        XmlSerializer serializer = new XmlSerializer(typeof(XMLFacturae_v3_2_1));
                        XMLFacturae_v3_2_1 oFacturaE = (XMLFacturae_v3_2_1)serializer.Deserialize(fs);
                        fs.Close();
                        fs = null;
                    }
                return true;
            }
            catch (Exception e)
            {
                _sMensajeError = "Error: " + e.Message + " - Detalle: " + e.InnerException.Message;
                fs.Close();
                fs = null;
                return false;
            }
        }


        /// <summary>
        /// Añade el documento en pdf y base64
        /// </summary>
        /// <param name="sFilepdf"></param>
        /// <param name="sFileFacturaEout"></param>
        /// <returns></returns>
        private Boolean AddAttachmentDocument_PDF(String sFilepdf,String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(_sFileFacturaE);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//AdditionalData");

                if (nodoAdd == null) //No existe el nodo AdditionalData
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoice");
                    String spdfB64 = getFileB64(sFilepdf);
                    nodo.AppendChild(CrearNodo_AdditionalData(xmlDoc, "NONE", "pdf", "BASE64", spdfB64));
                    xmlDoc.Save(sFileFacturaEout);
                }
                else
                {
                    XmlNode nodo1 = nodoAdd.FirstChild;
                    String spdfB64 = getFileB64(sFilepdf);
                    nodoAdd.InsertBefore(CrearNodo_RelatedDocuments(xmlDoc, "NONE", "pdf", "BASE64", spdfB64), nodo1);
                    xmlDoc.Save(sFileFacturaEout);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }




        /// <summary>
        /// Crea el nodo AdditionalData con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_AdditionalData(XmlDocument xmlDoc, 
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);


                XmlElement AdditionalData = xmlDoc.CreateElement("AdditionalData");
                AdditionalData.AppendChild(RelatedDocuments);

                return AdditionalData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Crea el nodo RelatedDocuments con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_RelatedDocuments(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);

                return RelatedDocuments;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve el fichero pdf en B64
        /// </summary>
        /// <param name="sfilePdf"></param>
        /// <returns></returns>
        private static String getFileB64(String sfilePdf)
        {
            try
            {
                String sBase64 = "";
                sBase64 = Convert.ToBase64String(readBinaryFile(sfilePdf));
                return sBase64;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve byte[] del fichero a leer
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }
        #endregion
    }
}
