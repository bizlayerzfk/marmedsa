﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrearFacturasProveedoresExtandar.EmpresaMarmedsa
{
    public class EmpresaMarmedsa
    {
        #region privada
        private string _emp;
        private string _nom;
        private string _cif;
        private string _del;
        private string _prov;
        private string _pais;
        private string _dir;
        private string _cp;
        private string _mail;
        #endregion

        #region publica
        public string EMP
        {
            set { _emp = value; }
            get { return _emp; }
        }
        public string NOM
        {
            set { _nom = value; }
            get { return _nom; }
        }
        public string CIF
        {
            set { _cif = value; }
            get { return _cif; }
        }
        public string DEL
        {
            set { _del = value; }
            get { return _del; }
        }
        public string PROV
        {
            set { _prov = value; }
            get { return _prov; }
        }
        public string PAIS
        {
            set { _pais = value; }
            get { return _pais; }
        }
        public string DIRECCION
        {
            set { _dir = value; }
            get { return _dir; }

        }
        public string CODIGOPOSTAL
        {
            set { _cp = value; }
            get { return _cp; }

        }

        public string MAIL
        {
            set { _mail = value; }
            get { return _mail; }

        }
        #endregion

        #region Constructor
        public EmpresaMarmedsa()
        {
        }
        #endregion
    }
}
