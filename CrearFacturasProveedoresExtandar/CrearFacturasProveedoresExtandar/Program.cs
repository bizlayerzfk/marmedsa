﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data.OleDb;
using AgiKeyMailLib;

namespace CrearFacturasProveedoresExtandar
{
    class Program
    {
        #region Varibles
        public static String sFilein="";
        public static String sFileout = "";
        public static String sFileExtension = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Extensions.xml";
        public static ArrayList oCiudad = null;
        public static ArrayList oDepartamento = null;
        public static String sDirectorioFacturas = "";
        public static String sDirectorioFacturasenviar = "";
        public static String[] sCIFExcluidos;
        public static ArrayList aListaEmpresas = null;
        public static String sFileListaEmpresasMarmedsa = "";
        public static ArrayList aListaSuplidos = null;
        public static String sFileListaSuplidos = "";
		public static String sListaDelegaciones = "";
		public static ArrayList aListaDelegaciones = null;
		public static ArrayList oPaises = null;

		//Variables para el informe
		private static ArrayList ResultadoProceso = null;
		private static ArrayList errorInforme = null;
		private static ArrayList intInforme = null;
		public static ArrayList listaFilasCheckTotales = new ArrayList();

		public static String sTaxIdentificationNumberSeller = "";
		public static String sTaxIdentificationNumberBuyer = "";
		public static String sInvoiceNumber = "";
		#endregion

		static void Main(string[] args)
        {
            sDirectorioFacturas = ConfigurationManager.AppSettings["sDirectorioFacturas"];
            sDirectorioFacturasenviar = ConfigurationManager.AppSettings["sDirectorioFacturasenviar"];
            sFileListaEmpresasMarmedsa = ConfigurationManager.AppSettings["sListaMarmedsa"];
            sFileListaSuplidos = ConfigurationManager.AppSettings["sListaSuplidos"];
			sListaDelegaciones = ConfigurationManager.AppSettings["sListaDelegaciones"];
			String scif = ConfigurationManager.AppSettings["sCifs"];
            Init();
            sCIFExcluidos = scif.Split(';');
			String sDirectorioMonitorizacionDirectorios = procesarDirectorios(sDirectorioFacturas);
			String sDirectorioMonitorizacion = procesarArchivos(sDirectorioFacturas);
			if (sDirectorioMonitorizacion == "") sDirectorioMonitorizacion = sDirectorioMonitorizacionDirectorios;


			if (sDirectorioMonitorizacion !="")
            {
				//Antes de todo hacemos el tratamiento de quitar el caracter "&"
				//Borramos si quedan fichero *_and.xml
				string[] fileNamesand = Directory.GetFiles(sDirectorioMonitorizacion, "*.and");
				for (int i = 0; i < fileNamesand.Length; i++)
				{
					try
					{
						BorrarFichero(fileNamesand[i]);
					}
					catch (Exception)
					{
					}
				}



				string[] fileNames = Directory.GetFiles(sDirectorioMonitorizacion, "*.xml");

                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFichero = fileNames[i];


					String sFicheroPdf = fileNames[i].Replace(".xml", ".pdf");
                    if (sFicheroPdf==sFichero) sFicheroPdf = fileNames[i].Replace(".XML", ".pdf");

                    if(!File.Exists(sFicheroPdf)) //renombrado de pdf de transporte Sargal
                    {
                        sFicheroPdf = RenombrarPDFTransportesSargal(sFichero,sFicheroPdf, sDirectorioMonitorizacion);
                    }
					//renombrar el pdf de Dyatrans Logistic
					//RenombrarPDFDyatransLogistic
					if (!File.Exists(sFicheroPdf)) //renombrado de pdf de transporte Sargal
					{
						sFicheroPdf = RenombrarPDFDyatransLogistic(sFichero, sFicheroPdf, sDirectorioMonitorizacion);
					}
					try
					{
						sTaxIdentificationNumberSeller = "";
						sTaxIdentificationNumberBuyer = "";
						sInvoiceNumber = "";
						CrearFicheroOriginal(sFichero);
						//Después del fichero original creamos el fichero sin &
						try
						{
							EliminarCaracteresEspeciales(sFichero);
						}
						catch (Exception e)
						{
						}
						int j = i + 1;
                        Console.WriteLine("Procesando " + j + " de " + fileNames.Length);
                        sFilein = sFichero;
                        sFileout = sFichero.Replace(".xml", "_version32.xml");
                        UpdateVersion32(sFilein, sFileout);
                        //Quitamos firma
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_version32.xml", "_sinfirma.xml");
                        DeleteSignature(sFilein, sFileout);
                        //Añadimos centro administrativo.
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_sinfirma.xml", "_conceros.xml");
                        Boolean bSuplido = false;
                        AddAdministrativeCentreBuyer(sFilein, sFileout, ref bSuplido);

                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conceros.xml", "_conadmcentre.xml");
                        AddAdministrativeCentreSeller(sFilein, sFileout);

                        //Miramos si es suplido debemos de informar los nuevos nodos Reimbursable
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conadmcentre.xml", "_Reimbursable.xml");
                        AddNodeReimbursable(sFilein, sFileout, bSuplido);

                        //Borramos las lineas con valor Cero
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_Reimbursable.xml", "_sincero.xml");
                        DeleteInvoiceLineZero(sFilein, sFileout);

                        //Ponemos el <SequenceNumber>1</SequenceNumber> antes de ItemDescription
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_sincero.xml", "_conSequenceNumber.xml");
                        AddSequenceNumberInvoiceLine(sFilein, sFileout);
                        
                        ///No añadimos el xml, solo para Sabadell.
                        ////Añadimos xml original 
                        //sFilein = sFileout;
                        //sFileout = sFilein.Replace("_conSequenceNumber.xml", "_conxml.xml");
                        //AddAttachmentDocument_xml(sFilein, sFichero, sFileout);

                        //Añadimos extension
                        sFilein = sFileout;
                        //sFileout = sFilein.Replace("_conxml.xml", "_conespacios.xml");
                        sFileout = sFilein.Replace("_conSequenceNumber.xml", "_conespacios.xml");
                        AddExtension(sFilein, sFileout);


                        //Quitamos los espacios en la referencia factura
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conespacios.xml", "_ISO2Buyer.xml");
                        QuitarEspaciosReferenciaFactura(sFilein, sFileout);

                        //Miramos de poner el código de pais en ISO2 solo al receptor
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_ISO2Buyer.xml", "_ISO2Seller.xml");
                        AddPaisCIFBuyer(sFilein,sFileout);

						//Miramos de poner el código de pais en ISO2 también para emisor por APT.
						sFilein = sFileout;
						sFileout = sFilein.Replace("_ISO2Seller.xml", "_Mailextension.xml");
						AddPaisCIFSeller(sFilein, sFileout);

						//Añadimos el mail de notificacion si viene en la factura o dependiendo empresas marmedsa
						sFilein = sFileout;
                        sFileout = sFilein.Replace("_Mailextension.xml", "_delegacioncorrectaseller.xml");
                        AddMailExtension(sFilein, sFileout);

						String sTipoError = "OK";
						////Informamos la delegación que nos viene en el seller en el buyer para suplidos.
						////Caso viene CAD es suplido y ponemos BCN por defecto, pues miramos si existe CAD
						////en la empresa de marmedsa y ponemos esa
						CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa = null;
						//#region controlar cambio de delegacion
						//Boolean bCambiarDelegacion = IsChangeDelegacionEmpMarmedsa(sFileout, ref oEmpresaMarmedsa);
						//sFilein = sFileout;
						//sFileout = sFilein.Replace("_delegacioncorrectaseller.xml", "_delegacioncorrecta.xml");
						////if (bCambiarDelegacion && bSuplido)
						//if (bCambiarDelegacion)
						//{

						//	String sficheroin = sFilein;
						//	String sficheroout = "";
						//	sficheroout = sFilein.Replace("_delegacioncorrectaseller.xml", ".del");
						//	//InformarDelegacionPorDefecto(sficheroin, sficheroout, oEmpresaMarmedsa.DEL);
						//	sTipoError = "moverErrorCambiarDelegacion";
						//	//Grabamos el bueno.
						//	XmlDocument documento = new XmlDocument();
						//	documento.Load(sFilein);
						//	documento.Save(sFileout);
						//	documento = null;

						//	//grabamos el fichero del
						//	documento = new XmlDocument();
						//	documento.Load(sficheroin);
						//	documento.Save(sficheroout);
						//	documento = null;
						//}
						//else
						//{
						//	XmlDocument documento = new XmlDocument();
						//	documento.Load(sFilein);
						//	documento.Save(sFileout);
						//	documento = null;

						//}
						//#endregion
						
						//Comprobamos que la delegación que nos viene en el buyer existe para la empresa
						//de marmedsa, si no existe ponemos la por defecto.
						oEmpresaMarmedsa = null;
						Boolean bExisteDelegacion = IsNoexistDelegacionEmpMarmedsa(sFileout,ref oEmpresaMarmedsa);
						sFilein = sFileout;
						sFileout = sFilein.Replace("_delegacioncorrectaseller.xml", ".xml");
						if (!bExisteDelegacion)
						{
							InformarDelegacionPorDefecto(sFilein, sFileout, oEmpresaMarmedsa.DEL);
						}
						else
						{
							XmlDocument documento = new XmlDocument();
							documento.Load(sFilein);
							documento.Save(sFileout);
							documento = null;

						}
						//#region check igic lo ponemos en check impuestos.
						////Comprobamos si el impuesto es IGIC para las delegaciones de Canarias
						////LPA - LAS PALMAS DE GRAN CANARIA
						////SCT - SANTA CRUZ DE TENERIFE
						////LPA - LAS PALMAS
						//Boolean bErrorIGIC = false;
						//if (IsDelegacionIGIC(sFileout))
						//	if (!IsTaxIGIC(sFileout)) bErrorIGIC = true;
						//if (IsTaxIGIC(sFileout))
						//	if (!IsDelegacionIGIC(sFileout)) bErrorIGIC = true;
						//if (bErrorIGIC)
						//{

						//	String sficheroin = sFileout;
						//	String sficheroout = sficheroin.Replace(".xml", ".igic");
						//	//La grabamos tal cual para que siga sin cambiar
						//	if (File.Exists(sficheroout)) File.Delete(sficheroout);
						//	XmlDocument documento = new XmlDocument();
						//	documento.Load(sficheroin);
						//	documento.Save(sficheroout);
						//	documento = null;
						//	sTipoError = "moverErrorIGICNOINFORMADO";
						//}
						//#endregion check igic lo ponemos en check impuestos.
						if (File.Exists(sFilein))
                        {

                            //Si la receptora es una entidad excluyente no la ponemos a procesar si no la movemos a CifExcluido.
                            if (IsCifReceptorExcluido(sFichero))
                            {
								moverAErrorExcluida(sFichero, sDirectorioMonitorizacion);
                                moverAErrorExcluida(sFicheroPdf, sDirectorioMonitorizacion);
								sTipoError = "moverAErrorExcluida";
							}
							//Si la receptora no es del grupo marmedsa, tratamos como error suplido
							else if (!bErrorSuplidos(sFichero))
								{
									moverAErrorSuplidos(sFichero, sDirectorioMonitorizacion);
									moverAErrorSuplidos(sFicheroPdf, sDirectorioMonitorizacion);
									sTipoError = "moverAErrorSuplidos";
								}
							else if (!CheckInvoiceGrossAmount(sFichero))
								{
									moverAErrorTotalFactura(sFichero, sDirectorioMonitorizacion);
									moverAErrorTotalFactura(sFicheroPdf, sDirectorioMonitorizacion);
									sTipoError = "moverAErrorTotalFactura";
								}
														//Copiamos los ficheros a la carpeta del toolkit
							CopiarArchivo(sFichero, sDirectorioFacturasenviar);
                            CopiarArchivo(sFicheroPdf, sDirectorioFacturasenviar);
							if (ResultadoProceso == null) ResultadoProceso = new ArrayList();
							ResultadoProceso.Add(sTipoError);

						}
						else //si no existe el fichero _conxml es que algo ha fallado.
                        {
                            moverAError(sFichero, sDirectorioMonitorizacion);
                            moverAError(sFicheroPdf, sDirectorioMonitorizacion);
							sTipoError = "moverAError";
							if (ResultadoProceso == null) ResultadoProceso = new ArrayList();
							ResultadoProceso.Add(sTipoError);

						}
					}
                    catch (Exception e)
                    {
						listaFilasCheckTotales.Add("ErrorProcesoFichero;;;;;;;;;;;;;;;" + sFichero);
					}
                }
                //Movemos todo a la subcarpetas procesadas.
                BorrarFacturasIntermedias(sDirectorioMonitorizacion);
                moverAProcesadas(sDirectorioMonitorizacion);
				//Creamos el fichero de resultado de chequear los totales.
				String sfile = sDirectorioMonitorizacion + "\\Resultado_chequeo_totales_" + DateTime.Now.ToString("yyyyMMddTHHmmssfff") + ".csv";
				CrearFicheroCSV(sfile, listaFilasCheckTotales);
				GenerarMailProceso(sfile);
			}


            Console.WriteLine("Fin proceso");

        }

        #region init vars
        private static void Init()
        {
            //Cargamos la lista de departamentos y ciudades.
            oCiudad = new ArrayList();
            oDepartamento = new ArrayList();
            oDepartamento.Add("AAE"); oCiudad.Add("ANNABA");
            oDepartamento.Add("ARG"); oCiudad.Add("ALGER");
            oDepartamento.Add("AZW"); oCiudad.Add("ARZEW");
            oDepartamento.Add("BJA"); oCiudad.Add("BEJAIA");
            oDepartamento.Add("DJE"); oCiudad.Add("Djen-Djen");
            oDepartamento.Add("MOS"); oCiudad.Add("MOSTAGANEM");
            oDepartamento.Add("ORN"); oCiudad.Add("ORAN");
            oDepartamento.Add("SKI"); oCiudad.Add("SKIKDA");
            oDepartamento.Add("BCN"); oCiudad.Add("BARCELONA");
            oDepartamento.Add("AGP"); oCiudad.Add("MALAGA");
            oDepartamento.Add("ALC"); oCiudad.Add("ALICANTE");
            oDepartamento.Add("ALD"); oCiudad.Add("ALCUDIA");
            oDepartamento.Add("ALG"); oCiudad.Add("ALGECIRAS");
            oDepartamento.Add("ALG"); oCiudad.Add("Puerto de Algeciras");
            oDepartamento.Add("BIO"); oCiudad.Add("BILBAO");
//            oDepartamento.Add("BIO"); oCiudad.Add("BIZKAIA");
            oDepartamento.Add("CAD"); oCiudad.Add("CADIZ");
            oDepartamento.Add("CAD"); oCiudad.Add("CÁDIZ");
            oDepartamento.Add("CAS"); oCiudad.Add("CASTELLON");
			oDepartamento.Add("CAS"); oCiudad.Add("CASTELLÓN");
			oDepartamento.Add("GIJ"); oCiudad.Add("GIJON");
            oDepartamento.Add("HUV"); oCiudad.Add("HUELVA");
            oDepartamento.Add("LCG"); oCiudad.Add("LA CORUÑA");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS DE GRAN CANARIA");
            oDepartamento.Add("MAD"); oCiudad.Add("MADRID");
            oDepartamento.Add("PMI"); oCiudad.Add("PALMA DE MALLORCA");
            oDepartamento.Add("SCT"); oCiudad.Add("SANTA CRUZ DE TENERIFE");
            oDepartamento.Add("SVQ"); oCiudad.Add("SEVILLA");
            oDepartamento.Add("TAR"); oCiudad.Add("TARRAGONA");
            oDepartamento.Add("VGO"); oCiudad.Add("VIGO");
            oDepartamento.Add("VLC"); oCiudad.Add("VALENCIA");
            oDepartamento.Add("CAR"); oCiudad.Add("CARTAGENA");
            oDepartamento.Add("LEI"); oCiudad.Add("ALMERIA");
            oDepartamento.Add("MOT"); oCiudad.Add("MOTRIL");
            oDepartamento.Add("SDR"); oCiudad.Add("SANTANDER");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS");
            oDepartamento.Add("LEH"); oCiudad.Add("LE HAVRE");
            oDepartamento.Add("MRS"); oCiudad.Add("MARSEILLE");
            oDepartamento.Add("MTU"); oCiudad.Add("MARTIGUES");
            oDepartamento.Add("PAR"); oCiudad.Add("PARIS");
            oDepartamento.Add("CSB"); oCiudad.Add("CASABLANCA");
            oDepartamento.Add("TNG"); oCiudad.Add("TANGIER");
            oDepartamento.Add("LIS"); oCiudad.Add("LISBOA");
            oDepartamento.Add("OPO"); oCiudad.Add("PORTO");
            oDepartamento.Add("SIE"); oCiudad.Add("SINES");
            oDepartamento.Add("SET"); oCiudad.Add("SETUBAL");
            oDepartamento.Add("OPO"); oCiudad.Add("OPORTO");
            //Cargamos la lista de empresas desde el excel.
            CargarListaEmpresas();
            CargarListaSuplidos();

            //Cargamos la lista de paises
            oPaises = new ArrayList();
            oPaises.Add("AF");
            oPaises.Add("AX");
            oPaises.Add("AL");
            oPaises.Add("DE");
            oPaises.Add("AD");
            oPaises.Add("AO");
            oPaises.Add("AI");
            oPaises.Add("AQ");
            oPaises.Add("AG");
            oPaises.Add("AN");
            oPaises.Add("SA");
            oPaises.Add("DZ");
            oPaises.Add("AR");
            oPaises.Add("AM");
            oPaises.Add("AW");
            oPaises.Add("AU");
            oPaises.Add("AT");
            oPaises.Add("AZ");
            oPaises.Add("BS");
            oPaises.Add("BH");
            oPaises.Add("BD");
            oPaises.Add("BB");
            oPaises.Add("BY");
            oPaises.Add("BE");
            oPaises.Add("BZ");
            oPaises.Add("BJ");
            oPaises.Add("BM");
            oPaises.Add("BT");
            oPaises.Add("BO");
            oPaises.Add("BA");
            oPaises.Add("BW");
            oPaises.Add("BV");
            oPaises.Add("BR");
            oPaises.Add("BN");
            oPaises.Add("BG");
            oPaises.Add("BF");
            oPaises.Add("BI");
            oPaises.Add("CV");
            oPaises.Add("KY");
            oPaises.Add("KH");
            oPaises.Add("CM");
            oPaises.Add("CA");
            oPaises.Add("CF");
            oPaises.Add("TD");
            oPaises.Add("CZ");
            oPaises.Add("CL");
            oPaises.Add("CN");
            oPaises.Add("CY");
            oPaises.Add("CX");
            oPaises.Add("VA");
            oPaises.Add("CC");
            oPaises.Add("CO");
            oPaises.Add("KM");
            oPaises.Add("CD");
            oPaises.Add("CG");
            oPaises.Add("CK");
            oPaises.Add("KP");
            oPaises.Add("KR");
            oPaises.Add("CI");
            oPaises.Add("CR");
            oPaises.Add("HR");
            oPaises.Add("CU");
            oPaises.Add("DK");
            oPaises.Add("DM");
            oPaises.Add("DO");
            oPaises.Add("EC");
            oPaises.Add("EG");
            oPaises.Add("SV");
            oPaises.Add("AE");
            oPaises.Add("ER");
            oPaises.Add("SK");
            oPaises.Add("SI");
            oPaises.Add("ES");
            oPaises.Add("UM");
            oPaises.Add("US");
            oPaises.Add("EE");
            oPaises.Add("ET");
            oPaises.Add("FO");
            oPaises.Add("PH");
            oPaises.Add("FI");
            oPaises.Add("FJ");
            oPaises.Add("FR");
            oPaises.Add("GA");
            oPaises.Add("GM");
            oPaises.Add("GE");
            oPaises.Add("GS");
            oPaises.Add("GH");
            oPaises.Add("GI");
            oPaises.Add("GD");
            oPaises.Add("GR");
            oPaises.Add("GL");
            oPaises.Add("GP");
            oPaises.Add("GU");
            oPaises.Add("GT");
            oPaises.Add("GF");
            oPaises.Add("GN");
            oPaises.Add("GQ");
            oPaises.Add("GW");
            oPaises.Add("GY");
            oPaises.Add("HT");
            oPaises.Add("HM");
            oPaises.Add("HN");
            oPaises.Add("HK");
            oPaises.Add("HU");
            oPaises.Add("IN");
            oPaises.Add("ID");
            oPaises.Add("IR");
            oPaises.Add("IQ");
            oPaises.Add("IE");
            oPaises.Add("IS");
            oPaises.Add("IL");
            oPaises.Add("IT");
            oPaises.Add("JM");
            oPaises.Add("JP");
            oPaises.Add("JO");
            oPaises.Add("KZ");
            oPaises.Add("KE");
            oPaises.Add("KG");
            oPaises.Add("KI");
            oPaises.Add("KW");
            oPaises.Add("LA");
            oPaises.Add("LS");
            oPaises.Add("LV");
            oPaises.Add("LB");
            oPaises.Add("LR");
            oPaises.Add("LY");
            oPaises.Add("LI");
            oPaises.Add("LT");
            oPaises.Add("LU");
            oPaises.Add("MO");
            oPaises.Add("MK");
            oPaises.Add("MG");
            oPaises.Add("MY");
            oPaises.Add("MW");
            oPaises.Add("MV");
            oPaises.Add("ML");
            oPaises.Add("MT");
            oPaises.Add("FK");
            oPaises.Add("MP");
            oPaises.Add("MA");
            oPaises.Add("MH");
            oPaises.Add("MQ");
            oPaises.Add("MU");
            oPaises.Add("MR");
            oPaises.Add("YT");
            oPaises.Add("MX");
            oPaises.Add("FM");
            oPaises.Add("MD");
            oPaises.Add("MC");
            oPaises.Add("MN");
            oPaises.Add("MS");
            oPaises.Add("MZ");
            oPaises.Add("MM");
            oPaises.Add("NA");
            oPaises.Add("NR");
            oPaises.Add("NP");
            oPaises.Add("NI");
            oPaises.Add("NE");
            oPaises.Add("NG");
            oPaises.Add("NU");
            oPaises.Add("NF");
            oPaises.Add("NO");
            oPaises.Add("NC");
            oPaises.Add("NZ");
            oPaises.Add("OM");
            oPaises.Add("NL");
            oPaises.Add("PK");
            oPaises.Add("PW");
            oPaises.Add("PS");
            oPaises.Add("PA");
            oPaises.Add("PG");
            oPaises.Add("PY");
            oPaises.Add("PE");
            oPaises.Add("PN");
            oPaises.Add("PF");
            oPaises.Add("PL");
            oPaises.Add("PT");
            oPaises.Add("PR");
            oPaises.Add("QA");
            oPaises.Add("GB");
            oPaises.Add("RE");
            oPaises.Add("RW");
            oPaises.Add("RO");
            oPaises.Add("RU");
            oPaises.Add("EH");
            oPaises.Add("SB");
            oPaises.Add("WS");
            oPaises.Add("AS");
            oPaises.Add("KN");
            oPaises.Add("SM");
            oPaises.Add("PM");
            oPaises.Add("VC");
            oPaises.Add("SH");
            oPaises.Add("LC");
            oPaises.Add("ST");
            oPaises.Add("SN");
            oPaises.Add("CS");
            oPaises.Add("SC");
            oPaises.Add("SL");
            oPaises.Add("SG");
            oPaises.Add("SY");
            oPaises.Add("SO");
            oPaises.Add("LK");
            oPaises.Add("SZ");
            oPaises.Add("ZA");
            oPaises.Add("SD");
            oPaises.Add("SE");
            oPaises.Add("CH");
            oPaises.Add("SR");
            oPaises.Add("SJ");
            oPaises.Add("TH");
            oPaises.Add("TW");
            oPaises.Add("TZ");
            oPaises.Add("TJ");
            oPaises.Add("IO");
            oPaises.Add("TF");
            oPaises.Add("TL");
            oPaises.Add("TG");
            oPaises.Add("TK");
            oPaises.Add("TO");
            oPaises.Add("TT");
            oPaises.Add("TN");
            oPaises.Add("TC");
            oPaises.Add("TM");
            oPaises.Add("TR");
            oPaises.Add("TV");
            oPaises.Add("UA");
            oPaises.Add("UG");
            oPaises.Add("UY");
            oPaises.Add("UZ");
            oPaises.Add("VU");
            oPaises.Add("VE");
            oPaises.Add("VN");
            oPaises.Add("VG");
            oPaises.Add("VI");
            oPaises.Add("WF");
            oPaises.Add("YE");
            oPaises.Add("DJ");
            oPaises.Add("ZM");
            oPaises.Add("ZW");
			oPaises.Add("EL");

			//Cargar lista delegaciones existentes por empresa.
			CargarListaDelegaciones();

		}

        private static void CargarListaEmpresas()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaEmpresasMarmedsa);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaEmpresas == null) aListaEmpresas = new ArrayList();
                            CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[1].ToString();
                            oEmpresa.CIF = rdrExcelData[2].ToString();
                            oEmpresa.DEL = rdrExcelData[3].ToString();
                            oEmpresa.PROV = rdrExcelData[4].ToString();
                            oEmpresa.PAIS = rdrExcelData[5].ToString();
                            oEmpresa.DIRECCION = rdrExcelData[6].ToString();
                            oEmpresa.CODIGOPOSTAL = rdrExcelData[7].ToString();
                            oEmpresa.MAIL = rdrExcelData[8].ToString();
                            aListaEmpresas.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas empresas.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }

        private static void CargarListaSuplidos()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaSuplidos);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaSuplidos == null) aListaSuplidos = new ArrayList();
                            CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[3].ToString();
                            oEmpresa.CIF = rdrExcelData[1].ToString();
                            aListaSuplidos.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas Suplidos.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }

		private static void CargarListaDelegaciones()
		{
			try
			{
				OleDbDataReader rdrExcelData = null;
				rdrExcelData = Excel.Excel.OpenExcelFile(sListaDelegaciones);
				if (rdrExcelData != null)
				{
					// Processa les dades recollides
					while (rdrExcelData.Read())
					{
						try
						{
							if (aListaDelegaciones == null) aListaDelegaciones = new ArrayList();
							CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa();
							oEmpresa.EMP = rdrExcelData[4].ToString();
							oEmpresa.NOM = rdrExcelData[1].ToString();
							oEmpresa.CIF = rdrExcelData[2].ToString();
							oEmpresa.DEL = rdrExcelData[3].ToString();
							aListaDelegaciones.Add(oEmpresa);
						}
						catch (Exception)
						{

						}
					}
					// Tanca la connexió a l'arxiu
					Excel.Excel.CloseExcelFile();
					Console.WriteLine("Cargada listas delegaciones.");
				}



			}
			catch (Exception e)
			{
				Excel.Excel.CloseExcelFile();
			}
		}
		#endregion

		#region Eliminar Firma
		private static void DeleteSignature(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2014/v3.2.1/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodo != null)
                {
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    XmlNode node1 = documento.SelectSingleNode("//Signature");
                    if (node1 == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                        xmlNsM.AddNamespace("ds", @"http://www.w3.org/2000/09/xmldsig#");
                        node1 = documento.SelectSingleNode("//ds:Signature", xmlNsM);
                    }
                    if (node1 != null)
                        nodo.RemoveChild(node1);
                    //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
                    documento.Save(sFileOut);
                    documento = null;
                }
                else
                {
                    documento.Save(sFileOut);
                    documento = null;

                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
        #endregion

        #region Quitar Lineas a 0
        private static void DeleteInvoiceLineZero(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    XmlDocument documento = new XmlDocument();
                    documento.Load(sFilein);
                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "ItemDescription")
                                    {
                                        if(oinvoiceline.InnerText.ToUpper()=="TOTAL")
                                        {

                                        }
                                    }
                                    if (oinvoiceline.Name == "UnitPriceWithoutTax")
                                    {
                                        if (isCero(oinvoiceline.InnerText))
                                        {
                                            nItems.RemoveChild(oNodo);
                                            documento.Save(sFilein);
                                            documento = null;
                                            bsalir = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }

        private static Boolean isCero(String value)
        {
            try
            {
                if (value.Equals("0.000000")) return true;
                else return false;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        #endregion

        #region Añadir AdministrativeCentres
        private static void AddAdministrativeCentreBuyer(String sFilein, String sFileOut, ref Boolean bSuplido)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                XmlNode nAddressInSpain = null;
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach(XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
								if (oNodoAddressInSpain.Name == "OverseasAddress")
								{
									//Cargamos al Adresspain para el nuevo nodo.
									nAddressInSpain = oNodoAddressInSpain;
								}
                            }
                        }
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oNodoTaxIdentification in oNodo)
                            {
                                if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;

								}
                            }
                        }
                        

                    }
                }

                //Antes de añadir todo, vemos si es suplido o no
                bSuplido = false;
                CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                bSuplido = !EsEmpresaMarmedsa(sTaxIdentificationNumber,ref  oEmpresa);
                if (!bSuplido) //Código anterior a suplidos.
                {
                    //Si no existe lo añadimos.
                    if (baddAdministrativeCentres)
                    {
                        nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                        Boolean binsertCentro = false;
                        if (nBuyerParty != null)
                        {
                            foreach (XmlNode oNodo in nBuyerParty)
                            {
                                //Para el sabadell si hay partyidentification
                                if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                {
                                    XmlNode nodo1 = oNodo;
                                    nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                    documento.Save(sFileout);
                                    documento = null;
                                    binsertCentro = true;
                                    break;
                                }
                            }
                            //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                            if (!binsertCentro)
                            {
                                nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                                if (nBuyerParty != null)
                                {
                                    foreach (XmlNode oNodo in nBuyerParty)
                                    {
                                        if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                        {
                                            XmlNode nodo1 = oNodo;
                                            nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                            documento.Save(sFileout);
                                            documento = null;
                                            binsertCentro = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
							//Cambio 13-03-2018
							//Proveedor Ricoh tiene los centros administrativos pero en el código no pone el valor correcto,
							//pone 1 y hay que cambiarlo, por eso los recorremos y los validamos.
							nBuyerParty = documento.SelectSingleNode("//BuyerParty");
							if (nBuyerParty != null)
							{
								String sCentreCode = "";
								foreach (XmlNode oNodo in nBuyerParty)
								{
									if (oNodo.Name == "AdministrativeCentres")
									{
										foreach (XmlNode oAdministrativeCentres in oNodo)
										{
											if (oAdministrativeCentres.Name == "AdministrativeCentre")
											{
												foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
												{
													if (oAdministrativeCentre.Name == "AddressInSpain")
													{
														foreach (XmlNode oAddressInSpain in oAdministrativeCentre)
														{
															if (oAddressInSpain.Name == "Town")
															{
																String scentrecodeTown = getCentreCode(oAddressInSpain.InnerText);
																if (scentrecodeTown != "") sCentreCode = scentrecodeTown;
															}
															if (oAddressInSpain.Name == "Province")
															{
																String scentrecodeprovince = getCentreCode(oAddressInSpain.InnerText);
																if (scentrecodeprovince != "" && sCentreCode == "") sCentreCode = scentrecodeprovince;

																//CentreCode.InnerText = getCentreCode(Province.InnerText);
															}

														}
													}
												}
											}
										}
										foreach (XmlNode oAdministrativeCentres in oNodo)
										{

											if (oAdministrativeCentres.Name == "AdministrativeCentre")
											{
												foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
												{
													if (oAdministrativeCentre.Name == "CentreCode")
													{
														String smyCentreCode = oAdministrativeCentre.InnerText.ToString().Trim();
														if (sCentreCode != "")
															if (sCentreCode != smyCentreCode)
															{
																oAdministrativeCentre.InnerText = sCentreCode;
															}
													}
												}
											}
										}
									}
								}
							}
							documento.Save(sFileout);
                            documento = null;
                        }
                    }
                    else
                    {
						//Cambio 13-03-2018
						//Proveedor Ricoh tiene los centros administrativos pero en el código no pone el valor correcto,
						//pone 1 y hay que cambiarlo, por eso los recorremos y los validamos.
						nBuyerParty = documento.SelectSingleNode("//BuyerParty");
						if (nBuyerParty != null)
						{
							String sCentreCode = "";
							foreach (XmlNode oNodo in nBuyerParty)
							{
								if (oNodo.Name == "AdministrativeCentres")
								{
									foreach (XmlNode oAdministrativeCentres in oNodo)
									{
										if (oAdministrativeCentres.Name == "AdministrativeCentre")
										{
											foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
											{
												if (oAdministrativeCentre.Name == "AddressInSpain")
												{
													foreach (XmlNode oAddressInSpain in oAdministrativeCentre)
													{
														if (oAddressInSpain.Name == "Town")
														{
															String scentrecodeTown = getCentreCode(oAddressInSpain.InnerText);
															if (scentrecodeTown != "") sCentreCode = scentrecodeTown;
														}
														if (oAddressInSpain.Name == "Province")
														{
															String scentrecodeprovince = getCentreCode(oAddressInSpain.InnerText);
															if (scentrecodeprovince != "" && sCentreCode =="") sCentreCode = scentrecodeprovince;

															//CentreCode.InnerText = getCentreCode(Province.InnerText);
														}

													}
												}
											}
										}
									}
									foreach (XmlNode oAdministrativeCentres in oNodo) 
									{
										
										if (oAdministrativeCentres.Name == "AdministrativeCentre")
										{
											foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
											{
												if (oAdministrativeCentre.Name == "CentreCode")
												{
														String smyCentreCode = oAdministrativeCentre.InnerText.ToString().Trim();
														if (sCentreCode != "")
															if (sCentreCode != smyCentreCode)
															{
																oAdministrativeCentre.InnerText = sCentreCode;
															}
												}
											}
										}
									}
								}
							}
						}
						documento.Save(sFileout);
                        documento = null;
                    }
                }
                else //es suplido
                {

                    CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oSuplido = null;
                    EsSuplido(sTaxIdentificationNumber, ref oSuplido);
                    if (oSuplido != null)
                    {
                        //Añadimos el centroadministrativo y modificamos los datos de la empresa suplida por la de Marmedsa.
                        CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpMar = GetEmpresaMarmedsa(oSuplido.EMP);
                        if (oEmpMar != null)
                        {
							
							
							nBuyerParty = documento.SelectSingleNode("//BuyerParty");
							Boolean binsertCentro = false;
							if (nBuyerParty != null)
                            {
								//Borramos los centros administrativos que vienen para poner el nuestro.
								foreach (XmlNode oNodo in nBuyerParty)
								{
									if (oNodo.Name == "AdministrativeCentres")
									{
										nBuyerParty.RemoveChild(oNodo);
									}
								}

								foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "TaxIdentification")
                                    {
                                        foreach (XmlNode oNodoTaxIdentification in oNodo)
                                        {
                                            if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoTaxIdentification.InnerText = oEmpMar.CIF;
												sTaxIdentificationNumberBuyer = oEmpMar.CIF;

											}
                                        }
                                    }
                                    //Para el sabadell si hay partyidentification
                                    if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentreSuplido(documento, oEmpMar), nodo1);
										binsertCentro = true;
										//documento.Save(sFileout);
										//documento = null;
										//break;
									}

								}
								//Si no existe el centro lo ponemos,ya que igual el PartyIdentification no existe
								if (!binsertCentro)
								{
									nBuyerParty = documento.SelectSingleNode("//BuyerParty");
									if (nBuyerParty != null)
									{
										foreach (XmlNode oNodo in nBuyerParty)
										{
											if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
											{
												XmlNode nodo1 = oNodo;
												nBuyerParty.InsertBefore(CrearNodo_AdministrativeCentreSuplido(documento, oEmpMar), nodo1);
												binsertCentro = true;
												break;
											}
											//Michel, para lematrans venían facturas Individual no de empresa y fallaba el crear el 
											//administrativecentre porque no lo tenemos en cuenta.
											if (oNodo.Name == "Individual")//Lo ponemos antes del Individual
											{
												XmlNode nodo1 = oNodo;
												nBuyerParty.InsertBefore(CrearNodo_AdministrativeCentreSuplido(documento, oEmpMar), nodo1);
												binsertCentro = true;
												break;
											}
										}
									}
								}
								//Miramos el legalEntity y cambiamos el overAddress
								Boolean bOverAddress = false;
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoLegalEntity.InnerText = oEmpMar.NOM;
                                            }
                                            if (oNodoLegalEntity.Name == "AddressInSpain")
                                            {
                                                foreach (XmlNode oNodoAddressInSpain in oNodoLegalEntity)
                                                {
                                                    if (oNodoAddressInSpain.Name == "Address")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.DIRECCION;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "PostCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.CODIGOPOSTAL;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Town")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Province")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "CountryCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PAIS;
                                                    }

                                                }


                                            }
                                            if (oNodoLegalEntity.Name == "OverseasAddress")
                                            {
                                                oNodo.RemoveChild(oNodoLegalEntity);
                                                bOverAddress = true;
                                            }
                                        }
                                    }
									if (oNodo.Name == "Individual")
									{
										foreach (XmlNode oNodoLegalEntity in oNodo)
										{
											if (oNodoLegalEntity.Name == "Name")
											{
												//Cargamos al Adresspain para el nuevo nodo.
												oNodoLegalEntity.InnerText = oEmpMar.NOM;
											}
											if (oNodoLegalEntity.Name == "AddressInSpain")
											{
												foreach (XmlNode oNodoAddressInSpain in oNodoLegalEntity)
												{
													if (oNodoAddressInSpain.Name == "Address")
													{
														oNodoAddressInSpain.InnerText = oEmpMar.DIRECCION;
													}
													if (oNodoAddressInSpain.Name == "PostCode")
													{
														oNodoAddressInSpain.InnerText = oEmpMar.CODIGOPOSTAL;
													}
													if (oNodoAddressInSpain.Name == "Town")
													{
														oNodoAddressInSpain.InnerText = oEmpMar.PROV;
													}
													if (oNodoAddressInSpain.Name == "Province")
													{
														oNodoAddressInSpain.InnerText = oEmpMar.PROV;
													}
													if (oNodoAddressInSpain.Name == "CountryCode")
													{
														oNodoAddressInSpain.InnerText = oEmpMar.PAIS;
													}

												}


											}
											if (oNodoLegalEntity.Name == "OverseasAddress")
											{
												oNodo.RemoveChild(oNodoLegalEntity);
												bOverAddress = true;
											}
										}
									}
									if (bOverAddress)
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {
   
                                                XmlNode nodo1 = oNodoLegalEntity;
                                                oNodo.InsertAfter(CrearNodo_AddressInSpain(documento, oEmpMar), nodo1);

                                            }
											if (oNodoLegalEntity.Name == "Name") //Individual
											{

												XmlNode nodo1 = oNodoLegalEntity;
												oNodo.InsertAfter(CrearNodo_AddressInSpain(documento, oEmpMar), nodo1);

											}
										}

                                    }

                                }

                            }
                            documento.Save(sFileout);
                            documento = null;
                        }
                    }
                }
                if (documento != null)
                {
                    documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        private static void AddAdministrativeCentreSeller(String sFilein, String sFileOut)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//SellerParty");
                XmlNode nAddressInSpain = null;
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        //Cargamos al Adresspain para el nuevo nodo.

                    }
                }
                //Si no existe lo añadimos.
                if (baddAdministrativeCentres)
                {
                    nBuyerParty = documento.SelectSingleNode("//SellerParty");
                    Boolean binsertCentro = false;
                    if (nBuyerParty != null)
                    {
                        foreach (XmlNode oNodo in nBuyerParty)
                        {
                            if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                            {
                                XmlNode nodo1 = oNodo;
                                nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                documento.Save(sFileout);
                                documento = null;
                                binsertCentro = true;
                                break;
                            }
                            //Cargamos al Adresspain para el nuevo nodo.

                        }
                        //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                        if (!binsertCentro)
                        {
                            nBuyerParty = documento.SelectSingleNode("//SellerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                        documento.Save(sFileout);
                                        documento = null;
                                        binsertCentro = true;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
					documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        //<AdministrativeCentres>
        //  <AdministrativeCentre>
        //    <CentreCode>BCN</CentreCode>
        //    <AddressInSpain>
        //      <Address>ZAL 2 - C/ NYEPA, 2-5</Address>
        //      <PostCode> 08820</PostCode>
        //      <Town> EL PRAT DE LLOBREGAT</Town>
        //      <Province></Province>
        //      <CountryCode>ESP</CountryCode>
        //    </AddressInSpain>
        //  </AdministrativeCentre>
        //</AdministrativeCentres>
        private static XmlElement CrearNodo_AdministrativeCentre(XmlDocument xmlDoc,XmlNode oAddressInSpain)
        {
            try
            {
				String sCentreCode = "";
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");
                
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");
                foreach (XmlNode oNodo in oAddressInSpain)
                {
                    if (oNodo.Name == "Address")
                    {
                        Address.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "PostCode")
                    {
                        PostCode.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Town")
                    {
                        Town.InnerText = oNodo.InnerText;
                        String scentrecodeTown = getCentreCode(Town.InnerText);
						if (scentrecodeTown != "")
						{
							CentreCode.InnerText = scentrecodeTown;
							sCentreCode = scentrecodeTown;
						}
                    }
                    if (oNodo.Name == "Province")
                    {
                        Province.InnerText = oNodo.InnerText;
                        String scentrecodeprovince = getCentreCode(Province.InnerText);
                        if (scentrecodeprovince != "" && sCentreCode == "") CentreCode.InnerText = scentrecodeprovince;

                        //CentreCode.InnerText = getCentreCode(Province.InnerText);
                    }
                    if (oNodo.Name == "CountryCode")
                    {
                        CountryCode.InnerText = oNodo.InnerText;
                    }
					if (oNodo.Name == "PostCodeAndTown")
					{
						try
						{
							PostCode.InnerText = oNodo.InnerText.Substring(0, 5);
							String sTown = oNodo.InnerText.Replace(PostCode.InnerText, "");
							sTown = sTown.Replace("-", "");
							Town.InnerText = sTown;

						}
						catch (Exception)
						{
						}
					}


				}



                
                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");
                
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private static XmlElement CrearNodo_AdministrativeCentreSuplido(XmlDocument xmlDoc, CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");

                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;
  

                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");

                CentreCode.InnerText = oEmpresa.DEL; //en las no suplidos lo cogemos según provincia de la receptora, aquí lo ponemos según tabla.
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static XmlElement CrearNodo_AddressInSpain(XmlDocument xmlDoc, CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;


                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);


                return AddressInSpain;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve el codigo de centro según provincia receptora
        /// </summary>
        /// <param name="sProvincia"></param>
        /// <returns></returns>
        private static String getCentreCode(String sProvincia)
        {
            try
            {
                String scentrecode = "";
                for(int i=0;i<oCiudad.Count;i++)
                {
                    String sciudad = oCiudad[i].ToString();
                    if (sProvincia.ToUpper().Trim() == sciudad.ToUpper().Trim())
                    {
                        scentrecode = oDepartamento[i].ToString();
                        break;
                    }
                    if (sProvincia.ToUpper().Trim().Contains(sciudad.ToUpper().Trim()))
                    {
                        scentrecode = oDepartamento[i].ToString();
                        break;
                    }

                }
                return scentrecode;
            }
            catch(Exception e)
            {
                return "";
            }
        }

		/// <summary>
		/// Devuelve la provincia según el codigo de centro receptor
		/// </summary>
		/// <param name="sCentreCode"></param>
		/// <returns></returns>
		private static String getProvincia(String sCentreCode)
		{
			try
			{
				String sprovincia = "";
				for (int i = 0; i < oDepartamento.Count; i++)
				{
					String scentrecode = oDepartamento[i].ToString();
					if (sCentreCode.ToUpper().Trim() == scentrecode.ToUpper().Trim())
					{
						sprovincia = oCiudad[i].ToString();
						break;
					}
					if (sCentreCode.ToUpper().Trim().Contains(scentrecode.ToUpper().Trim()))
					{
						sprovincia = oCiudad[i].ToString();
						break;
					}

				}
				return sprovincia;
			}
			catch (Exception e)
			{
				return "";
			}
		}

		private static Boolean EsEmpresaMarmedsa(String sCif,ref CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsempresaMarmedsa = false;
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsempresaMarmedsa = true;
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsempresaMarmedsa = true;
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsempresaMarmedsa = true;
                        break;
                    }



                }
                return bEsempresaMarmedsa;
            }
            catch (Exception e)
            {
                return bEsempresaMarmedsa;
            }
        }

        private static CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa GetEmpresaMarmedsa(String sEmp)
        {
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sEmp == oEmpresa.EMP)
                    {
                        return oEmpresa;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static Boolean EsSuplido(String sCif, ref CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsSuplido = false;
            try
            {
                for (int i = 0; i < aListaSuplidos.Count; i++)
                {
                    CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaSuplidos[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                }
                return bEsSuplido;
            }
            catch (Exception e)
            {
                return bEsSuplido;
            }
        }
        #endregion

        #region Añadir SequenceNumber a las lineas
        private static void AddSequenceNumberInvoiceLine(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    int sSecuencia = 1;
                    XmlDocument documento = new XmlDocument();
                    documento.Load(sFilein);
                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                Boolean bInsertarSeqNumber = true;
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "SequenceNumber")
                                    {
                                        bInsertarSeqNumber = false;
                                    }
                                }
                                if (bInsertarSeqNumber)
                                {
                                    foreach (XmlNode oinvoiceline in oNodo)
                                    {
                                        if (oinvoiceline.Name == "ItemDescription")
                                        {
                                            XmlNode nodo1 = oinvoiceline;
                                            oNodo.InsertBefore(CrearNodo_SequenceNumber(documento, sSecuencia), nodo1);
                                            documento.Save(sFilein);
                                            sSecuencia++;
                                        }
                                    }
                                }
                                else documento.Save(sFilein);
                            }
                        }

                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }



        private static XmlElement CrearNodo_SequenceNumber(XmlDocument xmlDoc,int iSecuencia)
        {
            try
            {
                XmlElement SequenceNumber = xmlDoc.CreateElement("SequenceNumber");
                SequenceNumber.InnerText = iSecuencia.ToString();

                return SequenceNumber;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir xml attachment
        private static Boolean AddAttachmentDocument_xml(String sFileFacturain, String sFilexml, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//AdditionalData");

                if (nodoAdd == null) //No existe el nodo AdditionalData
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoice");
                    String sxmlB64 = getFileB64(sFilexml);
                    nodo.AppendChild(CrearNodo_AdditionalData(xmlDoc, "NONE", "xml", "BASE64", sxmlB64));
                    xmlDoc.Save(sFileFacturaEout);
                }
                else
                {
                    XmlNode nodo1 = nodoAdd.FirstChild;
                    String sxmlB64 = getFileB64(sFilexml);
                    nodoAdd.InsertBefore(CrearNodo_RelatedDocuments(xmlDoc, "NONE", "xml", "BASE64", sxmlB64), nodo1);
                    xmlDoc.Save(sFileFacturaEout);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Crea el nodo AdditionalData con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_AdditionalData(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);


                XmlElement AdditionalData = xmlDoc.CreateElement("AdditionalData");
                AdditionalData.AppendChild(RelatedDocuments);

                return AdditionalData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Crea el nodo RelatedDocuments con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_RelatedDocuments(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);

                return RelatedDocuments;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir Extensiones
        private static Boolean AddExtension(String sFileFacturain, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);

                //Miramos si existe el nodo extensiones, ya que pueden venir las facturas desde digitalización
                Boolean bExisteExtensiones = false;
                XmlNode nodoExtensiones = xmlDoc.SelectSingleNode("//InvoiceExtensions");
                if (nodoExtensiones == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                    nodoExtensiones = xmlDoc.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                    if (nodoExtensiones != null) bExisteExtensiones = true;
                }
                else bExisteExtensiones = true;

                if (!bExisteExtensiones)
                {
                    XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Facturae");

                    if (nodoAdd == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                        xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                        nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                    }
                    if (nodoAdd == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                        xmlNsM.AddNamespace("m", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                        nodoAdd = xmlDoc.SelectSingleNode("//m:Facturae", xmlNsM);
                    }
                    if (nodoAdd == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                        xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2014/v3.2.1/Facturae");
                        nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                    }
                    if (nodoAdd == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                        xmlNsM.AddNamespace("m", @"http://www.facturae.es/Facturae/2014/v3.2.1/Facturae");
                        nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                    }
                    if (nodoAdd != null)
                    {
                        XmlNode nodo = xmlDoc.SelectSingleNode("//Invoices");
                        XmlNode onodo = xmlDoc.CreateElement("Extensions");
                        onodo.InnerXml = LeerFileExtension(sFileExtension);
                        //nodo.AppendChild(onodo);
                        //nodoAdd.AppendChild(onodo);
                        nodoAdd.InsertAfter(onodo, nodo);
                        xmlDoc.Save(sFileFacturaEout);
                    }
                    else xmlDoc.Save(sFileFacturaEout);
                }
                else xmlDoc.Save(sFileFacturaEout);
                //{"El nodo que desea insertar pertenece a otro contexto de documento."}
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static String LeerFileExtension(String sFileExtension)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileExtension);
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Extensions");
                if (nodoAdd != null)
                {
                    return nodoAdd.InnerXml;
                }
                else return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }

        #endregion

        #region CIF receptor Excluidos
        private static Boolean IsCifReceptorExcluido(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bCifExluido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < sCIFExcluidos.Length; i++)
                    {
                        if (sTaxIdentificationNumber == sCIFExcluidos[i])
                        { bCifExluido = true; break; }
                    }
                }
                
                return bCifExluido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
        #endregion

        #region Quitar espacios Referencia
        /// <summary>
        /// Quitamos espacios intermedios en blanco
        /// <BatchIdentifier>ESA8090318008 / 409 / 90</BatchIdentifier>
        /// <InvoiceNumber>08 / 409 / 90</InvoiceNumber>
        /// </summary>
        /// <param name="sFileFacturain"></param>
        /// <param name="sFileFacturaEout"></param>
        /// <returns></returns>
        private static Boolean QuitarEspaciosReferenciaFactura(String sFileFacturain, String sFileFacturaEout)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                documento.Load(sFileFacturain);
                XmlNode nBatchIdentifier = documento.SelectSingleNode("//BatchIdentifier");
                if (nBatchIdentifier != null)
                {
                    nBatchIdentifier.InnerText = nBatchIdentifier.InnerText.Replace(" ", "");
                }
                XmlNode nInvoiceNumber = documento.SelectSingleNode("//InvoiceNumber");
                if (nInvoiceNumber != null)
                {
					sInvoiceNumber = nInvoiceNumber.InnerText;
					nInvoiceNumber.InnerText = nInvoiceNumber.InnerText.Replace(" ", "");
                }
                documento.Save(sFileFacturaEout);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }

        #endregion

        #region Nodo Reimbursable para suplidos
        private static void AddNodeReimbursable(String sFilein, String sFileOut, Boolean bSuplido)
        {
            String sfileor = sFilein;
            XmlDocument documento = new XmlDocument();
            try
            {
                
                if (bSuplido)
                {
                    //Comprobamos si tiene o no los nodos.
                    
                    documento.Load(sFilein);
                    XmlNode InvoiceTotals = documento.SelectSingleNode("//InvoiceTotals");

                    String sInvoiceTotal = "0.00";
                    String sTotalExecutableAmount = "0.00";
                    foreach (XmlNode oNodo in InvoiceTotals)
                    {

                        if (oNodo.Name == "InvoiceTotal")
                        {
                            sInvoiceTotal = oNodo.InnerText;
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalGrossAmount")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalGrossAmountBeforeTaxes")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalOutstandingAmount")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalExecutableAmount")
                        {
                            sTotalExecutableAmount = oNodo.InnerText;
                        }

                    }
                    ////Los siguientes nodos deben informarse a cero.
                    //XmlNode InvoiceTotal = documento.SelectSingleNode("//InvoiceTotals//InvoiceTotal");

                    //if (InvoiceTotal != null)
                    //{
                    //    sInvoiceTotal = InvoiceTotal.InnerText;
                    //    InvoiceTotal.InnerText = "0.00";
                    //}

                    //XmlNode TotalGrossAmount = documento.SelectSingleNode("//InvoiceTotals//TotalGrossAmount");
                    //if (TotalGrossAmount != null) TotalGrossAmount.InnerText = "0.00";

                    //XmlNode TotalGrossAmountBeforeTaxes = documento.SelectSingleNode("//InvoiceTotals//TotalGrossAmountBeforeTaxes");
                    //if (TotalGrossAmountBeforeTaxes != null) TotalGrossAmountBeforeTaxes.InnerText = "0.00";

                    //XmlNode TotalOutstandingAmount = documento.SelectSingleNode("//InvoiceTotals//TotalOutstandingAmount");
                    //if (TotalOutstandingAmount != null) TotalOutstandingAmount.InnerText = "0.00";
                    //documento.Save(sFilein);


                    XmlNode InvoiceTotal = documento.SelectSingleNode("//InvoiceTotal");

                    if (InvoiceTotal != null)
                    {
                        sInvoiceTotal = InvoiceTotal.InnerText;
                        InvoiceTotal.InnerText = "0.00";
                    }
                    //Nodo ReimbursableExpenses
                    XmlNode ReimbursableExpenses = documento.SelectSingleNode("//ReimbursableExpenses");
                    if (ReimbursableExpenses == null) //Si no existe debemos crearlo.
                    {
                        XmlNode nodo1 = InvoiceTotal;
                        InvoiceTotals.InsertAfter(CrearNodo_ReimbursableExpenses(documento, sInvoiceTotal), nodo1);
                        documento.Save(sFilein);
                    }
                    XmlNode TotalExecutableAmount = documento.SelectSingleNode("//TotalExecutableAmount");
                    if (TotalExecutableAmount != null)
                    {
                        foreach (XmlNode oNodo in TotalExecutableAmount)
                        {

                            if (oNodo.Name == "TotalAmount")
                            {
                                sInvoiceTotal = oNodo.InnerText;
                            }
                        }
                    }
                    else sInvoiceTotal = sTotalExecutableAmount;
                    XmlNode TotalReimbursableExpenses = documento.SelectSingleNode("//TotalReimbursableExpenses");
                    if (TotalReimbursableExpenses == null)
                    {
                        //Se lo añadimos
                        InvoiceTotals.AppendChild(CrearNodo_TotalReimbursableExpenses(documento, sInvoiceTotal));
                        documento.Save(sFilein);
                    }
                    //< TotalReimbursableExpenses > 307.00 </ TotalReimbursableExpenses >
                }
                //Se graba igual aunque no tratemos el fichero como suplido
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                documento = null;
                sFilein = "";
            }


        }

        private static XmlElement CrearNodo_ReimbursableExpenses(XmlDocument xmlDoc, String sValor)
        {
            try
            {
                XmlElement ReimbursableExpensesAmount = xmlDoc.CreateElement("ReimbursableExpensesAmount");
                ReimbursableExpensesAmount.InnerText = sValor;

                

                XmlElement ReimbursableExpenses = xmlDoc.CreateElement("ReimbursableExpenses");
                ReimbursableExpenses.AppendChild(ReimbursableExpensesAmount);

                XmlElement ReimbursableExpenses1 = xmlDoc.CreateElement("ReimbursableExpenses");
                ReimbursableExpenses1.AppendChild(ReimbursableExpenses);

                return ReimbursableExpenses1;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        

        private static XmlElement CrearNodo_TotalReimbursableExpenses(XmlDocument xmlDoc, String sValor)
        {
            try
            {
                XmlElement TotalReimbursableExpenses = xmlDoc.CreateElement("TotalReimbursableExpenses");
                TotalReimbursableExpenses.InnerText = sValor;
                return TotalReimbursableExpenses;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region CIF con ES
        private static void AddPaisCIFSeller(String sFilein, String sFileOut)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nSellerParty = documento.SelectSingleNode("//SellerParty");
                String sPais = "";
                String sTaxIdentificationNumber = "";

                if (nSellerParty != null)
                {
                    foreach (XmlNode oNodo in nSellerParty)
                    {
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    foreach (XmlNode oNodoCountryCode in oNodoAddressInSpain)
                                    {
                                        if (oNodoCountryCode.Name == "CountryCode")
                                        {
                                            //El pais para el código de cif será solo los dos primeros
                                            try
                                            {
                                                sPais = oNodoCountryCode.InnerText.ToString().Substring(0, 2);
                                            }
                                            catch (Exception e)
                                            {
                                                sPais = "";
                                            }
                                        }
                                    }
                                }
								if (oNodoAddressInSpain.Name == "OverseasAddress")
								{
									foreach (XmlNode oNodoCountryCode in oNodoAddressInSpain)
									{
										if (oNodoCountryCode.Name == "CountryCode")
										{
											//El pais para el código de cif será solo los dos primeros
											try
											{
												sPais = oNodoCountryCode.InnerText.ToString().Substring(0, 2);
											}
											catch (Exception e)
											{
												sPais = "";
											}
										}
									}
								}
							}
                        }
                        if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                        {
                            foreach (XmlNode oTaxIdentificationNumber in oNodo)
                            {
                                if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")//Lo ponemos después del TaxIdentification
                                {
                                    sTaxIdentificationNumber = oTaxIdentificationNumber.InnerText.ToString();
									sTaxIdentificationNumberSeller = sTaxIdentificationNumber;
									break;
                                }
                            }

                        }

                    }
                    //Ahora revisamos si hay que cambiarlo, miramos primero si no es un pais y si no es el identificador de un pais, ponemos el pais.
                    if (!IsPais(sTaxIdentificationNumber.Substring(0, 2)) && !(sTaxIdentificationNumber.Substring(0,1).ToUpper().Equals("Z")))
                    {

                        if (!sTaxIdentificationNumber.Substring(0, 2).Equals(sPais))
                        {
                            sTaxIdentificationNumber = sPais + sTaxIdentificationNumber;
                            foreach (XmlNode oNodo in nSellerParty)
                            {
                                if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                                {
                                    foreach (XmlNode oTaxIdentificationNumber in oNodo)
                                    {
                                        if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")//Lo ponemos después del TaxIdentification
                                        {
                                            oTaxIdentificationNumber.InnerText = sTaxIdentificationNumber;
											sTaxIdentificationNumberSeller = sTaxIdentificationNumber;
											break;
                                        }
                                    }

                                }
                            }
                        }
                    }

                }
                //Grabamos el fichero.
                documento.Save(sFileout);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        private static void AddPaisCIFBuyer(String sFilein, String sFileOut)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sPais = "";
                String sTaxIdentificationNumber = "";

                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    foreach (XmlNode oNodoCountryCode in oNodoAddressInSpain)
                                    {
                                        if (oNodoCountryCode.Name == "CountryCode")
                                        {
                                            //El pais para el código de cif será solo los dos primeros
                                            try
                                            {
                                                sPais = oNodoCountryCode.InnerText.ToString().Substring(0, 2);
                                            }
                                            catch (Exception e)
                                            {
                                                sPais = "";
                                            }
                                        }
                                    }
                                }
								if (oNodoAddressInSpain.Name == "OverseasAddress")
								{
									foreach (XmlNode oNodoCountryCode in oNodoAddressInSpain)
									{
										if (oNodoCountryCode.Name == "CountryCode")
										{
											//El pais para el código de cif será solo los dos primeros
											try
											{
												sPais = oNodoCountryCode.InnerText.ToString().Substring(0, 2);
											}
											catch (Exception e)
											{
												sPais = "";
											}
										}
									}
								}
							}
                        }
                        if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                        {
                            foreach (XmlNode oTaxIdentificationNumber in oNodo)
                            {
                                if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")//Lo ponemos después del TaxIdentification
                                {
                                    sTaxIdentificationNumber = oTaxIdentificationNumber.InnerText.ToString();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
									break;
                                }
                            }
                           
                        }

                    }
                    //Ahora revisamos si hay que cambiarlo, miramos primero si no es un pais y si no es el identificador de un pais, ponemos el pais.
                    if (!IsPais(sTaxIdentificationNumber.Substring(0, 2)) && !(sTaxIdentificationNumber.Substring(0, 1).ToUpper().Equals("Z")))
                    {
                        if (!sTaxIdentificationNumber.Substring(0, 2).Equals(sPais))
                        {
                            sTaxIdentificationNumber = sPais + sTaxIdentificationNumber;
                            foreach (XmlNode oNodo in nBuyerParty)
                            {
                                if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                                {
                                    foreach (XmlNode oTaxIdentificationNumber in oNodo)
                                    {
                                        if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")//Lo ponemos después del TaxIdentification
                                        {
                                            oTaxIdentificationNumber.InnerText = sTaxIdentificationNumber;
											sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
											break;
                                        }
                                    }

                                }
                            }
                        }
                    }

                }
                //Grabamos el fichero.
                documento.Save(sFileout);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        /// <summary>
        /// Le pasamos las dos letras del cif y miramos si es de un pais, por ejemplo GB  y viene ESP en pais, para que no añada ESGB
        /// </summary>
        /// <param name="sDosLetras"></param>
        /// <returns></returns>
        private static Boolean IsPais(String sDosLetras)
        {
            Boolean bPais = false;
            try
            {
                for(int i=0;i<oPaises.Count;i++)
                {
                    if(oPaises[i].ToString().Equals(sDosLetras.ToUpper()))
                    {
                        bPais = true;
                        break;
                    }
                }
                return bPais;

            }
            catch(Exception)
            {
                return bPais;
            }
        }
        #endregion

        #region mail notificación
        private static void AddMailExtension(String sFilein, String sFileOut)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sPais = "";
                String sTaxIdentificationNumber = "";
                String sReceiverEmailAddresses = "";
                //Buscamos el TaxIdentificationNumber
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")//
                        {
                            foreach (XmlNode oTaxIdentificationNumber in oNodo)
                            {
                                if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentificationNumber.InnerText.ToString();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
									break;
                                }
                            }

                        }
                        //Miramos el mail si existe en el administrative centre
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            foreach (XmlNode oAdministrativeCentre in oNodo)
                            {
                                if (oAdministrativeCentre.Name == "AdministrativeCentre")
                                {
                                    foreach (XmlNode oContactDetails in oAdministrativeCentre)
                                    {
                                        if (oContactDetails.Name == "ContactDetails")
                                        {
                                            foreach(XmlNode oElectronicMail in oContactDetails)
                                            {
                                                if (oElectronicMail.Name == "ElectronicMail")
                                                {
                                                    sReceiverEmailAddresses = oElectronicMail.InnerText;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    //Ahora revisamos si lo tenemos (el mail)informado en nuestra tabla de empresa, si no, se quedará el de Mariona.
                    if (sReceiverEmailAddresses == "") sReceiverEmailAddresses = MailEmpresaMarmedsa(sTaxIdentificationNumber);

                    if (sReceiverEmailAddresses != "")
                    {
                        //si existe cambiamos el campo mail de la extension
                        try
                        {
                            XmlNode nodo = documento.SelectSingleNode("//InvoiceExtensions");

                            //Buscamos extension marmedsa
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("marmedsa", @"http://marmedsa.com/Factura_Extension.xsd");
                                nodo = documento.SelectSingleNode("//marmedsa:InvoiceExtensions", xmlNsM);
                            }
                            //si no lo encontramos buscamos ns4
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                                nodo = documento.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                            }

                            foreach (XmlNode onodoExtension in nodo)
                            {
                                foreach (XmlNode oReceiverEmailAddresses in onodoExtension)
                                {
                                    if (oReceiverEmailAddresses.Name == "EmailAdress")
                                    {
                                        oReceiverEmailAddresses.InnerText = sReceiverEmailAddresses;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }

                    }


                }
                //Grabamos el fichero.
                documento.Save(sFileout);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        /// <summary>
        /// Devuelve el mail de la empresa para la notificación.
        /// </summary>
        /// <param name="sCif"></param>
        /// <returns></returns>
        private static String MailEmpresaMarmedsa(String sCif)
        {
            String sMailEmpresaMarmedsa = "";
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                }
                return sMailEmpresaMarmedsa;
            }
            catch (Exception e)
            {
                return sMailEmpresaMarmedsa;
            }
        }
        #endregion

        #region Tratamiento Ficheros
        private static string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {
                string directorioDestino = "";
                bool OK = false;


                //myLog.WriteEntry("Los ficheros se procesan en: "+directorioDestino,EventLogEntryType.Information);	

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                //Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
                if (arrayFicheros.Length > 0)
                {
                    DateTime dt = DateTime.Now;
                    string fecha, hora;

                    //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    hora = dt.ToLongTimeString().Replace(":", "_");
                    string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                }
				//Al incorporar a las A.P. de Vigo y Castellón, estos envían ficheros xsig.
				//debemos renombrarlos a xml.
				for (int i = 0; i < arrayFicheros.Length; i++)
				{

					if (arrayFicheros[i].Extension.Equals(".xsig"))
					{
						String filexml = arrayFicheros[i].Name.Replace(".xsig", ".xml");
						MoverFichero(directorioMonitorizacion + arrayFicheros[i], directorioMonitorizacion + filexml);
					}
				}
				arrayFicheros = directInfo.GetFiles();
				
				// Al procesar el nuevo archivo incluido en el directorio especificado
				// debemos copiarlo al directorio temporal destinado para ello
				for (int i = 0; i < arrayFicheros.Length; i++)
                {
				
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            File.Delete(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }
                return directorioDestino;
            }
            catch (Exception e)
            {
                return "";
            }
        }


		private static string procesarDirectorios(string directorioMonitorizacion)
		{
			try
			{
				string directorioDestino = "";
				bool OK = false;


				String[] subdirectInfo = Directory.GetDirectories(directorioMonitorizacion,"*marmedsa*");
				for (int j = 0; j < subdirectInfo.Length; j++)
				{
					DirectoryInfo directInfo = new DirectoryInfo(subdirectInfo[j]);
					FileInfo[] arrayFicheros = directInfo.GetFiles();

					//Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
					if (arrayFicheros.Length > 0)
					{
						DateTime dt = DateTime.Now;
						string fecha, hora;

						//fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
						fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
						hora = dt.ToLongTimeString().Replace(":", "_");
						string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
						if (Directory.Exists(pathLog))
						{
							directorioDestino = pathLog;
						}
						else
						{
							directorioDestino = Directory.CreateDirectory(pathLog).FullName;
						}
					}
					//Al incorporar a las A.P. de Vigo y Castellón, estos envían ficheros xsig.
					//debemos renombrarlos a xml.
					for (int i = 0; i < arrayFicheros.Length; i++)
					{

						if (arrayFicheros[i].Extension.Equals(".xsig"))
						{
							//Miramos antes de dejar solo el nombre del fichero "id del email - el numero de adjunto - el nombre del fichero"
							String fileidmail = Path.GetFileName(arrayFicheros[i].Name);
							String filexml = arrayFicheros[i].Name;
							try
							{
								String[] afileidmail = fileidmail.Split('-');
								//puede tener muchos "-" quitamos los idmail y contador solo
								String squitar = afileidmail[0] + "-" + afileidmail[1] + "-";
								filexml = fileidmail.Replace(squitar, "");
							}
							catch (Exception)
							{
							}
							filexml = filexml.Replace(".xsig", ".xml");
							MoverFichero(subdirectInfo[j] + "\\" + arrayFicheros[i], directorioDestino + filexml);
						}
					}
					arrayFicheros = directInfo.GetFiles();

					// Al procesar el nuevo archivo incluido en el directorio especificado
					// debemos copiarlo al directorio temporal destinado para ello
					for (int i = 0; i < arrayFicheros.Length; i++)
					{
						//Miramos antes de dejar solo el nombre del fichero "id del email - el numero de adjunto - el nombre del fichero"
						String filexml = arrayFicheros[i].Name;
						String fileidmail = arrayFicheros[i].Name;
						try
						{
							String[] afileidmail = filexml.Split('-');
							//puede tener muchos "-" quitamos los idmail y contador solo
							String squitar = afileidmail[0] + "-" + afileidmail[1] + "-";
							filexml = fileidmail.Replace(squitar, "");
							//filexml = afileidmail[2];
						}
						catch (Exception)
						{
						}
						if (File.Exists(directorioDestino + filexml))
						{
							if (File.Equals(subdirectInfo[j] + "\\" + filexml, directorioDestino + arrayFicheros[i]))
							{

							}
							else
							{
								File.Delete(directorioDestino + filexml);
								MoverFichero(subdirectInfo[j] + "\\" + fileidmail, directorioDestino + filexml);
								
							}
						}
						else
						{
							MoverFichero(subdirectInfo[j] + "\\" + fileidmail, directorioDestino + filexml);
						}
					}
				}


				return directorioDestino;
			}
			catch (Exception e)
			{
				return "";
			}
		}

		private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
				//cambiamos ".XML" a ".xml"
				ficheroDestino = ficheroDestino.Replace(".XML", ".xml");
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static bool CrearFicheroOriginal(string sfile)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                String sFicheroOriginal = sfile.Replace(".xml", "_ORIGINAL.xml");
                if (!(sFicheroOriginal.IndexOf("_ORIGINAL")>0))  sFicheroOriginal = sfile.Replace(".XML", "_ORIGINAL.xml");
                File.Copy(sfile, sFicheroOriginal, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// Devuelve el fichero pdf en B64
        /// </summary>
        /// <param name="sfilePdf"></param>
        /// <returns></returns>
        private static String getFileB64(String sfilePdf)
        {
            try
            {
                String sBase64 = "";
                sBase64 = Convert.ToBase64String(readBinaryFile(sfilePdf));
                return sBase64;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve byte[] del fichero a leer
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }

        /// <summary>
        /// Movemos a procesadas para no crearlas en los siguientes procesos.
        /// </summary>
        private static void moverAProcesadas(String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioEnviada = sDirectorioMonitorizacion + "Procesadas";
                Directory.CreateDirectory(sDirectorioEnviada);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
				//Mejora, solo movemos pdf, xml y zip, los csv los dejamos.
                foreach (var fi in di.GetFiles("*.pdf"))
                {
                    String sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }
				foreach (var fi in di.GetFiles("*.xml"))
				{
					String sfileout = sDirectorioEnviada + "\\" + fi.Name;
					MoverFichero(fi.FullName, sfileout);
				}
				foreach (var fi in di.GetFiles("*.zip"))
				{
					String sfileout = sDirectorioEnviada + "\\" + fi.Name;
					MoverFichero(fi.FullName, sfileout);
				}
				foreach (var fi in di.GetFiles("*.del"))
				{
					String sfileout = sDirectorioEnviada + "\\" + fi.Name;
					MoverFichero(fi.FullName, sfileout);
				}
				foreach (var fi in di.GetFiles("*.igic"))
				{
					String sfileout = sDirectorioEnviada + "\\" + fi.Name;
					MoverFichero(fi.FullName, sfileout);
				}
			}
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Movemos a error para no procesarlas ni enviarlas
        /// </summary>
        private static void moverAError(String sFile,String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "Error\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Movemos a Excluidas las facturas que son de cif exluidos.
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorExcluida(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "CifExcluidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Si no encontramos el suplido las movemos a error para no enviar a facturación
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorSuplidos(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "ErrorCifSuplidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

		private static void BorrarFacturasIntermedias(String sDirectorioMonitorizacion)
		{
			try
			{
				DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
				foreach (var fi in di.GetFiles("*_sinfirma.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_conceros.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_conadmcentre.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_sincero.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_conSequenceNumber.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_conxml.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_conespacios.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_Reimbursable.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_ISO2Buyer.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_ISO2Seller.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_Mailextension.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_version32.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				
				foreach (var fi in di.GetFiles("*_delegacioncorrectaseller.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*_delegacioncorrecta.xml"))
				{
					BorrarFichero(fi.FullName);
				}
				foreach (var fi in di.GetFiles("*.and"))
				{
					BorrarFichero(fi.FullName);
				}
			}
			catch (Exception)
			{

			}
		}

		private static void BorrarFichero(string sFile)
		{
			try
			{
				File.Delete(sFile);
			}
			catch (Exception e)
			{
			}
		}
        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
				if (File.Exists(sFileOut)) File.Delete(sFileOut);
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }

		/// <summary>
		/// Si el total factura no coincide con el calculo de totales por linea da error.
		/// Controlamos los ivas.
		/// </summary>
		/// <param name="sFile"></param>
		/// <param name="sDirectorioMonitorizacion"></param>
		private static void moverAErrorTotalFactura(String sFile, String sDirectorioMonitorizacion)
		{
			try
			{
				//Primero borramos las facturas intermedias
				String sDirectorioError = sDirectorioMonitorizacion + "ErrorTotalFacturas\\";
				Directory.CreateDirectory(sDirectorioError);
				DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
				String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
				CopiarArchivo(sFile, sDirectorioError);

			}
			catch (Exception)
			{
			}
		}
		#endregion

		#region Tratamiento ficheros Transportes Salgar 
		//Buenas David, estamos revisando la mejor forma de tratar estos ficheros, 
		//    y según lo que comenta el proveedor, los ficheros siempre seran el xml 
		//    acabado en la referencia de la factura de 5 caracteres(A4957) y el pdf 
		//    tendrá lo mismo pero un “_” entre la “A” o “B” y el número no?

		//        Hola Amparo.
		//Una pregunta, a ver si se la puedes trasladar al informático.
		//¿Los nombres de los ficheros son siempre más o menos iguales?
		//Es decir, nos enviáis los ficheros con estos nombres:
		//                022017A4957.xml
		//                Factura_ A_4957.pdf
		//Donde en verde señalo lo que parece la referencia al número de factura.
		//Gracias por la ayuda.
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//Buenos días David.
		//Si, lo que me has señalado en verde es el número de factura.
		//La A es la serie por el puerto de Valencia y el número de factura.
		//En la última que te he enviado la B1053, la B es la serie del puerto de Algeciras y el número de factura por dicha serie.

		//Es decir Factura_ A_4957.pdf se convertirá en 022017A4957.xml
		private static String RenombrarPDFTransportesSargal(String sfilexml, String sfilepdf, String sdirectorio)
        {
			try
			{
				String sFile = "";
				String sReferencia = "";
				sReferencia = sfilexml.Replace(".xml", "").Replace(".XML", "").Replace(sdirectorio,"");

				sReferencia = ReferenciaFacturaSalgarv1(sReferencia);
				//sReferencia = sReferencia.Substring(sReferencia.Length-5,5);
				//sFile = sdirectorio + "Factura_ " + sReferencia.Substring(0, 1) + "_" + sReferencia.Substring(1, 4) + ".pdf";
				//nombre pdf es Factura_ A_5658.pdf y el xml es 182018A5658.xml
				string[] fileNames = Directory.GetFiles(sdirectorio, "*Factura_ " + sReferencia+ ".pdf");
				if (fileNames.Length == 1)
				{
					sFile = fileNames[0];
					MoverFichero(sFile, sfilepdf);
				}
				else
				{
					//Tenemos otra opción que es que la referencia esté al principio sin "_"
					//nombre pdf es R553.- NYK (A4020).pdf y xml es 182018R553.xml
					sReferencia = sfilexml.Replace(".xml", "").Replace(".XML", "").Replace(sdirectorio, "");
					sReferencia = ReferenciaFacturaSalgarv2(sReferencia);
					fileNames = Directory.GetFiles(sdirectorio, sReferencia + "*.pdf");
					if (fileNames.Length == 1)
					{
						sFile = fileNames[0];
						MoverFichero(sFile, sfilepdf);
					}
				}
				return sfilepdf;
			}
			catch (Exception e)
			{
				return "";
			}

        }

		/// <summary>
		/// nombre pdf es Factura_ A_5658.pdf y el xml es 182018A5658.xml
		/// </summary>
		/// <param name="sreferencia"></param>
		/// <returns></returns>
		private static String ReferenciaFacturaSalgarv1(String sreferencia)
		{
			String sRef = "";
			try
			{
				Boolean bSalir = false;
				int i = sreferencia.Length-1;
				do
				{

					String scaracter = sreferencia.Substring(i, 1);
					if (EsLetraMayuscula(scaracter))
					{
						sRef = scaracter + "_" + sRef;
						bSalir = true;
					}
					else
					{
						switch (scaracter)
						{
							case "_":
								sRef = scaracter + sRef;
								break;
							default:
								sRef = scaracter + sRef;
								break;
						}
					}
					i--;
				}
				while (!bSalir);
				return sRef;
			}
			catch (Exception e)
			{
				return sRef;
			}
		}

		/// <summary>
		/// nombre pdf es R553.- NYK (A4020).pdf y xml es 182018R553.xml
		/// </summary>
		/// <param name="sreferencia"></param>
		/// <returns></returns>
		private static String ReferenciaFacturaSalgarv2(String sreferencia)
		{
			String sRef = "";
			try
			{
				Boolean bSalir = false;
				int i = sreferencia.Length - 1;
				do
				{

					String scaracter = sreferencia.Substring(i, 1);
					if (EsLetraMayuscula(scaracter))
					{
						sRef = scaracter + sRef;
						bSalir = true;
					}
					else
					{
						switch (scaracter)
						{
							case "_":
								sRef = scaracter + sRef;
								break;
							default:
								sRef = scaracter + sRef;
								break;
						}
					}
					i--;
				}
				while (!bSalir);
				return sRef;
			}
			catch (Exception e)
			{
				return sRef;
			}
		}

		private static Boolean EsLetraMayuscula(String sCaracter)
		{
			try
			{
				String sAcaracter = "A";
				Byte bAcaracter = Encoding.ASCII.GetBytes(sAcaracter)[0];
				String sZcaracter = "Z";
				Byte bZcaracter = Encoding.ASCII.GetBytes(sZcaracter)[0];
				Byte bCaracter = Encoding.ASCII.GetBytes(sCaracter)[0];
				if (bCaracter >= bAcaracter && bCaracter <= bZcaracter) return true;
				else return false;
			}
			catch (Exception e)
			{
				return false;
			}
		}
		#endregion

		#region Tratamiento fichero Dyatrans Logistic
		//Hola.

		//Una pregunta, no podéis aplicar algún tipo de regla para este proveedor? Porque el número de factura parece coincidir en la primera parte del nombre del fichero ".xsig".

		//Gracias!
		//DRA
		//18101407.pdf
		//18101407_B61162798_A17000704.xsig
		private static String RenombrarPDFDyatransLogistic(String sfilexml, String sfilepdf, String sdirectorio)
		{
			try
			{
				String sFile = "";
				String sNombreFile = "";
				sNombreFile = sfilexml.Replace(".xml", "").Replace(".XML", "").Replace(sdirectorio, "");
				String[] sReferencia = sNombreFile.Split('_');
				string[] fileNames = Directory.GetFiles(sdirectorio, sReferencia[0] + ".pdf");
				if (fileNames.Length == 1)
				{
					sFile = fileNames[0];
					MoverFichero(sFile, sfilepdf);
				}
				return sfilepdf;
			}
			catch (Exception e)
			{
				return "";
			}
		}
		#endregion

		#region Error Suplidos (el cif receptor no es de marmedsa y no hemos encontrado el suplido en la tabla de suplidos)
		private static Boolean bErrorSuplidos(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bokSuplido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
								}
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < aListaEmpresas.Count; i++)
                    {
                        CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                        oEmpresa = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                        if (sTaxIdentificationNumber.IndexOf(oEmpresa.CIF) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.IndexOf(sTaxIdentificationNumber) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.Equals(sTaxIdentificationNumber))
                        {
                            bokSuplido = true;
                            break;
                        }
                    }
                }

                return bokSuplido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
		#endregion


		#region Generar mail de resultado

		/// <summary>
		/// Envía el mail del informe del proceso.
		/// </summary>
		private static void GenerarMailProceso(String sFicheroAdjunto)
		{
			try
			{
				//Creamos el cuerpo del mensaje
				String sCuerpo = "";
				if (ResultadoProceso != null)
				{
					sCuerpo = "Facturas Procesadas: " + ResultadoProceso.Count.ToString();
					sCuerpo = sCuerpo + "<p></p>" + "<p></p>";
					for (int i = 0; i < ResultadoProceso.Count; i++)
					{
						GenerarCuerpoMensaje(ResultadoProceso[i].ToString());
					}
					for (int i = 0; i < errorInforme.Count; i++)
					{
						sCuerpo = sCuerpo + errorInforme[i].ToString() + " - " + intInforme[i].ToString();
						sCuerpo = sCuerpo + "<p></p>";
					}
				}
				String sAsunto = "Informe Facturas Proveedores Extandar Marmedsa " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo, sFicheroAdjunto);
			}
			catch (Exception e)
			{
			}
		}

		/// <summary>
		/// Genera el mensaje con la cantidad de errores.
		/// </summary>
		/// <param name="sMensaje"></param>
		private static void GenerarCuerpoMensaje(String sMensaje)
		{
			try
			{
				if (errorInforme == null)
				{
					errorInforme = new ArrayList();
					intInforme = new ArrayList();
					errorInforme.Add(sMensaje);
					intInforme.Add(1);
				}
				else
				{
					Boolean bNew = true;
					for (int i = 0; i < errorInforme.Count; i++)
					{
						if (errorInforme[i].Equals(sMensaje))
						{
							intInforme[i] = (int)intInforme[i] + 1;
							bNew = false;
							break;
						}
					}
					if (bNew)
					{
						errorInforme.Add(sMensaje);
						intInforme.Add(1);
					}
				}

			}
			catch (Exception e)
			{
			}

		}
        #endregion

        #region Pasar de 3.2.1 a 3.2
        private static void UpdateVersion32(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2014/v3.2.1/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodo != null)
                {
                    //es versión 3.2.1
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    String sInnerXML = documento.InnerXml;
                    sInnerXML = sInnerXML.Replace("http://www.facturae.es/Facturae/2014/v3.2.1/Facturae", "http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    sInnerXML = sInnerXML.Replace("<SchemaVersion>3.2.1</SchemaVersion>", "<SchemaVersion>3.2</SchemaVersion>");
                    documento.InnerXml = sInnerXML;
                    documento.Save(sFileOut);
                    documento = null;
                }
                else
                {
                    documento.Save(sFileOut);
                    documento = null;

                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
		#endregion

		#region Validar delegación existe en empresa marmedsa
		
		private static Boolean IsNoexistDelegacionEmpMarmedsa(String sfile, ref CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
		{
			XmlDocument documento = new XmlDocument();
			try
			{
				Boolean bExisteDelegacion = false;
				documento.Load(sFilein);
				XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
				String sTaxIdentificationNumber = "";
				CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
				String smyCentreCode = "";
				if (nBuyerParty != null)
				{
					foreach (XmlNode oNodo in nBuyerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCode = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}

						}

						if (oNodo.Name == "TaxIdentification")
						{
							foreach (XmlNode oNodoTaxIdentification in oNodo)
							{
								if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
								{
									//Cargamos al Adresspain para el nuevo nodo.
									sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
									EsEmpresaMarmedsa(sTaxIdentificationNumber, ref oEmpresa);
								}
							}
						}
					}
				}

				if (oEmpresa != null)
				{
					oEmpresaMarmedsa = oEmpresa;
					if (smyCentreCode != "")
					{
						bExisteDelegacion = ExisteDelegacionEnEmpresa(oEmpresa.EMP, smyCentreCode);
					}
				}
				return bExisteDelegacion;
			}
			catch (Exception e)
			{
				return false;
			}
			finally
			{
				documento = null;
			}

		}

		private static Boolean ExisteDelegacionEnEmpresa(String sEmp, String sDel)
		{
			try
			{
				Boolean bExiste = false;
				CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oDelegacion = null;
				for (int i = 0; i < aListaDelegaciones.Count; i++)
				{
					oDelegacion = (CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa)aListaDelegaciones[i];
					if (oDelegacion.EMP == sEmp && oDelegacion.DEL == sDel)
					{
						bExiste = true;
						break;
					}
				}
				return bExiste;
			}
			catch (Exception)
			{
				return false;
			}
			
		}

		private static void InformarDelegacionPorDefecto(String sFilein, String sFileOut, String sDelegacionPorDefecto)
		{
			XmlDocument documento = new XmlDocument();
			try
			{
				Boolean bExisteDelegacion = false;
				documento.Load(sFilein);
				XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
				if (nBuyerParty != null)
				{
					foreach (XmlNode oNodo in nBuyerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											oAdministrativeCentre.InnerText = sDelegacionPorDefecto;
											break;
										}

									}
								}
							}

						}
					}
				}
				documento.Save(sFileOut);
			}
			catch (Exception e)
			{
			}
			finally
			{
				documento = null;
			}

		}

		/// <summary>
		/// Comprobamos si debemos cambiar la delegación receptora por la emisora
		/// </summary>
		/// <param name="sfile"></param>
		/// <param name="oEmpresaMarmedsa"></param>
		/// <returns></returns>
		private static Boolean IsChangeDelegacionEmpMarmedsa(String sfile, ref CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
		{
			XmlDocument documento = new XmlDocument();
			try
			{
				Boolean bCambiarDelegacion = false;
				documento.Load(sFilein);

				//Cargamos la delegación del Seller
				XmlNode nSellerParty = documento.SelectSingleNode("//SellerParty");
				String smyCentreCodeSeller = "";
				if (nSellerParty != null)
				{
					foreach (XmlNode oNodo in nSellerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCodeSeller = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}
						}
					}
				}

				XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
				String sTaxIdentificationNumber = "";
				CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
				String smyCentreCodeBuyer = "";
				if (nBuyerParty != null)
				{
					foreach (XmlNode oNodo in nBuyerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCodeBuyer = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}

						}

						if (oNodo.Name == "TaxIdentification")
						{
							foreach (XmlNode oNodoTaxIdentification in oNodo)
							{
								if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
								{
									//Cargamos al Adresspain para el nuevo nodo.
									sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
									EsEmpresaMarmedsa(sTaxIdentificationNumber, ref oEmpresa);
								}
							}
						}
					}
				}

				if (oEmpresa != null)
				{
					oEmpresaMarmedsa = oEmpresa;
					if (smyCentreCodeBuyer != "")
					{
						Boolean bExisteDelegacionBuyer = ExisteDelegacionEnEmpresa(oEmpresa.EMP, smyCentreCodeBuyer);
						if (smyCentreCodeSeller != "")
						{
							Boolean bExisteDelegacionSeller = ExisteDelegacionEnEmpresa(oEmpresa.EMP, smyCentreCodeSeller);
							if ((smyCentreCodeSeller != smyCentreCodeBuyer) && bExisteDelegacionSeller)
							{
								oEmpresa.DEL = smyCentreCodeSeller;
								//Compramos que sea un suplido
								if (ValueTotalReimbursableExpenses(documento) > 0)	bCambiarDelegacion = true;
								bCambiarDelegacion = true; //para comprobar las que hemos vuelto a cagar.							
							}
						}

						
					}
				}
				return bCambiarDelegacion;
			}
			catch (Exception e)
			{
				return false;
			}
			finally
			{
				documento = null;
			}

		}



		private static Boolean CheckChangeDelegacionEmpMarmedsa(String sfile, ref String sDelEmisora,ref String sDelReceptora)
		{
			XmlDocument documento = new XmlDocument();
			try
			{
				Boolean bCambiarDelegacion = false;
				documento.Load(sFilein);

				//Cargamos la delegación del Seller
				XmlNode nSellerParty = documento.SelectSingleNode("//SellerParty");
				String smyCentreCodeSeller = "";
				if (nSellerParty != null)
				{
					foreach (XmlNode oNodo in nSellerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCodeSeller = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}
						}
					}
				}

				XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
				String sTaxIdentificationNumber = "";
				CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
				String smyCentreCodeBuyer = "";
				if (nBuyerParty != null)
				{
					foreach (XmlNode oNodo in nBuyerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCodeBuyer = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}

						}

						if (oNodo.Name == "TaxIdentification")
						{
							foreach (XmlNode oNodoTaxIdentification in oNodo)
							{
								if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
								{
									//Cargamos al Adresspain para el nuevo nodo.
									sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
									sTaxIdentificationNumberBuyer = sTaxIdentificationNumber;
									EsEmpresaMarmedsa(sTaxIdentificationNumber, ref oEmpresa);
								}
							}
						}
					}
				}

				if (oEmpresa != null)
				{
					if (smyCentreCodeBuyer != "")
					{
						Boolean bExisteDelegacionBuyer = ExisteDelegacionEnEmpresa(oEmpresa.EMP, smyCentreCodeBuyer);
						if (smyCentreCodeSeller != "")
						{
							Boolean bExisteDelegacionSeller = ExisteDelegacionEnEmpresa(oEmpresa.EMP, smyCentreCodeSeller);
							if ((smyCentreCodeSeller != smyCentreCodeBuyer) && bExisteDelegacionSeller)
							{
								//Compramos que sea un suplido
								if (ValueTotalReimbursableExpenses(documento) > 0) bCambiarDelegacion = true;
								//bCambiarDelegacion = true; //para comprobar las que hemos vuelto a cagar.							
							}
						}


					}
				}
				sDelEmisora = smyCentreCodeSeller;
				sDelReceptora = smyCentreCodeBuyer;
				return bCambiarDelegacion;
			}
			catch (Exception e)
			{
				return false;
			}
			finally
			{
				documento = null;
			}

		}
		#endregion Validar delegación existe en empresa marmedsa

		#region Validar si el total de la factura es correcto
		///la idea es detectar que faltan impuestos en la factura Digitalizada

		private static Boolean CheckInvoiceGrossAmount(String sfile)
		{
			// Crea el contenidor del document de la factura
			XmlDocument xdcInvoice = new XmlDocument();
			xdcInvoice.Load(sfile);
			XmlNodeList xnlGrossAmount = null;
			XmlNodeList xnlInvoiceTotalGrossAmountBeforeTaxes = null;
			String sInvoiceTotalGrossAmountBeforeTaxes = String.Empty;
			Decimal dInvoiceTotalGrossAmountBeforeTaxes = 0;
			XmlNodeList xnlReimbursableExpensesAmount = null;
			String sInvoiceReimbursableExpensesAmount = String.Empty;
			Decimal dInvoiceReimbursableExpensesAmount = 0;

			String sInvoiceTaxesOutputsTaxAmount = String.Empty;
			Decimal dInvoiceTaxesOutputsTaxAmount = 0;
			Decimal dInvoiceTaxesOutputsTotal = 0;

			//sumamos el valor dInvoiceTaxesOutputsTotal + dInvoiceLineGrossAmountTotal
			//Debe ser igual a dInvoiceTotalGrossAmountBeforeTaxes o igual a dInvoiceReimbursableExpensesAmount
			Decimal dInvoiceTotalFacturaLinea = 0;
			Decimal dInvoiceTotalFactura = 0;

			try
			{

				//Suma de las tasas de factura
				XmlNodeList xnlInvoiceTaxesOutputs = null;
				xnlInvoiceTaxesOutputs = xdcInvoice.GetElementsByTagName("TaxesOutputs");
				if (xnlInvoiceTaxesOutputs != null && xnlInvoiceTaxesOutputs.Count > 0)
				{
					foreach (XmlElement itmInvoiceTaxOutput in xnlInvoiceTaxesOutputs)
					{
						// Comprova si el node pare és el de la factura o el de una línia de la factura
						if (itmInvoiceTaxOutput.ParentNode.Name == "Invoice")
						{
							// Carrega la llista d'elements de taxes de la factura
							XmlNodeList xnlInvoiceTaxes = itmInvoiceTaxOutput.GetElementsByTagName("Tax");
							// Comprova si s'ha carregat la llista d'elements de taxes de la factura
							if (xnlInvoiceTaxes != null && xnlInvoiceTaxes.Count > 0)
							{
								// Recorre la llista d'elements de taxes de la factura
								foreach (XmlElement itmInvoiceTax in xnlInvoiceTaxes)
								{
									try
									{
										// Recull el total de les taxes de la factura
										sInvoiceTaxesOutputsTaxAmount = ((XmlElement)((XmlElement)itmInvoiceTax.GetElementsByTagName("TaxAmount")[0])
											.GetElementsByTagName("TotalAmount")[0]).InnerText.Replace('.', ',');
										dInvoiceTaxesOutputsTaxAmount = Convert.ToDecimal(sInvoiceTaxesOutputsTaxAmount);
										String sInvoiceTaxTypeCode = ((XmlElement)itmInvoiceTax.GetElementsByTagName("TaxTypeCode")[0]).InnerText;
										if (sInvoiceTaxTypeCode == "04") //irpf resta
											dInvoiceTaxesOutputsTotal -= dInvoiceTaxesOutputsTaxAmount;
										else dInvoiceTaxesOutputsTotal += dInvoiceTaxesOutputsTaxAmount;
									}
									catch (Exception)
									{
									}
								}

							}
						}
					}
				}

				//Carga los GrossAmount
				xnlGrossAmount = xdcInvoice.GetElementsByTagName("GrossAmount");
				String sInvoiceLineGrossAmount = "";
				Decimal dInvoiceLineGrossAmount = 0;
				Decimal dInvoiceLineGrossAmountTotal = 0;

				//Suma Totales antes de tasas
				if (xnlGrossAmount != null && xnlGrossAmount.Count > 0)
				{
					foreach (XmlElement itmGrossAmount in xnlGrossAmount)
					{
						if (itmGrossAmount.ParentNode.Name == "InvoiceLine")
						{
							sInvoiceLineGrossAmount = ((XmlElement)itmGrossAmount).InnerText.Replace('.', ',');
							dInvoiceLineGrossAmount = Convert.ToDecimal(sInvoiceLineGrossAmount);
							dInvoiceLineGrossAmountTotal += dInvoiceLineGrossAmount;
						}
					}
				}

				//Cargar totalGrossAmountBeforeTaxes
				xnlInvoiceTotalGrossAmountBeforeTaxes = xdcInvoice.GetElementsByTagName("TotalGrossAmountBeforeTaxes");
				// Comprova si s'ha carregat la llista d'elements de totals bruts
				if (xnlInvoiceTotalGrossAmountBeforeTaxes != null && xnlInvoiceTotalGrossAmountBeforeTaxes.Count > 0)
				{
					// Recull el total de la línia
					sInvoiceTotalGrossAmountBeforeTaxes = ((XmlElement)xnlInvoiceTotalGrossAmountBeforeTaxes[0]).InnerText.Replace('.', ',');
					dInvoiceTotalGrossAmountBeforeTaxes = Convert.ToDecimal(sInvoiceTotalGrossAmountBeforeTaxes);
				}

				xnlReimbursableExpensesAmount = xdcInvoice.GetElementsByTagName("ReimbursableExpensesAmount");
				// Comprova si s'ha carregat la llista d'elements de totals bruts
				if (xnlReimbursableExpensesAmount != null && xnlReimbursableExpensesAmount.Count > 0)
				{
					// Recull el total de la línia
					sInvoiceReimbursableExpensesAmount = ((XmlElement)xnlReimbursableExpensesAmount[0]).InnerText.Replace('.', ',');
					dInvoiceReimbursableExpensesAmount = Convert.ToDecimal(sInvoiceReimbursableExpensesAmount);
				}
				//Suma valor lineas sin tasas + el valor de las tasas de factura
				dInvoiceTotalFacturaLinea = dInvoiceLineGrossAmountTotal + dInvoiceTaxesOutputsTotal;
				//Suma el valor del total bruto de la factura antes de tasas + el valor de las tasas de la factura
				dInvoiceTotalFactura = dInvoiceTotalGrossAmountBeforeTaxes + dInvoiceTaxesOutputsTotal;
				//TODO MICHEL,, IGUAL HAY QUE COGER EL TOTAL DE LA FACTURA
				XmlNodeList xnlTotalExecutableAmount = xdcInvoice.GetElementsByTagName("TotalExecutableAmount");
				String sInvoiceTotalExecutableAmount = "";
				Decimal dInvoiceTotalExecutableAmount = 0;
				if (xnlTotalExecutableAmount != null && xnlTotalExecutableAmount.Count > 0)
				{
					// Recull el total de la línia
					sInvoiceTotalExecutableAmount = ((XmlElement)xnlTotalExecutableAmount[0]).InnerText.Replace('.', ',');
					dInvoiceTotalExecutableAmount = Convert.ToDecimal(sInvoiceTotalExecutableAmount);
				}
				//Comprobamos sin dInvoiceLineGrossAmountTotal es igual a dInvoiceTotalGrossAmountBeforeTaxes
				String sLine = "";
				sLine = "'" + sInvoiceNumber + ";" 
					+ sTaxIdentificationNumberSeller + ";"
					+ sTaxIdentificationNumberBuyer + ";"
					+ dInvoiceLineGrossAmountTotal.ToString() + ";"
					+ dInvoiceTotalGrossAmountBeforeTaxes.ToString() + ";"
					+ dInvoiceReimbursableExpensesAmount.ToString() + ";"
					+ dInvoiceTotalExecutableAmount.ToString() + ";"
					+ dInvoiceTotalFacturaLinea.ToString() + ";"
					+ dInvoiceTaxesOutputsTotal.ToString() + ";"
					+ dInvoiceTotalFactura.ToString() + ";" ;

				#region Validación Delegacion suplidos
				Boolean bcheckCambioDelegacion = false;
				String scheckCambioDelegacion = "NO";
				
				String sDelEmisora = "";
				String sDelReceptora = "";
				bcheckCambioDelegacion = CheckChangeDelegacionEmpMarmedsa(sfile, ref sDelEmisora, ref sDelReceptora);
				if (bcheckCambioDelegacion) scheckCambioDelegacion = "SI";
				scheckCambioDelegacion += ";" + sDelEmisora + ";" + sDelReceptora + ";";

				#endregion Validación Delegacion suplidos

				#region Validación IGIC
				String scheckErrorIGIC = "NO";
				Boolean bcheckErrorIGIC = false;
				bcheckErrorIGIC = CheckErrorIgic(sfile);
				if (bcheckErrorIGIC && (dInvoiceTaxesOutputsTotal >0)) scheckErrorIGIC = "SI";
				#endregion Validación IGIC

				#region Validación Impuestos
				if (DifMenor1Euro(dInvoiceLineGrossAmountTotal, dInvoiceTotalGrossAmountBeforeTaxes)
					&& DifMenor1Euro(dInvoiceTotalFacturaLinea, dInvoiceTotalExecutableAmount))
				{
					sLine += "OK" +";" + scheckCambioDelegacion + scheckErrorIGIC +";" + sfile;
					listaFilasCheckTotales.Add(sLine);
					return true;
				}
				else if (dInvoiceReimbursableExpensesAmount != 0)
				{
					sLine += "OK" + ";" + scheckCambioDelegacion + scheckErrorIGIC + ";" + sfile;
					listaFilasCheckTotales.Add(sLine);
					return true;
				}
				else
				{
					sLine += "KO" + ";" + scheckCambioDelegacion + scheckErrorIGIC + ";" + sfile;
					listaFilasCheckTotales.Add(sLine);
					return true;
				}
				#endregion Validación 2
			}
			catch (Exception ectProgram)
			{
				// Registra l'error
				listaFilasCheckTotales.Add(sfile);
				return true;
			}
		}

		private static Boolean DifMenor1Euro(decimal value1, decimal value2)
		{
			try
			{
				if (value1 == 0) return false;
				if (value2 == 0) return false;
				Decimal dDif = 0;
				if (value1<0 & value2 >0) dDif =((-1)* value1) - value2;
				else if (value2 < 0 & value1 > 0) dDif = ( value1) - ((-1) *  value2);
				else if (value1 > value2) dDif = value1 - value2;
				else dDif = value2 - value1;
				if ( dDif <= 1) return true;
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}


		private static void CrearFicheroCSV(String sFicheroOut, ArrayList listaFilas)
		{
			StringBuilder file = new StringBuilder();
			String sCabecera = "";
			sCabecera = "NumeroFactura" + ";"
					+ "Emisor" + ";"
					+ "Receptor" + ";"
					+ "SumaInvoicelineGrossAmount" + ";"
					+ "NodoTotalGrossAmountBeforeTaxes" + ";"
					+ "NodoReimbursableExpensesAmount" + ";"
					+ "NodoTotalExecutableAmount" + ";"
					+ "SumaInvoicelineGrossAmount+SumaTaxesOutputsInvoice" + ";"
					+ "SumaTaxesOutputsInvoice" + ";"
					+ "NodoTotalGrossAmountBeforeTaxes+SumaTaxesOutputsInvoice" + ";"
					+ "ResultadoValidacion" + ";" 
					+ "CheckDelegacion " + ";"
					+ "DelEmisora " + ";"
					+ "DelReceptora " + ";"
					+ "CheckErrorIgic " + ";"
					+ "Fichero" + ";";
			file.Append(sCabecera);
			file.Append(";\n");
			foreach (String sLinea in listaFilas)
			{
				file.Append(sLinea);
				file.Append(";\n");
			}

			//Convierte el StringBuilder a array de bytes
			byte[] buffer = new byte[file.Length];
			for (int i = 0; i < file.Length; i++)
			{
				buffer[i] = (byte)file[i];
			}

			FileStream fsRecibo = new FileStream(sFicheroOut, FileMode.OpenOrCreate, FileAccess.Write);
			fsRecibo.Write(buffer, 0, buffer.Length);
			fsRecibo.Close();
			fsRecibo = null;

		}
		#endregion

		#region Eliminar caracter &
		private static void EliminarCaracteresEspeciales(String sfile)
		{
			String line;
			ArrayList aline = new ArrayList();
			String sfiletratar = "";
			Boolean bSave = false;
			try
			{
				sfiletratar = sfile.Replace(".xml", ".and");
				sfiletratar = sfiletratar.Replace(".XML", ".and");
				File.Copy(sfile, sfiletratar);
				//Pass the file path and file name to the StreamReader constructor
				StreamReader sr = new StreamReader(sfiletratar);

				//Read the first line of text
				line = sr.ReadLine();

				//Continue to read until you reach end of file
				while (line != null)
				{
					//write the lie to console window
					Console.WriteLine(line);
					//Read the next line
					if (line != null)
					{
						if (line.IndexOf("&amp;") > 0)
						{

						}
						else if (line.IndexOf("&lt;") > 0)
						{

						}
						else if (line.IndexOf("&gt;") > 0)
						{

						}
						else if (line.IndexOf("&") > 0)
						{
							line = line.Replace("&", "&amp;");
							bSave = true;
						}
						aline.Add(line);
					}
					line = sr.ReadLine();

				}

				//close the file
				sr.Close();
				sr = null;
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception: " + e.Message);
			}
			finally
			{
				Console.WriteLine("Executing finally block.");
			}
			//Si ha habido cambios al fichero lo grabamos si no, no hace falta.
			if (bSave)
			{
				//Ahora lo escribimos
				try
				{

					//Pass the filepath and filename to the StreamWriter Constructor
					StreamWriter sw = new StreamWriter(sfile);
					for (int i = 0; i < aline.Count; i++)
					{
						//Write a line of text
						sw.WriteLine(aline[i]);
					}


					//Close the file
					sw.Close();
					sw = null;
					File.Delete(sfiletratar);
				}
				catch (Exception e)
				{
					Console.WriteLine("Exception: " + e.Message);
				}
				finally
				{
					Console.WriteLine("Executing finally block.");
				}
			}
		}
		#endregion Eliminar caracter &

		#region valor nodo suplido
		private static Decimal ValueTotalReimbursableExpenses(XmlDocument xdcInvoice)
		{
			XmlNodeList xnlTotalReimbursableExpenses = null;
			String sInvoiceTotalReimbursableExpenses = String.Empty;
			Decimal dInvoiceTotalReimbursableExpenses = 0;

			try
			{


				xnlTotalReimbursableExpenses = xdcInvoice.GetElementsByTagName("TotalReimbursableExpenses");
				// Comprova si s'ha carregat la llista d'elements de totals bruts
				if (xnlTotalReimbursableExpenses != null && xnlTotalReimbursableExpenses.Count > 0)
				{
					// Recull el total de la línia
					sInvoiceTotalReimbursableExpenses = ((XmlElement)xnlTotalReimbursableExpenses[0]).InnerText.Replace('.', ',');
					dInvoiceTotalReimbursableExpenses = Convert.ToDecimal(sInvoiceTotalReimbursableExpenses);
				}
				return dInvoiceTotalReimbursableExpenses;
			}
			catch (Exception ectProgram)
			{
				// Registra l'error
				return -1;
			}
		}
		#endregion

		#region Validar tipo impuesto IGIC en delegaciones de canarias
		private static Boolean CheckErrorIgic(String sfile)
		{
			Boolean bErrorIGIC = false;
			try
			{
				//Comprobamos si el impuesto es IGIC para las delegaciones de Canarias
				//LPA - LAS PALMAS DE GRAN CANARIA
				//SCT - SANTA CRUZ DE TENERIFE
				//LPA - LAS PALMAS
				Boolean bisdelegacionigic = IsDelegacionIGIC(sfile);
				Boolean bistaxigic = IsTaxIGIC(sfile);
				if (bisdelegacionigic)
					if (!bistaxigic) bErrorIGIC = true;
				if (bistaxigic)
					if (!bisdelegacionigic) bErrorIGIC = true;
				return bErrorIGIC;
			}
			catch (Exception e)
			{
				Console.WriteLine("Error check igic " + e.Message);
				return bErrorIGIC;
				
			}
		}

		private static Boolean IsTaxIGIC(String sfile)
		{
			// Crea el contenidor del document de la factura
			XmlDocument xdcInvoice = new XmlDocument();
			xdcInvoice.Load(sfile);
			Boolean bIsIGIC = false;

			try
			{

				//Suma de las tasas de factura
				XmlNodeList xnlInvoiceTaxesOutputs = null;
				xnlInvoiceTaxesOutputs = xdcInvoice.GetElementsByTagName("TaxesOutputs");
				if (xnlInvoiceTaxesOutputs != null && xnlInvoiceTaxesOutputs.Count > 0)
				{
					foreach (XmlElement itmInvoiceTaxOutput in xnlInvoiceTaxesOutputs)
					{
						// Comprova si el node pare és el de la factura o el de una línia de la factura
						if (itmInvoiceTaxOutput.ParentNode.Name == "Invoice")
						{
							// Carrega la llista d'elements de taxes de la factura
							XmlNodeList xnlInvoiceTaxes = itmInvoiceTaxOutput.GetElementsByTagName("Tax");
							// Comprova si s'ha carregat la llista d'elements de taxes de la factura
							if (xnlInvoiceTaxes != null && xnlInvoiceTaxes.Count > 0)
							{
								// Recorre la llista d'elements de taxes de la factura
								foreach (XmlElement itmInvoiceTax in xnlInvoiceTaxes)
								{
									try
									{
										String sInvoiceTaxTypeCode = ((XmlElement)itmInvoiceTax.GetElementsByTagName("TaxTypeCode")[0]).InnerText;
										if (sInvoiceTaxTypeCode == "03") 
											bIsIGIC = true;
									}
									catch (Exception)
									{
									}
								}

							}
						}
					}
				}
				return bIsIGIC;
			}
			catch (Exception ectProgram)
			{
				// Registra l'error
				return false;
			}
		}

		private static Boolean IsDelegacionIGIC(String sfile)
		{
			XmlDocument documento = new XmlDocument();
			try
			{
				Boolean bDelIGIC = false;
				documento.Load(sFilein);
				XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
				String sTaxIdentificationNumber = "";
				CrearFacturasProveedoresExtandar.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
				String smyCentreCode = "";
				if (nBuyerParty != null)
				{
					foreach (XmlNode oNodo in nBuyerParty)
					{
						if (oNodo.Name == "AdministrativeCentres")
						{
							foreach (XmlNode oAdministrativeCentres in oNodo)
							{
								if (oAdministrativeCentres.Name == "AdministrativeCentre")
								{
									foreach (XmlNode oAdministrativeCentre in oAdministrativeCentres)
									{
										if (oAdministrativeCentre.Name == "CentreCode")
										{
											smyCentreCode = oAdministrativeCentre.InnerText.ToString().Trim();
											break;
										}

									}
								}
							}

						}
					}
				}
				if (smyCentreCode.Equals("LPA") || smyCentreCode.Equals("SCT")) bDelIGIC = true;
				return bDelIGIC;
			}
			catch (Exception e)
			{
				return false;
			}
			finally
			{
				documento = null;
			}

		}
		#endregion Validar tipo impuesto IGIC en delegaciones de canarias


	}
}
