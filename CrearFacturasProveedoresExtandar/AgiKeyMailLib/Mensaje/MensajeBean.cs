using System;
using System.Collections;

namespace AgiKeyMailLib.Mensaje
{
	
	public class MensajeBean
	{
		public MensajeBean()
		{
			
		}
		#region Especifico de EMAIL
		private string _mailEmisor;
		private string _mailReceptor;
		private ArrayList _mailsEnCopia;
		private ArrayList _adjuntos;
		public string MailEmisor
		{
			get
			{
				return _mailEmisor;
			}
			set
			{
				_mailEmisor = value;
			}
		}
		public string MailReceptor
		{
			get
			{
				return _mailReceptor;
			}
			set
			{
				_mailReceptor = value;
			}
		}
		public ArrayList Adjuntos
		{
			get
			{
				return _adjuntos;
			}
			set
			{
				_adjuntos = value;
			}
		}
		public ArrayList MailsEnCopia
		{
			get
			{
				return _mailsEnCopia;
			}
			set
			{
				_mailsEnCopia = value;
			}
		}
		

		#endregion

		#region General de Mensaje
		private string _mensaje;		
		private string _asunto;
		private string _id_plantilla;
		private int _id_entidad_propietaria;
		public string Mensaje
		{
			get
			{
				return _mensaje;
			}
			set
			{
				_mensaje = value;
			}
		}
		public string Asunto
		{
			get
			{
				return _asunto;
			}
			set
			{
				_asunto = value;
			}
		}
	
		public string IdPlantilla
		{
			get
			{
				return _id_plantilla;
			}
			set
			{
				_id_plantilla = value;
			}
		}
	
		public int IdEntidadPropietaria
		{
			get
			{
				return _id_entidad_propietaria;
			}
			set
			{
				_id_entidad_propietaria = value;
			}
		}

		#endregion

	}
}
