using System;
using System.Web.Mail;
using AgiKeyMailLib.Mensaje;


namespace AgiKeyMailLib.Comunicacion
{
	/// <summary>
	/// Descripci�n breve de crearXml.
	/// </summary>
	
	public class Comunicaciones
	{																						  
		string _smtpServer="";
		string _smtpServerUserName="";
		string _smtpServerPassword="";
		string _smtpServerPort="";
		string _smtpServerAuthenticationMode="";
		
		/// <summary>
		/// Descripci�n breve de crearXml.
		/// </summary>
		public Comunicaciones(config configuracionSMTP)
		{
			//
			// TODO: agregar aqu� la l�gica del constructor
			//
			try
			{
				// Cargamos el fichero de configuraci�n
				// Cargamos los datos principales
				_smtpServer = configuracionSMTP.smtp_server;
				_smtpServerUserName = configuracionSMTP.smtp_server_sender_username;
				_smtpServerPassword = configuracionSMTP.smtp_server_sender_password;
				_smtpServerPort = configuracionSMTP.smtp_server_port;
				_smtpServerAuthenticationMode = configuracionSMTP.smtp_server_authentication_mode;
			}
			catch(Exception e)
			{
				throw e;
				
			}
		} 
		
		
		/// <summary>
		/// Envia una comunicacion via mail ordinario
		/// </summary>
		/// <param name="idEntidadEmisora"></param>
		/// <param name="idEntidadReceptora"></param>
		/// <param name="asunto"></param>
		/// <param name="mensaje"></param>
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mensaje"></param>
		public void enviaMail(MensajeBean mensaje)
		{
		
			MailMessage mail = new MailMessage();


			//Tipos autenticacion Anonymous(0), Basic (1) y  NTLM (2)

			if (!_smtpServerAuthenticationMode.Equals("0"))
			{
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = _smtpServer;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = _smtpServerPort ;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2 ;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = _smtpServerAuthenticationMode ;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = _smtpServerUserName; 
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = _smtpServerPassword;
			}
			else
			{
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = _smtpServer;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = _smtpServerPort ;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2 ;
				mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = _smtpServerAuthenticationMode ;				
			}
			



			mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.BodyFormat =  System.Web.Mail.MailFormat.Html;
			mail.From = mensaje.MailEmisor;
			mail.To = mensaje.MailReceptor;
			
			if (mensaje.MailsEnCopia != null)
			{
				for (int i=0; i<mensaje.MailsEnCopia.Count; i++)
				{
					mail.Cc += mensaje.MailsEnCopia[i].ToString() + ";";
				}
			}
			mail.Subject = mensaje.Asunto;
			if (mensaje.Adjuntos != null)
			{
				for (int i=0; i<mensaje.Adjuntos.Count; i++)
				{
					MailAttachment adjunto = new MailAttachment(mensaje.Adjuntos[i].ToString());
					mail.Attachments.Add(adjunto);

				}
			}
						
			mail.Body = mensaje.Mensaje;
            if (_smtpServer.Length > 0)
            {
                //Enviamos el mail
                try
                {
                    SmtpMail.SmtpServer = _smtpServer;

                    for (int iIntentos = 1; iIntentos < 6; iIntentos++)
                    {
                        try
                        {
                            System.Web.Mail.SmtpMail.Send(mail);
                            string strMensajeLog;
                            strMensajeLog = DateTime.Now.ToLongDateString() + "-Se ha enviado el mail, '" + mensaje.Asunto + "' a " + "'" + mensaje.MailReceptor + "' ";
                            Console.WriteLine(strMensajeLog);
                            break;
                        }
                        catch (Exception e)
                        {
                            if (iIntentos == 5)
                            {
                                string strMensajeLog;
                                strMensajeLog = DateTime.Now.ToShortDateString() + "-No se ha enviado el mail, '" + mensaje.Asunto + "' " + e.Message.ToString();
                                Console.WriteLine(strMensajeLog);
                                break;
                            }
                        }
                    }


                }
                catch (Exception e)
                {
                    string strMensajeLog;
                    strMensajeLog = DateTime.Now.ToShortDateString() + "-No se ha enviado el mail, '" + mensaje.Asunto + "' " + e.Message.ToString();
                    Console.WriteLine(strMensajeLog);
                }

            }
            else
            {
                //No enviamos el mail y lo reflejamos en el log
                string strMensajeLog;
                strMensajeLog = DateTime.Now.ToShortDateString() + "-No se ha enviado el mail, '" + mensaje.Asunto + "' _smtpServer no mayoer 0";
                Console.WriteLine(strMensajeLog);
            }		
		}
	}

}



