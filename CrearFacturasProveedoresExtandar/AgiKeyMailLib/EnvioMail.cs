﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgiKeyMailLib.Comunicacion;
using System.IO;
using System.Xml.Serialization;
using AgiKeyMailLib.Mensaje;
using System.Collections;

namespace AgiKeyMailLib
{
    public class EnvioMail
    {
        #region Variables
        private static config oDatosConexion = null;
        #endregion

        #region Contructor
        public EnvioMail()
        {
            fCargarComunicacion();
        }
        #endregion

        #region Métodos públicos
        public Boolean EnviarMail(String sAsunto, String sCuerpo, String sFicheroAdjunto)
        {
            try
            {
                MensajeBean oMensajeBean = fCrearMensaje(sAsunto,sCuerpo, sFicheroAdjunto);
                if (oMensajeBean != null)
                {
                    EnviarMail(oMensajeBean);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

		public Boolean EnviarMail(String sAsunto, String sCuerpo, string[] oListaAdjuntos)
		{
			try
			{
				MensajeBean oMensajeBean = fCrearMensaje(sAsunto, sCuerpo,oListaAdjuntos);
				if (oMensajeBean != null)
				{
					EnviarMail(oMensajeBean);
				}
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}
		#endregion

		#region Comunicacion
		private static Boolean fCargarComunicacion()
        {
            try
            {
                String sRutaXMLConfig = AppDomain.CurrentDomain.BaseDirectory.ToString() + "configNotify.xml";
                oDatosConexion = cargaFicheroXML(sRutaXMLConfig);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Carga Configuracion: " + e.Message);
                return false;
            }
        }

        #region cargaFicheroXML
        /// <summary>
        /// Lee todos los parámetros del fichero XML necesarios para enviar un mensaje.
        /// </summary>
        /// <param name="_rutaFicheroXML">path dónde se encuentra el fichero XML a leer.</param>
        /// 
        /// <returns> Objeto config con los datos de la conexión al servidor SMTP</returns>
        private static config cargaFicheroXML(string _rutaFicheroXML)
        {
            try
            {
                config configuracion = new config();
                FileStream fs = new FileStream(_rutaFicheroXML, FileMode.Open, FileAccess.Read);
                XmlSerializer serializer = new XmlSerializer(typeof(config));
                configuracion = (config)serializer.Deserialize(fs);
                return configuracion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
        #endregion

        #region Envio Mensaje
        /// <summary>
        /// Función que crea el objeto mensaje
        /// </summary>
        /// <param name="sEntidadEmisora"></param>
        /// <param name="sMailReceptor"></param>
        /// <returns></returns>
        private static MensajeBean fCrearMensaje(String sAsunto, String sCuerpo, String ficheroadjunto)
        {
            try
            {
                MensajeBean oMensajeBean = new MensajeBean();
                oMensajeBean.MailEmisor = oDatosConexion.mail_emisor;
                oMensajeBean.MailReceptor = oDatosConexion.mail_receptor;
                oMensajeBean.Asunto = sAsunto;
                oMensajeBean.Mensaje = sCuerpo;
				if (File.Exists(ficheroadjunto))
				{
					oMensajeBean.Adjuntos = new ArrayList();
					oMensajeBean.Adjuntos.Add(ficheroadjunto);
				}
                return oMensajeBean;
            }
            catch (Exception)
            {
                return null;
            }
        }

		/// <summary>
		/// Crea el mensaje con adjuntos.
		/// </summary>
		/// <param name="sAsunto"></param>
		/// <param name="sCuerpo"></param>
		/// <param name="oListaAdjuntos"></param>
		/// <returns></returns>
		private static MensajeBean fCrearMensaje(String sAsunto, String sCuerpo, string[] oListaAdjuntos)
		{
			try
			{
				MensajeBean oMensajeBean = new MensajeBean();
				oMensajeBean.MailEmisor = oDatosConexion.mail_emisor;
				oMensajeBean.MailReceptor = oDatosConexion.mail_receptor;
				oMensajeBean.Asunto = sAsunto;
				oMensajeBean.Mensaje = sCuerpo;
				if (oListaAdjuntos != null)
				{
					oMensajeBean.Adjuntos = new ArrayList();
					for (int i = 0; i < oListaAdjuntos.Length; i++)
					{
						oMensajeBean.Adjuntos.Add(oListaAdjuntos[i]);
					}
				}
				return oMensajeBean;
			}
			catch (Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// Envia el mensaje al receptor
		/// </summary>
		/// <param name="oMensajeBean"></param>
		/// <returns></returns>
		private static Boolean EnviarMail(MensajeBean oMensajeBean)
        {
            try
            {
                Comunicaciones oComunicacion = new Comunicaciones(oDatosConexion);
                oComunicacion.enviaMail(oMensajeBean);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion
    }
}
