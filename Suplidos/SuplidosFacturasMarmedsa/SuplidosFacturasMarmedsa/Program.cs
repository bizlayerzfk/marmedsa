﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SuplidosFacturasMarmedsa
{
    class Program
    {
        #region Varibles
        public static String sFilein = "";
        public static String sFileout = "";
        public static String sFileExtension = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Extensions.xml";
        public static ArrayList oCiudad = null;
        public static ArrayList oDepartamento = null;
        public static String sDirectorioFacturas = "";
        public static String sDirectorioFacturasenviar = "";
        public static ArrayList aListaEmpresas = null;
        public static String sFileListaEmpresasMarmedsa = "";
        public static ArrayList aListaSuplidos = null;
        public static String sFileListaSuplidos = "";
        #endregion

        static void Main(string[] args)
        {

            sDirectorioFacturas = ConfigurationManager.AppSettings["sDirectorioFacturas"];
            sDirectorioFacturasenviar = ConfigurationManager.AppSettings["sDirectorioFacturasenviar"];
            sFileListaEmpresasMarmedsa = ConfigurationManager.AppSettings["sListaMarmedsa"];
            sFileListaSuplidos = ConfigurationManager.AppSettings["sListaSuplidos"];
            Init();
            String sDirectorioMonitorizacion = procesarArchivos(sDirectorioFacturas);
            if (sDirectorioMonitorizacion != "")
            {
                string[] fileNames = Directory.GetFiles(sDirectorioMonitorizacion, "*.xml");

                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFichero = fileNames[i];
                    String sFicheroPdf = fileNames[i].Replace("xml", "pdf");
                    try
                    {
                        int j = i + 1;
                        Console.WriteLine("Procesando " + j + " de " + fileNames.Length);
                        //Añadimos centro administrativo.
                        sFilein = sFichero;
                        sFileout = sFichero.Replace(".xml", "_suplido.xml");
                        Boolean bSuplidos = false;
                        AddAdministrativeCentreBuyer(sFilein, sFileout,ref bSuplidos);
                        if (bSuplidos)
                        {
                            RenombrarFichero(sFileout, sFilein);
                        }
                        else RenombrarFichero(sFileout, sFilein); 

                        //Comprobamos si hay que añadir las extensiones
                        if (!TieneExtensionesMarmedsa(sFilein))
                        {
                            sFileout = sFichero.Replace(".xml", "_extension.xml");
                            AddExtension(sFilein, sFileout);
                            RenombrarFichero(sFileout, sFilein);
                        }


                        if (File.Exists(sFilein))
                        {
                            //Copiamos los ficheros a la carpeta del toolkit
                            CopiarArchivo(sFichero, sDirectorioFacturasenviar);
                            CopiarArchivo(sFicheroPdf, sDirectorioFacturasenviar);
                        }
                        else //si no existe el fichero _conxml es que algo ha fallado.
                        {
                            moverAError(sFichero, sDirectorioMonitorizacion);
                            moverAError(sFicheroPdf, sDirectorioMonitorizacion);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                //Movemos todo a la subcarpetas procesadas.
                BorrarFacturasIntermedias(sDirectorioMonitorizacion);
                moverAProcesadas(sDirectorioMonitorizacion);

            }


            Console.WriteLine("Fin proceso");

        }

        #region init vars
        private static void Init()
        {
            //Cargamos la lista de departamentos y ciudades.
            oCiudad = new ArrayList();
            oDepartamento = new ArrayList();
            oDepartamento.Add("AAE"); oCiudad.Add("ANNABA");
            oDepartamento.Add("ARG"); oCiudad.Add("ALGER");
            oDepartamento.Add("AZW"); oCiudad.Add("ARZEW");
            oDepartamento.Add("BJA"); oCiudad.Add("BEJAIA");
            oDepartamento.Add("DJE"); oCiudad.Add("Djen-Djen");
            oDepartamento.Add("MOS"); oCiudad.Add("MOSTAGANEM");
            oDepartamento.Add("ORN"); oCiudad.Add("ORAN");
            oDepartamento.Add("SKI"); oCiudad.Add("SKIKDA");
            oDepartamento.Add("BCN"); oCiudad.Add("BARCELONA");
            oDepartamento.Add("AGP"); oCiudad.Add("MALAGA");
            oDepartamento.Add("ALC"); oCiudad.Add("ALICANTE");
            oDepartamento.Add("ALD"); oCiudad.Add("ALCUDIA");
            oDepartamento.Add("ALG"); oCiudad.Add("ALGECIRAS");
            oDepartamento.Add("BIO"); oCiudad.Add("BILBAO");
            oDepartamento.Add("CAD"); oCiudad.Add("CADIZ");
            oDepartamento.Add("CAS"); oCiudad.Add("CASTELLON");
            oDepartamento.Add("GIJ"); oCiudad.Add("GIJON");
            oDepartamento.Add("HUV"); oCiudad.Add("HUELVA");
            oDepartamento.Add("LCG"); oCiudad.Add("LA CORUÑA");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS DE GRAN CANARIA");
            oDepartamento.Add("MAD"); oCiudad.Add("MADRID");
            oDepartamento.Add("PMI"); oCiudad.Add("PALMA DE MALLORCA");
            oDepartamento.Add("SCT"); oCiudad.Add("SANTA CRUZ DE TENERIFE");
            oDepartamento.Add("SVQ"); oCiudad.Add("SEVILLA");
            oDepartamento.Add("TAR"); oCiudad.Add("TARRAGONA");
            oDepartamento.Add("VGO"); oCiudad.Add("VIGO");
            oDepartamento.Add("VLC"); oCiudad.Add("VALENCIA");
            oDepartamento.Add("CAR"); oCiudad.Add("CARTAGENA");
            oDepartamento.Add("LEI"); oCiudad.Add("ALMERIA");
            oDepartamento.Add("MOT"); oCiudad.Add("MOTRIL");
            oDepartamento.Add("SDR"); oCiudad.Add("SANTANDER");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS");
            oDepartamento.Add("LEH"); oCiudad.Add("LE HAVRE");
            oDepartamento.Add("MRS"); oCiudad.Add("MARSEILLE");
            oDepartamento.Add("MTU"); oCiudad.Add("MARTIGUES");
            oDepartamento.Add("PAR"); oCiudad.Add("PARIS");
            oDepartamento.Add("CSB"); oCiudad.Add("CASABLANCA");
            oDepartamento.Add("TNG"); oCiudad.Add("TANGIER");
            oDepartamento.Add("LIS"); oCiudad.Add("LISBOA");
            oDepartamento.Add("OPO"); oCiudad.Add("PORTO");
            oDepartamento.Add("SIE"); oCiudad.Add("SINES");
            oDepartamento.Add("SET"); oCiudad.Add("SETUBAL");
            oDepartamento.Add("OPO"); oCiudad.Add("OPORTO");
            //Cargamos la lista de empresas desde el excel.
            CargarListaEmpresas();
            CargarListaSuplidos();
        }

        private static void CargarListaEmpresas()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaEmpresasMarmedsa);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaEmpresas == null) aListaEmpresas = new ArrayList();
                            SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[1].ToString();
                            oEmpresa.CIF = rdrExcelData[2].ToString();
                            oEmpresa.DEL = rdrExcelData[3].ToString();
                            oEmpresa.PROV = rdrExcelData[4].ToString();
                            oEmpresa.PAIS = rdrExcelData[5].ToString();
                            oEmpresa.DIRECCION = rdrExcelData[6].ToString();
                            oEmpresa.CODIGOPOSTAL = rdrExcelData[7].ToString();
                            aListaEmpresas.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas empresas.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }

        private static void CargarListaSuplidos()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaSuplidos);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaSuplidos == null) aListaSuplidos = new ArrayList();
                            SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[3].ToString();
                            oEmpresa.CIF = rdrExcelData[1].ToString();
                            aListaSuplidos.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas Suplidos.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }
        #endregion

        #region Añadir AdministrativeCentres
        private static void AddAdministrativeCentreBuyer(String sFilein, String sFileOut , ref Boolean bSuplido)
        {
            try
            {
                bSuplido = false;
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                XmlNode nAddressInSpain = null;
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oNodoTaxIdentification in oNodo)
                            {
                                if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
                                }
                            }
                        }


                    }
                }

                //Antes de añadir todo, vemos si es suplido o no
                bSuplido = false;
                SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                bSuplido = !EsEmpresaMarmedsa(sTaxIdentificationNumber, ref oEmpresa);
                if (!bSuplido) //Código anterior a suplidos.
                {
                    //Si no existe lo añadimos.
                    if (baddAdministrativeCentres)
                    {
                        nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                        Boolean binsertCentro = false;
                        if (nBuyerParty != null)
                        {
                            foreach (XmlNode oNodo in nBuyerParty)
                            {
                                //Para el sabadell si hay partyidentification
                                if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                {
                                    XmlNode nodo1 = oNodo;
                                    nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                    documento.Save(sFileout);
                                    documento = null;
                                    binsertCentro = true;
                                    break;
                                }
                            }
                            //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                            if (!binsertCentro)
                            {
                                nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                                if (nBuyerParty != null)
                                {
                                    foreach (XmlNode oNodo in nBuyerParty)
                                    {
                                        if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                        {
                                            XmlNode nodo1 = oNodo;
                                            nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                            documento.Save(sFileout);
                                            documento = null;
                                            binsertCentro = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            documento.Save(sFileout);
                            documento = null;
                        }
                    }
                }
                else //es suplido
                {

                    SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oSuplido = null;
                    EsSuplido(sTaxIdentificationNumber, ref oSuplido);
                    Console.WriteLine("Es suplido..");
                    if (oSuplido != null)
                    {
                        Console.WriteLine("leido suplido..");
                        //Añadimos el centroadministrativo y modificamos los datos de la empresa suplida por la de Marmedsa.
                        SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpMar = GetEmpresaMarmedsa(oSuplido.EMP);
                        if (oEmpMar != null)
                        {
                            nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "TaxIdentification")
                                    {
                                        foreach (XmlNode oNodoTaxIdentification in oNodo)
                                        {
                                            if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoTaxIdentification.InnerText = oEmpMar.CIF;
                                            }
                                        }
                                    }
                                    //Para el sabadell si hay partyidentification
                                    if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentreSuplido(documento, oEmpMar), nodo1);
                                        //documento.Save(sFileout);
                                        //documento = null;
                                        //break;
                                    }
                                }
                                //Miramos el legalEntity y cambiamos el overAddress
                                Boolean bOverAddress = false;
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoLegalEntity.InnerText = oEmpMar.NOM;
                                            }
                                            if (oNodoLegalEntity.Name == "AddressInSpain")
                                            {
                                                foreach (XmlNode oNodoAddressInSpain in oNodoLegalEntity)
                                                {
                                                    if (oNodoAddressInSpain.Name == "Address")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.DIRECCION;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "PostCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.CODIGOPOSTAL;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Town")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Province")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "CountryCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PAIS;
                                                    }

                                                }


                                            }
                                            if (oNodoLegalEntity.Name == "OverseasAddress")
                                            {
                                                oNodo.RemoveChild(oNodoLegalEntity);
                                                bOverAddress = true;
                                            }
                                        }
                                    }
                                    if (bOverAddress)
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {

                                                XmlNode nodo1 = oNodoLegalEntity;
                                                oNodo.InsertAfter(CrearNodo_AddressInSpain(documento, oEmpMar), nodo1);

                                            }
                                        }

                                    }

                                }

                            }
                            documento.Save(sFileout);
                            documento = null;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        private static void AddAdministrativeCentreSeller(String sFilein, String sFileOut)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//SellerParty");
                XmlNode nAddressInSpain = null;
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        //Cargamos al Adresspain para el nuevo nodo.

                    }
                }
                //Si no existe lo añadimos.
                if (baddAdministrativeCentres)
                {
                    nBuyerParty = documento.SelectSingleNode("//SellerParty");
                    Boolean binsertCentro = false;
                    if (nBuyerParty != null)
                    {
                        foreach (XmlNode oNodo in nBuyerParty)
                        {
                            if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                            {
                                XmlNode nodo1 = oNodo;
                                nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                documento.Save(sFileout);
                                documento = null;
                                binsertCentro = true;
                                break;
                            }
                            //Cargamos al Adresspain para el nuevo nodo.

                        }
                        //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                        if (!binsertCentro)
                        {
                            nBuyerParty = documento.SelectSingleNode("//SellerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                        documento.Save(sFileout);
                                        documento = null;
                                        binsertCentro = true;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        //<AdministrativeCentres>
        //  <AdministrativeCentre>
        //    <CentreCode>BCN</CentreCode>
        //    <AddressInSpain>
        //      <Address>ZAL 2 - C/ NYEPA, 2-5</Address>
        //      <PostCode> 08820</PostCode>
        //      <Town> EL PRAT DE LLOBREGAT</Town>
        //      <Province></Province>
        //      <CountryCode>ESP</CountryCode>
        //    </AddressInSpain>
        //  </AdministrativeCentre>
        //</AdministrativeCentres>
        private static XmlElement CrearNodo_AdministrativeCentre(XmlDocument xmlDoc, XmlNode oAddressInSpain)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");

                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");
                foreach (XmlNode oNodo in oAddressInSpain)
                {
                    if (oNodo.Name == "Address")
                    {
                        Address.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "PostCode")
                    {
                        PostCode.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Town")
                    {
                        Town.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Province")
                    {
                        Province.InnerText = oNodo.InnerText;
                        CentreCode.InnerText = getCentreCode(Province.InnerText);
                    }
                    if (oNodo.Name == "CountryCode")
                    {
                        CountryCode.InnerText = oNodo.InnerText;
                    }

                }




                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");

                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private static XmlElement CrearNodo_AdministrativeCentreSuplido(XmlDocument xmlDoc, SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");

                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;


                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");

                CentreCode.InnerText = oEmpresa.DEL; //en las no suplidos lo cogemos según provincia de la receptora, aquí lo ponemos según tabla.
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static XmlElement CrearNodo_AddressInSpain(XmlDocument xmlDoc, SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;


                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);


                return AddressInSpain;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve el codigo de centro según provincia receptora
        /// </summary>
        /// <param name="sProvincia"></param>
        /// <returns></returns>
        private static String getCentreCode(String sProvincia)
        {
            try
            {
                String scentrecode = "";
                for (int i = 0; i < oCiudad.Count; i++)
                {
                    String sciudad = oCiudad[i].ToString();
                    if (sProvincia.ToUpper().Trim() == sciudad.ToUpper().Trim())
                    {
                        scentrecode = oDepartamento[i].ToString();
                        break;
                    }

                }
                return scentrecode;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private static Boolean EsEmpresaMarmedsa(String sCif, ref SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsempresaMarmedsa = false;
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0 || sCif == oEmpresa.CIF)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsempresaMarmedsa = true;
                        break;
                    }
                }
                return bEsempresaMarmedsa;
            }
            catch (Exception e)
            {
                return bEsempresaMarmedsa;
            }
        }

        private static SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa GetEmpresaMarmedsa(String sEmp)
        {
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sEmp == oEmpresa.EMP)
                    {
                        return oEmpresa;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static Boolean EsSuplido(String sCif, ref SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsSuplido = false;
            try
            {
                for (int i = 0; i < aListaSuplidos.Count; i++)
                {
                    SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (SuplidosFacturasMarmedsa.EmpresaMarmedsa.EmpresaMarmedsa)aListaSuplidos[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF == sCif)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                }
                return bEsSuplido;
            }
            catch (Exception e)
            {
                return bEsSuplido;
            }
        }
        #endregion

        #region Quitar IVA suplidos

        private static void QuitarIvaSuplidos(String sFilein, String sFileOut)
        {
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(sFilein);

                //XmlNodeList nodeTaxInvoice = xDoc.GetElementsByTagName()

                XmlNodeList nodeItems = xDoc.GetElementsByTagName("InvoiceLine");
                if (nodeItems != null)
                {

                    foreach (XmlNode onodoInvoiceLine in nodeItems)
                    {
                        XmlNodeList Quantity = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("Quantity");
                        XmlNodeList UnitPriceWithoutTax = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("UnitPriceWithoutTax");
                        Decimal iTotalCost = Convert.ToDecimal(Quantity[0].InnerText.Replace(".", ",")) * Convert.ToDecimal(UnitPriceWithoutTax[0].InnerText.Replace(".", ","));
                        iTotalCost = Math.Round(iTotalCost, 2);
                        XmlNodeList TotalCost = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TotalCost");
                        if (TotalCost[0].InnerText == "")
                            TotalCost[0].InnerText = iTotalCost.ToString().Replace(",", ".") + "0000";

                        //Buscamos el valor en el nodo TAX
                        XmlNodeList GrossAmount = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("GrossAmount");
                        XmlNodeList TaxesOutputs = ((XmlElement)onodoInvoiceLine).GetElementsByTagName("TaxesOutputs");
                        XmlNodeList Tax = ((XmlElement)TaxesOutputs[0]).GetElementsByTagName("Tax");
                        XmlNodeList TaxableBase = ((XmlElement)Tax[0]).GetElementsByTagName("TaxableBase");
                        XmlNodeList TotalAmount = ((XmlElement)TaxableBase[0]).GetElementsByTagName("TotalAmount");
                        //if (TotalAmount[0].InnerText == "")
                        //Ponemos a 2 decimales   
                        String stotalamount = GrossAmount[0].InnerText;
                        int ipunto = stotalamount.IndexOf(".");
                        TotalAmount[0].InnerText = stotalamount.Substring(0, ipunto + 3);
                    }
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                xDoc.Save(sFileOut);
                xDoc = null;
            }

        }
        #endregion

        #region Extensiones Marmedsa
        private static Boolean TieneExtensionesMarmedsa(String sFilein)
        {
            Boolean bExisteExtension = false;
            String sfileor = sFilein;
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(sFilein);


                XmlNode nodo = xDoc.SelectSingleNode("//InvoiceExtensions");

                //Buscamos extension marmedsa
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xDoc.NameTable);
                    //ext="LeaseplanInvoiceExtension
                    xmlNsM.AddNamespace("marmedsa", @"http://marmedsa.com/Factura_Extension.xsd");
                    nodo = xDoc.SelectSingleNode("//marmedsa:InvoiceExtensions", xmlNsM);
                }
                //si no lo encontramos buscamos ns4
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xDoc.NameTable);
                    //ext="LeaseplanInvoiceExtension
                    xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                    nodo = xDoc.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                }

                if (nodo != null)
                {
                    bExisteExtension = true;
                }
                xDoc = null;
                return bExisteExtension;
            }
            catch (Exception e)
            {
                return bExisteExtension;
            }
        }

        private static Boolean AddExtension(String sFileFacturain, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Facturae");

                if (nodoAdd == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodoAdd != null)
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoices");
                    XmlNode onodo = xmlDoc.CreateElement("Extensions");
                    onodo.InnerXml = LeerFileExtension(sFileExtension);
                    //nodo.AppendChild(onodo);
                    //nodoAdd.AppendChild(onodo);
                    nodoAdd.InsertAfter(onodo, nodo);
                    xmlDoc.Save(sFileFacturaEout);
                }
                //{"El nodo que desea insertar pertenece a otro contexto de documento."}
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static String LeerFileExtension(String sFileExtension)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileExtension);
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Extensions");
                if (nodoAdd != null)
                {
                    return nodoAdd.InnerXml;
                }
                else return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }
        #endregion

        #region Tratamiento Ficheros
        private static string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {
                string directorioDestino = "";
                bool OK = false;


                //myLog.WriteEntry("Los ficheros se procesan en: "+directorioDestino,EventLogEntryType.Information);	

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                //Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
                if (arrayFicheros.Length > 0)
                {
                    DateTime dt = DateTime.Now;
                    string fecha, hora;

                    //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    hora = dt.ToLongTimeString().Replace(":", "_");
                    string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                }
                // Al procesar el nuevo archivo incluido en el directorio especificado
                // debemos copiarlo al directorio temporal destinado para ello
                for (int i = 0; i < arrayFicheros.Length; i++)
                {
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            File.Delete(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }
                return directorioDestino;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error copiar archivo: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Devuelve el fichero pdf en B64
        /// </summary>
        /// <param name="sfilePdf"></param>
        /// <returns></returns>
        private static String getFileB64(String sfilePdf)
        {
            try
            {
                String sBase64 = "";
                sBase64 = Convert.ToBase64String(readBinaryFile(sfilePdf));
                return sBase64;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve byte[] del fichero a leer
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }

        /// <summary>
        /// Movemos a procesadas para no crearlas en los siguientes procesos.
        /// </summary>
        private static void moverAProcesadas(String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioEnviada = sDirectorioMonitorizacion + "Procesadas";
                Directory.CreateDirectory(sDirectorioEnviada);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*.*"))
                {
                    String sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error mover a procesada: " + e.Message);
            }
        }

        /// <summary>
        /// Movemos a error para no procesarlas ni enviarlas
        /// </summary>
        private static void moverAError(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "Error\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error a error: " + e.Message);
            }
        }

        /// <summary>
        /// Movemos a Excluidas las facturas que son de cif exluidos.
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorExcluida(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "CifExcluidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception e)
            {
                Console.WriteLine("Error mover a excluida: "+ e.Message);
            }
        }

        private static void BorrarFacturasIntermedias(String sDirectorioMonitorizacion)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*_sinfirma.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conceros.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conadmcentre.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_sincero.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conSequenceNumber.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conxml.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conespacios.xml"))
                {
                    File.Delete(fi.FullName);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error borrarfacturasintermedias: "+ e.Message);
            }
        }
        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error moverfichero: "+ e.Message);
            }
        }

        private static void RenombrarFichero(string sFile, string sFileOut)
        {
            try
            {
                if (File.Exists(sFile))
                {
                    if (File.Exists(sFileOut)) File.Delete(sFileOut);
                    File.Move(sFile, sFileOut);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error renombrar: "+ e.Message);
            }
        }
        #endregion
    }
}
