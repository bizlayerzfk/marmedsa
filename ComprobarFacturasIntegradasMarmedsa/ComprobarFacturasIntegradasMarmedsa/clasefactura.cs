﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComprobarFacturasIntegradasMarmedsa
{
	public class clasefactura
	{
		public string id { get; set; }
		public string cifproveedor { get; set; }
		public string nombre { get; set; }
		public string factura { get; set; }
		public string fechafactura { get; set; }
		public string fecharegistrobbdd { get; set; }
	}
}
