﻿using BotMailsToolkits;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComprobarFacturasIntegradasMarmedsa
{
	class Program
	{
		public static String sContador = "";
		public static String sMensaje = "";
		public static String sFacturasSinIntegrar = "";
		static void Main(string[] args)
		{
			try
			{
				ArrayList olistafaltan = GetFacturasFaltanAgikey(args);

				//Creamos el mensaje con los resultados.
				String sTitulo = "Comprobación Integración Facturas Digitalizadas " + DateTime.Now.ToShortDateString();
				String sError = "";
				if (olistafaltan != null && olistafaltan.Count > 0) sError = "ERROR ";
				String detalle = "Resumen proceso :" + "<p></p>";
				detalle+= sContador + "<p></p>";

				sTitulo = sError + sTitulo;
				detalle += sFacturasSinIntegrar;
				String mailsDestinos = ConfigurationManager.AppSettings["sMailsDestino"];
				string resp = EnvioMail.enviarMail2(mailsDestinos, "info@agikey.com", sTitulo, detalle);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error proceso: " + e.Message);
			}
		}

		/// <summary>
		/// Devuelve las facturas que faltan en Agikey
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		private static ArrayList GetFacturasFaltanAgikey(String[] args)
		{
			try
			{
				//Argumentos sería fecha inicio y fecha fin, si no viene sería el día de ayer y hoy.
				//La fecha posterior de agikey es un día más que la de digitalización por si vienen por la madrugada
				String sfechaanterior = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
				String sfechaposteriorDig = DateTime.Now.ToString("dd/MM/yyyy");
				String sfechaposteriorAgikey = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
				try
				{
					if (args.Length > 1)
					{
						DateTime dFechaanterior = Convert.ToDateTime(args[0].ToString());
						DateTime dFechaposterior = Convert.ToDateTime(args[1].ToString());
						sfechaanterior = dFechaanterior.ToString("dd/MM/yyyy");
						sfechaposteriorDig = dFechaposterior.ToString("dd/MM/yyyy");
						sfechaposteriorAgikey = dFechaposterior.AddDays(1).ToString("dd/MM/yyyy");
					}
				}
				catch (Exception)
				{
					sfechaanterior = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
					sfechaposteriorDig = DateTime.Now.ToString("dd/MM/yyyy");
					sfechaposteriorAgikey = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
				}

				ArrayList olistadigitalizacion = GetListaFacturasDigitalizacion(GetQueryDigitalizacion(sfechaanterior, sfechaposteriorDig));
				ArrayList olistaAgiKey = GetListaFacturasAgiKey(GetQueryAgiKey(sfechaanterior, sfechaposteriorAgikey));
				return CompararListas(olistadigitalizacion, olistaAgiKey);
			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return null;
			}


		}

		#region Obtener Facturas Digitalizacion


		private static String GetQueryDigitalizacion(String sfechaanterior, String sfechaposterior)
		{
			try
			{
				String query = "set dateformat dmy;";
				//Descargadas y confirmadas
				query += "SELECT * FROM edocScanCert.dbo.FACTURAS " +
					" where activo = 'S' " +
					" AND EXISTS(SELECT* FROM LOTES WHERE IDCLIENTE= 62 AND LOTES.IDLOTE= FACTURAS.IDLOTE) " +
					" and((FECHAALTA >= '<fechaanterior>' and FECHAALTA < '<fechaposterior>') " +
					" or(FechaResolucion >= '<fechaanterior>' and FechaResolucion < '<fechaposterior>')) " +
					" and Situacion = 'FACTURA INTEGRADA' ";
				query = query.Replace("<fechaanterior>", sfechaanterior);
				query = query.Replace("<fechaposterior>", sfechaposterior);
				return query;
			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return "";
			}
		}

		private static ArrayList GetListaFacturasDigitalizacion(String sQuery)
		{
			try
			{
				ArrayList olista = null;
				DataSet ds = new DataSet();
				_strCadenaConexion = ConfigurationManager.AppSettings["conectionstring"];
				createConnection();
				setCommandText(sQuery);
				cargarDataSet(ds, "dep");
				if (ds != null)
				{
					if (ds.Tables["dep"].Rows.Count > 0)
					{
						for (int i = 0; i < ds.Tables["dep"].Rows.Count; i++)
						{
							clasefactura ofactura = new clasefactura();
							ofactura.id = ds.Tables["dep"].Rows[i]["IDFACTURA"].ToString();
							ofactura.cifproveedor = ds.Tables["dep"].Rows[i]["cif"].ToString();
							ofactura.nombre = ds.Tables["dep"].Rows[i]["ENTIDAD"].ToString();
							ofactura.factura = ds.Tables["dep"].Rows[i]["NUMERO"].ToString();
							ofactura.fecharegistrobbdd = ds.Tables["dep"].Rows[i]["FECHAALTA"].ToString();
							ofactura.fechafactura = ds.Tables["dep"].Rows[i]["FECHAALTA"].ToString();
							if (olista == null) olista = new ArrayList();
							olista.Add(ofactura);
						}

					}
				}
				cerrarConexion();
				if (olista != null)
				{
					Console.WriteLine("Facturas Digitalización: " + olista.Count.ToString());
					sContador += "<p></p>" +  "Facturas Digitalización: " + olista.Count.ToString();
				}
				else
				{
					Console.WriteLine("Facturas Digitalización: 0");
					sContador += "<p></p>" +  "Facturas Digitalización: 0";
				}
				return olista;
			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return null;
			}
		}
		#endregion

		#region Obtener Facturas Agikey
		private static ArrayList GetListaFacturasAgiKey(String sQuery)
		{
			try
			{
				DataTable ds = null;
				ArrayList olista = null;
				wsagikeymarmedsa.MarmedsaServiceSoapClient oservicio = new wsagikeymarmedsa.MarmedsaServiceSoapClient();
				DataSet dt = oservicio.GetDataSet(sQuery, "marmedsa@agikey.com", "AgiKey12");
				ds = dt.Tables[0];
				if (ds != null)
				{
					if (ds.Rows.Count > 0)
					{
						for (int i = 0; i < ds.Rows.Count; i++)
						{
							clasefactura ofactura = new clasefactura();
							ofactura.id = ds.Rows[i]["id"].ToString();
							ofactura.cifproveedor = ds.Rows[i]["cifemisor"].ToString();
							ofactura.nombre = ds.Rows[i]["nombreemisor"].ToString();
							ofactura.factura = ds.Rows[i]["InvoiceNumber"].ToString();
							ofactura.fecharegistrobbdd = ds.Rows[i]["fecharegistroagikey"].ToString();
							ofactura.fechafactura = ds.Rows[i]["IssueDate"].ToString();
							if (olista == null) olista = new ArrayList();
							olista.Add(ofactura);
						}

					}
				}
				if (olista != null)
				{
					Console.WriteLine("Facturas Agikey: " + olista.Count.ToString());
					sContador +=   "Facturas Agikey: " + olista.Count.ToString();
				}
				else
				{
					Console.WriteLine("Facturas AgiKey: 0");
					sContador += "<p></p>" +  "Facturas Agikey: 0";
				}
				return olista;

			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return null;
			}
		}

		private static String GetQueryAgiKey(String sfechaanterior, String sfechaposterior)
		{
			try
			{
				String query = "set dateformat dmy;";
				//Descargadas y confirmadas
				query += "select " +
					" F.id,EE.Cif as cifemisor, EE.Nombre as nombreemisor, EE.IdGrupoEmpresa as grupoemisor,ER.Cif as cifreceptor, " +
					" ER.Nombre as nombrereceptor,ER.IdGrupoEmpresa as gruporeceptor,F.InvoiceNumber,F.CreateAt as fecharegistroagikey,F.IssueDate " +
					" from facturas as F " +
					" inner join Empresas as EE on F.IdEmpresaEmisora = EE.Id " +
					" inner join Empresas as ER on F.IdEmpresaReceptora = ER.Id " +
					" where " +
					" F.Id in (" +
					" select IdFacturaE from Historicos " +
					" where State = 1 and(CreateAt >= '<fechaanterior>' and CreateAt < '<fechaposterior>') " +
					" ) " +
					" and " +
					" ( " +
					" F.IdEmpresaReceptora in (select Id from Empresas where IdGrupoEmpresa = 2) " +
					//" and not(F.IdEmpresaEmisora in (select Id from Empresas where IdGrupoEmpresa= 2) ) " +
					" ) " +
					" order by F.Id desc";
				query = query.Replace("<fechaanterior>", sfechaanterior);
				query = query.Replace("<fechaposterior>", sfechaposterior);
				return query;
			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return "";
			}
		}
		#endregion

		#region Comparar listas
		/// <summary>
		/// Compara las dos listas y devuelve una con las facturas de digitalización que no encuentra en AgiKey
		/// </summary>
		/// <param name="olistadigitalizacion"></param>
		/// <param name="olistaAgikey"></param>
		/// <returns></returns>
		private static ArrayList CompararListas(ArrayList olistadigitalizacion, ArrayList olistaAgikey)
		{
			try
			{
				ArrayList oListaComparar = null;
				if (olistadigitalizacion != null)
				{
					for (int i = 0; i < olistadigitalizacion.Count; i++)
					{
						clasefactura ofacturadigitalizacion = (clasefactura)olistadigitalizacion[i];
						Boolean bExiste = false;
						Console.WriteLine("Comparar digitalizacion" + (i + 1) + " de " + olistadigitalizacion.Count);
						if (olistaAgikey != null)
						{
							for (int j = 0; j < olistaAgikey.Count; j++)
							{
								clasefactura ofacturaAgiKey = (clasefactura)olistaAgikey[j];
								Console.WriteLine("Comparar agikey" + (j + 1) + " de " + olistaAgikey.Count);
								if (ofacturadigitalizacion.factura == ofacturaAgiKey.factura)
									if (ofacturadigitalizacion.cifproveedor == ofacturaAgiKey.cifproveedor)
									{
										bExiste = true;
										break;
									}
							}
						}
						if (!bExiste)
						{
							if (oListaComparar == null) oListaComparar = new ArrayList();
							oListaComparar.Add(ofacturadigitalizacion);
							sFacturasSinIntegrar += "Factura: ID: " + ofacturadigitalizacion.id + " - CIF: " +
								ofacturadigitalizacion.cifproveedor + " - NOMBRE: " + ofacturadigitalizacion.nombre +
								" - NUMERO: " + ofacturadigitalizacion.factura +"<p></p>";
						}
					}
				}
				
				if (oListaComparar != null)
				{
					Console.WriteLine("Facturas en digitalización y no en Agikey: : " + oListaComparar.Count.ToString());
					sContador += "<p></p>" +  "Facturas en digitalización y no en Agikey: : " + oListaComparar.Count.ToString();
				}
				else
				{
					Console.WriteLine("Facturas en digitalización y no en Agikey: 0");
					sContador += "<p></p>" +  "Facturas en digitalización y no en Agikey: 0";
				}
				return oListaComparar;
			}
			catch (Exception e)
			{
				sMensaje = e.Message;
				return null;
			}
		}
		#endregion

		#region Base de Datos
		private static object _objComando;
		private static object _objDataAdapter;
		private static object _objDataReader;
		private static object _objConexion;
		private static string _strTipoBBDD;
		private static object _objTransaccion;
		private static string _strCadenaConexion;
		private static string _CommandTimeout = "3000";


		private static void EjecutarSQL(String sql)
		{
			try
			{
				setCommandText(sql);
				int resutl = ejecutarSentencia();
				Console.WriteLine("Ejecutar " + sql + " Resultado: " + resutl.ToString());
			}
			catch (Exception e)
			{
				Console.WriteLine("Error ejectura SQL");
			}
		}
		private static void createConnection()
		{
			try
			{
				// Creamos un objeto xxxConnection
				newConnection();
				// Abrimos la conexion
				openConnection();
				// Creamos un data adapter para almacenar los resultados
				newDataAdapter();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error createconnetion:" + e.Message);
				throw e;
			}
		}

		private static void newConnection()
		{
			try
			{
				_objConexion = new SqlConnection(_strCadenaConexion);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newConnection:" + e.Message);
				throw e;
			}
		}

		private static void openConnection()
		{
			try
			{
				((SqlConnection)(_objConexion)).Open();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error openConnection:" + e.Message);
				throw e;
			}
		}

		private static void newDataAdapter()
		{
			try
			{
				_objDataAdapter = new SqlDataAdapter();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newDataAdapter:" + e.Message);
				throw e;
			}

		}

		private static void setCommandText(string nombreSentencia)
		{
			string sentencia;
			try
			{
				// Creamos un objeto xxxCommand
				newCommand();
				sentencia = nombreSentencia;
				((SqlCommand)(_objComando)).CommandText = sentencia;
				((SqlCommand)(_objComando)).CommandTimeout = System.Int32.Parse(_CommandTimeout);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error setCommandText:" + e.Message);
				throw e;
			}

		}

		private static void newCommand()
		{
			try
			{
				_objComando = new SqlCommand();
				((SqlCommand)(_objComando)).Connection = ((SqlConnection)(_objConexion));
				if (!(_objTransaccion == null))
				{
					((SqlCommand)(_objComando)).Transaction = ((System.Data.SqlClient.SqlTransaction)(_objTransaccion));
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newCommand:" + e.Message);
				throw e;
			}
		}

		private static int ejecutarSentencia()
		{
			int intResultado = 0;
			try
			{
				intResultado = ((SqlCommand)(_objComando)).ExecuteNonQuery();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error ejecutarSentencia:" + e.Message);
				throw e;
			}
			return intResultado;
		}

		private static void cerrarConexion()
		{
			try
			{
				if (_objConexion != null)
				{
					if (((SqlConnection)(_objConexion)).State == ConnectionState.Open) ((SqlConnection)(_objConexion)).Close();
				}
				_objConexion = null;

			}
			catch (Exception e)
			{
				Console.WriteLine("Error cerrarConexion:" + e.Message);
				throw e;
			}
		}

		private static void cargarDataSet(DataSet ds, string tabla)
		{
			string sen = "";
			try
			{
				((SqlDataAdapter)(_objDataAdapter)).SelectCommand = ((SqlCommand)(_objComando));
				sen = ((SqlDataAdapter)(_objDataAdapter)).SelectCommand.CommandText.ToString();
				((SqlDataAdapter)(_objDataAdapter)).Fill(ds, tabla);
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		#endregion

	}
}
