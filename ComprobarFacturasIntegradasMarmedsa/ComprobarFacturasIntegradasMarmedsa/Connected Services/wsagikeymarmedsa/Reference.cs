﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://bizlayer/", ConfigurationName="wsagikeymarmedsa.MarmedsaServiceSoap")]
    public interface MarmedsaServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/EnviarFacturaEMarmedsa", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsEnviarFacturas")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsEnviarFacturasWeb EnviarFacturaEMarmedsa(string cifEntidad, string facturasZipB64, string anexoZipB64, int idFormato, bool emitidoPorReceptor, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/EnviarFacturaEMarmedsa", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsEnviarFacturas")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsEnviarFacturasWeb> EnviarFacturaEMarmedsaAsync(string cifEntidad, string facturasZipB64, string anexoZipB64, int idFormato, bool emitidoPorReceptor, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/DescargarFacturasMarmedsa", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaServicesWebDescargaFacturas")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas DescargarFacturasMarmedsa(string cifentidadpeticion, string cifEmisor, string cifReceptor, string FechaEntradaDesde, string FechaEntradaHasta, string Formato, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/DescargarFacturasMarmedsa", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaServicesWebDescargaFacturas")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas> DescargarFacturasMarmedsaAsync(string cifentidadpeticion, string cifEmisor, string cifReceptor, string FechaEntradaDesde, string FechaEntradaHasta, string Formato, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/DescargarFacturasMarmedsaAgrupadas", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaServicesWebDescargaFacturas")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSDescargaFacturasMarmedsaZIP DescargarFacturasMarmedsaAgrupadas(string cifentidadpeticion, string sFicheroPeticionB64, string Formato, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/DescargarFacturasMarmedsaAgrupadas", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaServicesWebDescargaFacturas")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSDescargaFacturasMarmedsaZIP> DescargarFacturasMarmedsaAgrupadasAsync(string cifentidadpeticion, string sFicheroPeticionB64, string Formato, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WSUrlDetalleFacturasOutCaptcha", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsURLsAcceso")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsURLsAcceso WSUrlDetalleFacturasOutCaptcha(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WSUrlDetalleFacturasOutCaptcha", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsURLsAcceso")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsURLsAcceso> WSUrlDetalleFacturasOutCaptchaAsync(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WsDescargaDetalleFactura", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsURLsAcceso")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas WsDescargaDetalleFactura(string idFiscalEmisor, string idFiscalReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WsDescargaDetalleFactura", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWsURLsAcceso")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas> WsDescargaDetalleFacturaAsync(string idFiscalEmisor, string idFiscalReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsAltaUsuarioAgikey", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        string wsAltaUsuarioAgikey(string NombreUsuario, string MailUsuario, string CifEmpresa, bool EnviarMail, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsAltaUsuarioAgikey", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        System.Threading.Tasks.Task<string> wsAltaUsuarioAgikeyAsync(string NombreUsuario, string MailUsuario, string CifEmpresa, bool EnviarMail, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsReprocesarFactura", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        string wsReprocesarFactura(int idFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsReprocesarFactura", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        System.Threading.Tasks.Task<string> wsReprocesarFacturaAsync(int idFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsAltaUsuarioTerceroAgikey", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        string wsAltaUsuarioTerceroAgikey(string NombreUsuario, string MailUsuario, string CifEmpresa, string EmpresaPlataforma, bool EnviarMail, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/wsAltaUsuarioTerceroAgikey", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="String")]
        System.Threading.Tasks.Task<string> wsAltaUsuarioTerceroAgikeyAsync(string NombreUsuario, string MailUsuario, string CifEmpresa, string EmpresaPlataforma, bool EnviarMail, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WSConfirmarDescargaFactura", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWSConfirmacionDescargaFactura")]
        ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSConfirmacionDescargaFactura WSConfirmarDescargaFactura(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/WSConfirmarDescargaFactura", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="RespuestaWSConfirmacionDescargaFactura")]
        System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSConfirmacionDescargaFactura> WSConfirmarDescargaFacturaAsync(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/GetDataSet", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(RespuestaWSBase))]
        [return: System.ServiceModel.MessageParameterAttribute(Name="DataSet")]
        System.Data.DataSet GetDataSet(string squery, string idUsuario, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://bizlayer/GetDataSet", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="DataSet")]
        System.Threading.Tasks.Task<System.Data.DataSet> GetDataSetAsync(string squery, string idUsuario, string password);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class RespuestaWsEnviarFacturasWeb : RespuestaWSBase {
        
        private string receiptEfecField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ReceiptEfec {
            get {
                return this.receiptEfecField;
            }
            set {
                this.receiptEfecField = value;
                this.RaisePropertyChanged("ReceiptEfec");
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RespuestaWSConfirmacionDescargaFactura))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RespuestaWsURLsAcceso))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RespuestaWSDescargaFacturasMarmedsaZIP))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RespuestaServicesWebDescargaFacturas))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RespuestaWsEnviarFacturasWeb))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public abstract partial class RespuestaWSBase : object, System.ComponentModel.INotifyPropertyChanged {
        
        private bool statusOKField;
        
        private string errorMessageField;
        
        private string errorStackTraceField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public bool StatusOK {
            get {
                return this.statusOKField;
            }
            set {
                this.statusOKField = value;
                this.RaisePropertyChanged("StatusOK");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string ErrorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                this.errorMessageField = value;
                this.RaisePropertyChanged("ErrorMessage");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string ErrorStackTrace {
            get {
                return this.errorStackTraceField;
            }
            set {
                this.errorStackTraceField = value;
                this.RaisePropertyChanged("ErrorStackTrace");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class FacturaSWebDescargaBean : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string refFacturaField;
        
        private string idFiscalEmisorField;
        
        private string idFiscalReceptorField;
        
        private System.DateTime fechaFacturaField;
        
        private string ficheroB64PDFField;
        
        private string ficheroB64FacturaEField;
        
        private string gUIDField;
        
        private string ficheroB64AnexoField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string RefFactura {
            get {
                return this.refFacturaField;
            }
            set {
                this.refFacturaField = value;
                this.RaisePropertyChanged("RefFactura");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string IdFiscalEmisor {
            get {
                return this.idFiscalEmisorField;
            }
            set {
                this.idFiscalEmisorField = value;
                this.RaisePropertyChanged("IdFiscalEmisor");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string IdFiscalReceptor {
            get {
                return this.idFiscalReceptorField;
            }
            set {
                this.idFiscalReceptorField = value;
                this.RaisePropertyChanged("IdFiscalReceptor");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public System.DateTime FechaFactura {
            get {
                return this.fechaFacturaField;
            }
            set {
                this.fechaFacturaField = value;
                this.RaisePropertyChanged("FechaFactura");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string FicheroB64PDF {
            get {
                return this.ficheroB64PDFField;
            }
            set {
                this.ficheroB64PDFField = value;
                this.RaisePropertyChanged("FicheroB64PDF");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string FicheroB64FacturaE {
            get {
                return this.ficheroB64FacturaEField;
            }
            set {
                this.ficheroB64FacturaEField = value;
                this.RaisePropertyChanged("FicheroB64FacturaE");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string GUID {
            get {
                return this.gUIDField;
            }
            set {
                this.gUIDField = value;
                this.RaisePropertyChanged("GUID");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string FicheroB64Anexo {
            get {
                return this.ficheroB64AnexoField;
            }
            set {
                this.ficheroB64AnexoField = value;
                this.RaisePropertyChanged("FicheroB64Anexo");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class RespuestaWSConfirmacionDescargaFactura : RespuestaWSBase {
        
        private string receiptEfecField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ReceiptEfec {
            get {
                return this.receiptEfecField;
            }
            set {
                this.receiptEfecField = value;
                this.RaisePropertyChanged("ReceiptEfec");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class RespuestaWsURLsAcceso : RespuestaWSBase {
        
        private FacturaSWebDescargaBean facturaField;
        
        private string receiptEfecField;
        
        private string urlAccesoPrivadoField;
        
        private string urlAccesoPublicoField;
        
        private string fileCsvB64Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public FacturaSWebDescargaBean Factura {
            get {
                return this.facturaField;
            }
            set {
                this.facturaField = value;
                this.RaisePropertyChanged("Factura");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string ReceiptEfec {
            get {
                return this.receiptEfecField;
            }
            set {
                this.receiptEfecField = value;
                this.RaisePropertyChanged("ReceiptEfec");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string UrlAccesoPrivado {
            get {
                return this.urlAccesoPrivadoField;
            }
            set {
                this.urlAccesoPrivadoField = value;
                this.RaisePropertyChanged("UrlAccesoPrivado");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string UrlAccesoPublico {
            get {
                return this.urlAccesoPublicoField;
            }
            set {
                this.urlAccesoPublicoField = value;
                this.RaisePropertyChanged("UrlAccesoPublico");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string FileCsvB64 {
            get {
                return this.fileCsvB64Field;
            }
            set {
                this.fileCsvB64Field = value;
                this.RaisePropertyChanged("FileCsvB64");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class RespuestaWSDescargaFacturasMarmedsaZIP : RespuestaWSBase {
        
        private string receiptEfecField;
        
        private string fileZipB64Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ReceiptEfec {
            get {
                return this.receiptEfecField;
            }
            set {
                this.receiptEfecField = value;
                this.RaisePropertyChanged("ReceiptEfec");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string FileZipB64 {
            get {
                return this.fileZipB64Field;
            }
            set {
                this.fileZipB64Field = value;
                this.RaisePropertyChanged("FileZipB64");
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://bizlayer/")]
    public partial class RespuestaServicesWebDescargaFacturas : RespuestaWSBase {
        
        private string receiptEfecField;
        
        private FacturaSWebDescargaBean[] listaFacturasField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string ReceiptEfec {
            get {
                return this.receiptEfecField;
            }
            set {
                this.receiptEfecField = value;
                this.RaisePropertyChanged("ReceiptEfec");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ListaFacturas", Order=1)]
        public FacturaSWebDescargaBean[] ListaFacturas {
            get {
                return this.listaFacturasField;
            }
            set {
                this.listaFacturasField = value;
                this.RaisePropertyChanged("ListaFacturas");
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MarmedsaServiceSoapChannel : ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.MarmedsaServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MarmedsaServiceSoapClient : System.ServiceModel.ClientBase<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.MarmedsaServiceSoap>, ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.MarmedsaServiceSoap {
        
        public MarmedsaServiceSoapClient() {
        }
        
        public MarmedsaServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MarmedsaServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MarmedsaServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MarmedsaServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsEnviarFacturasWeb EnviarFacturaEMarmedsa(string cifEntidad, string facturasZipB64, string anexoZipB64, int idFormato, bool emitidoPorReceptor, string idUsuario, string password) {
            return base.Channel.EnviarFacturaEMarmedsa(cifEntidad, facturasZipB64, anexoZipB64, idFormato, emitidoPorReceptor, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsEnviarFacturasWeb> EnviarFacturaEMarmedsaAsync(string cifEntidad, string facturasZipB64, string anexoZipB64, int idFormato, bool emitidoPorReceptor, string idUsuario, string password) {
            return base.Channel.EnviarFacturaEMarmedsaAsync(cifEntidad, facturasZipB64, anexoZipB64, idFormato, emitidoPorReceptor, idUsuario, password);
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas DescargarFacturasMarmedsa(string cifentidadpeticion, string cifEmisor, string cifReceptor, string FechaEntradaDesde, string FechaEntradaHasta, string Formato, string idUsuario, string password) {
            return base.Channel.DescargarFacturasMarmedsa(cifentidadpeticion, cifEmisor, cifReceptor, FechaEntradaDesde, FechaEntradaHasta, Formato, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas> DescargarFacturasMarmedsaAsync(string cifentidadpeticion, string cifEmisor, string cifReceptor, string FechaEntradaDesde, string FechaEntradaHasta, string Formato, string idUsuario, string password) {
            return base.Channel.DescargarFacturasMarmedsaAsync(cifentidadpeticion, cifEmisor, cifReceptor, FechaEntradaDesde, FechaEntradaHasta, Formato, idUsuario, password);
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSDescargaFacturasMarmedsaZIP DescargarFacturasMarmedsaAgrupadas(string cifentidadpeticion, string sFicheroPeticionB64, string Formato, string idUsuario, string password) {
            return base.Channel.DescargarFacturasMarmedsaAgrupadas(cifentidadpeticion, sFicheroPeticionB64, Formato, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSDescargaFacturasMarmedsaZIP> DescargarFacturasMarmedsaAgrupadasAsync(string cifentidadpeticion, string sFicheroPeticionB64, string Formato, string idUsuario, string password) {
            return base.Channel.DescargarFacturasMarmedsaAgrupadasAsync(cifentidadpeticion, sFicheroPeticionB64, Formato, idUsuario, password);
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsURLsAcceso WSUrlDetalleFacturasOutCaptcha(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WSUrlDetalleFacturasOutCaptcha(idFiscalEmisor, idDepartamentoEmisor, idFiscalReceptor, idDepartamentoReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWsURLsAcceso> WSUrlDetalleFacturasOutCaptchaAsync(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WSUrlDetalleFacturasOutCaptchaAsync(idFiscalEmisor, idDepartamentoEmisor, idFiscalReceptor, idDepartamentoReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas WsDescargaDetalleFactura(string idFiscalEmisor, string idFiscalReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WsDescargaDetalleFactura(idFiscalEmisor, idFiscalReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaServicesWebDescargaFacturas> WsDescargaDetalleFacturaAsync(string idFiscalEmisor, string idFiscalReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WsDescargaDetalleFacturaAsync(idFiscalEmisor, idFiscalReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public string wsAltaUsuarioAgikey(string NombreUsuario, string MailUsuario, string CifEmpresa, bool EnviarMail, string idUsuario, string password) {
            return base.Channel.wsAltaUsuarioAgikey(NombreUsuario, MailUsuario, CifEmpresa, EnviarMail, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<string> wsAltaUsuarioAgikeyAsync(string NombreUsuario, string MailUsuario, string CifEmpresa, bool EnviarMail, string idUsuario, string password) {
            return base.Channel.wsAltaUsuarioAgikeyAsync(NombreUsuario, MailUsuario, CifEmpresa, EnviarMail, idUsuario, password);
        }
        
        public string wsReprocesarFactura(int idFactura, string idUsuario, string password) {
            return base.Channel.wsReprocesarFactura(idFactura, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<string> wsReprocesarFacturaAsync(int idFactura, string idUsuario, string password) {
            return base.Channel.wsReprocesarFacturaAsync(idFactura, idUsuario, password);
        }
        
        public string wsAltaUsuarioTerceroAgikey(string NombreUsuario, string MailUsuario, string CifEmpresa, string EmpresaPlataforma, bool EnviarMail, string idUsuario, string password) {
            return base.Channel.wsAltaUsuarioTerceroAgikey(NombreUsuario, MailUsuario, CifEmpresa, EmpresaPlataforma, EnviarMail, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<string> wsAltaUsuarioTerceroAgikeyAsync(string NombreUsuario, string MailUsuario, string CifEmpresa, string EmpresaPlataforma, bool EnviarMail, string idUsuario, string password) {
            return base.Channel.wsAltaUsuarioTerceroAgikeyAsync(NombreUsuario, MailUsuario, CifEmpresa, EmpresaPlataforma, EnviarMail, idUsuario, password);
        }
        
        public ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSConfirmacionDescargaFactura WSConfirmarDescargaFactura(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WSConfirmarDescargaFactura(idFiscalEmisor, idDepartamentoEmisor, idFiscalReceptor, idDepartamentoReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<ComprobarFacturasIntegradasMarmedsa.wsagikeymarmedsa.RespuestaWSConfirmacionDescargaFactura> WSConfirmarDescargaFacturaAsync(string idFiscalEmisor, string idDepartamentoEmisor, string idFiscalReceptor, string idDepartamentoReceptor, string referenciaFactura, string fechaFactura, string idUsuario, string password) {
            return base.Channel.WSConfirmarDescargaFacturaAsync(idFiscalEmisor, idDepartamentoEmisor, idFiscalReceptor, idDepartamentoReceptor, referenciaFactura, fechaFactura, idUsuario, password);
        }
        
        public System.Data.DataSet GetDataSet(string squery, string idUsuario, string password) {
            return base.Channel.GetDataSet(squery, idUsuario, password);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> GetDataSetAsync(string squery, string idUsuario, string password) {
            return base.Channel.GetDataSetAsync(squery, idUsuario, password);
        }
    }
}
