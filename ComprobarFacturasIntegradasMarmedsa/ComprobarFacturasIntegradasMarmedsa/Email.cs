﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotMailsToolkits
{
	class Email
	{
		internal int Id { get; set; }
		internal string Asunto { get; set; }
		internal int IdEntrada { get; set; }
		internal long IdExterno { get; set; }
		internal DateTime? FechaEntrada { get; set; }
		internal DateTime? FechaLeido { get; set; }
		internal string Emisor { get; set; }
		internal int idRemesa { get; set; }
		internal int idEmailPadre { get; set; }
		internal string Cuerpo { get; set; }


		public Email(string Asunto, string Cuerpo, int IdEntrada, long IdExterno, DateTime? FechaEntrada, string Emisor, int idRemesa, int idEmailPadre = -1)
		{
			this.Asunto = Asunto;
			this.Cuerpo = Cuerpo;
			this.IdEntrada = IdEntrada;
			this.IdExterno = IdExterno;
			this.FechaEntrada = FechaEntrada;
			this.Emisor = Emisor;
			this.idRemesa = idRemesa;
			this.idEmailPadre = idEmailPadre;

		}

		//internal int InsertarEmail()
		//{
		//	Id = DatabaseConnection.executeScalarInt(queries.Insert.NuevoEmail(Asunto, IdEntrada, IdExterno, FechaEntrada, Emisor, idRemesa, idEmailPadre), System.Data.CommandType.Text, ConnectionString.DB);
		//	return Id;
		//}

		//internal static bool YaRegistrado(long uid, int IdEntrada)
		//{
		//	int AuxId = DatabaseConnection.executeScalarInt(queries.Select.GetIdExterno(uid, IdEntrada), System.Data.CommandType.Text, ConnectionString.DB);
		//	return (AuxId > 0);

		//}

	}
}
