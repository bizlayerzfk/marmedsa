
package Invoice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the Invoice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IDataFileName_QNAME = new QName("http://ws.bsf.indra.es", "name");
    private final static QName _IDataFileToken_QNAME = new QName("http://ws.bsf.indra.es", "token");
    private final static QName _IDataFileBase64Content_QNAME = new QName("http://ws.bsf.indra.es", "base64Content");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: Invoice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PutInvoice }
     * 
     */
    public PutInvoice createPutInvoice() {
        return new PutInvoice();
    }

    /**
     * Create an instance of {@link ConfirmReceptionResponse }
     * 
     */
    public ConfirmReceptionResponse createConfirmReceptionResponse() {
        return new ConfirmReceptionResponse();
    }

    /**
     * Create an instance of {@link GetStateInvoice }
     * 
     */
    public GetStateInvoice createGetStateInvoice() {
        return new GetStateInvoice();
    }

    /**
     * Create an instance of {@link PutInvoiceResponse }
     * 
     */
    public PutInvoiceResponse createPutInvoiceResponse() {
        return new PutInvoiceResponse();
    }

    /**
     * Create an instance of {@link GetStateInvoiceResponse }
     * 
     */
    public GetStateInvoiceResponse createGetStateInvoiceResponse() {
        return new GetStateInvoiceResponse();
    }

    /**
     * Create an instance of {@link GetInvoiceFilesResponse }
     * 
     */
    public GetInvoiceFilesResponse createGetInvoiceFilesResponse() {
        return new GetInvoiceFilesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfIDataFile }
     * 
     */
    public ArrayOfIDataFile createArrayOfIDataFile() {
        return new ArrayOfIDataFile();
    }

    /**
     * Create an instance of {@link GetInvoiceFiles }
     * 
     */
    public GetInvoiceFiles createGetInvoiceFiles() {
        return new GetInvoiceFiles();
    }

    /**
     * Create an instance of {@link ConfirmReception }
     * 
     */
    public ConfirmReception createConfirmReception() {
        return new ConfirmReception();
    }

    /**
     * Create an instance of {@link IDataFile }
     * 
     */
    public IDataFile createIDataFile() {
        return new IDataFile();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bsf.indra.es", name = "name", scope = IDataFile.class)
    public JAXBElement<String> createIDataFileName(String value) {
        return new JAXBElement<String>(_IDataFileName_QNAME, String.class, IDataFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bsf.indra.es", name = "token", scope = IDataFile.class)
    public JAXBElement<String> createIDataFileToken(String value) {
        return new JAXBElement<String>(_IDataFileToken_QNAME, String.class, IDataFile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.bsf.indra.es", name = "base64Content", scope = IDataFile.class)
    public JAXBElement<byte[]> createIDataFileBase64Content(byte[] value) {
        return new JAXBElement<byte[]>(_IDataFileBase64Content_QNAME, byte[].class, IDataFile.class, ((byte[]) value));
    }

}
