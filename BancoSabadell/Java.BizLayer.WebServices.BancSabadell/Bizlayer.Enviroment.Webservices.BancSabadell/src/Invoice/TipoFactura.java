
package Invoice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoFactura.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoFactura">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TXT"/>
 *     &lt;enumeration value="FACTURAE"/>
 *     &lt;enumeration value="XML_BSF"/>
 *     &lt;enumeration value="XML_BSF_NE"/>
 *     &lt;enumeration value="OWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoFactura")
@XmlEnum
public enum TipoFactura {

    TXT,
    FACTURAE,
    XML_BSF,
    XML_BSF_NE,
    OWN;

    public String value() {
        return name();
    }

    public static TipoFactura fromValue(String v) {
        return valueOf(v);
    }

}
