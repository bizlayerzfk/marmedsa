package Bizlayer.Application.BancSabadell.Invoices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import Invoice.*;

public class Invoices
{
	
	static
	{
	    java.net.Authenticator.setDefault
	    (
	    	new java.net.Authenticator()
	    	{
		        @Override
		        protected java.net.PasswordAuthentication getPasswordAuthentication()
		        {
		            return new java.net.PasswordAuthentication("A08148710WS", "A08148710".toCharArray());
		        }
		    }
	    );
	}
	
	public static String sDirectorioDescarga="";
	public static String sDescargarconToken="";
	
	public static void main(String[] args) throws IOException
	{
		/* Cargamos las propiedades */
		String directorioejecucion = directorioEjecucion();
		CargarPropiedades();
		/* llamadas con objeto */
		Invoice.BSFConnectorInvoiceService svcBancSabadellInvoice = new Invoice.BSFConnectorInvoiceService();
		BSFConnectorInvoiceServicePortType svc = svcBancSabadellInvoice.getBSFConnectorInvoiceServiceHttpPort();
		Invoice.ArrayOfIDataFile cltInvoices = DescargarFacturas(svc);
		ConfirmarDescarga(svc,cltInvoices);
	}
	

	/* llamadas con objeto ws creado */
	private static Invoice.ArrayOfIDataFile DescargarFacturas(BSFConnectorInvoiceServicePortType svc) throws IOException
	{
		Invoice.ArrayOfIDataFile cltInvoices = svc.getInvoiceFiles("A08148710");
		if(cltInvoices != null)
		{
			for(int x=0;x<cltInvoices.getIDataFile().size();x++) {
				  String snombrefichero = cltInvoices.getIDataFile().get(x).getName().getValue().toString();
				  String stoken = cltInvoices.getIDataFile().get(x).getToken().getValue().toString();
				  System.out.println("TOKEN: " + stoken);
				  int posToken = snombrefichero.indexOf("_TK");
				  if (stoken.equals("") || posToken < 1)
				  {
					  System.out.println("TOKEN VACIO");
					  System.out.println(snombrefichero);
					  snombrefichero = sDirectorioDescarga + snombrefichero;
					  System.out.println("Descargando: " + snombrefichero);
					  byte[] fileArray = cltInvoices.getIDataFile().get(x).getBase64Content().getValue();
					  FileOutputStream fileOuputStream = new FileOutputStream(snombrefichero);
					  fileOuputStream.write(fileArray);
					  fileOuputStream.close();
				  }
				  if (sDescargarconToken.equals("SI"))
				  {
					  System.out.println("TOKEN SI");
					  System.out.println(snombrefichero);
					  snombrefichero = sDirectorioDescarga + snombrefichero;
					  System.out.println("Descargando: " + snombrefichero);
					  byte[] fileArray = cltInvoices.getIDataFile().get(x).getBase64Content().getValue();
					  FileOutputStream fileOuputStream = new FileOutputStream(snombrefichero);
					  fileOuputStream.write(fileArray);
					  fileOuputStream.close();
				  }
				  
			}
			System.out.println("Respuesta OK");
		}
		System.out.println("Finalizado..");
		return cltInvoices;
	}
	
	private static boolean ConfirmarDescarga(BSFConnectorInvoiceServicePortType svc, Invoice.ArrayOfIDataFile cltInvoices)
	{
		String sToken="";
		boolean sconfirm = false;
		if(cltInvoices != null)
		{
			for(int x=0;x<cltInvoices.getIDataFile().size();x++) {
				  sToken = cltInvoices.getIDataFile().get(x).getToken().getValue().toString();
				  sconfirm = svc.confirmReception("A08148710",sToken);
				  System.out.println("Confirmar " + sToken + " resultado " + sconfirm);
			}
			
		}	
		
		return sconfirm;
	
	}

	
	/* Cargamos el fichero propiedades */
	private static void CargarPropiedades()
	{
		Properties propiedades = new Properties();
	    InputStream entrada = null;

	    try {

	        entrada = new FileInputStream("InvoicesBSabadell.properties");

	        // cargamos el archivo de propiedades
	        propiedades.load(entrada);

	        // obtenemos las propiedades y las imprimimos
	        System.out.println(propiedades.getProperty("carpetadescarga"));
	        sDirectorioDescarga = propiedades.getProperty("carpetadescarga");
	        System.out.println(propiedades.getProperty("descargarcontoken"));
	        sDescargarconToken = propiedades.getProperty("descargarcontoken");

	    } catch (IOException ex) {
	        ex.printStackTrace();
	    } finally {
	        if (entrada != null) {
	            try {
	                entrada.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	private static String directorioEjecucion()
	{
	     File miDir = new File (".");
	     try {
	       System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
	       return miDir.getCanonicalPath();
	       }
	     catch(Exception e) {
	       e.printStackTrace();
	       return "";
	       }
	}
}
