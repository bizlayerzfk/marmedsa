﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace CrearFacturaMarmedsaSabadell.Excel
{
    public static class Excel
    {

        #region Properties

        private static OleDbConnection itmConnection = null;
        /// <summary>
        /// Retorna la connexió a un arxiu excel
        /// </summary>
        public static OleDbConnection Connection
        {
            get { return itmConnection; }
        }

        #endregion Properties


        #region Methods

        #region Public

        public static void CloseExcelFile()
        {
            try
            {
                // Comprova si l'arxiu és obert
                if (itmConnection != null && itmConnection.State != ConnectionState.Closed)
                {
                    // Tanca l'arxiu
                    itmConnection.Close();
                    itmConnection.Dispose();
                }
            }
            catch (Exception)
            {
            }
        }

        public static OleDbDataReader OpenExcelFile(String prmExcelFilePath)
        {
            String sConnectionString = String.Empty;
            String sQuery = String.Empty;
            OleDbCommand cmdExcelFile = null;
            // Crea el contenedor del resultado
            OleDbDataReader rdrResult = null;
            try
            {
                sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + prmExcelFilePath +
                    ";Extended Properties='Excel 8.0;HDR=YES;MaxScanRows=0;IMEX=1';";
                //sConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + prmExcelFilePath +
                //    ";Extended Properties='Excel 12.0;HDR=YES;MaxScanRows=0;IMEX=1';";
                // Crea la conexión al archivo excel
                itmConnection = new OleDbConnection(sConnectionString);
                // Abre la conexión con el archivo
                itmConnection.Open();
                // Configura la consulta
                sQuery = "SELECT * FROM [Hoja1$] WHERE Emp <> ''";
                // Crea el ejecutor de la consulta
                cmdExcelFile = new OleDbCommand(sQuery, itmConnection);
                // Ejecuta la consulta y recoge el resultado
                rdrResult = cmdExcelFile.ExecuteReader();
            }
            catch (Exception ectExcel)
            {
                
                // Tanca la connexió a l'arxiu
                CloseExcelFile();
                // Inicialitza el contenidor del resultat
                rdrResult = null;
            }
            // Retorna el resultat
            return rdrResult;
        }

        #endregion Public

        #endregion Methods

    }
}
