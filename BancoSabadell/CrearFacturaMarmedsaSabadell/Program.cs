﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data.OleDb;
using AgiKeyMailLib;

namespace CrearFacturaMarmedsaSabadell
{
    class Program
    {
        #region Varibles
        public static String sFilein="";
        public static String sFileout = "";
        public static String sFileExtension = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Extensions.xml";
        public static ArrayList oCiudad = null;
        public static ArrayList oDepartamento = null;
        public static String sDirectorioFacturas = "";
        public static String sDirectorioFacturasenviar = "";
        public static String[] sCIFReceptoresExcluidos;
        public static String[] sCIFEmisoresAceptados;
        public static ArrayList aListaEmpresas = null;
        public static String sFileListaEmpresasMarmedsa = "";
        public static ArrayList aListaSuplidos = null;
        public static String sFileListaSuplidos = "";


		//Variables para el informe
		private static ArrayList ResultadoProceso = null;
		private static ArrayList errorInforme = null;
		private static ArrayList intInforme = null;
		#endregion

		static void Main(string[] args)
        {
            
            sDirectorioFacturas = ConfigurationManager.AppSettings["sDirectorioFacturas"];
            sDirectorioFacturasenviar = ConfigurationManager.AppSettings["sDirectorioFacturasenviar"];
            sFileListaEmpresasMarmedsa = ConfigurationManager.AppSettings["sListaMarmedsa"];
            sFileListaSuplidos = ConfigurationManager.AppSettings["sListaSuplidos"];
            String scif = ConfigurationManager.AppSettings["sCifs"];
            String sCifEmisores = ConfigurationManager.AppSettings["sCifEmisores"]; 
            Init();
            sCIFReceptoresExcluidos = scif.Split(';');
            sCIFEmisoresAceptados = sCifEmisores.Split(';');
            String sDirectorioMonitorizacion = procesarArchivos(sDirectorioFacturas);
            if (sDirectorioMonitorizacion !="")
            { 
                string[] fileNames = Directory.GetFiles(sDirectorioMonitorizacion, "*.xml");

                for (int i = 0; i < fileNames.Length; i++)
                {
                    String sFichero = fileNames[i];

                    String sFicheroPdf = fileNames[i].Replace("xml", "pdf");
					String sFicheroZip = fileNames[i].Replace("xml", "zip");


					try
                    {
                        CrearFicheroOriginal(sFichero);
                        int j = i + 1;
                        Console.WriteLine("Procesando " + j + " de " + fileNames.Length);
                        sFilein = sFichero;
                        sFileout = sFichero.Replace(".xml", "_sinfirma.xml");
                        DeleteSignature(sFilein, sFileout);

                        //Añadimos centro administrativo.
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_sinfirma.xml", "_conceros.xml");
                        Boolean bSuplido = false;
                        AddAdministrativeCentreBuyer(sFilein, sFileout, ref bSuplido);

                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conceros.xml", "_conadmcentre.xml");
                        AddAdministrativeCentreSeller(sFilein, sFileout);

                        //Miramos si es suplido debemos de informar los nuevos nodos Reimbursable
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conadmcentre.xml", "_Reimbursable.xml");
                        AddNodeReimbursable(sFilein, sFileout, bSuplido);

                        //Borramos las lineas con valor Cero
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_Reimbursable.xml", "_sincero.xml");
                        DeleteInvoiceLineZero(sFilein, sFileout);

                        //Ponemos el <SequenceNumber>1</SequenceNumber> antes de ItemDescription
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_sincero.xml", "_conSequenceNumber.xml");
                        AddSequenceNumberInvoiceLine(sFilein, sFileout);

                        //Añadimos xml original 
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conSequenceNumber.xml", "_conxml.xml");
                        AddAttachmentDocument_xml(sFilein, sFichero, sFileout);

                        //Añadimos extension
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conxml.xml", "_conespacios.xml");
                        AddExtension(sFilein, sFileout);

                        //Quitamos los espacios en la referencia factura
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_conespacios.xml", "_Mailextension.xml");
                        QuitarEspaciosReferenciaFactura(sFilein, sFileout);

                        //Añadimos el mail de notificacion si viene en la factura o dependiendo empresas marmedsa
                        sFilein = sFileout;
                        sFileout = sFilein.Replace("_Mailextension.xml", ".xml");
                        AddMailExtension(sFilein, sFileout);

						String sTipoError = "OK";
						if (File.Exists(sFilein))
                        {
                            //Si la receptora es una entidad excluyente no la ponemos a procesar si no la movemos a CifExcluido.
                            if (IsCifReceptorExcluido(sFichero))
                            {
                                moverAErrorExcluida(sFichero, sDirectorioMonitorizacion);
                                moverAErrorExcluida(sFicheroPdf, sDirectorioMonitorizacion);
								moverAErrorExcluida(sFicheroZip, sDirectorioMonitorizacion);
								sTipoError = "moverAErrorExcluida";
							}
                            //Si el cif emisor está aceptado seguimos si no lo movemos a erroremisornoaceptado
                            if (!IsCifEmisorAceptado(sFichero))
                            {
                                moverAErrorEmisorNoAceptado(sFichero, sDirectorioMonitorizacion);
                                moverAErrorEmisorNoAceptado(sFicheroPdf, sDirectorioMonitorizacion);
								moverAErrorEmisorNoAceptado(sFicheroZip, sDirectorioMonitorizacion);
								sTipoError = "moverAErrorEmisorNoAceptado";
							}
                            //Si la receptora no es del grupo marmedsa, tratamos como error suplido
                            if (!bErrorSuplidos(sFichero))
                            {
                                moverAErrorSuplidos(sFichero, sDirectorioMonitorizacion);
                                moverAErrorSuplidos(sFicheroPdf, sDirectorioMonitorizacion);
								moverAErrorSuplidos(sFicheroZip, sDirectorioMonitorizacion);
								sTipoError = "moverAErrorSuplidos";
							}
                            //Copiamos los ficheros a la carpeta del toolkit
                            CopiarArchivo(sFichero, sDirectorioFacturasenviar);
                            CopiarArchivo(sFicheroPdf, sDirectorioFacturasenviar);
							CopiarArchivo(sFicheroZip, sDirectorioFacturasenviar);
							if (ResultadoProceso == null) ResultadoProceso = new ArrayList();
							ResultadoProceso.Add(sTipoError);
						}
                        else //si no existe el fichero _conxml es que algo ha fallado.
                        {
                            moverAError(sFichero, sDirectorioMonitorizacion);
                            moverAError(sFicheroPdf, sDirectorioMonitorizacion);
							moverAError(sFicheroZip, sDirectorioMonitorizacion);
							sTipoError = "moverAError";
							if (ResultadoProceso == null) ResultadoProceso = new ArrayList();
							ResultadoProceso.Add(sTipoError);
						}
                    }
                    catch (Exception)
                    {
                    }
                }
                //Movemos todo a la subcarpetas procesadas.
                BorrarFacturasIntermedias(sDirectorioMonitorizacion);
                moverAProcesadas(sDirectorioMonitorizacion);
				GenerarMailProceso();

			}


            Console.WriteLine("Fin proceso");

        }

        #region init vars
        private static void Init()
        {
            //Cargamos la lista de departamentos y ciudades.
            oCiudad = new ArrayList();
            oDepartamento = new ArrayList();
            oDepartamento.Add("AAE"); oCiudad.Add("ANNABA");
            oDepartamento.Add("ARG"); oCiudad.Add("ALGER");
            oDepartamento.Add("AZW"); oCiudad.Add("ARZEW");
            oDepartamento.Add("BJA"); oCiudad.Add("BEJAIA");
            oDepartamento.Add("DJE"); oCiudad.Add("Djen-Djen");
            oDepartamento.Add("MOS"); oCiudad.Add("MOSTAGANEM");
            oDepartamento.Add("ORN"); oCiudad.Add("ORAN");
            oDepartamento.Add("SKI"); oCiudad.Add("SKIKDA");
            oDepartamento.Add("BCN"); oCiudad.Add("BARCELONA");
            oDepartamento.Add("AGP"); oCiudad.Add("MALAGA");
            oDepartamento.Add("ALC"); oCiudad.Add("ALICANTE");
            oDepartamento.Add("ALD"); oCiudad.Add("ALCUDIA");
            oDepartamento.Add("ALG"); oCiudad.Add("ALGECIRAS");
            oDepartamento.Add("ALG"); oCiudad.Add("Puerto de algeciras");
            oDepartamento.Add("BIO"); oCiudad.Add("BILBAO");
//            oDepartamento.Add("BIO"); oCiudad.Add("BIZKAIA");
            oDepartamento.Add("CAD"); oCiudad.Add("CADIZ");
            //oDepartamento.Add("CAD"); oCiudad.Add("Cádiz");
            oDepartamento.Add("CAS"); oCiudad.Add("CASTELLON");
            oDepartamento.Add("GIJ"); oCiudad.Add("GIJON");
            oDepartamento.Add("HUV"); oCiudad.Add("HUELVA");
            oDepartamento.Add("LCG"); oCiudad.Add("LA CORUÑA");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS DE GRAN CANARIA");
            oDepartamento.Add("MAD"); oCiudad.Add("MADRID");
            oDepartamento.Add("PMI"); oCiudad.Add("PALMA DE MALLORCA");
            oDepartamento.Add("SCT"); oCiudad.Add("SANTA CRUZ DE TENERIFE");
            oDepartamento.Add("SVQ"); oCiudad.Add("SEVILLA");
            oDepartamento.Add("TAR"); oCiudad.Add("TARRAGONA");
            oDepartamento.Add("VGO"); oCiudad.Add("VIGO");
            oDepartamento.Add("VLC"); oCiudad.Add("VALENCIA");
            oDepartamento.Add("CAR"); oCiudad.Add("CARTAGENA");
            oDepartamento.Add("LEI"); oCiudad.Add("ALMERIA");
            oDepartamento.Add("MOT"); oCiudad.Add("MOTRIL");
            oDepartamento.Add("SDR"); oCiudad.Add("SANTANDER");
            oDepartamento.Add("LPA"); oCiudad.Add("LAS PALMAS");
            oDepartamento.Add("LEH"); oCiudad.Add("LE HAVRE");
            oDepartamento.Add("MRS"); oCiudad.Add("MARSEILLE");
            oDepartamento.Add("MTU"); oCiudad.Add("MARTIGUES");
            oDepartamento.Add("PAR"); oCiudad.Add("PARIS");
            oDepartamento.Add("CSB"); oCiudad.Add("CASABLANCA");
            oDepartamento.Add("TNG"); oCiudad.Add("TANGIER");
            oDepartamento.Add("LIS"); oCiudad.Add("LISBOA");
            oDepartamento.Add("OPO"); oCiudad.Add("PORTO");
            oDepartamento.Add("SIE"); oCiudad.Add("SINES");
            oDepartamento.Add("SET"); oCiudad.Add("SETUBAL");
            oDepartamento.Add("OPO"); oCiudad.Add("OPORTO");
            //Cargamos la lista de empresas desde el excel.
            CargarListaEmpresas();
            CargarListaSuplidos();
        }

        private static void CargarListaEmpresas()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaEmpresasMarmedsa);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaEmpresas == null) aListaEmpresas = new ArrayList();
                            CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[1].ToString();
                            oEmpresa.CIF = rdrExcelData[2].ToString();
                            oEmpresa.DEL = rdrExcelData[3].ToString();
                            oEmpresa.PROV = rdrExcelData[4].ToString();
                            oEmpresa.PAIS = rdrExcelData[5].ToString();
                            oEmpresa.DIRECCION = rdrExcelData[6].ToString();
                            oEmpresa.CODIGOPOSTAL = rdrExcelData[7].ToString();
                            oEmpresa.MAIL = rdrExcelData[8].ToString();
                            aListaEmpresas.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas empresas.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }

        private static void CargarListaSuplidos()
        {
            try
            {
                OleDbDataReader rdrExcelData = null;
                rdrExcelData = Excel.Excel.OpenExcelFile(sFileListaSuplidos);
                if (rdrExcelData != null)
                {
                    // Processa les dades recollides
                    while (rdrExcelData.Read())
                    {
                        try
                        {
                            if (aListaSuplidos == null) aListaSuplidos = new ArrayList();
                            CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = new CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa();
                            oEmpresa.EMP = rdrExcelData[0].ToString();
                            oEmpresa.NOM = rdrExcelData[3].ToString();
                            oEmpresa.CIF = rdrExcelData[1].ToString();
                            aListaSuplidos.Add(oEmpresa);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    // Tanca la connexió a l'arxiu
                    Excel.Excel.CloseExcelFile();
                    Console.WriteLine("Cargada listas Suplidos.");
                }



            }
            catch (Exception e)
            {
                Excel.Excel.CloseExcelFile();
            }
        }
        #endregion

        #region Eliminar Firma
        private static void DeleteSignature(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);

                XmlNode nodo = documento.SelectSingleNode("//Facturae");
                //fe:Facturae xmlns:fe="http://www.facturae.es/Facturae/2009/v3.2/Facturae"
                if (nodo == null)
                {
                    var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodo = documento.SelectSingleNode("//fe:Facturae", xmlNsM);
                }



                if (nodo != null)
                {
                    //ds:Signature Id="xmldsig-f37aba70-7c96-4b62-a577-d27352fed7ae" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                    XmlNode node1 = documento.SelectSingleNode("//Signature");
                    if (node1 == null)
                    {
                        var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                        xmlNsM.AddNamespace("ds", @"http://www.w3.org/2000/09/xmldsig#");
                        node1 = documento.SelectSingleNode("//ds:Signature", xmlNsM);
                    }
                    if (node1 != null)
                        nodo.RemoveChild(node1);
                    //sFilein = sFilein.Replace(".xml", "_sinSignature.xml");
                    documento.Save(sFileOut);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }
        #endregion

        #region Quitar Lineas a 0
        private static void DeleteInvoiceLineZero(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    XmlDocument documento = new XmlDocument();
                    documento.Load(sFilein);
                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "ItemDescription")
                                    {
                                        if(oinvoiceline.InnerText.ToUpper()=="TOTAL")
                                        {

                                        }
                                    }
                                    if (oinvoiceline.Name == "UnitPriceWithoutTax")
                                    {
                                        if (isCero(oinvoiceline.InnerText))
                                        {
                                            nItems.RemoveChild(oNodo);
                                            documento.Save(sFilein);
                                            documento = null;
                                            bsalir = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }

        private static Boolean isCero(String value)
        {
            try
            {
                if (value.Equals("0.000000")) return true;
                else return false;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        #endregion

        #region Añadir AdministrativeCentres
        private static void AddAdministrativeCentreBuyer(String sFilein, String sFileOut, ref Boolean bSuplido)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                XmlNode nAddressInSpain = null;
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach(XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oNodoTaxIdentification in oNodo)
                            {
                                if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    //Cargamos al Adresspain para el nuevo nodo.
                                    sTaxIdentificationNumber = oNodoTaxIdentification.InnerText.Trim();
                                }
                            }
                        }
                        

                    }
                }

                //Antes de añadir todo, vemos si es suplido o no
                bSuplido = false;
                CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                bSuplido = !EsEmpresaMarmedsa(sTaxIdentificationNumber,ref  oEmpresa);
                if (!bSuplido) //Código anterior a suplidos.
                {
                    //Si no existe lo añadimos.
                    if (baddAdministrativeCentres)
                    {
                        nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                        Boolean binsertCentro = false;
                        if (nBuyerParty != null)
                        {
                            foreach (XmlNode oNodo in nBuyerParty)
                            {
                                //Para el sabadell si hay partyidentification
                                if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                {
                                    XmlNode nodo1 = oNodo;
                                    nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                    documento.Save(sFileout);
                                    documento = null;
                                    binsertCentro = true;
                                    break;
                                }
                            }
                            //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                            if (!binsertCentro)
                            {
                                nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                                if (nBuyerParty != null)
                                {
                                    foreach (XmlNode oNodo in nBuyerParty)
                                    {
                                        if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                        {
                                            XmlNode nodo1 = oNodo;
                                            nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                            documento.Save(sFileout);
                                            documento = null;
                                            binsertCentro = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            documento.Save(sFileout);
                            documento = null;
                        }
                    }
                    else
                    {
                        documento.Save(sFileout);
                        documento = null;
                    }
                }
                else //es suplido
                {
                    
                    CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oSuplido = null;
                    EsSuplido(sTaxIdentificationNumber, ref oSuplido);
                    if (oSuplido != null)
                    {
                        //Añadimos el centroadministrativo y modificamos los datos de la empresa suplida por la de Marmedsa.
                        CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpMar = GetEmpresaMarmedsa(oSuplido.EMP);
                        if (oEmpMar != null)
                        {
                            nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "TaxIdentification")
                                    {
                                        foreach (XmlNode oNodoTaxIdentification in oNodo)
                                        {
                                            if (oNodoTaxIdentification.Name == "TaxIdentificationNumber")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoTaxIdentification.InnerText = oEmpMar.CIF;
                                            }
                                        }
                                    }
                                    //Para el sabadell si hay partyidentification
                                    if (oNodo.Name == "PartyIdentification")//Lo ponemos después del PartyIdentification que si viene en el Sabadell
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentreSuplido(documento, oEmpMar), nodo1);
                                        //documento.Save(sFileout);
                                        //documento = null;
                                        //break;
                                    }
                                }
                                //Miramos el legalEntity y cambiamos el overAddress
                                Boolean bOverAddress = false;
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {
                                                //Cargamos al Adresspain para el nuevo nodo.
                                                oNodoLegalEntity.InnerText = oEmpMar.NOM;
                                            }
                                            if (oNodoLegalEntity.Name == "AddressInSpain")
                                            {
                                                foreach (XmlNode oNodoAddressInSpain in oNodoLegalEntity)
                                                {
                                                    if (oNodoAddressInSpain.Name == "Address")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.DIRECCION;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "PostCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.CODIGOPOSTAL;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Town")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "Province")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PROV;
                                                    }
                                                    if (oNodoAddressInSpain.Name == "CountryCode")
                                                    {
                                                        oNodoAddressInSpain.InnerText = oEmpMar.PAIS;
                                                    }

                                                }


                                            }
                                            if (oNodoLegalEntity.Name == "OverseasAddress")
                                            {
                                                oNodo.RemoveChild(oNodoLegalEntity);
                                                bOverAddress = true;
                                            }
                                        }
                                    }
                                    if (bOverAddress)
                                    {
                                        foreach (XmlNode oNodoLegalEntity in oNodo)
                                        {
                                            if (oNodoLegalEntity.Name == "CorporateName")
                                            {
   
                                                XmlNode nodo1 = oNodoLegalEntity;
                                                oNodo.InsertAfter(CrearNodo_AddressInSpain(documento, oEmpMar), nodo1);

                                            }
                                        }

                                    }

                                }

                            }
                            documento.Save(sFileout);
                            documento = null;
                        }
                    }
                }
                if (documento != null)
                {
                    documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        private static void AddAdministrativeCentreSeller(String sFilein, String sFileOut)
        {
            try
            {
                Boolean baddAdministrativeCentres = true;
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//SellerParty");
                XmlNode nAddressInSpain = null;
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            baddAdministrativeCentres = false; //Si ya lo tiene no lo añadimos.
                        }
                        if (oNodo.Name == "LegalEntity")
                        {
                            foreach (XmlNode oNodoAddressInSpain in oNodo)
                            {
                                if (oNodoAddressInSpain.Name == "AddressInSpain")
                                {
                                    nAddressInSpain = oNodoAddressInSpain;
                                }
                            }
                        }
                        //Cargamos al Adresspain para el nuevo nodo.

                    }
                }
                //Si no existe lo añadimos.
                if (baddAdministrativeCentres)
                {
                    nBuyerParty = documento.SelectSingleNode("//SellerParty");
                    Boolean binsertCentro = false;
                    if (nBuyerParty != null)
                    {
                        foreach (XmlNode oNodo in nBuyerParty)
                        {
                            if (oNodo.Name == "TaxIdentification")//Lo ponemos después del TaxIdentification
                            {
                                XmlNode nodo1 = oNodo;
                                nBuyerParty.InsertAfter(CrearNodo_AdministrativeCentre(documento, nAddressInSpain), nodo1);
                                documento.Save(sFileout);
                                documento = null;
                                binsertCentro = true;
                                break;
                            }
                            //Cargamos al Adresspain para el nuevo nodo.

                        }
                        //Para Autoridad Portuaria BCN no hay partyidentification y lo ponemos delante del LegalEntity
                        if (!binsertCentro)
                        {
                            nBuyerParty = documento.SelectSingleNode("//SellerParty");
                            if (nBuyerParty != null)
                            {
                                foreach (XmlNode oNodo in nBuyerParty)
                                {
                                    if (oNodo.Name == "LegalEntity")//Lo ponemos antes del LegalEntity para Portic-BCN
                                    {
                                        XmlNode nodo1 = oNodo;
                                        nBuyerParty.InsertBefore((CrearNodo_AdministrativeCentre(documento, nAddressInSpain)), nodo1);
                                        documento.Save(sFileout);
                                        documento = null;
                                        binsertCentro = true;
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    documento.Save(sFileout);
                    documento = null;
                }
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        //<AdministrativeCentres>
        //  <AdministrativeCentre>
        //    <CentreCode>BCN</CentreCode>
        //    <AddressInSpain>
        //      <Address>ZAL 2 - C/ NYEPA, 2-5</Address>
        //      <PostCode> 08820</PostCode>
        //      <Town> EL PRAT DE LLOBREGAT</Town>
        //      <Province></Province>
        //      <CountryCode>ESP</CountryCode>
        //    </AddressInSpain>
        //  </AdministrativeCentre>
        //</AdministrativeCentres>
        private static XmlElement CrearNodo_AdministrativeCentre(XmlDocument xmlDoc,XmlNode oAddressInSpain)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");
                
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");
                foreach (XmlNode oNodo in oAddressInSpain)
                {
                    if (oNodo.Name == "Address")
                    {
                        Address.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "PostCode")
                    {
                        PostCode.InnerText = oNodo.InnerText;
                    }
                    if (oNodo.Name == "Town")
                    {
                        Town.InnerText = oNodo.InnerText;
                        String scentrecodeTown = getCentreCode(Town.InnerText);
                        if (scentrecodeTown != "") CentreCode.InnerText = scentrecodeTown;
                    }
                    if (oNodo.Name == "Province")
                    {
                        Province.InnerText = oNodo.InnerText;
                        String scentrecodeprovince = getCentreCode(Province.InnerText);
                        if (scentrecodeprovince != "") CentreCode.InnerText = scentrecodeprovince;

                        //CentreCode.InnerText = getCentreCode(Province.InnerText);
                    }
                    if (oNodo.Name == "CountryCode")
                    {
                        CountryCode.InnerText = oNodo.InnerText;
                    }

                }



                
                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");
                
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        private static XmlElement CrearNodo_AdministrativeCentreSuplido(XmlDocument xmlDoc, CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement CentreCode = xmlDoc.CreateElement("CentreCode");

                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;
  

                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);

                XmlElement AdministrativeCentre = xmlDoc.CreateElement("AdministrativeCentre");

                CentreCode.InnerText = oEmpresa.DEL; //en las no suplidos lo cogemos según provincia de la receptora, aquí lo ponemos según tabla.
                AdministrativeCentre.AppendChild(CentreCode);
                AdministrativeCentre.AppendChild(AddressInSpain);

                XmlElement AdministrativeCentres = xmlDoc.CreateElement("AdministrativeCentres");
                AdministrativeCentres.AppendChild(AdministrativeCentre);

                return AdministrativeCentres;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static XmlElement CrearNodo_AddressInSpain(XmlDocument xmlDoc, CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa)
        {
            try
            {
                XmlElement Address = xmlDoc.CreateElement("Address");
                XmlElement PostCode = xmlDoc.CreateElement("PostCode");
                XmlElement Town = xmlDoc.CreateElement("Town");
                XmlElement Province = xmlDoc.CreateElement("Province");
                XmlElement CountryCode = xmlDoc.CreateElement("CountryCode");

                Address.InnerText = oEmpresa.DIRECCION;
                PostCode.InnerText = oEmpresa.CODIGOPOSTAL;
                Town.InnerText = oEmpresa.PROV;
                Province.InnerText = oEmpresa.PROV;
                CountryCode.InnerText = oEmpresa.PAIS;


                XmlElement AddressInSpain = xmlDoc.CreateElement("AddressInSpain");
                AddressInSpain.AppendChild(Address);
                AddressInSpain.AppendChild(PostCode);
                AddressInSpain.AppendChild(Town);
                AddressInSpain.AppendChild(Province);
                AddressInSpain.AppendChild(CountryCode);


                return AddressInSpain;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve el codigo de centro según provincia receptora
        /// </summary>
        /// <param name="sProvincia"></param>
        /// <returns></returns>
        private static String getCentreCode(String sProvincia)
        {
            try
            {
                String scentrecode = "";
                for(int i=0;i<oCiudad.Count;i++)
                {
                    String sciudad = oCiudad[i].ToString();
                    if (sProvincia.ToUpper().Trim() == sciudad.ToUpper().Trim())
                    {
                        scentrecode = oDepartamento[i].ToString();
                        break;
                    }
                    
                }
                return scentrecode;
            }
            catch(Exception e)
            {
                return "";
            }
        }

        private static Boolean EsEmpresaMarmedsa(String sCif,ref CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsempresaMarmedsa = false;
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0 || sCif==oEmpresa.CIF)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsempresaMarmedsa = true;
                        break;
                    }
                }
                return bEsempresaMarmedsa;
            }
            catch (Exception e)
            {
                return bEsempresaMarmedsa;
            }
        }

        private static CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa GetEmpresaMarmedsa(String sEmp)
        {
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sEmp == oEmpresa.EMP)
                    {
                        return oEmpresa;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static Boolean EsSuplido(String sCif, ref CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresaMarmedsa)
        {
            Boolean bEsSuplido = false;
            try
            {
                for (int i = 0; i < aListaSuplidos.Count; i++)
                {
                    CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa)aListaSuplidos[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        oEmpresaMarmedsa = oEmpresa;
                        bEsSuplido = true;
                        break;
                    }
                }
                return bEsSuplido;
            }
            catch (Exception e)
            {
                return bEsSuplido;
            }
        }
        #endregion

        #region Añadir SequenceNumber a las lineas
        private static void AddSequenceNumberInvoiceLine(String sFilein, String sFileOut)
        {
            String sfileor = sFilein;
            try
            {
                Boolean bsalir = false;
                do
                {
                    int sSecuencia = 1;
                    XmlDocument documento = new XmlDocument();
                    documento.Load(sFilein);
                    bsalir = true;
                    XmlNode nItems = documento.SelectSingleNode("//Items");
                    if (nItems != null)
                    {
                        foreach (XmlNode oNodo in nItems)
                        {
                            if (oNodo.Name == "InvoiceLine")
                            {
                                foreach (XmlNode oinvoiceline in oNodo)
                                {
                                    if (oinvoiceline.Name == "ItemDescription")
                                    {
                                        XmlNode nodo1 = oinvoiceline;
                                        oNodo.InsertBefore(CrearNodo_SequenceNumber(documento, sSecuencia), nodo1);
                                        documento.Save(sFilein);
                                        sSecuencia++;
                                    }
                                }
                            }
                        }
                    }
                }
                while (!bsalir);
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }


        }



        private static XmlElement CrearNodo_SequenceNumber(XmlDocument xmlDoc,int iSecuencia)
        {
            try
            {
                XmlElement SequenceNumber = xmlDoc.CreateElement("SequenceNumber");
                SequenceNumber.InnerText = iSecuencia.ToString();

                return SequenceNumber;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir xml attachment
        private static Boolean AddAttachmentDocument_xml(String sFileFacturain, String sFilexml, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//AdditionalData");

                if (nodoAdd == null) //No existe el nodo AdditionalData
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoice");
                    String sxmlB64 = getFileB64(sFilexml);
                    nodo.AppendChild(CrearNodo_AdditionalData(xmlDoc, "NONE", "xml", "BASE64", sxmlB64));
                    xmlDoc.Save(sFileFacturaEout);
                }
                else
                {
                    XmlNode nodo1 = nodoAdd.FirstChild;
                    String sxmlB64 = getFileB64(sFilexml);
                    nodoAdd.InsertBefore(CrearNodo_RelatedDocuments(xmlDoc, "NONE", "xml", "BASE64", sxmlB64), nodo1);
                    xmlDoc.Save(sFileFacturaEout);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Crea el nodo AdditionalData con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_AdditionalData(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);


                XmlElement AdditionalData = xmlDoc.CreateElement("AdditionalData");
                AdditionalData.AppendChild(RelatedDocuments);

                return AdditionalData;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Crea el nodo RelatedDocuments con un documento según los parámetros informados..
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="sCompressionAlgorithm">por defecto NONE</param>
        /// <param name="sFormat">pdf,xml, etc</param>
        /// <param name="sEncoding">por defecto será BASE64</param>
        /// <param name="sData">string en base64 que contiene el fichero</param>
        /// <returns></returns>
        private static XmlElement CrearNodo_RelatedDocuments(XmlDocument xmlDoc,
                                                            String sCompressionAlgorithm,
                                                            String sFormat,
                                                            String sEncoding,
                                                            String sData)
        {
            try
            {
                XmlElement AttachmentCompressionAlgorithm = xmlDoc.CreateElement("AttachmentCompressionAlgorithm");
                AttachmentCompressionAlgorithm.InnerText = sCompressionAlgorithm;
                XmlElement AttachmentFormat = xmlDoc.CreateElement("AttachmentFormat");
                AttachmentFormat.InnerText = sFormat;
                XmlElement AttachmentEncoding = xmlDoc.CreateElement("AttachmentEncoding");
                AttachmentEncoding.InnerText = sEncoding;
                XmlElement AttachmentData = xmlDoc.CreateElement("AttachmentData");
                AttachmentData.InnerText = sData;

                XmlElement Attachment = xmlDoc.CreateElement("Attachment");
                Attachment.AppendChild(AttachmentCompressionAlgorithm);
                Attachment.AppendChild(AttachmentFormat);
                Attachment.AppendChild(AttachmentEncoding);
                Attachment.AppendChild(AttachmentData);

                XmlElement RelatedDocuments = xmlDoc.CreateElement("RelatedDocuments");
                RelatedDocuments.AppendChild(Attachment);

                return RelatedDocuments;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Añadir Extensiones
        private static Boolean AddExtension(String sFileFacturain, String sFileFacturaEout)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileFacturain);
                //Primero miramos si existe el nodo AdditionalData
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Facturae");

                if (nodoAdd == null)
                {
                    var xmlNsM = new XmlNamespaceManager(xmlDoc.NameTable);
                    xmlNsM.AddNamespace("fe", @"http://www.facturae.es/Facturae/2009/v3.2/Facturae");
                    nodoAdd = xmlDoc.SelectSingleNode("//fe:Facturae", xmlNsM);
                }

                if (nodoAdd != null)
                {
                    XmlNode nodo = xmlDoc.SelectSingleNode("//Invoices");
                    XmlNode onodo = xmlDoc.CreateElement("Extensions");
                    onodo.InnerXml = LeerFileExtension(sFileExtension);
                    //nodo.AppendChild(onodo);
                    //nodoAdd.AppendChild(onodo);
                    nodoAdd.InsertAfter(onodo, nodo);
                    xmlDoc.Save(sFileFacturaEout);
                }
                //{"El nodo que desea insertar pertenece a otro contexto de documento."}
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static String LeerFileExtension(String sFileExtension)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(sFileExtension);
                XmlNode nodoAdd = xmlDoc.SelectSingleNode("//Extensions");
                if (nodoAdd != null)
                {
                    return nodoAdd.InnerXml;
                }
                else return "";

            }
            catch (Exception e)
            {
                return "";
            }
        }

        #endregion

        #region CIF receptor Excluidos
        private static Boolean IsCifReceptorExcluido(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bCifExluido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < sCIFReceptoresExcluidos.Length; i++)
                    {
                        if (sTaxIdentificationNumber == sCIFReceptoresExcluidos[i])
                        { bCifExluido = true; break; }
                    }
                }
                
                return bCifExluido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
        #endregion

        #region Quitar espacios Referencia
        /// <summary>
        /// Quitamos espacios intermedios en blanco
        /// <BatchIdentifier>ESA8090318008 / 409 / 90</BatchIdentifier>
        /// <InvoiceNumber>08 / 409 / 90</InvoiceNumber>
        /// </summary>
        /// <param name="sFileFacturain"></param>
        /// <param name="sFileFacturaEout"></param>
        /// <returns></returns>
        private static Boolean QuitarEspaciosReferenciaFactura(String sFileFacturain, String sFileFacturaEout)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                documento.Load(sFileFacturain);
                XmlNode nBatchIdentifier = documento.SelectSingleNode("//BatchIdentifier");
                if (nBatchIdentifier != null)
                {
                    nBatchIdentifier.InnerText = nBatchIdentifier.InnerText.Replace(" ", "");
                }
                XmlNode nInvoiceNumber = documento.SelectSingleNode("//InvoiceNumber");
                if (nInvoiceNumber != null)
                {
                    nInvoiceNumber.InnerText = nInvoiceNumber.InnerText.Replace(" ", "");
                }
                documento.Save(sFileFacturaEout);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }

        #endregion

        #region Nodo Reimbursable para suplidos
        private static void AddNodeReimbursable(String sFilein, String sFileOut, Boolean bSuplido)
        {
            String sfileor = sFilein;
            XmlDocument documento = new XmlDocument();
            try
            {
                
                if (bSuplido)
                {
                    //Comprobamos si tiene o no los nodos.
                    
                    documento.Load(sFilein);
                    XmlNode InvoiceTotals = documento.SelectSingleNode("//InvoiceTotals");

                    String sInvoiceTotal = "0.00";
                    foreach (XmlNode oNodo in InvoiceTotals)
                    {

                        if (oNodo.Name == "InvoiceTotal")
                        {
                            sInvoiceTotal = oNodo.InnerText;
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalGrossAmount")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalGrossAmountBeforeTaxes")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }
                        if (oNodo.Name == "TotalOutstandingAmount")
                        {
                            oNodo.InnerText = "0.00";
                            documento.Save(sFilein);
                        }

                    }
                    ////Los siguientes nodos deben informarse a cero.
                    //XmlNode InvoiceTotal = documento.SelectSingleNode("//InvoiceTotals//InvoiceTotal");

                    //if (InvoiceTotal != null)
                    //{
                    //    sInvoiceTotal = InvoiceTotal.InnerText;
                    //    InvoiceTotal.InnerText = "0.00";
                    //}

                    //XmlNode TotalGrossAmount = documento.SelectSingleNode("//InvoiceTotals//TotalGrossAmount");
                    //if (TotalGrossAmount != null) TotalGrossAmount.InnerText = "0.00";

                    //XmlNode TotalGrossAmountBeforeTaxes = documento.SelectSingleNode("//InvoiceTotals//TotalGrossAmountBeforeTaxes");
                    //if (TotalGrossAmountBeforeTaxes != null) TotalGrossAmountBeforeTaxes.InnerText = "0.00";

                    //XmlNode TotalOutstandingAmount = documento.SelectSingleNode("//InvoiceTotals//TotalOutstandingAmount");
                    //if (TotalOutstandingAmount != null) TotalOutstandingAmount.InnerText = "0.00";
                    //documento.Save(sFilein);


                    XmlNode InvoiceTotal = documento.SelectSingleNode("//InvoiceTotal");

                    if (InvoiceTotal != null)
                    {
                        sInvoiceTotal = InvoiceTotal.InnerText;
                        InvoiceTotal.InnerText = "0.00";
                    }
                    //Nodo ReimbursableExpenses
                    XmlNode ReimbursableExpenses = documento.SelectSingleNode("//ReimbursableExpenses");
                    if (ReimbursableExpenses == null) //Si no existe debemos crearlo.
                    {
                        XmlNode nodo1 = InvoiceTotal;
                        InvoiceTotals.InsertAfter(CrearNodo_ReimbursableExpenses(documento, sInvoiceTotal), nodo1);
                        documento.Save(sFilein);
                    }
                    XmlNode TotalExecutableAmount = documento.SelectSingleNode("//TotalExecutableAmount");
                    if (TotalExecutableAmount != null) sInvoiceTotal = TotalExecutableAmount.InnerText;
                    XmlNode TotalReimbursableExpenses = documento.SelectSingleNode("//TotalReimbursableExpenses");
                    if (TotalReimbursableExpenses == null)
                    {
                        //Se lo añadimos
                        InvoiceTotals.AppendChild(CrearNodo_TotalReimbursableExpenses(documento, sInvoiceTotal));
                        documento.Save(sFilein);
                    }
                    //< TotalReimbursableExpenses > 307.00 </ TotalReimbursableExpenses >
                }
                //Se graba igual aunque no tratemos el fichero como suplido
                XmlDocument documentoout = new XmlDocument();
                documentoout.Load(sFilein);
                documentoout.Save(sFileOut);
                documentoout = null;
            }
            catch (Exception e)
            {
                documento = null;
                sFilein = "";
            }


        }

        private static XmlElement CrearNodo_ReimbursableExpenses(XmlDocument xmlDoc, String sValor)
        {
            try
            {
                XmlElement ReimbursableExpensesAmount = xmlDoc.CreateElement("ReimbursableExpensesAmount");
                ReimbursableExpensesAmount.InnerText = sValor;

                

                XmlElement ReimbursableExpenses = xmlDoc.CreateElement("ReimbursableExpenses");
                ReimbursableExpenses.AppendChild(ReimbursableExpensesAmount);

                XmlElement ReimbursableExpenses1 = xmlDoc.CreateElement("ReimbursableExpenses");
                ReimbursableExpenses1.AppendChild(ReimbursableExpenses);

                return ReimbursableExpenses1;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        

        private static XmlElement CrearNodo_TotalReimbursableExpenses(XmlDocument xmlDoc, String sValor)
        {
            try
            {
                XmlElement TotalReimbursableExpenses = xmlDoc.CreateElement("TotalReimbursableExpenses");
                TotalReimbursableExpenses.InnerText = sValor;
                return TotalReimbursableExpenses;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region CIF Emisores Aceptados
        private static Boolean IsCifEmisorAceptado(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bCifAceptado = false;
                documento.Load(sFilein);
                XmlNode nSellerParty = documento.SelectSingleNode("//SellerParty");
                String sTaxIdentificationNumber = "";
                if (nSellerParty != null)
                {
                    foreach (XmlNode oNodo in nSellerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < sCIFEmisoresAceptados.Length; i++)
                    {
                        if (sTaxIdentificationNumber == sCIFEmisoresAceptados[i])
                        { bCifAceptado = true; break; }
                    }
                }

                return bCifAceptado;
            }
            catch (Exception e)
            {
                return true;
            }
            finally
            {
                documento = null;
            }

        }
        #endregion


        #region mail notificación
        private static void AddMailExtension(String sFilein, String sFileOut)
        {
            try
            {
                XmlDocument documento = new XmlDocument();
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sPais = "";
                String sTaxIdentificationNumber = "";
                String sReceiverEmailAddresses = "";
                //Buscamos el TaxIdentificationNumber
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")//
                        {
                            foreach (XmlNode oTaxIdentificationNumber in oNodo)
                            {
                                if (oTaxIdentificationNumber.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentificationNumber.InnerText.ToString();
                                    break;
                                }
                            }

                        }
                        //Miramos el mail si existe en el administrative centre
                        if (oNodo.Name == "AdministrativeCentres")
                        {
                            foreach (XmlNode oAdministrativeCentre in oNodo)
                            {
                                if (oAdministrativeCentre.Name == "AdministrativeCentre")
                                {
                                    foreach (XmlNode oContactDetails in oAdministrativeCentre)
                                    {
                                        if (oContactDetails.Name == "ContactDetails")
                                        {
                                            foreach (XmlNode oElectronicMail in oContactDetails)
                                            {
                                                if (oElectronicMail.Name == "ElectronicMail")
                                                {
                                                    sReceiverEmailAddresses = oElectronicMail.InnerText;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    //Ahora revisamos si lo tenemos (el mail)informado en nuestra tabla de empresa, si no, se quedará el de Mariona.
                    if (sReceiverEmailAddresses == "") sReceiverEmailAddresses = MailEmpresaMarmedsa(sTaxIdentificationNumber);

                    if (sReceiverEmailAddresses != "")
                    {
                        //si existe cambiamos el campo mail de la extension
                        try
                        {
                            XmlNode nodo = documento.SelectSingleNode("//InvoiceExtensions");

                            //Buscamos extension marmedsa
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("marmedsa", @"http://marmedsa.com/Factura_Extension.xsd");
                                nodo = documento.SelectSingleNode("//marmedsa:InvoiceExtensions", xmlNsM);
                            }
                            //si no lo encontramos buscamos ns4
                            if (nodo == null)
                            {
                                var xmlNsM = new XmlNamespaceManager(documento.NameTable);
                                //ext="LeaseplanInvoiceExtension
                                xmlNsM.AddNamespace("ns4", @"http://marmedsa.com/Factura_Extension");
                                nodo = documento.SelectSingleNode("//ns4:InvoiceExtensions", xmlNsM);
                            }

                            foreach (XmlNode onodoExtension in nodo)
                            {
                                foreach (XmlNode oReceiverEmailAddresses in onodoExtension)
                                {
                                    if (oReceiverEmailAddresses.Name == "EmailAdress")
                                    {
                                        oReceiverEmailAddresses.InnerText = sReceiverEmailAddresses;
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }

                    }


                }
                //Grabamos el fichero.
                documento.Save(sFileout);
                documento = null;
            }
            catch (Exception e)
            {
                sFilein = "";
            }

        }

        /// <summary>
        /// Devuelve el mail de la empresa para la notificación.
        /// </summary>
        /// <param name="sCif"></param>
        /// <returns></returns>
        private static String MailEmpresaMarmedsa(String sCif)
        {
            String sMailEmpresaMarmedsa = "";
            try
            {
                for (int i = 0; i < aListaEmpresas.Count; i++)
                {
                    CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                    oEmpresa = (CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                    if (sCif.IndexOf(oEmpresa.CIF) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.IndexOf(sCif) > 0)
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                    if (oEmpresa.CIF.Equals(sCif))
                    {
                        sMailEmpresaMarmedsa = oEmpresa.MAIL.Trim();
                        break;
                    }
                }
                return sMailEmpresaMarmedsa;
            }
            catch (Exception e)
            {
                return sMailEmpresaMarmedsa;
            }
        }
        #endregion

        #region Tratamiento Ficheros
        private static string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {
                string directorioDestino = "";
                bool OK = false;


                //myLog.WriteEntry("Los ficheros se procesan en: "+directorioDestino,EventLogEntryType.Information);	

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                //Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
                if (arrayFicheros.Length > 0)
                {
                    DateTime dt = DateTime.Now;
                    string fecha, hora;

                    //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    hora = dt.ToLongTimeString().Replace(":", "_");
                    string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                }
                // Al procesar el nuevo archivo incluido en el directorio especificado
                // debemos copiarlo al directorio temporal destinado para ello
                for (int i = 0; i < arrayFicheros.Length; i++)
                {
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            File.Delete(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            File.Delete(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }
                return directorioDestino;
            }
            catch (Exception e)
            {
                return "";
            }
        }

        private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static bool CrearFicheroOriginal(string sfile)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                String sFicheroOriginal = sfile.Replace(".xml", "_ORIGINAL.xml");
                File.Copy(sfile, sFicheroOriginal, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// Devuelve el fichero pdf en B64
        /// </summary>
        /// <param name="sfilePdf"></param>
        /// <returns></returns>
        private static String getFileB64(String sfilePdf)
        {
            try
            {
                String sBase64 = "";
                sBase64 = Convert.ToBase64String(readBinaryFile(sfilePdf));
                return sBase64;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve byte[] del fichero a leer
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static byte[] readBinaryFile(string filePath)
        {
            byte[] ficheroByteArray;
            FileStream fichero = new FileStream(@filePath, FileMode.Open, FileAccess.Read);
            BinaryReader ficheroBinario = new BinaryReader(fichero);
            ficheroByteArray = ficheroBinario.ReadBytes(System.Convert.ToInt32(fichero.Length));
            ficheroBinario.Close();
            fichero.Close();
            return ficheroByteArray;
        }

        /// <summary>
        /// Movemos a procesadas para no crearlas en los siguientes procesos.
        /// </summary>
        private static void moverAProcesadas(String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioEnviada = sDirectorioMonitorizacion + "Procesadas";
                Directory.CreateDirectory(sDirectorioEnviada);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*.*"))
                {
                    String sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Movemos a error para no procesarlas ni enviarlas
        /// </summary>
        private static void moverAError(String sFile,String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "Error\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Movemos a Excluidas las facturas que son de cif exluidos.
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorExcluida(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "CifExcluidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// Movemos a Emisor no aceptado las facturas que son de emisores no aceptados.
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorEmisorNoAceptado(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "CifEmisorNoAceptado\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Si no encontramos el suplido las movemos a error para no enviar a facturación
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sDirectorioMonitorizacion"></param>
        private static void moverAErrorSuplidos(String sFile, String sDirectorioMonitorizacion)
        {
            try
            {
                //Primero borramos las facturas intermedias
                String sDirectorioError = sDirectorioMonitorizacion + "ErrorCifSuplidos\\";
                Directory.CreateDirectory(sDirectorioError);
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                String sfileout = sFile.Replace(sDirectorioMonitorizacion, sDirectorioError);
                MoverFichero(sFile, sfileout);

            }
            catch (Exception)
            {
            }
        }
        private static void BorrarFacturasIntermedias(String sDirectorioMonitorizacion)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(sDirectorioMonitorizacion);
                foreach (var fi in di.GetFiles("*_sinfirma.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conceros.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conadmcentre.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_sincero.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conSequenceNumber.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conxml.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_conespacios.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_Reimbursable.xml"))
                {
                    File.Delete(fi.FullName);
                }
                foreach (var fi in di.GetFiles("*_Mailextension.xml"))
                {
                    File.Delete(fi.FullName);
                }
            }
            catch (Exception)
            {

            }
        }
        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }
        #endregion

        #region Error Suplidos (el cif receptor no es de marmedsa y no hemos encontrado el suplido en la tabla de suplidos)
        private static Boolean bErrorSuplidos(String sfile)
        {
            XmlDocument documento = new XmlDocument();
            try
            {
                Boolean bokSuplido = false;
                documento.Load(sFilein);
                XmlNode nBuyerParty = documento.SelectSingleNode("//BuyerParty");
                String sTaxIdentificationNumber = "";
                if (nBuyerParty != null)
                {
                    foreach (XmlNode oNodo in nBuyerParty)
                    {
                        if (oNodo.Name == "TaxIdentification")
                        {
                            foreach (XmlNode oTaxIdentification in oNodo)
                            {
                                if (oTaxIdentification.Name == "TaxIdentificationNumber")
                                {
                                    sTaxIdentificationNumber = oTaxIdentification.InnerText;
                                }
                            }
                        }
                    }
                }
                if (sTaxIdentificationNumber != "")
                {
                    for (int i = 0; i < aListaEmpresas.Count; i++)
                    {
                        CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa oEmpresa = null;
                        oEmpresa = (CrearFacturaMarmedsaSabadell.EmpresaMarmedsa.EmpresaMarmedsa)aListaEmpresas[i];
                        if (sTaxIdentificationNumber.IndexOf(oEmpresa.CIF) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.IndexOf(sTaxIdentificationNumber) > 0)
                        {
                            bokSuplido = true;
                            break;
                        }
                        if (oEmpresa.CIF.Equals(sTaxIdentificationNumber))
                        {
                            bokSuplido = true;
                            break;
                        }
                    }
                }

                return bokSuplido;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                documento = null;
            }

        }
		#endregion

		#region Generar mail de resultado

		/// <summary>
		/// Envía el mail del informe del proceso.
		/// </summary>
		private static void GenerarMailProceso()
		{
			try
			{
				//Creamos el cuerpo del mensaje
				String sCuerpo = "";
				if (ResultadoProceso != null)
				{
					sCuerpo = "Facturas Procesadas: " + ResultadoProceso.Count.ToString();
					sCuerpo = sCuerpo + "<p></p>" + "<p></p>";
					for (int i = 0; i < ResultadoProceso.Count; i++)
					{
						GenerarCuerpoMensaje(ResultadoProceso[i].ToString());
					}
					for (int i = 0; i < errorInforme.Count; i++)
					{
						sCuerpo = sCuerpo + errorInforme[i].ToString() + " - " + intInforme[i].ToString();
						sCuerpo = sCuerpo + "<p></p>";
					}
				}
				String sAsunto = "Informe Facturas Sabadell Marmedsa " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo);
			}
			catch (Exception e)
			{
			}
		}

		/// <summary>
		/// Genera el mensaje con la cantidad de errores.
		/// </summary>
		/// <param name="sMensaje"></param>
		private static void GenerarCuerpoMensaje(String sMensaje)
		{
			try
			{
				if (errorInforme == null)
				{
					errorInforme = new ArrayList();
					intInforme = new ArrayList();
					errorInforme.Add(sMensaje);
					intInforme.Add(1);
				}
				else
				{
					Boolean bNew = true;
					for (int i = 0; i < errorInforme.Count; i++)
					{
						if (errorInforme[i].Equals(sMensaje))
						{
							intInforme[i] = (int)intInforme[i] + 1;
							bNew = false;
							break;
						}
					}
					if (bNew)
					{
						errorInforme.Add(sMensaje);
						intInforme.Add(1);
					}
				}

			}
			catch (Exception e)
			{
			}

		}
		#endregion
	}
}
