﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnzipFiles
{
	class MergePdf
	{
		public static byte[] bMergeFiles(string[] sourceFiles, int idFactura, bool comprimir = false)
		{
			try
			{
				int f = 0;
				MemoryStream ms = new MemoryStream();
				// we create a reader for a certain document
				PdfReader reader = new PdfReader(sourceFiles[f]);
				// we retrieve the total number of pages
				int n = reader.NumberOfPages;
				//Console.WriteLine("There are " + n + " pages in the original file.");
				// step 1: creation of a document-object
				iTextSharp.text.Document document = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));
				// step 2: we create a writer that listens to the document
				PdfWriter writer = PdfWriter.GetInstance(document, ms);
				// step 3: we open the document
				document.Open();
				PdfContentByte cb = writer.DirectContent;
				PdfImportedPage page;

				if (comprimir)
				{
					writer.CompressionLevel = PdfStream.BEST_COMPRESSION;
					reader.RemoveFields();
					reader.RemoveUnusedObjects();
					reader.RemoveUnusedObjects();

					writer.SetFullCompression();
				}


				int rotation;
				// step 4: we add content
				while (f < sourceFiles.Length)
				{
					int i = 0;
					while (i < n)
					{
						i++;
						document.SetPageSize(reader.GetPageSizeWithRotation(i));
						document.NewPage();
						page = writer.GetImportedPage(reader, i);
						rotation = reader.GetPageRotation(i);
						if (rotation == 90 || rotation == 270)
						{
							cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
						}
						else
						{
							cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
						}
						//Console.WriteLine("Processed page " + i);
					}
					f++;
					if (f < sourceFiles.Length)
					{
						reader = new PdfReader(sourceFiles[f]);
						// we retrieve the total number of pages
						n = reader.NumberOfPages;
						//Console.WriteLine("There are " + n + " pages in the original file.");
					}
				}
				// step 5: we close the document
				document.Close();
				reader.Close();
				writer.Close();

				return ms.ToArray();
			}
			catch (Exception e)
			{
				//new Error(idFactura, 500, e.Message, e.ToString(), true, "", -1);
				string strOb = e.Message;
				return null;
			}
		}



		public static Boolean MergeFiles(string destinationFile, string[] sourceFiles)
		{
			Boolean bMergeFiles = true;
			PdfReader reader = null;
			PdfWriter writer = null;
			iTextSharp.text.Document document = null;
			try
			{
				int f = 0;
				// we create a reader for a certain document
				reader = new PdfReader(sourceFiles[f]);
				// we retrieve the total number of pages
				int n = reader.NumberOfPages;
				//Console.WriteLine("There are " + n + " pages in the original file.");
				// step 1: creation of a document-object
				document = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));
				// step 2: we create a writer that listens to the document
				writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
				// step 3: we open the document
				document.Open();
				PdfContentByte cb = writer.DirectContent;
				PdfImportedPage page;
				int rotation;
				// step 4: we add content
				while (f < sourceFiles.Length)
				{
					int i = 0;
					while (i < n)
					{
						i++;
						document.SetPageSize(reader.GetPageSizeWithRotation(i));
						document.NewPage();
						page = writer.GetImportedPage(reader, i);
						rotation = reader.GetPageRotation(i);
						if (rotation == 90 || rotation == 270)
						{
							cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
						}
						else
						{
							cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
						}
						//Console.WriteLine("Processed page " + i);
					}
					f++;
					if (f < sourceFiles.Length)
					{
						reader = new PdfReader(sourceFiles[f]);
						// we retrieve the total number of pages
						n = reader.NumberOfPages;
						//Console.WriteLine("There are " + n + " pages in the original file.");
					}
				}

			}
			catch (Exception e)
			{
				//new Error(idFactura, 500, e.Message, e.ToString(), true, "", -1);
				string strOb = e.Message;
				bMergeFiles = false;
			}
			finally
			{
				// step 5: we close the document
				if (document !=null)  document.Close();
				if (reader != null) reader.Close();
				if (writer != null) writer.Close();
			}
			return bMergeFiles;
		}

		public int CountPageNo(string strFileName)
		{
			// we create a reader for a certain document
			PdfReader reader = new PdfReader(strFileName);
			// we retrieve the total number of pages
			return reader.NumberOfPages;
		}

		public static Boolean ReadPDF(string strFileName)
		{
			try
			{
				// we create a reader for a certain document
				PdfReader reader = new PdfReader(strFileName);
				// we retrieve the total number of pages
				int ipages= reader.NumberOfPages;
				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine("Error leer pdf.. " + strFileName);
				return false;
			}

		}
	}
}
