﻿using AgiKeyMailLib;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;

namespace UnzipFiles
{
    class Program
    {
        #region Variables
        static String  DirectorioZip = "";
        static String DirectorioDesZip = "";
        static String DirectorioFactura = "";
        static String DirectorioFacturaMarmedsa = "";
		static String DirectorioAdjuntos = "";
		static String DirectorioFacturaMarmedsaPDF = "";
		static String DirectorioFacturaPDF = "";
		static String TipoUnZip = "";
		#endregion
		static void Main(string[] args)
        {
			TipoUnZip = ConfigurationManager.AppSettings["TipoProceso"];
			DirectorioZip = ConfigurationManager.AppSettings["DirectorioZip"];
			DirectorioDesZip = ConfigurationManager.AppSettings["DirectorioDesZip"];
			DirectorioFactura = ConfigurationManager.AppSettings["DirectorioFactura"];
			DirectorioFacturaMarmedsa = ConfigurationManager.AppSettings["DirectorioFacturaMarmedsa"];
			DirectorioAdjuntos = ConfigurationManager.AppSettings["DirectorioAdjuntos"];
			DirectorioFacturaMarmedsaPDF = ConfigurationManager.AppSettings["DirectorioFacturaMarmedsaPDF"];
			DirectorioFacturaPDF = ConfigurationManager.AppSettings["DirectorioFacturaPDF"];

			if (TipoUnZip.Equals("BS")) ProcesoUnZipSabadell();
			if (TipoUnZip.Equals("MAIL")) ProcesoUnZipMail();

		}

		#region Unzip ficheros sabadell
		private static void ProcesoUnZipSabadell()
		{
			DirectorioZip = procesarArchivos(DirectorioZip);
			if (DirectorioZip != "")
			{
				DirectorioDesZip = CrearDirectorioProceso(DirectorioDesZip);
				DirectorioFactura = CrearDirectorioProceso(DirectorioFactura);
				DirectorioFacturaPDF = CrearDirectorioProceso(DirectorioFacturaPDF);
				string[] fileNamesZip = Directory.GetFiles(DirectorioZip, "*.zip");
				UnzipFiles(DirectorioZip,DirectorioDesZip, fileNamesZip);
				//Procesar facturas
				procesarFacturas();
			}
			else Console.WriteLine("No hay ficheros a deszipear.");
		}

		private static void UnzipFiles(string zstPath,string dstPath, string[] sources)
		{
			Directory.CreateDirectory(dstPath);
			Console.WriteLine("Deszipeando Ficheros.");
			String sDirectorioProcesado = zstPath + "\\procesado\\";
			Directory.CreateDirectory(sDirectorioProcesado);
			String sMensajeUpzip = "";
			foreach (var zippath in sources)
			{
				try
				{
					Console.WriteLine("Deszipeando Fichero " + zippath);
					ZipFile.ExtractToDirectory(zippath, dstPath);
					//Despues de extrarlo lo pasamos los pdf's y xml's a una carpeta de facturas
					DirectoryInfo di = new DirectoryInfo(dstPath);
					//Si hay xml o xsig al zip vamos a envairlo todo a la carpeta de circuito xml
					//Si no hay xml o xsig al zip vamos a enviarlo todo a la carpeta del circuito pdf
					FileInfo[] ficherosxml = di.GetFiles("*.xml", SearchOption.AllDirectories);
					FileInfo[] ficherosxsig = di.GetFiles("*.xsig", SearchOption.AllDirectories);
					Boolean bcircuitoxml = false;
					if (ficherosxml.Length > 0 || ficherosxsig.Length > 0) bcircuitoxml = true;
					if (bcircuitoxml)
					{
						foreach (var fi in di.GetFiles("*.pdf", SearchOption.AllDirectories))
						{
							String sfileout = DirectorioFactura + "\\" + fi.Name;
							MoverFichero(fi.FullName, sfileout);
						}

						foreach (var fi in di.GetFiles("*.xml", SearchOption.AllDirectories))
						{
							String sfileout = DirectorioFactura + "\\" + fi.Name;
							MoverFichero(fi.FullName, sfileout);
						}

						foreach (var fi in di.GetFiles("*.xsig", SearchOption.AllDirectories))
						{
							String sfileout = DirectorioFactura + "\\" + fi.Name;
							MoverFichero(fi.FullName, sfileout);
						}
					}
					else
					{
						foreach (var fi in di.GetFiles("*.pdf", SearchOption.AllDirectories))
						{
							String sfileout = DirectorioFacturaPDF + "\\" + fi.Name.Replace(".PDF",".pdf");
							MoverFichero(fi.FullName, sfileout);
							if (!MergePdf.ReadPDF(sfileout))
							{
								//Mandamos un mail de erro al abrir el fichero.
								String smensaje = "Fichero ZIP: " + zippath + " - Fichero pdf: " + sfileout;
								GenerarMailErrorPDF(smensaje);
							}
						}
						//Una vez movido todos los pdf's lo debemos juntar.

						String[] sFilesMerge = Directory.GetFiles(DirectorioFacturaPDF);
						if (sFilesMerge.Length > 0)
						{
							String sfilemerge = sFilesMerge[0].Replace(".pdf", "_merge.pdf");
							Boolean bMergeFiles =  MergePdf.MergeFiles(sfilemerge, sFilesMerge);
							if (bMergeFiles)
							{
								//Revisamos si el fichero generado _merge se puede leer:
								if (MergePdf.ReadPDF(sfilemerge))
								{
									String sfilemergeok = sfilemerge.Replace(DirectorioFacturaPDF, DirectorioFacturaMarmedsaPDF);
									//MoverFichero(sfilemerge, sfilemergeok);
									if (CopiarArchivo(sfilemerge, DirectorioFacturaMarmedsaPDF))
										sMensajeUpzip = sMensajeUpzip + "<p></p> " + "Extraido fichero " + sfilemerge + " - a carpeta " + DirectorioFacturaMarmedsaPDF + "...";
									else sMensajeUpzip = sMensajeUpzip + "<p></p> " + "ERROR Extraido fichero " + sfilemerge + " - a carpeta " + DirectorioFacturaMarmedsaPDF + "...";

								}
								else
								{
									//Mandamos un mail de erro al abrir el fichero.
									String smensaje = "Fichero ZIP: " + zippath + " - Fichero pdf:  < p ></ p > " + "ERROR Merge fichero " + sfilemerge + "...";
									GenerarMailErrorPDF(smensaje);
								}
							}
							else
							{
								//Mandamos un mail de erro al abrir el fichero.
								String smensaje = "Fichero ZIP: " + zippath + " - Fichero pdf:  < p ></ p > " + "ERROR Merge fichero " + sfilemerge + "...";
								GenerarMailErrorPDF(smensaje);
							}


							sFilesMerge = Directory.GetFiles(DirectorioFacturaPDF);
							for (int i = 0; i < sFilesMerge.Length; i++)
							{
								String sprocesado = DirectorioFacturaPDF + "\\procesado\\";
								Directory.CreateDirectory(sprocesado);
								String sprocesadoout = sFilesMerge[i].Replace(DirectorioFacturaPDF, sprocesado);
								MoverFichero(sFilesMerge[i], sprocesadoout);

							}
						}
					}
					//borramos los ficheros que queden por si vienen repetidos.
					foreach (var fi in di.GetFiles("*.*", SearchOption.AllDirectories))
					{
						EliminarFichero(fi.FullName);
					}

					//Movemos el zip a procesado
					String sficheroprocesado = zippath.Replace(zstPath, sDirectorioProcesado);
					MoverFichero(zippath, sficheroprocesado);
				}
				catch (Exception e)
				{
					Console.WriteLine("Error deszipeando fichero " + zippath + " - " + e.Message);
					GenerarMailProceso("Error deszipeando fichero " + zippath + " - " + e.Message);
				}
				
			}
			if (sMensajeUpzip != "") GenerarMailProceso("Descompresión de ficheros", sMensajeUpzip);
			Console.WriteLine("Fin proceso Deszipeando Ficheros.");



		}
		#endregion

		#region Unzip fichero mails
		private static void ProcesoUnZipMail()
		{
			DirectorioZip = procesarDirectorios(DirectorioZip);
			if (DirectorioZip != "")
			{
				DirectorioDesZip = CrearDirectorioProceso(DirectorioDesZip);
				DirectorioFactura = CrearDirectorioProceso(DirectorioFactura);
				DirectorioFacturaPDF = CrearDirectorioProceso(DirectorioFacturaPDF);
				string[] fileNamesZip = Directory.GetFiles(DirectorioZip, "*.zip");
				UnzipFiles(DirectorioZip,DirectorioDesZip, fileNamesZip);
				//Procesar facturas
				procesarFacturas();
			}
			else Console.WriteLine("No hay ficheros a deszipear.");

		}
		#endregion



		#region Tratamiento Ficheros

		/// <summary>
		/// Copia las facturas a la carpeta de Generar xml de marmedsa
		/// Mueve las facturas a una subcarpeta "enviadas"
		/// </summary>
		private static void procesarFacturas()
        {
            try
            {
                String sDirectorioEnviada = DirectorioFactura + "enviadas";
                Directory.CreateDirectory(sDirectorioEnviada);
                DirectoryInfo di = new DirectoryInfo(DirectorioFactura);
                foreach (var fi in di.GetFiles("*.pdf"))
                {
                    String sfileout = DirectorioFacturaMarmedsa + "\\";
                    CopiarArchivo(fi.FullName, sfileout);
                    sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }

                foreach (var fi in di.GetFiles("*.xml"))
                {
					//Tratamos los anexos aqui.
					//Buscamos por nombre fichero que debe tener
					String sreferencia = fi.Name.Replace(".xml", "");
					DirectoryInfo diAdjuntos = new DirectoryInfo(DirectorioAdjuntos);
					String sDirectorioZip = DirectorioAdjuntos + sreferencia + @"\";
					foreach (var fiadjunto in diAdjuntos.GetFiles(sreferencia + "*.*"))
					{
						Directory.CreateDirectory(sDirectorioZip);
						CopiarArchivo(fiadjunto.FullName, sDirectorioZip);
					}
					String sfileout = DirectorioFacturaMarmedsa + "\\";
					if (Directory.Exists(sDirectorioZip))
					{
						String sFileZip = fi.FullName.Replace(".xml", ".zip");
						ZipFile.CreateFromDirectory(sDirectorioZip, sFileZip);
						CopiarArchivo(sFileZip, sfileout);
					}
                    CopiarArchivo(fi.FullName, sfileout);
                    sfileout = sDirectorioEnviada + "\\" + fi.Name;
                    MoverFichero(fi.FullName, sfileout);
                }

				//añadimos .xsig
				foreach (var fi in di.GetFiles("*.xsig"))
				{
					//Tratamos los anexos aqui.
					//Buscamos por nombre fichero que debe tener
					String sreferencia = fi.Name.Replace(".xsig", "");
					DirectoryInfo diAdjuntos = new DirectoryInfo(DirectorioAdjuntos);
					String sDirectorioZip = DirectorioAdjuntos + sreferencia + @"\";
					foreach (var fiadjunto in diAdjuntos.GetFiles(sreferencia + "*.*"))
					{
						Directory.CreateDirectory(sDirectorioZip);
						CopiarArchivo(fiadjunto.FullName, sDirectorioZip);
					}
					String sfileout = DirectorioFacturaMarmedsa + "\\";
					if (Directory.Exists(sDirectorioZip))
					{
						String sFileZip = fi.FullName.Replace(".xsig", ".zip");
						ZipFile.CreateFromDirectory(sDirectorioZip, sFileZip);
						CopiarArchivo(sFileZip, sfileout);
					}
					CopiarArchivo(fi.FullName, sfileout);
					sfileout = sDirectorioEnviada + "\\" + fi.Name;
					MoverFichero(fi.FullName, sfileout);
				}

			}
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Mueve los zip al directorio de procesos de hoy
        /// </summary>
        /// <param name="directorioMonitorizacion"></param>
        /// <returns></returns>
        private static string procesarArchivos(string directorioMonitorizacion)
        {
            try
            {
                string directorioDestino = "";
                bool OK = false;


                //myLog.WriteEntry("Los ficheros se procesan en: "+directorioDestino,EventLogEntryType.Information);	

                DirectoryInfo directInfo = new DirectoryInfo(directorioMonitorizacion);
                FileInfo[] arrayFicheros = directInfo.GetFiles();

                //Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
                if (arrayFicheros.Length > 0)
                {
                    DateTime dt = DateTime.Now;
                    string fecha, hora;

                    //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                    hora = dt.ToLongTimeString().Replace(":", "_");
                    string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
                    if (Directory.Exists(pathLog))
                    {
                        directorioDestino = pathLog;
                    }
                    else
                    {
                        directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                    }
                }
                // Al procesar el nuevo archivo incluido en el directorio especificado
                // debemos copiarlo al directorio temporal destinado para ello
                for (int i = 0; i < arrayFicheros.Length; i++)
                {
                    if (File.Exists(directorioDestino + arrayFicheros[i]))
                    {
                        if (File.Equals(directorioMonitorizacion + arrayFicheros[i], directorioDestino + arrayFicheros[i]))
                        {

                        }
                        else
                        {
                            EliminarFichero(directorioDestino + arrayFicheros[i]);
                            OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);



                            if (OK)
                            {
                                EliminarFichero(directorioMonitorizacion + arrayFicheros[i]);
                            }
                        }
                    }
                    else
                    {
                        OK = CopiarArchivo(directorioMonitorizacion + arrayFicheros[i], directorioDestino);
                        if (OK)
                        {
                            EliminarFichero(directorioMonitorizacion + arrayFicheros[i]);
                        }
                    }
                }

				//Ahora hacemos lo mismo para los subdirectorios donde descarga el mail los zip
				DirectoryInfo[] subdirectInfo = directInfo.GetDirectories(directorioMonitorizacion);
				for (int j = 0; j < subdirectInfo.Length; j++)
				{
					FileInfo[] subarrayFicheros = subdirectInfo[j].GetFiles();

					//Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
					if (subarrayFicheros.Length > 0)
					{
						DateTime dt = DateTime.Now;
						string fecha, hora;

						//fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
						fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
						hora = dt.ToLongTimeString().Replace(":", "_");
						string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
						if (Directory.Exists(pathLog))
						{
							directorioDestino = pathLog;
						}
						else
						{
							directorioDestino = Directory.CreateDirectory(pathLog).FullName;
						}
					}
					// Al procesar el nuevo archivo incluido en el directorio especificado
					// debemos copiarlo al directorio temporal destinado para ello
					for (int i = 0; i < subarrayFicheros.Length; i++)
					{
						if (File.Exists(directorioDestino + subarrayFicheros[i]))
						{
							if (File.Equals(directorioMonitorizacion + subarrayFicheros[i], directorioDestino + subarrayFicheros[i]))
							{

							}
							else
							{
								EliminarFichero(directorioDestino + subarrayFicheros[i]);
								OK = CopiarArchivo(directorioMonitorizacion + subarrayFicheros[i], directorioDestino);



								if (OK)
								{
									EliminarFichero(directorioMonitorizacion + subarrayFicheros[i]);
								}
							}
						}
						else
						{
							OK = CopiarArchivo(directorioMonitorizacion + subarrayFicheros[i], directorioDestino);
							if (OK)
							{
								EliminarFichero(directorioMonitorizacion + subarrayFicheros[i]);
							}
						}
					}
				}
				return directorioDestino;
            }
            catch (Exception e)
            {
                return "";
            }
        }



        /// <summary>
        /// Crear el directorio con fecha de procesado.
        /// </summary>
        /// <param name="sDirectorio"></param>
        /// <returns></returns>
        private static String CrearDirectorioProceso(String sDirectorio)
        {
            try
            {
                String directorioDestino = "";
                DateTime dt = DateTime.Now;
                string fecha, hora;

                //fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
                hora = dt.ToLongTimeString().Replace(":", "_");
                string pathLog = sDirectorio + "\\Procesados_" + fecha + @"\";
                if (Directory.Exists(pathLog))
                {
                    directorioDestino = pathLog;
                }
                else
                {
                    directorioDestino = Directory.CreateDirectory(pathLog).FullName;
                }
                return directorioDestino;
            }
            catch (Exception)
            {
                return "";
            }

        }


        private static bool CopiarArchivo(string url, string directorioDestino)
        {
            try
            {
                // Copiamos el fichero correspondiente a la factura en un directorio temporal
                string ficheroDestino = directorioDestino + Path.GetFileName(url);
                File.Copy(url, ficheroDestino, true);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void MoverFichero(string sFile, string sFileOut)
        {
            try
            {
				if (File.Exists(sFileOut)) File.Delete(sFileOut);
                File.Move(sFile, sFileOut);
            }
            catch (Exception e)
            {
            }
        }

        private static void EliminarFichero(string sFile)
        {
            try
            {
                File.Delete(sFile);
            }
            catch (Exception e)
            {
            }
        }

		/// <summary>
		/// Mueve los zip al directorio de procesos de hoy
		/// </summary>
		/// <param name="directorioMonitorizacion"></param>
		/// <returns></returns>
		private static string procesarDirectorios(string directorioMonitorizacion)
		{
			try
			{
				string directorioDestino = "";
				bool OK = false;
				String[] subdirectInfo = Directory.GetDirectories(directorioMonitorizacion);
				for (int j = 0; j < subdirectInfo.Length; j++)
				{
					//Si el directorio tiene Procesados_ no se procesara por si es algo antiguo.
					if (!subdirectInfo[j].Contains("Procesados_"))
					{
						DirectoryInfo directInfo = new DirectoryInfo(subdirectInfo[j]);
						FileInfo[] subarrayFicheros = directInfo.GetFiles();

						//Si hay ficheros a procesar creamos el directorio de procesados, si no, no se crea.
						if (subarrayFicheros.Length > 0)
						{
							DateTime dt = DateTime.Now;
							string fecha, hora;

							//fecha = dt.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
							fecha = dt.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo).Replace("/", "-");
							hora = dt.ToLongTimeString().Replace(":", "_");
							string pathLog = directorioMonitorizacion + "\\Procesados_" + fecha + @"\";
							if (Directory.Exists(pathLog))
							{
								directorioDestino = pathLog;
							}
							else
							{
								directorioDestino = Directory.CreateDirectory(pathLog).FullName;
							}
						}
						// Al procesar el nuevo archivo incluido en el directorio especificado
						// debemos copiarlo al directorio temporal destinado para ello
						String directoriozip = subdirectInfo[j] + "\\";
						for (int i = 0; i < subarrayFicheros.Length; i++)
						{
							if (File.Exists(directorioDestino + subarrayFicheros[i]))
							{
								if (File.Equals(directoriozip + subarrayFicheros[i], directorioDestino + subarrayFicheros[i]))
								{

								}
								else
								{
									EliminarFichero(directorioDestino + subarrayFicheros[i]);
									OK = CopiarArchivo(directoriozip + subarrayFicheros[i], directorioDestino);



									if (OK)
									{
										EliminarFichero(directoriozip + subarrayFicheros[i]);
									}
								}
							}
							else
							{
								OK = CopiarArchivo(directoriozip + subarrayFicheros[i], directorioDestino);
								if (OK)
								{
									EliminarFichero(directoriozip + subarrayFicheros[i]);
								}
							}
						}
					}
					
				}
				return directorioDestino;
			}
			catch (Exception e)
			{
				return "";
			}
		}
		#endregion

		#region Generar mail de resultado

		/// <summary>
		/// Envía el mail del informe del proceso.
		/// </summary>
		private static void GenerarMailProceso(String sFicheroAdjunto)
		{
			try
			{
				//Creamos el cuerpo del mensaje
				String sCuerpo = sFicheroAdjunto;
				String sAsunto = "Error descomprimir fichero " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo, "");
			}
			catch (Exception e)
			{
			}
		}

		private static void GenerarMailProceso(String sAsunto, String sCuerpo)
		{
			try
			{
				sAsunto = sAsunto + " " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo, "");
			}
			catch (Exception e)
			{
			}
		}

		private static void GenerarMailErrorPDF(String sFicheroAdjunto)
		{
			try
			{
				//Creamos el cuerpo del mensaje
				String sCuerpo = sFicheroAdjunto;
				String sAsunto = "Error leer fichero pdf " + DateTime.Now.ToString();
				EnvioMail oMail = new EnvioMail();
				oMail.EnviarMail(sAsunto, sCuerpo, "");
			}
			catch (Exception e)
			{
			}
		}
		#endregion
	}
}
