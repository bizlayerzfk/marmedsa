﻿
#region Imports

using System;
using System.Configuration;
using System.IO;
using ZLayer.Enviroment.BancSabadell;
using ZLayer.Enviroment.BancSabadell.FilesWebService;

#endregion Imports

namespace ZLayer.Applications.BancSabadell.Bot
{

    public class Program
    {
		#region Variables
		public static string sDirectorioFacturas = "";
		public static string sDirectorioAdjuntos = "";
		public static string sDescargarconTokenFacturas = "";
		public static string sDescargarconTokenAdjuntos = "";
		#endregion

		public static void Main(String[] args)
        {
			sDirectorioFacturas = ConfigurationManager.AppSettings["sDirectorioFacturas"];
			sDirectorioAdjuntos = ConfigurationManager.AppSettings["sDirectorioAdjuntos"];
			sDescargarconTokenFacturas = ConfigurationManager.AppSettings["sDescargarconTokenFacturas"];
			sDescargarconTokenAdjuntos = ConfigurationManager.AppSettings["sDescargarconTokenAdjuntos"];

			DescargarFacturas();
			DescargarAdjuntos();


		}

		public static void DescargarFacturas()
		{
			// Crea el contenidor dels arxius recollits
			IDataFile[] cltFiles = null;
			try
			{
				// Recull els arxius
				cltFiles = Files.GetFiles("A08148710", "A08148710WS", "A08148710", DirectorioInbox.FACTURAS);
				//// Comprova si s'han recollit arxius
				if (cltFiles != null)
				{
					// Processa els arxius recollits
					for (int i = 0; i < cltFiles.Length; i++)
					{
						String sToken = cltFiles[i].token;
						String snombrefichero = cltFiles[i].name;
						int posToken = snombrefichero.IndexOf("_TK");
						Boolean sdescargado = false;
						if (sToken.Equals("") || posToken < 1)
						{
							snombrefichero = sDirectorioFacturas + snombrefichero;
							byte[] fileArray = cltFiles[i].base64Content;
							byteArrayToFile(fileArray, snombrefichero);
							sdescargado = true;
						}
						if (sDescargarconTokenFacturas.Equals("SI") && !sdescargado)
						{
							snombrefichero = sDirectorioFacturas + snombrefichero;
							byte[] fileArray = cltFiles[i].base64Content;
							byteArrayToFile(fileArray, snombrefichero);
						}
					}
					Files.ConfirmFiles("A08148710", "A08148710WS", "A08148710", cltFiles, DirectorioInbox.FACTURAS);
				}
			}
			catch (Exception ectProcess)
			{
				// Registra l'error
			}
		}

		public static void DescargarAdjuntos()
		{
			// Crea el contenidor dels arxius recollits
			IDataFile[] cltFiles = null;
			try
			{
				// Recull els arxius
				cltFiles = Files.GetFiles("A08148710", "A08148710WS", "A08148710", DirectorioInbox.ADJUNTOS);
				//// Comprova si s'han recollit arxius
				if (cltFiles != null)
				{
					// Processa els arxius recollits
					for (int i = 0; i < cltFiles.Length; i++)
					{
						String sToken = cltFiles[i].token;
						String snombrefichero = cltFiles[i].name;
						int posToken = snombrefichero.IndexOf("_TK");
						Boolean sdescargado = false;
						if (sToken.Equals("") || posToken < 1)
						{
							snombrefichero = sDirectorioAdjuntos + snombrefichero;
							byte[] fileArray = cltFiles[i].base64Content;
							byteArrayToFile(fileArray, snombrefichero);
							sdescargado = true;
						}
						if (sDescargarconTokenAdjuntos.Equals("SI") && !sdescargado)
						{
							snombrefichero = sDirectorioAdjuntos + snombrefichero;
							byte[] fileArray = cltFiles[i].base64Content;
							byteArrayToFile(fileArray, snombrefichero);
						}
					}
					Files.ConfirmFiles("A08148710", "A08148710WS", "A08148710", cltFiles, DirectorioInbox.ADJUNTOS);

				}
			}
			catch (Exception ectProcess)
			{
				// Registra l'error
			}
		}


		#region funciones ficheros
		private static void CrearFile(String sB64, String sFileout)
		{
			try
			{
				String spdfbase64 = sB64;
				byte[] bBase64;

				bBase64 = Convert.FromBase64String(spdfbase64);
				byteArrayToFile(bBase64, sFileout);
			}
			catch (Exception e)
			{

			}
		}

		private static void byteArrayToFile(byte[] data, string filePath)
		{
			FileStream fs = null;
			try
			{
				fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
				fs.Write(data, 0, data.GetUpperBound(0) + 1);
			}
			catch (Exception e)
			{

			}
			finally
			{
				if (fs != null) fs.Close();
			}
		}
		#endregion

	}

}
