﻿
#region Imports

using System;

using ZLayer.Enviroment.BancSabadell.FilesWebService;

#endregion Imports

namespace ZLayer.Enviroment.BancSabadell
{

    public static class Files
    {

        /// <summary>
        /// Recull els arxius del Banc Sabadell fent servir el seu servei web
        /// </summary>
        /// <returns>Un System.Array de ZLayer.Enviroment.BancSabadell.FilesWebService.IDataFile amb el resultat de l'enviament</returns>
        public static IDataFile[] GetFiles(String prmCompanyName, String prmUserName, String prmUserPass
            , DirectorioInbox prmDirectory = DirectorioInbox.FACTURAS, ElementType prmFileType = ElementType.FACTURAE)
        {
            // Crea el contenidor del servei del BSF
            BSFConnectorFilesManagementServicePortTypeClient svcBSFClient = null;
            // Crea el contenidor del resultat
            IDataFile[] dtfResult = null;
            try
            {
                // Comprova que s'informen el nom de l'empresa i les dades de l'usuari
                if (!String.IsNullOrEmpty(prmCompanyName) && !String.IsNullOrEmpty(prmUserName) &&
                    !String.IsNullOrEmpty(prmUserPass))
                {
                    // Informa les dades de l'usuari
                    Connection.UserName = prmUserName;
                    Connection.UserPass = prmUserPass;
                    // Recull la connexió al servei d'arxius
                    svcBSFClient = Connection.Files;
                    // Recull els arxius
                    dtfResult = svcBSFClient.getElements(prmCompanyName, prmDirectory, prmFileType, 0);
                    // Tanca la connexió al servei d'arxius
                    svcBSFClient.Close();
                }
            }
            catch (Exception ectGetFiles)
            {
                // Informa del resultat
                dtfResult = null;
            }
            // Retorna el resultat
            return dtfResult;
        }



		public static Boolean ConfirmFiles(String prmCompanyName, String prmUserName, String prmUserPass
			, IDataFile[] prmDateFiles, DirectorioInbox prmDirectory = DirectorioInbox.FACTURAS, ElementType prmFileType = ElementType.FACTURAE)
		{
			try
			{
				String sToken = "";
				// Crea el contenidor del servei del BSF
				BSFConnectorFilesManagementServicePortTypeClient svcBSFClient = null;

				// Comprova que s'informen el nom de l'empresa i les dades de l'usuari
				if (!String.IsNullOrEmpty(prmCompanyName) && !String.IsNullOrEmpty(prmUserName) &&
					!String.IsNullOrEmpty(prmUserPass))
				{
					// Informa les dades de l'usuari
					Connection.UserName = prmUserName;
					Connection.UserPass = prmUserPass;
					// Recull la connexió al servei d'arxius
					svcBSFClient = Connection.Files;
					for (int i = 0; i < prmDateFiles.Length; i++)
					{
						sToken = prmDateFiles[i].token;
						Boolean sconfirm = svcBSFClient.confirmReception(prmCompanyName, prmDirectory, prmFileType, sToken);
						sToken = sToken.Replace("_", "");
						sconfirm = svcBSFClient.confirmReception(prmCompanyName, prmDirectory, prmFileType, sToken);
					}
				}
				// Tanca la connexió al servei d'arxius
				svcBSFClient.Close();
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}
	}
	
}
