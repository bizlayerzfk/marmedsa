﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZLayer.Enviroment.BancSabadell.InvoicesWebService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TipoFactura", Namespace="http://ws.bsf.indra.es")]
    public enum TipoFactura : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        TXT = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        FACTURAE = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        XML_BSF = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        XML_BSF_NE = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        OWN = 4,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="IDataFile", Namespace="http://ws.bsf.indra.es")]
    [System.SerializableAttribute()]
    public partial class IDataFile : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] base64ContentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string indexField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tokenField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] base64Content {
            get {
                return this.base64ContentField;
            }
            set {
                if ((object.ReferenceEquals(this.base64ContentField, value) != true)) {
                    this.base64ContentField = value;
                    this.RaisePropertyChanged("base64Content");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string index {
            get {
                return this.indexField;
            }
            set {
                if ((object.ReferenceEquals(this.indexField, value) != true)) {
                    this.indexField = value;
                    this.RaisePropertyChanged("index");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                if ((object.ReferenceEquals(this.nameField, value) != true)) {
                    this.nameField = value;
                    this.RaisePropertyChanged("name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string token {
            get {
                return this.tokenField;
            }
            set {
                if ((object.ReferenceEquals(this.tokenField, value) != true)) {
                    this.tokenField = value;
                    this.RaisePropertyChanged("token");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ws.bsf.indra.es", ConfigurationName="InvoicesWebService.BSFConnectorInvoiceServicePortType")]
    public interface BSFConnectorInvoiceServicePortType {
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        int getStateInvoice(string in0, long in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<int> getStateInvoiceAsync(string in0, long in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        long putInvoice(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.InvoicesWebService.TipoFactura> in1, byte[] in2);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<long> putInvoiceAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.InvoicesWebService.TipoFactura> in1, byte[] in2);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        bool confirmReception(string in0, string in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<bool> confirmReceptionAsync(string in0, string in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        ZLayer.Enviroment.BancSabadell.InvoicesWebService.IDataFile[] getInvoiceFiles(string in0);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<ZLayer.Enviroment.BancSabadell.InvoicesWebService.IDataFile[]> getInvoiceFilesAsync(string in0);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface BSFConnectorInvoiceServicePortTypeChannel : ZLayer.Enviroment.BancSabadell.InvoicesWebService.BSFConnectorInvoiceServicePortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class BSFConnectorInvoiceServicePortTypeClient : System.ServiceModel.ClientBase<ZLayer.Enviroment.BancSabadell.InvoicesWebService.BSFConnectorInvoiceServicePortType>, ZLayer.Enviroment.BancSabadell.InvoicesWebService.BSFConnectorInvoiceServicePortType {
        
        public BSFConnectorInvoiceServicePortTypeClient() {
        }
        
        public BSFConnectorInvoiceServicePortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public BSFConnectorInvoiceServicePortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BSFConnectorInvoiceServicePortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BSFConnectorInvoiceServicePortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int getStateInvoice(string in0, long in1) {
            return base.Channel.getStateInvoice(in0, in1);
        }
        
        public System.Threading.Tasks.Task<int> getStateInvoiceAsync(string in0, long in1) {
            return base.Channel.getStateInvoiceAsync(in0, in1);
        }
        
        public long putInvoice(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.InvoicesWebService.TipoFactura> in1, byte[] in2) {
            return base.Channel.putInvoice(in0, in1, in2);
        }
        
        public System.Threading.Tasks.Task<long> putInvoiceAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.InvoicesWebService.TipoFactura> in1, byte[] in2) {
            return base.Channel.putInvoiceAsync(in0, in1, in2);
        }
        
        public bool confirmReception(string in0, string in1) {
            return base.Channel.confirmReception(in0, in1);
        }
        
        public System.Threading.Tasks.Task<bool> confirmReceptionAsync(string in0, string in1) {
            return base.Channel.confirmReceptionAsync(in0, in1);
        }
        
        public ZLayer.Enviroment.BancSabadell.InvoicesWebService.IDataFile[] getInvoiceFiles(string in0) {
            return base.Channel.getInvoiceFiles(in0);
        }
        
        public System.Threading.Tasks.Task<ZLayer.Enviroment.BancSabadell.InvoicesWebService.IDataFile[]> getInvoiceFilesAsync(string in0) {
            return base.Channel.getInvoiceFilesAsync(in0);
        }
    }
}
