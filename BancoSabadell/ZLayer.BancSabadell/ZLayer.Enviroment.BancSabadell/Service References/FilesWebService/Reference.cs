﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZLayer.Enviroment.BancSabadell.FilesWebService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DirectorioOutbox", Namespace="http://ws.bsf.indra.es")]
    public enum DirectorioOutbox : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EMPTY = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        FACTURAS = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        RESPUESTAS = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ADJUNTOS = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        PEDIDOS = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        GBITAL = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ADHESIONES = 6,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        CENTROS = 7,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EMPRESAS = 8,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        USUARIOS = 9,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        LIBROSII = 10,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        CONSULTASII = 11,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        RESPUESTASSII = 12,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ElementType", Namespace="http://ws.bsf.indra.es")]
    public enum ElementType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EMPTY = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        TXT = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        MTXT = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        XML = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        FACTURAE = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        XML_BSF = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        XML_BSF_NE = 6,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        OWN = 7,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        DIGITAL = 8,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        en = 9,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        es = 10,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        it = 11,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ExtensionElement", Namespace="http://ws.bsf.indra.es")]
    public enum ExtensionElement : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EMPTY = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        pdf = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        txt = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        xml = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        xls = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        xlsx = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        csv = 6,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DirectorioInbox", Namespace="http://ws.bsf.indra.es")]
    public enum DirectorioInbox : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        EMPTY = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        FACTURAS = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ESTADOS = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ADJUNTOS = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ADHESIONES = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        DOCUMENTACION = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        RESPUESTAS = 6,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        RESPUESTASSII = 7,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        CONSULTASII = 8,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        CORRECCIONESSII = 9,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="IDataFile", Namespace="http://ws.bsf.indra.es")]
    [System.SerializableAttribute()]
    public partial class IDataFile : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] base64ContentField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string indexField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tokenField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] base64Content {
            get {
                return this.base64ContentField;
            }
            set {
                if ((object.ReferenceEquals(this.base64ContentField, value) != true)) {
                    this.base64ContentField = value;
                    this.RaisePropertyChanged("base64Content");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string index {
            get {
                return this.indexField;
            }
            set {
                if ((object.ReferenceEquals(this.indexField, value) != true)) {
                    this.indexField = value;
                    this.RaisePropertyChanged("index");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                if ((object.ReferenceEquals(this.nameField, value) != true)) {
                    this.nameField = value;
                    this.RaisePropertyChanged("name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string token {
            get {
                return this.tokenField;
            }
            set {
                if ((object.ReferenceEquals(this.tokenField, value) != true)) {
                    this.tokenField = value;
                    this.RaisePropertyChanged("token");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://ws.bsf.indra.es", ConfigurationName="FilesWebService.BSFConnectorFilesManagementServicePortType")]
    public interface BSFConnectorFilesManagementServicePortType {
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        int getStateElement(string in0, long in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<int> getStateElementAsync(string in0, long in1);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        long putElement(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioOutbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ExtensionElement> in4, byte[] in5);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<long> putElementAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioOutbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ExtensionElement> in4, byte[] in5);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        bool confirmReception(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<bool> confirmReceptionAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        ZLayer.Enviroment.BancSabadell.FilesWebService.IDataFile[] getElements(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, System.Nullable<int> in3);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [return: System.ServiceModel.MessageParameterAttribute(Name="out")]
        System.Threading.Tasks.Task<ZLayer.Enviroment.BancSabadell.FilesWebService.IDataFile[]> getElementsAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, System.Nullable<int> in3);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface BSFConnectorFilesManagementServicePortTypeChannel : ZLayer.Enviroment.BancSabadell.FilesWebService.BSFConnectorFilesManagementServicePortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class BSFConnectorFilesManagementServicePortTypeClient : System.ServiceModel.ClientBase<ZLayer.Enviroment.BancSabadell.FilesWebService.BSFConnectorFilesManagementServicePortType>, ZLayer.Enviroment.BancSabadell.FilesWebService.BSFConnectorFilesManagementServicePortType {
        
        public BSFConnectorFilesManagementServicePortTypeClient() {
        }
        
        public BSFConnectorFilesManagementServicePortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public BSFConnectorFilesManagementServicePortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BSFConnectorFilesManagementServicePortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BSFConnectorFilesManagementServicePortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int getStateElement(string in0, long in1) {
            return base.Channel.getStateElement(in0, in1);
        }
        
        public System.Threading.Tasks.Task<int> getStateElementAsync(string in0, long in1) {
            return base.Channel.getStateElementAsync(in0, in1);
        }
        
        public long putElement(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioOutbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ExtensionElement> in4, byte[] in5) {
            return base.Channel.putElement(in0, in1, in2, in3, in4, in5);
        }
        
        public System.Threading.Tasks.Task<long> putElementAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioOutbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ExtensionElement> in4, byte[] in5) {
            return base.Channel.putElementAsync(in0, in1, in2, in3, in4, in5);
        }
        
        public bool confirmReception(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3) {
            return base.Channel.confirmReception(in0, in1, in2, in3);
        }
        
        public System.Threading.Tasks.Task<bool> confirmReceptionAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, string in3) {
            return base.Channel.confirmReceptionAsync(in0, in1, in2, in3);
        }
        
        public ZLayer.Enviroment.BancSabadell.FilesWebService.IDataFile[] getElements(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, System.Nullable<int> in3) {
            return base.Channel.getElements(in0, in1, in2, in3);
        }
        
        public System.Threading.Tasks.Task<ZLayer.Enviroment.BancSabadell.FilesWebService.IDataFile[]> getElementsAsync(string in0, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.DirectorioInbox> in1, System.Nullable<ZLayer.Enviroment.BancSabadell.FilesWebService.ElementType> in2, System.Nullable<int> in3) {
            return base.Channel.getElementsAsync(in0, in1, in2, in3);
        }
    }
}
