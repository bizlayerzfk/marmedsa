﻿
#region Imports

using System;
using System.ServiceModel;

using ZLayer.Enviroment.BancSabadell.InvoicesWebService;
using ZLayer.Enviroment.BancSabadell.FilesWebService;

#endregion Imports

namespace ZLayer.Enviroment.BancSabadell
{

    public static class Connection
    {

        #region Properties

        #region Connectors

        private static BSFConnectorInvoiceServicePortTypeClient itmInvoices = null;
        /// <summary>
        /// Retorna la connexió al servei web de factures del Banc Sabadell
        /// </summary>
        public static BSFConnectorInvoiceServicePortTypeClient Invoices
        {
            get
            {
                if (!String.IsNullOrEmpty(UserName) && !String.IsNullOrEmpty(UserPass))
                {
                    if (itmInvoices != null && itmInvoices.State == CommunicationState.Opened)
                    {
                        if (itmInvoices.ClientCredentials.UserName.UserName != UserName ||
                            itmInvoices.ClientCredentials.UserName.Password != UserPass)
                        {
                            itmInvoices = new BSFConnectorInvoiceServicePortTypeClient();
                            itmInvoices.ClientCredentials.UserName.UserName = UserName;
                            itmInvoices.ClientCredentials.UserName.Password = UserPass;
                        }
                    }
                    else
                    {
                        itmInvoices = new BSFConnectorInvoiceServicePortTypeClient();
                        itmInvoices.ClientCredentials.UserName.UserName = UserName;
                        itmInvoices.ClientCredentials.UserName.Password = UserPass;
                    }
                }
                else
                {
                    itmInvoices = null;
                }
                return itmInvoices;
            }
        }

        private static BSFConnectorFilesManagementServicePortTypeClient itmFiles = null;
        /// <summary>
        /// Retorna la connexió al servei web d'arxius del Banc Sabadell
        /// </summary>
        public static BSFConnectorFilesManagementServicePortTypeClient Files
        {
            get
            {
                if (!String.IsNullOrEmpty(UserName) && !String.IsNullOrEmpty(UserPass))
                {
                    if (itmFiles != null && itmFiles.State == CommunicationState.Opened)
                    {
                        if (itmFiles.ClientCredentials.UserName.UserName != UserName ||
                            itmFiles.ClientCredentials.UserName.Password != UserPass)
                        {
                            itmFiles = new BSFConnectorFilesManagementServicePortTypeClient();
                            itmFiles.ClientCredentials.UserName.UserName = UserName;
                            itmFiles.ClientCredentials.UserName.Password = UserPass;
                        }
                    }
                    else
                    {
                        itmFiles = new BSFConnectorFilesManagementServicePortTypeClient();
                        itmFiles.ClientCredentials.UserName.UserName = UserName;
                        itmFiles.ClientCredentials.UserName.Password = UserPass;
                    }
                }
                else
                {
                    itmFiles = null;
                }
                return itmFiles;
            }
        }

        #endregion Connectors


        #region Data

        private static String sUserName = String.Empty;
        /// <summary>
        /// Retorna o estableix el nom de l'usuari
        /// </summary>
        public static String UserName
        {
            get { return sUserName; }
            set { sUserName = value; }
        }

        private static String sUserPass = String.Empty;
        /// <summary>
        /// Retorna o estableix la paraula de pas de l'usuari
        /// </summary>
        public static String UserPass
        {
            get { return sUserPass; }
            set { sUserPass = value; }
        }

        #endregion Data

        #endregion Properties

    }

}
