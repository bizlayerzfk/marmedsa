﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotMailsToolkits
{
	class Program
	{
		internal const string Servidor = "mail.zertifika.com";
		internal const string Email = "maarcos@zertifika.com";
		internal const string Password = "Nata03*";
		internal int Id { get; set; }

		internal static string sMensajesAgrupados { get; set; }
		internal static DateTime sFechaMail { get; set; }
		//valores de control
		//Proveedores estandar
		internal static int idFacturasProcesadas { get; set; }
		internal static int idFacturasOK { get; set; }
		internal static int idFacturasProcesadasError { get; set; }
		internal static int idFacturasErrorFacturaE { get; set; }
		internal static int idFacturasErrorSuplidos { get; set; }
		internal static int idFacturasErrorEmisorNoAceptado { get; set; }
		internal static int idFacturasErrorTotal { get; set; }
		//Toolkit
		internal static int idFacturasProcesadasT { get; set; }
		internal static int idFacturasOKT { get; set; }
		internal static int idFacturasProcesadasErrorT { get; set; }
		internal static int idFacturasErrorFacturaET { get; set; }
		internal static int idFacturasErrorSuplidosT { get; set; }
		internal static int idFacturasErrorEmisorNoAceptadoT { get; set; }
		//Portic
		internal static int idFacturasProcesadasP { get; set; }
		internal static int idFacturasOKP { get; set; }
		internal static int idFacturasProcesadasErrorP { get; set; }
		internal static int idFacturasErrorFacturaEP { get; set; }
		internal static int idFacturasErrorSuplidosP { get; set; }
		internal static int idFacturasErrorEmisorNoAceptadoP { get; set; }
		//Sabadell
		internal static int idFacturasProcesadasS { get; set; }
		internal static int idFacturasOKS { get; set; }
		internal static int idFacturasProcesadasErrorS { get; set; }
		internal static int idFacturasErrorFacturaES { get; set; }
		internal static int idFacturasErrorSuplidosS { get; set; }
		internal static int idFacturasErrorEmisorNoAceptadoS { get; set; }
		//Valores a buscar
		internal static string sFacProcesadas = "Facturas Procesadas: ";
		internal static string sFacOk = "OK - ";
		internal static string sFacError = "ProcesadosERROR - ";
		internal static string sFacEFFacturaE = "ErrorFormatoFacturaE - ";
		internal static string sFacErrorMoverSuplidos = "moverAErrorSuplidos - ";
		internal static string sFacErrorEmisorNoAceptado = "moverAErrorEmisorNoAceptado - ";
		internal static string sFacErrorTotal = "moverAErrorTotalFactura - ";

		static void Main(string[] args)
		{


			ProcesarResumenDescargas(args);
			ProcesarResumenMails(args);


		}

		#region ProcesarEmails
		private static Boolean ProcesarResumenMails(String[] args)
		{
			try
			{
				if (args.Length > 0)
				{
					try
					{
						sFechaMail = Convert.ToDateTime(args[0].ToString());
					}
					catch (Exception)
					{
					}
				}
				else sFechaMail = DateTime.Now.AddDays(-1);
				String sResumenEnvio = "";
				String sSubject = "Informe Facturas Proveedores Extandar Marmedsa";
				sMensajesAgrupados = "";
				ProcesarEmails(sSubject);
				sResumenEnvio += sMensajesAgrupados;
				sSubject = "Informe Facturas Sabadell Marmedsa";
				sMensajesAgrupados = "";
				ProcesarEmails(sSubject);
				sResumenEnvio += sMensajesAgrupados;
				sSubject = "Informe Facturas Portic Marmedsa";
				sMensajesAgrupados = "";
				ProcesarEmails(sSubject);
				sResumenEnvio += sMensajesAgrupados;
				sSubject = "Informe Envio Facturas Marmedsa";
				sMensajesAgrupados = "";
				ProcesarEmails(sSubject);
				sResumenEnvio += sMensajesAgrupados;

				sResumenEnvio = "";
				//Proveedores extandar
				sResumenEnvio = "Facturas Procesadas Proveedores Extandar: " + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas Procesadas: " + idFacturasProcesadas.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas OK: " + idFacturasOK.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ProcesadasError: " + idFacturasProcesadasError.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorFormatoFacturaE: " + idFacturasErrorFacturaE.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverSuplidos: " + idFacturasErrorSuplidos.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverEmisorNoAceptado: " + idFacturasErrorEmisorNoAceptado.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverTotalFactura: " + idFacturasErrorTotal.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "<br>";
				//Portic
				sResumenEnvio += "Facturas Procesadas Portic: " + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas Procesadas: " + idFacturasProcesadasP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas OK: " + idFacturasOKP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ProcesadasError: " + idFacturasProcesadasErrorP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorFormatoFacturaE: " + idFacturasErrorFacturaEP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverSuplidos: " + idFacturasErrorSuplidosP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverEmisorNoAceptado: " + idFacturasErrorEmisorNoAceptadoP.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "<br>";
				//Sabadell
				sResumenEnvio += "Facturas Procesadas Sabadell: " + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas Procesadas: " + idFacturasProcesadasS.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas OK: " + idFacturasOKS.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ProcesadasError: " + idFacturasProcesadasErrorS.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorFormatoFacturaE: " + idFacturasErrorFacturaES.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverSuplidos: " + idFacturasErrorSuplidosS.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverEmisorNoAceptado: " + idFacturasErrorEmisorNoAceptadoS.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "<br>";
				//Toolkit
				sResumenEnvio += "Facturas Procesadas Toolkit: " + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas Procesadas: " + idFacturasProcesadasT.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas OK: " + idFacturasOKT.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ProcesadasError: " + idFacturasProcesadasErrorT.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorFormatoFacturaE: " + idFacturasErrorFacturaET.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverSuplidos: " + idFacturasErrorSuplidosT.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "Facturas ErrorMoverEmisorNoAceptado: " + idFacturasErrorEmisorNoAceptadoT.ToString() + "<p></p>" + "<p></p>";
				sResumenEnvio += "<br>";
				if (idFacturasProcesadas != idFacturasOK || idFacturasProcesadasT != idFacturasOKT)
				{
					String mailsDestinos = ConfigurationManager.AppSettings["sMailsDestino"];
					string resp = EnvioMail.enviarMail2(mailsDestinos, "info@agikey.com", "ERROR - Reporte facturas marmedsa", sResumenEnvio);
				}
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}
		private static Boolean ProcesarEmails(String sSubject)
		{
			Dictionary<string, int> tabla = new Dictionary<string, int>();
			try
			{
				Imap imap = new Imap();

				imap.Connect(Servidor);

				imap.Login(Email, Password);

				List<long> uidList = new List<long>();

				imap.SelectInbox();

				//uidList = imap.Search().Where(Expression.SentSince(DateTime.Now.AddDays(-1)));

				//uidList = imap.Search(Flag.All);

				//uidList = imap.Search(Flag.All);
				



				//uidList = imap.Search().Where(
				//		  Expression.Subject("Informe Envio Facturas Marmedsa"));

				//Buscamos los del día anterior.
				uidList = imap.Search().Where(
					Expression.And(
					Expression.Subject(sSubject),
					Expression.SentOn(sFechaMail)
					)
					);

				uidList.Sort((a, b) => -1 * a.CompareTo(b));

				/*
                * 
                *    uidList = imap.Search().Where(
               Expression.And(
               Expression.Since(new DateTime(2018, 11, 14)),
               Expression.HasFlag(Flag.Unseen)));
                 */



				//uidList = imap.Search().Where(
				//   Expression.And(
				//       Expression.SentOn(new DateTime(2019, 01, 17)),
				//       Expression.HasFlag(Flag.Unseen)));


				for (int i = 0; i < uidList.Count; i++)
				{
					long uid = uidList[i];
					bool Leido = false;
					try
					{

						Leido = imap.GetFlagsByUID(uid).Contains(Flag.Seen);




						IMail emailI = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

						Email em = new Email(emailI.Subject,emailI.GetBodyAsText(), 1, Convert.ToInt32(uid), emailI.Date, emailI.From.First().Address, 1);

						ProcesarMail(em,sSubject);
						//ProcesarEmail(em, emailI, false, tabla, idRemesa, carpeta, Signes: Signes);

					}
					catch (Exception eexx)
					{

					}
					finally
					{
						// Prefiere Andrés que se queden todos como leidos
						//if (!Leido) imap.MarkMessageUnseenByUID(uid);
					}
				}
				sMensajesAgrupados = sSubject + "\n" + sMensajesAgrupados;
				Console.WriteLine(sMensajesAgrupados);


				return true;
			}
			catch (Exception e)
			{

				return false;
			}
		}

		private static void ProcesarMail(Email oemail,String sSubject)
		{
			try
			{
				//Estraemos la información
				oemail.Cuerpo = oemail.Cuerpo.Replace("\n", "");
				oemail.Cuerpo = oemail.Cuerpo.Replace("\r", "");
				int idfp = oemail.Cuerpo.IndexOf(sFacProcesadas);
				int idok = oemail.Cuerpo.IndexOf(sFacOk);
				int idProcesadosERROR = oemail.Cuerpo.IndexOf(sFacError);
				int idErrorFormatoFacturaE = oemail.Cuerpo.IndexOf(sFacEFFacturaE);
				int idErrorMoverSuplidos = oemail.Cuerpo.IndexOf(sFacErrorMoverSuplidos);
				int idErrorEmisorNoAceptado = oemail.Cuerpo.IndexOf(sFacErrorEmisorNoAceptado);
				int idErrorTotalFactura = oemail.Cuerpo.IndexOf(sFacErrorTotal);

				int isfp = 0;
				int isok = 0;
				int ispe = 0;
				int iseff = 0;
				int isesupli = 0;
				int iseemisornoaceptado = 0;
				int iserrortotal = 0;

				if (idfp > -1)
				{
					isfp = extraerNumero(oemail.Cuerpo.Substring(idfp,oemail.Cuerpo.Length-idfp), sFacProcesadas);
				}
				if (idok > -1)
				{
					isok = extraerNumero(oemail.Cuerpo.Substring(idok, oemail.Cuerpo.Length - idok), sFacOk);
				}
				if (idProcesadosERROR > -1)
				{
					ispe = extraerNumero(oemail.Cuerpo.Substring(idProcesadosERROR, oemail.Cuerpo.Length - idProcesadosERROR), sFacError);
				}
				if (idErrorFormatoFacturaE > -1)
				{
					iseff = extraerNumero(oemail.Cuerpo.Substring(idErrorFormatoFacturaE, oemail.Cuerpo.Length - idErrorFormatoFacturaE), sFacEFFacturaE);
				}
				if (idErrorMoverSuplidos > -1)
				{
					isesupli = extraerNumero(oemail.Cuerpo.Substring(idErrorMoverSuplidos, oemail.Cuerpo.Length - idErrorMoverSuplidos), sFacErrorMoverSuplidos);
				}
				if (idErrorEmisorNoAceptado > -1)
				{
					iseemisornoaceptado = extraerNumero(oemail.Cuerpo.Substring(idErrorEmisorNoAceptado, oemail.Cuerpo.Length - idErrorEmisorNoAceptado), sFacErrorEmisorNoAceptado);
				}
				if (idErrorTotalFactura > -1)
				{
					iserrortotal = extraerNumero(oemail.Cuerpo.Substring(idErrorTotalFactura, oemail.Cuerpo.Length - idErrorTotalFactura), sFacErrorTotal);
				}

				if (sSubject.Equals("Informe Facturas Proveedores Extandar Marmedsa"))
				{

					if (idfp > -1)
						idFacturasProcesadas += isfp;
					if (idok > -1)
						idFacturasOK += isok;
					if (idProcesadosERROR > -1)
						idFacturasProcesadasError +=  ispe;
					if (idErrorFormatoFacturaE > -1)
						idFacturasErrorFacturaE += iseff;
					if (idErrorMoverSuplidos > -1)
						idFacturasErrorSuplidos += isesupli;
					if (idErrorEmisorNoAceptado > -1)
						idFacturasErrorEmisorNoAceptado += iseemisornoaceptado;
					if (idErrorTotalFactura > -1)
						idFacturasErrorTotal += iserrortotal;
				}
				if (sSubject.Equals("Informe Facturas Sabadell Marmedsa"))
				{

					if (idfp > -1)
						idFacturasProcesadasS += isfp;
					if (idok > -1)
						idFacturasOKS += isok;
					if (idProcesadosERROR > -1)
						idFacturasProcesadasErrorS += ispe;
					if (idErrorFormatoFacturaE > -1)
						idFacturasErrorFacturaES += iseff;
					if (idErrorMoverSuplidos > -1)
						idFacturasErrorSuplidosS += isesupli;
					if (idErrorEmisorNoAceptado > -1)
						idFacturasErrorEmisorNoAceptadoS += iseemisornoaceptado;
				}
				if (sSubject.Equals("Informe Facturas Portic Marmedsa"))
				{

					if (idfp > -1)
						idFacturasProcesadasP += isfp;
					if (idok > -1)
						idFacturasOKP += isok;
					if (idProcesadosERROR > -1)
						idFacturasProcesadasErrorP += ispe;
					if (idErrorFormatoFacturaE > -1)
						idFacturasErrorFacturaEP += iseff;
					if (idErrorMoverSuplidos > -1)
						idFacturasErrorSuplidosP += isesupli;
					if (idErrorEmisorNoAceptado > -1)
						idFacturasErrorEmisorNoAceptadoP += iseemisornoaceptado;
				}
				if (sSubject.Equals("Informe Envio Facturas Marmedsa"))
				{

					if (idfp > -1)
						idFacturasProcesadasT += isfp;
					if (idok > -1)
						idFacturasOKT += isok;
					if (idProcesadosERROR > -1)
						idFacturasProcesadasErrorT += ispe;
					if (idErrorFormatoFacturaE > -1)
						idFacturasErrorFacturaET += iseff;
					if (idErrorMoverSuplidos > -1)
						idFacturasErrorSuplidosT += isesupli;
					if (idErrorEmisorNoAceptado > -1)
						idFacturasErrorEmisorNoAceptadoT += iseemisornoaceptado;
				}



				sMensajesAgrupados += oemail.Cuerpo;

			}
			catch (Exception)
			{
			}
		}

		private static int extraerNumero(String sValor, String sTipo)
		{
			int iResult = 0;
			try
			{
				String sResult = "";
				String sTexto = sValor.Replace(sTipo, "");
				for (int i = 0; i < sTexto.Length; i++)
				{
					String schar = sTexto.Substring(i,1);
					Boolean bIsnumero = int.TryParse(schar, out int ichar);
					if (bIsnumero) sResult += schar;
					else break;

				}
				iResult = Convert.ToInt32(sResult);
				return iResult;
			}
			catch (Exception)
			{
				return 0;
			}
		}

		#endregion ProcesarEmails

		#region Resumen Descargas

		private static void ProcesarResumenDescargas(string[] args)
		{
			try
			{
				_strCadenaConexion = ConfigurationManager.AppSettings["conectionstring"];
				createConnection();
				DataSet ds = getDataSet(args);
				int idConfirmadas = 0, idDescargadas = 0, idEnviadas = 0, idRecibidas = 0;
				if (ds != null)
				{
					if (ds.Tables["dep"].Rows.Count > 0)
					{
						String detalle = "Detalle " + DateTime.Now.AddDays(-1).ToString() + "<p></p>";
						for (int i = 0; i < ds.Tables["dep"].Rows.Count; i++)
						{
							detalle += ds.Tables["dep"].Rows[i]["Detalle"].ToString() + " - " + ds.Tables["dep"].Rows[i]["Facturas"].ToString() + "<p></p>";
							//Cargamos los totales para comprobar luego si hay que mandar aviso urgente
							try
							{
								if (ds.Tables["dep"].Rows[i]["Detalle"].ToString().Contains("Confirmadas"))
									idConfirmadas = Convert.ToInt32(ds.Tables["dep"].Rows[i]["Facturas"].ToString());
								if (ds.Tables["dep"].Rows[i]["Detalle"].ToString().Contains("Descargadas"))
									idDescargadas = Convert.ToInt32(ds.Tables["dep"].Rows[i]["Facturas"].ToString());
								if (ds.Tables["dep"].Rows[i]["Detalle"].ToString().Contains("Enviadas"))
									idEnviadas = Convert.ToInt32(ds.Tables["dep"].Rows[i]["Facturas"].ToString());
								if (ds.Tables["dep"].Rows[i]["Detalle"].ToString().Contains("Recibidas"))
									idRecibidas = Convert.ToInt32(ds.Tables["dep"].Rows[i]["Facturas"].ToString());
							}
							catch (Exception)
							{
							}
						}
						String sTitulo = "Resumen Descarga Facturas Marmedsa";
						String sError = "";
						if (idDescargadas != idConfirmadas) sError+= "ERROR - Confirmadas ";
						int idcantidad = Convert.ToInt32(ConfigurationManager.AppSettings["idCantidad"]);
						if (idEnviadas < idcantidad) sError += "ERROR - Enviadas < " + idcantidad.ToString() + " " ;
						if (idRecibidas < idcantidad) sError += "ERROR - Recibidas < " + idcantidad.ToString() + " ";
						Console.WriteLine("idConfirmadas " + idConfirmadas + " - idDescargadas " + idDescargadas + " - idEnviadas " + idEnviadas + " - idRecibidas " + idRecibidas);
						sTitulo = sError + sTitulo;
						String mailsDestinos = ConfigurationManager.AppSettings["sMailsDestino"];
						string resp = EnvioMail.enviarMail2(mailsDestinos, "info@agikey.com",sTitulo, detalle);
					}
				}
				else Console.WriteLine("No hay dataset con datos");
				cerrarConexion();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error procesar " + e.Message);
			}
		}
		private static DataSet getDataSet(string[] args)
		{
			try
			{
				DataSet ds = new DataSet(); ;
				String query = "set dateformat dmy;";
				//Descargadas y confirmadas
				query += "select " +
					"'Descargadas <fechaanterior>' as Detalle,COUNT(*) as facturas " +
					"from facturas as F " +
					"inner join Empresas as EE on F.IdEmpresaEmisora = EE.Id " +
					"inner join Empresas as ER on F.IdEmpresaReceptora = ER.Id " +
					"inner join Historicos as H on F.id = H.IdFacturaE " +
					"where " +
					"H.Id in ( " +
					"select top 1 id from Historicos " +
					"where IdFacturaE = F.Id and State = 3 and(CreateAt >= '<fechaanterior>' and CreateAt < '<fechaposterior>') " +
					"order by Id desc) " +
					"union " +
					"select " +
					"'Confirmadas <fechaanterior>' as Detalle,COUNT(*) as facturas " +
					"from facturas as F " +
					"inner join Empresas as EE on F.IdEmpresaEmisora = EE.Id " +
					"inner join Empresas as ER on F.IdEmpresaReceptora = ER.Id " +
					"inner join ConfirmacionDescargaFacturas as CDF on F.id = CDF.IdFacturaE " +
					"where " +
					"CDF.Id in ( " +
					"select top 1 id from ConfirmacionDescargaFacturas " +
					"where IdFacturaE = F.Id and PendingConfirmation = 0 and((CreateAt >= '<fechaanterior>' and CreateAt < '<fechaposterior>') or(UpdateAt >= '<fechaanterior>' and UpdateAt < '<fechaposterior>')) " +
					"order by Id desc)";
				//Enviadas y recibidas por grupo Marmedsa
				query += " union " +
					"select  " +
					"'Enviadas <fechaanterior>' as Detalle,COUNT(*) as facturas  " +
					"from  " +
					"Facturas as F  " +
					"inner join Empresas as EE on F.IdEmpresaEmisora = EE.Id  " +
					"where F.Id in (select distinct IdFacturaE from Historicos  " +
					"where (CreateAt >= '<fechaanterior>' and CreateAt<'<fechaposterior>')  " +
					"and State = 1) " +
					"and EE.IdGrupoEmpresa = 2 ";
				query += " union " +
					"select   " +
					"'Recibidas <fechaanterior>' as Detalle,COUNT(*) as facturas " +
					"from " +
					"Facturas as F " +
					"inner join Empresas as EE on F.IdEmpresaEmisora = EE.Id " +
					"inner join Empresas as ER on F.IdEmpresaReceptora = ER.Id " +
					"where F.Id in (select distinct IdFacturaE from Historicos " +
					"where (CreateAt >= '<fechaanterior>' and CreateAt<'<fechaposterior>') " +
					"and State = 1) " +
					"and EE.IdGrupoEmpresa is null";

				String sfechaanterior = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
				String sfechaposterior = DateTime.Now.ToString("dd/MM/yyyy");
				if (args.Length > 0)
				{
					try
					{
						DateTime sFecha = Convert.ToDateTime(args[0].ToString());
						sfechaanterior = sFecha.ToString("dd/MM/yyyy");
						sfechaposterior = sFecha.AddDays(1).ToString("dd/MM/yyyy");

					}
					catch (Exception)
					{
						sfechaanterior = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
						sfechaposterior = DateTime.Now.ToString("dd/MM/yyyy");
					}
				}
				query = query.Replace("<fechaanterior>", sfechaanterior);
				query = query.Replace("<fechaposterior>", sfechaposterior);
				setCommandText(query);
				cargarDataSet(ds, "dep");
				return ds;
			}
			catch (Exception e)
			{
				Console.WriteLine("Error cargar dataset " + e.Message);
				return null;
			}
		}
		#endregion Resumen Descargas

		#region Base de Datos
		private static object _objComando;
		private static object _objDataAdapter;
		private static object _objDataReader;
		private static object _objConexion;
		private static string _strTipoBBDD;
		private static object _objTransaccion;
		private static string _strCadenaConexion;
		private static string _CommandTimeout = "3000";


		private static void EjecutarSQL(String sql)
		{
			try
			{
				setCommandText(sql);
				int resutl = ejecutarSentencia();
				Console.WriteLine("Ejecutar " + sql + " Resultado: " + resutl.ToString());
			}
			catch (Exception e)
			{
				Console.WriteLine("Error ejectura SQL");
			}
		}
		private static void createConnection()
		{
			try
			{
				// Creamos un objeto xxxConnection
				newConnection();
				// Abrimos la conexion
				openConnection();
				// Creamos un data adapter para almacenar los resultados
				newDataAdapter();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error createconnetion:" + e.Message);
				throw e;
			}
		}

		private static void newConnection()
		{
			try
			{
				_objConexion = new SqlConnection(_strCadenaConexion);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newConnection:" + e.Message);
				throw e;
			}
		}

		private static void openConnection()
		{
			try
			{
				((SqlConnection)(_objConexion)).Open();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error openConnection:" + e.Message);
				throw e;
			}
		}

		private static void newDataAdapter()
		{
			try
			{
				_objDataAdapter = new SqlDataAdapter();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newDataAdapter:" + e.Message);
				throw e;
			}

		}

		private static void setCommandText(string nombreSentencia)
		{
			string sentencia;
			try
			{
				// Creamos un objeto xxxCommand
				newCommand();
				sentencia = nombreSentencia;
				((SqlCommand)(_objComando)).CommandText = sentencia;
				((SqlCommand)(_objComando)).CommandTimeout = System.Int32.Parse(_CommandTimeout);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error setCommandText:" + e.Message);
				throw e;
			}

		}

		private static void newCommand()
		{
			try
			{
				_objComando = new SqlCommand();
				((SqlCommand)(_objComando)).Connection = ((SqlConnection)(_objConexion));
				if (!(_objTransaccion == null))
				{
					((SqlCommand)(_objComando)).Transaction = ((System.Data.SqlClient.SqlTransaction)(_objTransaccion));
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Error newCommand:" + e.Message);
				throw e;
			}
		}

		private static int ejecutarSentencia()
		{
			int intResultado = 0;
			try
			{
				intResultado = ((SqlCommand)(_objComando)).ExecuteNonQuery();
			}
			catch (Exception e)
			{
				Console.WriteLine("Error ejecutarSentencia:" + e.Message);
				throw e;
			}
			return intResultado;
		}

		private static void cerrarConexion()
		{
			try
			{
				if (_objConexion != null)
				{
					if (((SqlConnection)(_objConexion)).State == ConnectionState.Open) ((SqlConnection)(_objConexion)).Close();
				}
				_objConexion = null;

			}
			catch (Exception e)
			{
				Console.WriteLine("Error cerrarConexion:" + e.Message);
				throw e;
			}
		}

		private static void cargarDataSet(DataSet ds, string tabla)
		{
			string sen = "";
			try
			{
				((SqlDataAdapter)(_objDataAdapter)).SelectCommand = ((SqlCommand)(_objComando));
				sen = ((SqlDataAdapter)(_objDataAdapter)).SelectCommand.CommandText.ToString();
				((SqlDataAdapter)(_objDataAdapter)).Fill(ds, tabla);
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		#endregion
	}
}
