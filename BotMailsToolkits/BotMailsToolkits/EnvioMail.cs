﻿using Limilabs.Client.SMTP;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BotMailsToolkits
{
	class EnvioMail
	{


		public static string enviarMail2(string direccionEmailTo, string direccionEmailFrom,
			string asunto, string contenido, string nombreDocumento = "", byte[] documento = null, string Host = "mail.zertifika.com", string User = "info@zertifika.com",
			string Pass = "r3dM#z41#z41#z41", string nombreFrom = "", string mailCCO = "", List<string> mailCCs = null, int idEstado = -1, int Port = 587, int idEmpresa = -1)
		{

			try
			{

				direccionEmailTo = direccionEmailTo.Trim();
				direccionEmailFrom = direccionEmailFrom.Trim();


				using (Smtp smtp = new Smtp())
				{
					try
					{
						smtp.Connect(Host, Port);
						if (!Host.ToLower().Contains("zertifika") && !direccionEmailFrom.ToLower().Contains("fnmt.es"))
							smtp.StartTLS();


						if (!direccionEmailFrom.ToLower().Contains("fnmt.es"))
						{
							smtp.UseBestLogin(User, Pass);
						}

						MailBuilder builder = new MailBuilder();

						builder.Html = contenido;
						builder.From.Add(new MailBox(direccionEmailFrom, nombreFrom));
						String[] smails = direccionEmailTo.Split(';');
						for (int i = 0;i< smails.Length; i++)
						{
							builder.To.Add(new MailBox(smails[i]));
						}

						builder.Subject = asunto;

						if (!string.IsNullOrEmpty(mailCCO))
						{
							builder.Bcc.Add(new MailBox(mailCCO.Trim()));
						}

						// Añadimos CCs
						if (mailCCs != null && mailCCs.Count > 0)
						{
							foreach (string s in mailCCs)
							{
								builder.Cc.Add(new MailBox(s.Trim()));
							}
						}

						if (documento != null)
						{
							builder.AddAttachment(documento).FileName = nombreDocumento;
						}


						IMail email = builder.Create();

						var a = smtp.SendMessage(email);

						smtp.Close();

					}
					catch (Exception e)
					{

						return "-1";
					}
				}




				return "1";

			}
			catch (Exception ex)
			{

				return "-1";
			}
		}


		public static int Envio(string contenido, string destinatario)
		{
			try
			{

				MailMessage mail = new MailMessage("info@zertifika.com", destinatario);
				SmtpClient client = new SmtpClient();

				client.Port = 25;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.UseDefaultCredentials = false;
				client.Host = "mail.zertifika.com";

				mail.Subject = "Reporte facturas descargadas";
				mail.Body = contenido;
				client.Send(mail);

				return 1;

			}
			catch (Exception e)
			{

				return -1;
			}
		}

	}
}
